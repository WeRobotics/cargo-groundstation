#include <QApplication>
#include <QtGamepad/QGamepad>
#include <QElapsedTimer>
#include <QDebug>

#include "gamepadmanager.h"
#include "systems/system_manager.h"
#include "logs/loggingcategories.h"
#include "commander.h"

#define MAX_DETECTION_TRIALS 10
#define SEND_INTERVAL_MS 50
#define CHECK_CONNECTION_INTERVAL_MS 1000

GamepadManager::GamepadManager(QSharedPointer<SystemManager> comManager) : QObject(),
_gamepad(nullptr),
_comManager(comManager),
_sendContinuous(false),
_sendOnce(false),
_hasControl(false)
{
    // Initialisation
    _payload.target = Commander::getInstance()->getDroneId();
    _payload.x = 0;
    _payload.y = 0;
    _payload.z = 0;
    _payload.r = 0;
    _payload.buttons = 0;

    _connectionTimer = new QTimer(this);
    connect(_connectionTimer, SIGNAL(timeout()), this, SLOT(checkConnection()));
    _connectionTimer->start(CHECK_CONNECTION_INTERVAL_MS);

    _sendMessageTimer = new QTimer(this);
    connect(_sendMessageTimer, SIGNAL(timeout()), this, SLOT(sendCommandToDrone()));
    _sendMessageTimer->start(SEND_INTERVAL_MS);

    // Gamepad detection
    int i = 0;
    do
    {
        QApplication::processEvents();
        i++;
    } while ((i < MAX_DETECTION_TRIALS) && (QGamepadManager::instance()->connectedGamepads().isEmpty()));

    if(i >= MAX_DETECTION_TRIALS)
    {
        qWarning(generalLog()) << "No gamepad found.";
        return;
    }

    // If gamepad detected, create gamepad instance
    initGamepadInstance();
}

GamepadManager::~GamepadManager()
{
    qInfo() << "~GamepadManager()";
    if(_gamepad)
    {
        gamepadConnected(false);
        delete _gamepad;
        _gamepad = nullptr;
    }
    if(_sendMessageTimer)
    {
        delete _sendMessageTimer;
        _sendMessageTimer = nullptr;
    }
    if(_connectionTimer)
    {
        delete _connectionTimer;
        _connectionTimer = nullptr;
    }

    _comManager.clear();
}

void GamepadManager::updateJoystickCommand(char buttonChar, double value)
{
    value = value * 1000;
    int16_t value_int = static_cast<int16_t>(value >= 0.0 ? (value + 0.5) : (value - 0.5));

    if(buttonChar == 'X')
        _payload.x = value_int;
    else if (buttonChar == 'Y')
        _payload.y = value_int;
    else if (buttonChar == 'Z')
        _payload.z = value_int;
    else if (buttonChar == 'R')
        _payload.r = value_int;

    return;
}

void GamepadManager::updateButtonCommand(char buttonChar, bool pressed)
{
    if(pressed)
    {
        if(buttonChar == 'A')
            _payload.buttons |= (1 << 0);
        else if (buttonChar == 'B')
            _payload.buttons |= (pressed << 1);
        else if (buttonChar == 'X')
            _payload.buttons |= (pressed << 2);
        else if (buttonChar == 'Y')
            _payload.buttons |= (pressed << 3);
    }
    else
    {
        if(buttonChar == 'A')
            _payload.buttons &= ~(1 << 0);
        else if (buttonChar == 'B')
            _payload.buttons &= ~(1 << 1);
        else if (buttonChar == 'X')
            _payload.buttons &= ~(1 << 2);
        else if (buttonChar == 'Y')
            _payload.buttons &= ~(1 << 3);
    }

    _sendOnce = true;
    sendCommandToDrone();
    _sendOnce = false;

    return;
}

void GamepadManager::updateVirtualSendContinuous(bool shouldSend)
{
    _sendContinuous = shouldSend;
}

void GamepadManager::updateVirtualJoystickCommand(QString buttonChar, double value)
{
    updateJoystickCommand(buttonChar.at(0).toLatin1(), value);
}

void GamepadManager::updateVirtualButtonCommand(QString buttonChar, bool pressed)
{
    updateButtonCommand(buttonChar.at(0).toLatin1(), pressed);
}

void GamepadManager::sendCommandToDrone()
{
    _payload.target = Commander::getInstance()->getDroneId();

    if((_hasControl) && ((_sendContinuous) || (_sendOnce)))
    {
        mavlink_message_t message;
        mavlink_msg_manual_control_encode(_comManager->getLocalSystemId(), _comManager->getLocalComponentId(), &message, &_payload);
        _comManager->sendMessage(message);
    }

    return;
}

void GamepadManager::updateControlState(bool hasControl)
{
    qInfo(generalLog()) << "Gamepad authorised: " << hasControl;
    _hasControl = hasControl;
    return;
}

void GamepadManager::checkConnection()
{
    if(_gamepad)
    {
        if(QGamepadManager::instance()->connectedGamepads().isEmpty())
        {
            if(Commander::getInstance())
            {
                Commander::getInstance()->infoToDisplay(tr("Gamepad disconnected"));
            }
            gamepadConnected(false);
            qWarning(generalLog()) << "Gamepad disconnected.";
            delete _gamepad;
            _gamepad = nullptr;
        }
        else
        {
            gamepadConnected(true);
        }
    }
    else
    {
        if(!(QGamepadManager::instance()->connectedGamepads().isEmpty()))
        {
            if(Commander::getInstance())
            {
                Commander::getInstance()->infoToDisplay(tr("Gamepad connected"));
            }
            initGamepadInstance();
        }
        else
        {
            gamepadConnected(false);
        }
    }
}

void GamepadManager::initGamepadInstance()
{
    auto gamepads = QGamepadManager::instance()->connectedGamepads();

    _gamepad = new QGamepad(*gamepads.begin(), this);

    connect(_gamepad, &QGamepad::axisLeftXChanged, this, [=](double value){updateJoystickCommand('R', value);});
    connect(_gamepad, &QGamepad::axisLeftYChanged, this, [=](double value){updateJoystickCommand('Z', value);});
    connect(_gamepad, &QGamepad::axisRightXChanged, this, [=](double value){updateJoystickCommand('Y', value);});
    connect(_gamepad, &QGamepad::axisRightYChanged, this, [=](double value){updateJoystickCommand('X', value);});

    // 'A' button currently unsused
//    connect(_gamepad, &QGamepad::buttonAChanged, this, [=](bool pressed){update_button_command('A', pressed);});
    connect(_gamepad, &QGamepad::buttonBChanged, this, [=](bool pressed){updateButtonCommand('B', pressed);});
    connect(_gamepad, &QGamepad::buttonXChanged, this, [=](bool pressed){updateButtonCommand('X', pressed);});
    connect(_gamepad, &QGamepad::buttonYChanged, this, [=](bool pressed){updateButtonCommand('Y', pressed);});

    connect(_gamepad, &QGamepad::buttonL2Changed, this, [=](bool pressed){_sendContinuous = pressed;});

    gamepadConnected(true);
    qInfo(generalLog()) << "Gamepad connected.";
}
