import QtPositioning 5.6
import QtLocation 5.11
import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.11
import QtQuick.Window 2.2
import "qmlcomponents"

/*
 * The mainWindow ApplicationWindow is the main QML object.
 * It contains all other objects and global properties.
 *
 * It manages the ground stations list and some statemachine signals.
 */
ApplicationWindow
{
    id: mainWindow

    readonly property string darkBackgroundColor: "#88000000"
    readonly property string darkItemOnDarkBackgroundColor: "#88000000" //"#FFA500" (orange)
    readonly property string disabledTextColor: "#60000000"
    readonly property string enabledTextColor: "#dd000000"
    readonly property string itemOnDarkBackgroundColor: "#ffffff"//"#dcdcdc"
    readonly property string problemTextColor: "red"
    readonly property string routeColor: Material.accent
    readonly property string weRoboticsColor: "#C64F38"
    readonly property int iconSize: mainWindow.height / 11.5
    readonly property int routeWidth: 5

    property string connectedStationsIdList: ""
    property string pluginName: "osm"
    property var connectedStationsList: ListModel{}
    property var infoAndCredits: InfoAndCredits{}
    property var infoComponent
    property var mainMap: Map{visible: false}
    property var mapComponent
    property var mapPlugin: MapPlugin{}
    property var pluginComponent
    property var tabletLocation
    property var tabletLocationAccuracy
    property int connectedGroundstationsCounter: 0
    property int currentCommand: 0
    property int currentRouteId: 0 // Number in file name. Can be anything (not twice the same)
    property int currentRouteIndex: 0 // Number in list of missions. From 0 to nbMissions - 1
    property int currentRouteVersion: 0
    property int currentGroundstationState: 0
    property int currentDroneState: 0
    property bool antennaIsConnected: false
    property bool droneIsConnected: false
    property bool gamepadIsConnected: false
    property bool hasManualControl: false
    property bool lockOnDrone: false
    property bool webserverIsConnected: false
    property bool isHighLatency: false

    signal changeComm
    signal changeMap
    signal confirmSignal
    signal noRouteSelected
    signal routeSelected(int ID, double length)
    signal locationAccuracyChanged
    signal hideTelemetryAndRouteSelection
    signal radioStatusReset


    function findGroundstationIndexInList(systemId)
    {
        for (var i = 0; i <= connectedStationsList.count - 1; i++)
            if (connectedStationsList.get(i).systemId === systemId)
                return i;
    }

    Material.accent: "#11BAC7"//(blue)    //  "#F48FB1"//(pink)   // "#80DEEA"
//    Material.background: "#6F6866"
//    Material.foreground: "#FBFAF8"
    Material.primary: "darkblue"
    Material.theme: Material.Light
    visibility: "FullScreen"
    visible: true

    Connections
    {
        target: stateMachine
        function onAntennaConnected(isConnected) {
            antennaIsConnected = isConnected;
        }
        function onControlStatusChanged(hasControl) {
            hasManualControl = hasControl;
        }
        function onDroneConnected(isConnected) {
            droneIsConnected = isConnected;
        }
        function onDroneStateChanged(newState, newStateStr) {
            currentDroneState = newState;
        }
        function onGamepadConnected(isConnected) {
            gamepadIsConnected = isConnected;
        }
        function onInfoToDisplay(message) {
            infoMessageListContainer.model.append({ text: message });
        }
        function onStateMachineStateChanged(newState) {
            currentGroundstationState = newState;
        }
        function onWebserverConnected(isConnected) {
            webserverIsConnected = isConnected;
        }
        function connectionTypeUpdated(hlState) {
            isHighLatency = hlState;
        }

        function onGroundstationConnected(isConnected, systemId) {
            if(isConnected)
            {
                // Add station to connected stations list
                connectedGroundstationsCounter += 1
                connectedStationsList.append({"systemId": systemId})

                // Recreate list of station ids to display
                connectedStationsIdList = ""
                for (var i = 0; i <= connectedStationsList.count - 1; i++)
                {
                    if(i && (i%7 == 0))
                    {
                        connectedStationsIdList += "\n"
                    }
                    const stationIdToAdd = connectedStationsList.get(i).systemId;
                    connectedStationsIdList += " " + stationIdToAdd
                }
            }
            else
            {
                // Remove station from connected stations list
                var gsIndex = findGroundstationIndexInList(systemId)
                connectedStationsList.remove(gsIndex)
                connectedGroundstationsCounter -= 1

                // Recreate list of station ids to display
                connectedStationsIdList = ""
                for (var j = 0; j <= connectedStationsList.count - 1; j++)
                {
                    if(j && (j%7 == 0))
                    {
                        connectedStationsIdList += "\n"
                    }
                    const stationIdToRemove = connectedStationsList.get(j).systemId;
                    connectedStationsIdList += " " + stationIdToRemove
                }
            }
        }
    }


    Buttons
    {
        id: allButtons
    }

    RouteSelectionBox
    {
        id: routeSelectionBox
    }

    TelemetryBox
    {
        id: telemetryBox
    }

    PopupMessage
    {
        id: popupMessage
    }

    InfoMessage
    {
        id: infoMessageListContainer
    }

    TabletLocation
    {
        id: tabletLocationDisplay
    }

    SettingsPopup
    {
        id: settingsPopup
    }

    ReceptionPopup
    {
        id: receptionRequestPopup
    }

    WarningMessagePopup
    {
        id: warningMessagePopup
    }

    FirstPopup
    {
        id: weatherCheckPopup
    }

    SecondPopup
    {
        id: safetyCheckPopup
    }
}
