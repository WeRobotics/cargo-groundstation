package werobotics.cargo_gs;

import com.mapbox.mapboxsdk.geometry.LatLngBounds;

public class OfflineMaps
{
    // Set up the OfflineManager
    OfflineManager offlineManager = OfflineManager.getInstance(Android_permission.this);

    // Create a bounding box for the offline region
    LatLngBounds latLngBounds = new LatLngBounds.Builder()
    .include(new LatLng(28.180623, 83.003073)) // Northeast
    .include(new LatLng(28.024844, 82.698645)) // Southwest
    .build();

    // Define the offline region
    OfflineTilePyramidRegionDefinition definition = new OfflineTilePyramidRegionDefinition(
    mapboxMap.getStyleUrl(),
    latLngBounds,
    10,
    20,
    Android_permission.this.getResources().getDisplayMetrics().density);




    // Implementation that uses JSON to store Pyuthan as the offline region name.
    byte[] metadata;
    try {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(JSON_FIELD_REGION_NAME, "Pyuthan region");
        String json = jsonObject.toString();
        metadata = json.getBytes(JSON_CHARSET);
    } catch (Exception exception) {
        Log.e(TAG, "Failed to encode metadata: " + exception.getMessage());
        metadata = null;
    }




    // Create the region asynchronously
    offlineManager.createOfflineRegion(definition, metadata,
        new OfflineManager.CreateOfflineRegionCallback() {
            @Override
            public void onCreate(OfflineRegion offlineRegion) {
                offlineRegion.setDownloadState(OfflineRegion.STATE_ACTIVE);

                // Monitor the download progress using setObserver
                offlineRegion.setObserver(new OfflineRegion.OfflineRegionObserver() {
                    @Override
                    public void onStatusChanged(OfflineRegionStatus status) {

                        // Calculate the download percentage
                        double percentage = status.getRequiredResourceCount() >= 0
                        ? (100.0 * status.getCompletedResourceCount() / status.getRequiredResourceCount()) :
                        0.0;

                        if (status.isComplete()) {
                            // Download complete
                            Log.d(TAG, "Region downloaded successfully.");
                        } else if (status.isRequiredResourceCountPrecise()) {
                            Log.d(TAG, percentage);
                        }
                    }

                    @Override
                    public void onError(OfflineRegionError error) {
                        // If an error occurs, print to logcat
                        Log.e(TAG, "onError reason: " + error.getReason());
                        Log.e(TAG, "onError message: " + error.getMessage());
                    }

                    @Override
                    public void mapboxTileCountLimitExceeded(long limit) {
                        // Notify if offline region exceeds maximum tile count
                        Log.e(TAG, "Mapbox tile count limit exceeded: " + limit);
                    }
                });
            }

        @Override
        public void onError(String error) {
            Log.e(TAG, "Error: " + error);
        }
    });
}
