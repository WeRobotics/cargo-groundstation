#ifndef ROUTEDATA_H
#define ROUTEDATA_H

#include <QObject>
#include <QVector>
#include <QGeoCoordinate>
#include <QNetworkReply>

/*
 * The RouteData class is a class managing useful information about a specific mission
 * and making it accessible from the GUI.
 *
 * It reads mission information from a qGroundControl generated mission file named as follows:
 * id_version_name-without-underscore.plan
 * Example: 4_1_Bohechio-ElCoco.plan
 *
 * It contains the following information retrieved from the tablet mission file or constructed from these :
 * - Which can (indirectly) be accessed from the GUI:
 * @var _waypointList: a vector of QGeoCoordinate containing the list of waypoints of the mission.
 * @var _rallyPointList: a vector of QGeoCoordinate containing the list of rally points of the mission.
 * @var _offlineGroundAltitudeList: a vector of double containing the flight altitude above ground at every waypoint.
 * @var _groundAltitudeList:  a vector of double containing the flight altitude above ground all along the route (from online query).
 * Warning: _groundAltitudeList currently not working properly. Altitude differs depending on query source.
 * @var _name: a string containing the name of the mission, from file name.
 * @var _displayName: a string containing the name displayed on the GUI, including mission index, name, id and version.
 * @var _id: an integer containing the id of the mission, from file name. Can be any positive integer.
 * @var _index: an integer containing the index of the mission in the list. From 0 to nb_of_routes.
 * @var _version: an integer containing the version of the mission, from file name.
 * @var _length: a double containing the horizontal length of the route in meters.
 * @var _maxAlt: a double containing the maximum altitude reached during the  mission.
 * @var _minAlt: a double containing the minimum altitude reached during the  mission.
 * @var _isReady: a boolean indicating whether the elevation data has been received and processed (= _groundAltitudeList is filled properly)
 * - Which cannot be accessed from the GUI:
 * @var _firstWaypointGroundAltitude: a double containing the ground altitude of the first waypoint.
 * @var _firstWaypointRelativeFlightAltitude: a double containing the flight altitude of the first waypoint (relative to its ground altitude).
 * @var_altitudeValueDiff: a double containing the difference between first waypoint ground altitude and the first elevation data query point ground altitude, for debugging.
 */
class RouteData : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString displayName READ displayName NOTIFY displayNameChanged)
    Q_PROPERTY(int id READ id NOTIFY idChanged)
    Q_PROPERTY(int index READ index NOTIFY indexChanged)
    Q_PROPERTY(int version READ version NOTIFY versionChanged)
    Q_PROPERTY(double length READ length NOTIFY lengthChanged)
    Q_PROPERTY(double maxAlt READ maxAlt NOTIFY maxAltChanged)
    Q_PROPERTY(double minAlt READ minAlt NOTIFY minAltChanged)
    Q_PROPERTY(QVector<double> offlineGroundAltitudeList READ offlineGroundAltitudeList NOTIFY offlineGroundAltitudeListChanged)
    Q_PROPERTY(QVector<double> groundAltitudeList READ groundAltitudeList NOTIFY groundAltitudeListChanged)
    Q_PROPERTY(bool isReady READ isReady NOTIFY isReadyChanged)

public:
    RouteData(const int index);
    RouteData(const int index, const QString missionFileName);
    ~RouteData();

    Q_INVOKABLE int getNumberOfWaypoints() const;
    Q_INVOKABLE int getNumbeOfRallyPoints() const;
    Q_INVOKABLE int getOfflineGroundAltitudeListSize() const;
    Q_INVOKABLE int getGroundAltitudeListSize() const;
    Q_INVOKABLE QGeoCoordinate getWaypoint(int n) const;
    Q_INVOKABLE QGeoCoordinate getRallyPoint(int n) const;
//    Q_INVOKABLE void setWaypoint(int n, QGeoCoordinate point);
//    Q_INVOKABLE void insertWaypoint(int n, QGeoCoordinate point);
    Q_INVOKABLE void appendWaypoint(QGeoCoordinate point);
    Q_INVOKABLE void appendRallyPoint(QGeoCoordinate point);
//    Q_INVOKABLE void prependWaypoint(QGeoCoordinate point);

    Q_INVOKABLE void setName(QString name);

    QString name() const;
    QString displayName() const;
    int id() const;
    int index() const;
    int version() const;
    double length() const;
    double maxAlt() const;
    double minAlt() const;
    QVector<double> offlineGroundAltitudeList() const;
    QVector<double> groundAltitudeList() const;
    bool isReady() const;
    QGeoCoordinate* getHomePosition();

signals:
    void nameChanged() const;
    void displayNameChanged() const;
    void idChanged() const;
    void indexChanged() const;
    void versionChanged() const;
    void lengthChanged() const;
    void maxAltChanged() const;
    void minAltChanged() const;
    void offlineGroundAltitudeListChanged() const;
    void groundAltitudeListChanged() const;
    void isReadyChanged() const;

private slots:
    void readMissionFromFile(const QString missionFileName);
    void sendElevationQuery();
    void handleElevationData(QSharedPointer<QNetworkReply> networkReply);

private:
    QVector<QGeoCoordinate> _waypointList;
    QVector<QGeoCoordinate> _rallyPointList;
    QVector<double> _offlineGroundAltitudeList;
    QVector<double> _groundAltitudeList;
    QString _name;
    QString _displayName;
    QGeoCoordinate _homePosition;
    int _id;
    const int _index;
    int _version;
    double _length;
    double _maxAlt;
    double _minAlt;
    bool _isReady;
    double _firstWaypointGroundAltitude;
    double _firstWaypointRelativeFlightAltitude;
    double _altitudeValueDiff;
    QSharedPointer<QNetworkAccessManager> _networkAccessManager;
};

#endif // ROUTEDATA_H
