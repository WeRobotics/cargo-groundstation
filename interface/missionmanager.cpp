#include "missionmanager.h"

#include <QDir>
#include <QStandardPaths>
#include <QRegExpValidator>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QPointer>

#include "logs/loggingcategories.h"
#include "translation/translation.h"

MissionManager::MissionManager():
    QObject()
{
    createRouteList();
}

MissionManager::~MissionManager()
{
    qInfo() << "~ControlInterface()";
    foreach (QVariant route, _routeList) {
            route.clear();
    }
    _routeList.clear();
}

void MissionManager::createRouteList()
{
    int counter = 0;

    RouteData* newRoute = new RouteData(counter++);
    _routeList.append(QVariant::fromValue(newRoute));

    QDir directory(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation));
    directory.mkpath("Cargo_GS/missions");
    directory.setPath(directory.path().append("/Cargo_GS/missions"));

    QStringList missions = directory.entryList(QStringList() << "*.plan", QDir::Files);

    QRegExp regexString("^[0-9]+_[0-9]+_[^./]+\\.plan$");

    QPointer<QRegExpValidator> validator = new QRegExpValidator(regexString, this);

    foreach(QString filename, missions)
    {
        int pos = 0;
        if(validator->validate(filename, pos) == QValidator::Acceptable)
        {
            newRoute = new RouteData(counter++, directory.path() + "/" + filename);
            _routeList.append(QVariant::fromValue(newRoute));
        }
        else
        {
            qInfo(generalLog()) << "Failed to open mission plan: " << filename << " . Filename is not formatted correctly (ID_Version_Name).";
        }
    }
}

QVariantList MissionManager::getMisionList() {
    return _routeList;
}

void MissionManager::setActiveMission(uint8_t missionId, uint8_t versionId) {
    RouteData* thisRoute = nullptr;
    _activeRoute = nullptr;

    qInfo(generalLog()) << "Searching for the new active mission.";
    foreach (QVariant route, _routeList) {
            thisRoute = route.value<RouteData*>();
            if(thisRoute->id() == missionId && (thisRoute->version() == versionId || versionId == 255)) {
                _activeRoute = thisRoute;
                qInfo(generalLog()) << "Mission manager : Active mission set to \"" << _activeRoute->name() << "\"";
            }
    }
}

void MissionManager::clearActiveMission() {
    _activeRoute = nullptr;
}

RouteData* MissionManager::getActiveMission() {
    return _activeRoute;
}
