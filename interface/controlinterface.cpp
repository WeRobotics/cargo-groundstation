#include <QtQuickControls2/QQuickStyle>
#include <QtQml/QQmlApplicationEngine>
#include <QQmlComponent>
#include <QDebug>
#include <QQmlContext>
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QQuickItem>
#include <QDir>
#include <QStandardPaths>
#include <QRegExpValidator>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QPointer>

#include <iostream>
#include <fstream>

#include "controlinterface.h"
#include "logs/loggingcategories.h"
#include "translation/translation.h"

ControlInterface::ControlInterface(QSharedPointer<QQmlApplicationEngine> engine): QObject()
{
    qInfo(generalLog()) << "Initialising interface.";
    qputenv("QT_QUICK_CONTROLS_STYLE", "material");

    _translator.reset(new Translator(engine));
    engine->rootContext()->setContextProperty("translator", _translator.get());

    // Launch application main window
    _component.reset(new QQmlComponent(engine.get(), QUrl(QLatin1String("qrc:/qmlcomponents/ConfigErrorMain.qml"))));
    _object.reset(_component->create()); // Object = Application Window
}

ControlInterface::ControlInterface(QSharedPointer<QQmlApplicationEngine> engine,
                                   QSharedPointer<StateMachine> stateMachine,
                                   QSharedPointer<GamepadManager> gamepadManager,
                                   QSharedPointer<MissionManager> missionManager,
                                   QString defaultIp, QString defaultPort,
                                   QString defaultSendingPilotName, QString defaultReceivingPilotName,
                                   QVariantList relayStationList): QObject()
{
    qInfo(generalLog()) << "Initialising interface.";
    qputenv("QT_QUICK_CONTROLS_STYLE", "material");

    // Expose objects to QM
    engine->rootContext()->setContextProperty("defaultIp", QVariant::fromValue(defaultIp));
    engine->rootContext()->setContextProperty("defaultPort", QVariant::fromValue(defaultPort));
    engine->rootContext()->setContextProperty("defaultSendingPilotName", QVariant::fromValue(defaultSendingPilotName));
    engine->rootContext()->setContextProperty("defaultReceivingPilotName", QVariant::fromValue(defaultReceivingPilotName));

    engine->rootContext()->setContextProperty("stateMachine", QVariant::fromValue(stateMachine.get())); // QML does not accept QSharedPointer
    engine->rootContext()->setContextProperty("gamepadManager", QVariant::fromValue(gamepadManager.get()));
    engine->rootContext()->setContextProperty("routeList", QVariant::fromValue(missionManager->getMisionList()));
    engine->rootContext()->setContextProperty("relayStationList", QVariant::fromValue(relayStationList));

    _translator.reset(new Translator(engine));
    engine->rootContext()->setContextProperty("translator", _translator.get());

    // Launch application main window
    _component.reset(new QQmlComponent(engine.get(), QUrl(QLatin1String("qrc:/main.qml"))));
    _object.reset(_component->create()); // Object = Application Window
}

ControlInterface::~ControlInterface()
{
    qInfo() << "~ControlInterface()";
}

