#include <QDebug>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <cfloat>

#include "routedata.h"
#include "logs/loggingcategories.h"

RouteData::RouteData(const int index) : QObject(),
_name(tr("Select route")),
_displayName(tr("Select route")),
_id(0),
_index(index),
_version(0),
_length(0),
_maxAlt(0),
_minAlt(DBL_MAX),
_isReady(false),
_firstWaypointGroundAltitude(0),
_networkAccessManager(QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this)))
{
    this->appendWaypoint(QGeoCoordinate(0, 0, 0));
}

RouteData::RouteData(const int index, const QString missionFileName) : QObject(),
_name(tr("Unknown")),
_displayName(tr("Unknown")),
_id(0),
_index(index),
_version(0),
_length(0),
_maxAlt(0),
_minAlt(DBL_MAX),
_isReady(false),
_firstWaypointGroundAltitude(0),
_networkAccessManager(QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this)))
{
    QStringList missionInfo = missionFileName.split("/").last().split(".").first().split("_");

    _id = missionInfo.first().toInt();
    if(missionInfo.length() > 2)
    {
        _version = missionInfo[1].toInt();
        _name = missionInfo.last();
    }
    _displayName = QString::number(_index) + ") " + _name + " (" + QString::number(_id) + "." + QString::number(_version) + ")";

    readMissionFromFile(missionFileName);

//    sendElevationQuery();
}

RouteData::~RouteData()
{
    qInfo() << "~RouteData()";
    _networkAccessManager.clear();
}

QGeoCoordinate* RouteData::getHomePosition() {
    return &_homePosition;
}

void RouteData::readMissionFromFile(const QString missionFileName)
{
    QFile missionFile(missionFileName);
    QJsonDocument missionPlan;

    if(!missionFile.open(QIODevice::ReadOnly))
    {
        qInfo(generalLog()) << "Failed to open mission plan: " << missionFileName;
    }
    else
    {
        qInfo(generalLog()) << "Mission plan opened: " << missionFileName.split("/").last();
        missionPlan = QJsonDocument().fromJson(missionFile.readAll());
    }

    missionFile.close();

    if (missionPlan.isObject())
    {
        QJsonObject jsonObject = missionPlan.object();
        QJsonObject tempJsonObject;
        QJsonValue jsonValue;
        double lat, lon, alt;

        if (jsonObject.contains(QStringLiteral("mission")))
        {
            jsonValue = jsonObject.value(QStringLiteral("mission"));
            tempJsonObject = jsonValue.toObject();

            jsonValue = tempJsonObject.value(QStringLiteral("plannedHomePosition"));
            QJsonArray hpData = jsonValue.toArray();

            _homePosition.setLatitude(hpData.at(0).toDouble());
            _homePosition.setLongitude(hpData.at(1).toDouble());
            _homePosition.setAltitude(hpData.at(2).toDouble());

            jsonValue = tempJsonObject.value(QStringLiteral("items"));
            QJsonArray pointsArray = jsonValue.toArray();

            for(int i = 0; i < pointsArray.size(); i++)
            {
                jsonValue = pointsArray.at(i);
                tempJsonObject = jsonValue.toObject();

                if (tempJsonObject.contains(QStringLiteral("command")))
                {
                    int pointType = tempJsonObject.value(QStringLiteral("command")).toInt();
                    if(pointType == 195) // Rally point
                    {
                        jsonValue = tempJsonObject.value(QStringLiteral("params"));
                        QJsonArray pointArray = jsonValue.toArray();

                        lat = pointArray.at(4).toDouble();
                        lon = pointArray.at(5).toDouble();
                        alt = pointArray.at(6).toDouble();
                        this->appendRallyPoint(QGeoCoordinate(lat, lon, alt));
                        continue;
                    }
                    else if((pointType == 16) || (pointType == 21) || (pointType == 22)) // Waypoint, Take off or land
                    {
                        if(!_firstWaypointGroundAltitude)
                        {
                            _firstWaypointRelativeFlightAltitude = tempJsonObject.value(QStringLiteral("Altitude")).toDouble();
                        }

                        jsonValue = tempJsonObject.value(QStringLiteral("params"));
                        QJsonArray pointArray = jsonValue.toArray();

                        lat = pointArray.at(4).toDouble();
                        lon = pointArray.at(5).toDouble();
                        alt = pointArray.at(6).toDouble();
                        this->appendWaypoint(QGeoCoordinate(lat, lon, alt));

                        if(!_firstWaypointGroundAltitude)
                        {
                            _firstWaypointGroundAltitude = alt - _firstWaypointRelativeFlightAltitude;
                        }

                        if (tempJsonObject.contains(QStringLiteral("Altitude")))
                        {
                            int altitudeAboveGround = tempJsonObject.value(QStringLiteral("Altitude")).toInt();
                            _offlineGroundAltitudeList.append(alt - altitudeAboveGround);

                            if((alt - altitudeAboveGround) < minAlt())
                                _minAlt = alt - altitudeAboveGround;
                        }
                    }
                }
            }
        }
    }
    else
    {
        qWarning(generalLog()) << "Mission plan is empty.";
    }
}

// Currently not used
void RouteData::sendElevationQuery()
{
//    QJsonObject inputLineObj;
//    inputLineObj["geometryType"] = "esriGeometryPolyline";
//    QJsonObject spatialRef;
//    spatialRef["wkid"] = 4326;
//    inputLineObj["spatialReference"] = spatialRef;
//    QJsonArray fields;
//    QJsonObject objIdField;
//    objIdField["name"] = "ObjectId";
//    objIdField["alias"] = "ObjectId";
//    objIdField["type"] = "esriFieldTypeOID";
//    QJsonObject nameField;
//    nameField["name"] = "Name";
//    nameField["alias"] = "Name";
//    nameField["type"] = "esriFieldTypeString";
//    fields.append(objIdField);
//    fields.append(nameField);
//    inputLineObj["fields"] = fields;

//    QJsonArray path;
//    for(int i = 0; i < _waypointsList.size(); i++)
//    {
//        QJsonArray point;
//        point.append(_waypointsList[i].latitude());
//        point.append(_waypointsList[i].longitude());
//        path.append(point);
//    }
//    QJsonArray paths;
//    paths.append(path);

//    QJsonObject geometry;
//    geometry["paths"] = paths;
//    QJsonObject attributes;
//    attributes["ObjectID"] = 1;
//    attributes["Name"] = "Bern";
//    QJsonObject feature1;
//    feature1["attributes"] = attributes;
//    feature1["geometry"] = geometry;
//    QJsonArray features;
//    features.append(feature1);
//    inputLineObj["features"] = features;

//    QJsonDocument inputLineDoc(inputLineObj);
//    QString inputLineString(inputLineDoc.toJson(QJsonDocument::Compact));

//    QUrl urlElevation("http://elevation.arcgis.com/arcgis/rest/services/Tools/ElevationSync/GPServer/Profile/execute");
//    QUrlQuery queryElevation;
//    queryElevation.addQueryItem("f", "json");
//    queryElevation.addQueryItem("InputLineFeatures", inputLineString);
//    queryElevation.addQueryItem("ProfileIDField", "ObjectID");
//    queryElevation.addQueryItem("DEMResolution", "FINEST");
////    queryElevation.addQueryItem("MaximumSampleDistance", QString::number(samplingDistance));
////    queryElevation.addQueryItem("MaximumSampleDistanceUnits", "Meters");
//    queryElevation.addQueryItem("returnZ", "true");
//    queryElevation.addQueryItem("returnM", "true");

    QString path("49.976032,14.128741875,49.974939,15.13063");

//    QString path;
//    for(int i = 0; i < 1; i++) //_waypointsList.size()
//    {
//        QString point;
//        if(i)
//            point.append(",");
//        point.append(QString::number(_waypointsList[i].latitude()));
//        point.append(",");
//        point.append(QString::number(_waypointsList[i].longitude()));
//        path.append(point);
//    }
    QUrl urlElevation("https://api.airmap.com/elevation/v1/ele/path");
    QUrlQuery queryElevation;
    queryElevation.addQueryItem("points", path);
    urlElevation.setQuery(queryElevation);

    QNetworkRequest request;
    request.setUrl(urlElevation);
    //request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    request.setRawHeader("X-API-Key", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVkZW50aWFsX2lkIjoiY3JlZGVudGlhbHwzTGx5TVJYdHZFS2w4b0ZuZzROT3djOEVKTEtsIiwiYXBwbGljYXRpb25faWQiOiJhcHBsaWNhdGlvbnw2eWRKejdZdGJhRFlCYmhRZFFncExVb09LR3ciLCJvcmdhbml6YXRpb25faWQiOiJkZXZlbG9wZXJ8cXY4SnlnZ3RiRVhEQU5mNXBxd2tYVU82d3g2NiIsImlhdCI6MTU1NTg2NjcwN30.Otimooi41C_DEdFQT1V6AZLsRoI80MRPNqMeE0XAhEI");

    QSslConfiguration sslConf = request.sslConfiguration();
    sslConf.setPeerVerifyMode(QSslSocket::VerifyNone);
    request.setSslConfiguration(sslConf);

    QSharedPointer<QNetworkReply> repElevation = QSharedPointer<QNetworkReply>(_networkAccessManager->get(request));
    connect(repElevation.get(), &QNetworkReply::finished, this, [this, repElevation]() { handleElevationData(repElevation); });

//    QString path;
//    for(int i = 0; i < 3; i++) //_waypointsList.size()
//    {
//        QString point;
//        if(i)
//            point.append(",");
//        point.append(QString::number(_waypointsList[i].latitude()));
//        point.append(",");
//        point.append(QString::number(_waypointsList[i].longitude()));
//        path.append(point);
//    }
//    QUrl urlElevation("https://api.airmap.com/elevation/v1/ele/path");
//    QUrlQuery queryElevation;
//    queryElevation.addQueryItem("points", path);

//    urlElevation.setQuery(queryElevation);

//    if(_id ==8)
//        qInfo() << urlElevation;

//    QNetworkRequest request;
//    request.setUrl(urlElevation);
//    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

//    QSslConfiguration sslConf = request.sslConfiguration();
//    sslConf.setPeerVerifyMode(QSslSocket::VerifyNone);
//    request.setSslConfiguration(sslConf);

//    QSharedPointer<QNetworkReply> repElevation = QSharedPointer<QNetworkReply>(_networkAccessManager->post(request, "{\"airmap\":{\"api_key\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjcmVkZW50aWFsX2lkIjoiY3JlZGVudGlhbHwzTGx5TVJYdHZFS2w4b0ZuZzROT3djOEVKTEtsIiwiYXBwbGljYXRpb25faWQiOiJhcHBsaWNhdGlvbnw2eWRKejdZdGJhRFlCYmhRZFFncExVb09LR3ciLCJvcmdhbml6YXRpb25faWQiOiJkZXZlbG9wZXJ8cXY4SnlnZ3RiRVhEQU5mNXBxd2tYVU82d3g2NiIsImlhdCI6MTU1NTg2NjcwN30.Otimooi41C_DEdFQT1V6AZLsRoI80MRPNqMeE0XAhEI\",\"client_id\":\"3ad2ff94-41ea-47e1-98ae-23a8236bce3e\",\"callback_url\":null},\"mapbox\":{\"access_token\":null}}"));
//    connect(repElevation.get(), &QNetworkReply::finished, this, [this, repElevation]() { handleElevationNetworkData(repElevation); });
}

// Currently not used
void RouteData::handleElevationData(QSharedPointer<QNetworkReply> networkReply)
{
    if (!networkReply)
    {
        return;
    }

    qInfo() << networkReply->errorString();

    if (!networkReply->error())
    {
        qInfo(networkReply->readAll());
        qInfo("Success !");
    }
    else
    {
        qInfo(networkReply->readAll());
        qInfo("Error !");
    }

    qInfo("Here !");

    return;


    if (!networkReply)
    {
        return;
    }

    if (!networkReply->error())
    {
        QJsonDocument document = QJsonDocument::fromJson(networkReply->readAll());

        if (document.isObject())
        {
            QJsonObject obj = document.object();
            QJsonObject tempObject;
            QJsonValue val;

            if (obj.contains(QStringLiteral("results")))
            {
                val = obj.value(QStringLiteral("results"));
                QJsonArray resultsArray = val.toArray();

                val = resultsArray.at(0);
                tempObject = val.toObject();
                if(tempObject.contains(QStringLiteral("value")))
                {
                    val = tempObject.value(QStringLiteral("value"));
                    tempObject = val.toObject();
                    if(tempObject.contains(QStringLiteral("features")))
                    {
                        val = tempObject.value(QStringLiteral("features"));
                        QJsonArray featuresArray = val.toArray();

                        val = featuresArray.at(0);
                        tempObject = val.toObject();

                        if(tempObject.contains(QStringLiteral("geometry")))
                        {
                            val = tempObject.value(QStringLiteral("geometry"));
                            tempObject = val.toObject();

                            if(tempObject.contains(QStringLiteral("paths")))
                            {
                                val = tempObject.value(QStringLiteral("paths"));
                                QJsonArray pathsArray = val.toArray();

                                val = pathsArray.at(0);
                                QJsonArray pathArray = val.toArray();

                                bool firstWaypoint = true;
                                for(const auto &waypoint : _waypointList)
                                {
                                    int closest = -1;
                                    double distanceMin = DBL_MAX;

                                    for(int i = 0; i < pathArray.size(); i++)
                                    {
                                        val = pathArray.at(i);
                                        QJsonArray point = val.toArray();

                                        if(firstWaypoint)
                                        {
                                            if(!i)
                                            {
                                                _altitudeValueDiff = _firstWaypointGroundAltitude - point.at(2).toDouble();
                                            }
                                            _groundAltitudeList.append(point.at(2).toDouble() + _altitudeValueDiff);

//                                            if(_id ==8)
//                                            {
//                                                qInfo(generalLog()) << "i: " << i << ", waypoint: " << waypoint;
//                                                qInfo(generalLog()) << "_firstWaypointGroundAltitude: " << _firstWaypointGroundAltitude;
//                                                qInfo(generalLog()) << "_altitudeValueDiff: " << _altitudeValueDiff;
//                                                qInfo(generalLog()) << "Altitude from query: " << point.at(2).toDouble();
//                                            }

                                            if((point.at(2).toDouble() + _altitudeValueDiff) < minAlt())
                                            {
                                                _minAlt = point.at(2).toDouble() + _altitudeValueDiff;
                                            }
                                        }

                                        double distance = waypoint.distanceTo(QGeoCoordinate(point.at(0).toDouble(), point.at(1).toDouble()));
                                        if (distance < distanceMin)
                                        {
                                            distanceMin = distance;
                                            closest = i;
                                        }
                                    }
                                    firstWaypoint = false;
                                }
                                _isReady = true;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            qWarning(generalLog()) << "Elevation request response is empty.";
        }
    }
    else
    {
        qWarning(generalLog()) << "Elevation query error: " << networkReply->errorString();
    }

    networkReply->deleteLater();

    return;
}

int RouteData::getNumberOfWaypoints() const
{
    return _waypointList.size();
}

int RouteData::getNumbeOfRallyPoints() const
{
    return _rallyPointList.size();
}

int RouteData::getOfflineGroundAltitudeListSize() const
{
    return _offlineGroundAltitudeList.size();
}

int RouteData::getGroundAltitudeListSize() const
{
    return _isReady ? _groundAltitudeList.size() : _offlineGroundAltitudeList.size();
}

QGeoCoordinate RouteData::getWaypoint(int n) const
{
    return (n < _waypointList.size()) ? _waypointList[n] : QGeoCoordinate(); // QGeoCoordinate() => isValid property = false
}

QGeoCoordinate RouteData::getRallyPoint(int n) const
{
    return (n < _rallyPointList.size()) ? _rallyPointList[n] : QGeoCoordinate(); // QGeoCoordinate() => isValid property = false
}

void RouteData::appendWaypoint(QGeoCoordinate point)
{
    if(point.altitude() > maxAlt())
    {
        _maxAlt = point.altitude();
    }

    if(!_waypointList.isEmpty())
    {
        _length += _waypointList.last().distanceTo(point);
    }

    _waypointList.append(point);
}

void RouteData::appendRallyPoint(QGeoCoordinate point)
{
    _rallyPointList.append(point);
}

void RouteData::setName(QString name)
{
    if (name != _name)
    {
        _name = name;
        emit nameChanged();
    }
}

QString RouteData::name() const
{
    return _name;
}

QString RouteData::displayName() const
{
    return _displayName;
}

int RouteData::id() const
{
    return _id;
}

int RouteData::index() const
{
    return _index;
}

int RouteData::version() const
{
    return _version;
}

double RouteData::length() const
{
    return _length;
}

double RouteData::maxAlt() const
{
    return _maxAlt;
}

double RouteData::minAlt() const
{
    return _minAlt;
}

bool RouteData::isReady() const
{
    return _isReady;
}

QVector<double> RouteData::offlineGroundAltitudeList() const
{
    return _offlineGroundAltitudeList;
}

QVector<double> RouteData::groundAltitudeList() const
{
    return _isReady ? _groundAltitudeList : _offlineGroundAltitudeList;
}
