#ifndef MISSIONMANAGER_H
#define MISSIONMANAGER_H

#include <QVariant>
#include "routedata.h"


class MissionManager : public QObject
{
    Q_OBJECT
public:
    MissionManager();
    ~MissionManager();

    QVariantList getMisionList();
    RouteData* getActiveMission();

public slots:
    void setActiveMission(uint8_t missionId, uint8_t versionId);
    void clearActiveMission();

private:
    QVariantList _routeList;
    RouteData* _activeRoute = nullptr;

    void createRouteList();
};

#endif // MISSIONMANAGER_H
