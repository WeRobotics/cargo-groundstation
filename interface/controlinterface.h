#ifndef CONTROLINTERFACE_H
#define CONTROLINTERFACE_H

#include <QObject>
#include <QApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QMap>

#include "statemachine/statemachine.h"
#include "interface/routedata.h"
#include "gamepadmanager.h"
#include "translation/translation.h"
#include "missionmanager.h"

/*
 * The ControlInterface class is the class managing the GUI.
 *
 * Creates and initialises the predefined routes list using the qGroundControl generated mission files.
 *
 * Exposes necessary objects and parameters to the QML code and creates and launches the application window.
 * @param defaultIp: default tcp connection ip to pre-fill the field.
 * @param defaultPort: default tcp connection port to pre-fill the field.
 * Warning: defaultIp and defaultPort currently unused. See README.md to re-activate functionality.
 * @param defaultSendingPilotName: default sending pilot name to pre-fill the field.
 * @param defaultReceivingPilotName: default receiving pilot name to pre-fill the field.
 * Warning: defaultReceivingPilotName currently unused. See README.md to re-activate functionality.
 * @object stateMachine: pointer on StateMachine object to enable state change events to be triggered by GUI events.
 * @object gamepadManager: pointer on GamepadManager object to enable virtual gamepad.
 * @object routeList: list of pointers on RouteData objects. Contains all predefined missions to display in GUI combobox for selection.
 * @object relayStationList: list of pointers on RelaySystem objects. Contains all relay stations to display on GUI map.
 * @object translator: pointer on Translator object. Enables language change from GUI combobox.
 */
class ControlInterface : public QObject
{
    Q_OBJECT

public:
    explicit ControlInterface(QSharedPointer<QQmlApplicationEngine> engine);

    explicit ControlInterface(QSharedPointer<QQmlApplicationEngine> engine,
                              QSharedPointer<StateMachine> stateMachine,
                              QSharedPointer<GamepadManager> gamepadManager,
                              QSharedPointer<MissionManager> missionManager,
                              QString defaultIp, QString defaultPort,
                              QString defaultSendingPilotName, QString defaultReceivingPilotName,
                              QVariantList relayStationList);
    ~ControlInterface();

private:

    QScopedPointer<QQmlComponent> _component;
    QScopedPointer<QObject> _object;

    QScopedPointer<Translator> _translator;
};

#endif // CONTROLINTERFACE_H
