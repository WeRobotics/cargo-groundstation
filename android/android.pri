include($$PWD/../Library/qtandroidserialport/src/qtandroidserialport.pri)
#message("Adding Serial Java Classes")
QT += androidextras
ANDROID_PACKAGE_SOURCE_DIR = $$PWD
OTHER_FILES += \
    $$PWD/AndroidManifest.xml \
    $$PWD/res/xml/device_filter.xml \
    $$PWD/src/com/hoho/android/usbserial/driver/CdcAcmSerialDriver.java \
    $$PWD/src/com/hoho/android/usbserial/driver/CommonUsbSerialDriver.java \
    $$PWD/src/com/hoho/android/usbserial/driver/Cp2102SerialDriver.java \
    $$PWD/src/com/hoho/android/usbserial/driver/FtdiSerialDriver.java \
    $$PWD/src/com/hoho/android/usbserial/driver/ProlificSerialDriver.java \
    $$PWD/src/com/hoho/android/usbserial/driver/UsbId.java \
    $$PWD/src/com/hoho/android/usbserial/driver/UsbSerialDriver.java \
    $$PWD/src/com/hoho/android/usbserial/driver/UsbSerialProber.java \
    $$PWD/src/com/hoho/android/usbserial/driver/UsbSerialRuntimeException.java \
#    $$PWD/src/werobotics/cargo_gs/QGCActivity.java \
    $$PWD/src/werobotics/cargo_gs/UsbIoManager.java \
    $$PWD/src/werobotics/cargo_gs/TaiSync.java

#DISTFILES += \
#    $$PWD/android/gradle/wrapper/gradle-wrapper.jar \
#    $$PWD/android/gradlew \
#    $$PWD/android/res/values/libs.xml \
#    $$PWD/android/build.gradle \
#    $$PWD/android/gradle/wrapper/gradle-wrapper.properties \
#    $$PWD/android/gradlew.bat

DISTFILES += \
    $$PWD/src/werobotics/cargo_gs/Android_permission.java \
#    $$PWD/src/werobotics/cargo_gs/QtFtDev.java
