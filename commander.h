#ifndef COMMANDER_H
#define COMMANDER_H

#include <QObject>
#include <QSharedPointer>
#include <QTimer>
#include <QtQml/QQmlApplicationEngine>
#include <QFile>

#include "communication/mavlink_handler.h"
#include "systems/system_manager.h"
#include "timer/timeout_handler.h"
#include "timer/call_every_handler.h"
#include "statemachine/statemachine.h"
#include "weatherreport.h"
#include "lockhelper.h"
#include "gamepadmanager.h"
#include "interface/controlinterface.h"
#include "interface/missionmanager.h"

/*
 * The Commander class is a singleton class.
 * It is at the core of the cargo_groundstation application.
 *
 * It is the link between the other main parts:
 * - the MAVLink communication and systems handler,
 * - the control interface (GUI, gamepad),
 * - the state machine.
 *
 * It also manages logging:
 * @log uncategorized: any unexpected or debug log.
 * @log general: information about the state of the application or the mission, the connection, etc.
 * @log telemetry: drone telemetry data (lat, lon, alt, vNorth, vEast, vUp).
 * @log sensor: drone sensor data (time from mission start, distance remaining, raspi temperature).
 * Warning: temperature should be changed to cargo temp + humidity.
 * @log messagesSentOrReceived: all messages sent or received including connection type, sender id, message id and message type.
 * @log missionInfo: take off and landing data.
 */
class Commander : public QObject
{
    Q_OBJECT

public:
   static Commander* getInstance()
   {
     if (!_instance)
     {
         _instance = new Commander();
     }
     return _instance;
   }

   // delete copy and move constructors and assign operators
   Commander(Commander const &) = delete; // Copy construct
   Commander(Commander &&) = delete; // Move construct
   Commander &operator=(Commander const &) = delete; // Copy assign
   Commander &operator=(Commander &&) = delete; // Move assign

private:
   static Commander* _instance;
   Commander();
   ~Commander();

public:
    TimeoutHandler _timeoutHandler;
    CallEveryHandler _callEveryHandler;

    void init(QObject *parent);

    uint8_t getDroneId() const;
    void setDroneId(uint8_t id);
    uint8_t getLocalStationId() const;
    void setLocalStationId(uint8_t id);
    bool openMissionInfoLogFile() const;
    void closeMissionInfoLogFile() const;
    void setSendingChannel(ConnectionType channel);
    ConnectionResult addDirectConnection();
    ConnectionResult addWebserverConnection();
    void removeDirectConnection();

signals:
    void telemetryUpdated(QString lat, QString lon, QString alt, QString vx, QString vy, QString vz);
    void attitudeUpdated(QString roll, QString pitch, QString yaw);
    void droneBatteryUpdated(QString level);
    void droneTimeDistanceTemperatureHumidityUpdated(uint64_t time, float distance, float temperature, float humidity);
    void droneStateChanged(uint32_t newState, uint8_t missionId);
    void radioStatusUpdated(uint64_t address, uint8_t status);
    void commandAcknowledged(uint16_t cmdType, uint8_t ackResult, uint8_t senderId);
    void messageToDisplay(QString title, QString message, QString details);
    void infoToDisplay(QString message);
    void debugDataToDisplay(QString xDebug, QString yDebug, QString zDebug);
    void forwardRssi(uint8_t local, uint8_t remote);
    void forwardDroneErrorMessage(uint8_t severity, QString errorText);
    void resetState();
    void receptionRequestReceived(int stationId);
    void receptionAckReceived();
    void webserverConnected(bool isConnected);
    void groundstationConnected(bool isConnected, uint8_t systemId);
    void droneConnected(bool isConnected);
    void connectionTypeUpdated(bool type);

public slots:
    void checkConnection();
    void checkTimeout();
    bool forwardMessage(const mavlink_message_t &message);
    void sendRadioDetectionMessage();

private:
    void start();

    bool checkAndroidWritePermission() const;
    void setupLogs();
    void setupConnections();

    QSharedPointer<ControlInterface> _controlInterface;
    QSharedPointer<QQmlApplicationEngine> _engine;
    QSharedPointer<SystemManager> _comManager;
    QSharedPointer<StateMachine> _stateMachine;
    QSharedPointer<WeatherReport> _weatherObject;
    QScopedPointer<KeepAwakeHelper> _keepScreenOnHelper;
    QSharedPointer<GamepadManager> _gamepadManager;
    QSharedPointer<MissionManager> _missionManager;

    QString _currentLogFilesPath;
    QString _dailyMissionsFilePath;
    Time _time{};
    QTimer* _connectionTimer;
    QTimer* _systemTimer;

    uint8_t _droneId;
    uint8_t _localStationId;
};

#endif // COMMANDER_H
