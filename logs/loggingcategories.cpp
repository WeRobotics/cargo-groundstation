#include "loggingcategories.h"

// .txt
Q_LOGGING_CATEGORY(generalLog, "general")

// .csv
Q_LOGGING_CATEGORY(telemetryLog, "telemetry")
Q_LOGGING_CATEGORY(sensorLog, "sensor")
Q_LOGGING_CATEGORY(messagesSentOrReceivedLog, "messages")
Q_LOGGING_CATEGORY(missionInfoLog, "mission")
