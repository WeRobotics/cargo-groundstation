#ifndef LOGGINGCATEGORIES_H
#define LOGGINGCATEGORIES_H

#include <QLoggingCategory>

#include "mavlink_include.h"

/*
 * Logging categories: see Commander class.
 */

Q_DECLARE_LOGGING_CATEGORY(generalLog)
Q_DECLARE_LOGGING_CATEGORY(telemetryLog)
Q_DECLARE_LOGGING_CATEGORY(sensorLog)
Q_DECLARE_LOGGING_CATEGORY(messagesSentOrReceivedLog)
Q_DECLARE_LOGGING_CATEGORY(missionInfoLog)

inline const char *mavMessageUsedStr(const uint32_t message)
{
    switch (message)
    {
        case MAVLINK_MSG_ID_HEARTBEAT:
            return "MAVLINK_MSG_ID_HEARTBEAT";
        case MAVLINK_MSG_ID_SYS_STATUS:
            return "MAVLINK_MSG_ID_SYS_STATUS";
        case MAVLINK_MSG_ID_PING:
            return "MAVLINK_MSG_ID_PING";
        case MAVLINK_MSG_ID_ATTITUDE:
            return "MAVLINK_MSG_ID_ATTITUDE";
        case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
            return "MAVLINK_MSG_ID_GLOBAL_POSITION_INT";
        case MAVLINK_MSG_ID_COMMAND_LONG:
            return "MAVLINK_MSG_ID_COMMAND_LONG";
        case MAVLINK_MSG_ID_COMMAND_ACK:
            return "MAVLINK_MSG_ID_COMMAND_ACK";
        case MAVLINK_MSG_ID_RADIO_STATUS:
            return "MAVLINK_MSG_ID_RADIO_STATUS";
        case MAVLINK_MSG_ID_DEBUG_VECT:
            return "MAVLINK_MSG_ID_DEBUG_VECT";
        case MAVLINK_MSG_ID_STATUSTEXT:
            return "MAVLINK_MSG_ID_STATUSTEXT";
        case MAVLINK_MSG_ID_MANUAL_CONTROL:
            return "MAVLINK_MSG_ID_MANUAL_CONTROL";
        default:
            return "UNKNOWN";
    }
}

#endif // LOGGINGCATEGORIES_H
