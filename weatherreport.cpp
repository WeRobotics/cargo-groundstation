#include <QtGlobal>
#include <QUrl>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "weatherreport.h"
#include "logs/loggingcategories.h"

#define ZERO_KELVIN 273.15

WeatherReport::WeatherReport() : QObject(),
_networkAccessManager(QSharedPointer<QNetworkAccessManager>(new QNetworkAccessManager(this))),
_appId(QStringLiteral("744ec81b120aaadb0f0b76940c1b06f2"))
{}

WeatherReport::~WeatherReport()
{
    qInfo() << "~WeatherReport()";
    _networkAccessManager.clear();
}

void WeatherReport::checkWeatherConditions()
{
    reinitialiseReport();

    // Get tablet position
    // AAA TODO use start of route instead
    QSharedPointer<QGeoPositionInfoSource> source = QSharedPointer<QGeoPositionInfoSource>((QGeoPositionInfoSource::createDefaultSource(this)));
    if(source)
    {
        double lat = source->lastKnownPosition().coordinate().latitude();
        double lon = source->lastKnownPosition().coordinate().longitude();

        if(!source->lastKnownPosition().coordinate().isValid())
        {
            _flightAllowed = false;

            _weatherReportTitle = tr("Weather query failed.");
            _weatherReportBody = tr("Can't access location data.\nTurn localization on and check\nlocation on Google Maps.");

            weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
            qWarning(generalLog()) << "Weather query failed: can't access location data.";
            return;
        }

        _lat = QString::number(lat);
        _lon = QString::number(lon);
        qInfo(generalLog()) << "Checking weather forecast at coordinates: " << QGeoCoordinate(lat, lon) << ".";
    }
    else
    {
        _flightAllowed = false;

        _weatherReportTitle = tr("Weather query failed.");
        _weatherReportBody = tr("Can't access location data.\nTurn localization on and check\nlocation on Google Maps.");

        weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
        qWarning(generalLog()) << "Weather query failed: can't access location data.";
        return;
    }

    // Query weather report
    QUrl urlWeather("http://api.openweathermap.org/data/2.5/weather");
    QUrlQuery queryWeather;
    queryWeather.addQueryItem("lat", _lat);
    queryWeather.addQueryItem("lon", _lon);
    queryWeather.addQueryItem("mode", "json");
    queryWeather.addQueryItem("APPID", _appId);
    urlWeather.setQuery(queryWeather);

    QSharedPointer<QNetworkReply> repWeather = QSharedPointer<QNetworkReply>(_networkAccessManager->get(QNetworkRequest(urlWeather)));
    connect(repWeather.get(), &QNetworkReply::finished, this, [this, repWeather]() { handleWeatherData(repWeather); });

    // Query solar flares report
    QUrl urlFlares("https://api.nasa.gov/DONKI/FLR");
    QUrlQuery queryFlares;
    queryFlares.addQueryItem("startDate", QDate::currentDate().toString("yyyy-MM-dd"));
    queryFlares.addQueryItem("endDate", QDate::currentDate().toString("yyyy-MM-dd")); // Specify hours
    queryFlares.addQueryItem("api_key", "kC1eYv8r3PRmDn492p6NXbnIt8H4iz5vew889901");
    urlFlares.setQuery(queryFlares);

    QSharedPointer<QNetworkReply> repFlares = QSharedPointer<QNetworkReply>(_networkAccessManager->get(QNetworkRequest(urlFlares)));
    connect(repFlares.get(), &QNetworkReply::finished, this, [this, repFlares]() { handleSolarFlaresData(repFlares); });
}

void WeatherReport::handleWeatherData(QSharedPointer<QNetworkReply> networkReply)
{
    if (!networkReply)
    {
        _flightAllowed = false;

        _weatherReportTitle = tr("Weather query failed.");
        _weatherReportBody = tr("Can't access weather data.\nCheck internet connection.");

        weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
        qWarning(generalLog()) << "Weather query failed: can't access weather data.";
        return;
    }

    // Extract data from weather report (json file)
    if (!networkReply->error())
    {
        QJsonDocument weatherDataJson = QJsonDocument::fromJson(networkReply->readAll());
//        qInfo() << document;

        if (weatherDataJson.isObject())
        {
            QJsonObject jsonObject = weatherDataJson.object();
            QJsonObject tempJsonObject;
            QJsonValue jsonValue;

            if (jsonObject.contains(QStringLiteral("weather")))
            {
                jsonValue = jsonObject.value(QStringLiteral("weather"));
                QJsonArray weatherArray = jsonValue.toArray();
                jsonValue = weatherArray.at(0);
                tempJsonObject = jsonValue.toObject();
                _generalConditionsReportId = tempJsonObject.value(QStringLiteral("id")).toInt();
                _generalConditionsReport = tempJsonObject.value(QStringLiteral("description")).toString();
                _generalConditionsReport.replace(0, 1, _generalConditionsReport[0].toUpper());
                if((_generalConditionsReportId < 800) && (_generalConditionsReportId != 701) && (_generalConditionsReportId != 721) && (_generalConditionsReportId != 741)) // https://openweathermap.org/weather-conditions
                    _flightAllowed = false;
            }
            if (jsonObject.contains(QStringLiteral("main")))
            {
                jsonValue = jsonObject.value(QStringLiteral("main"));
                tempJsonObject = jsonValue.toObject();
                jsonValue = tempJsonObject.value(QStringLiteral("temp"));
                _temperatureReport = jsonValue.toDouble();
                if(((_temperatureReport-ZERO_KELVIN) < -10) || ((_temperatureReport-ZERO_KELVIN) > 40))
                    _flightAllowed = false;
            }
            if (jsonObject.contains(QStringLiteral("wind")))
            {
                jsonValue = jsonObject.value(QStringLiteral("wind"));
                tempJsonObject = jsonValue.toObject();
                _windSpeedReport = tempJsonObject.value(QStringLiteral("speed")).toDouble();
                if(_windSpeedReport >= 10)
                    _flightAllowed = false;
            }
            if (jsonObject.contains(QStringLiteral("sys")))
            {
                _queryTime = jsonObject.value(QStringLiteral("dt")).toInt();
                jsonValue = jsonObject.value(QStringLiteral("sys"));
                tempJsonObject = jsonValue.toObject();
                _sunriseTime = tempJsonObject.value(QStringLiteral("sunrise")).toInt();
                _sunsetTime = tempJsonObject.value(QStringLiteral("sunset")).toInt();
                if((_queryTime > _sunsetTime) || (_queryTime < _sunriseTime))
                    _flightAllowed = false;
            }
        }
        else
        {
            _flightAllowed = false;

            _weatherReportTitle = tr("Weather query failed.");
            _weatherReportBody = tr("Can't access weather data.\nCheck internet connection.");

            weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
            qWarning(generalLog()) << "Weather query failed: can't access weather data.";
            return;
        }
    }
    else
    {
        _flightAllowed = false;

        _weatherReportTitle = tr("Weather query failed.");
        _weatherReportBody = tr("Can't access weather data.\nCheck internet connection.");

        weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
        qWarning(generalLog()) << "Weather query failed: can't access weather data: " << networkReply->errorString();
        return;
    }

    // When all data (weather and solar flares) are extracted, write report and display results in first popup
    if(_flareReportReady)
    {
        writeReport();

        if(_flightAllowed)
            weatherInfoReady(true, _weatherReportTitle, _weatherReportBody);
        else
            weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
    }
    else
    {
        _weatherReportReady = true;
    }

    return;
}

void WeatherReport::handleSolarFlaresData(QSharedPointer<QNetworkReply> networkReply)
{
    if (!networkReply)
    {
        _flightAllowed = false;

        _weatherReportTitle = tr("Weather query failed.");
        _weatherReportBody = tr("Can't access weather data.\nCheck internet connection.");

        weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
        qWarning(generalLog()) << "Weather query failed: can't access solar flare data.";
        return;
    }

    // Extract data from solar flare report (json file)
    if (!networkReply->error())
    {
        QJsonDocument solarFlareDataJson = QJsonDocument::fromJson(networkReply->readAll());
//        qInfo() << document;

        if (solarFlareDataJson.isArray())
        {
            _flareReport = tr("Risk detected");
            _flightAllowed = false;
        }
        else
            _flareReport = tr("None");
    }
    else
    {
        _flightAllowed = false;

        _weatherReportTitle = tr("Weather query failed.");
        _weatherReportBody = tr("Can't access weather data.\nCheck internet connection.");

        weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
        qWarning(generalLog()) << "Weather query failed: can't access solar flare data: " << networkReply->errorString();
        return;
    }

    // When all data (weather and solar flares) are extracted, write report and display results in first popup
    if(_weatherReportReady)
    {
        writeReport();

        if(_flightAllowed)
            weatherInfoReady(true, _weatherReportTitle, _weatherReportBody);
        else
            weatherInfoReady(false, _weatherReportTitle, _weatherReportBody);
    }
    else
    {
        _flareReportReady = true;
    }

    return;
}

void WeatherReport::reinitialiseReport()
{
    _flightAllowed = true;
    _weatherReportTitle = nullptr;
    _weatherReportBody = nullptr;
    _generalConditionsReport = nullptr;
    _flareReport = nullptr;
    _temperatureReport = 0;
    _windSpeedReport = 0;
    _sunriseTime = 0;
    _sunsetTime = 0;
    _queryTime = 0;
    _flareReportReady = false;
    _weatherReportReady = false;
}

void WeatherReport::writeReport()
{
    _weatherReportBody = "";
    if(_flightAllowed)
    {
        _weatherReportTitle = tr("Weather conditions and timing permit flight.");
    }
    else
    {
        _weatherReportTitle = tr("Weather conditions and/or timing prohibit flight.");
    }

    _weatherReportBody.append(_generalConditionsReport + "\n");
    _weatherReportBody.append(tr("Temp: ") + QString::number(qRound(_temperatureReport-ZERO_KELVIN)) + QChar(0260) + "C\n");
    _weatherReportBody.append(tr("Wind speed: ") + QString::number(_windSpeedReport) + "m/s\n");
    _weatherReportBody.append(tr("Solar flares: ") + _flareReport + "\n");
    if(_queryTime > _sunsetTime)
        _weatherReportBody.append(tr("Time since sunset: ") + QTime(0,0,0,0).addSecs(_queryTime - _sunsetTime).toString("h'h'mm"));
    else if(_queryTime < _sunriseTime)
        _weatherReportBody.append(tr("Time until sunrise: ") + QTime(0,0,0,0).addSecs(_sunriseTime - _queryTime).toString("h'h'mm"));
    else
        _weatherReportBody.append(tr("Time until sunset: ") + QTime(0,0,0,0).addSecs(_sunsetTime - _queryTime).toString("h'h'mm"));

    qInfo(generalLog()).nospace().noquote() << "\n//////////////////////////////////////////////////\n   " << "Weather report" << ":\n" << _weatherReportBody << "   \n//////////////////////////////////////////////////";

    return;
}
