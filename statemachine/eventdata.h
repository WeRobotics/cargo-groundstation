#ifndef EVENTDATA_H
#define EVENTDATA_H

/*
 * The EventData class is a class used to forward data during a state machine state change.
 *
 * The event data must inherit from the EventData base class in order to give the state machine engine
 * a common base class for which to delete all event data.
 * See http://web.archive.org/web/20180512232134/http://www.drdobbs.com/cpp/state-machine-design-in-c/184401236?pgno=2
 */
class EventData
{
public:
    virtual ~EventData() {};
};

#endif // EVENTDATA_H
