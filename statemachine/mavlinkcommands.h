#ifndef MAVLINKCOMMANDS_H
#define MAVLINKCOMMANDS_H

#include "../communication/mavlink_handler.h"
#include <QObject>

inline const char *mavCmdUsedStr(const MAV_CMD command)
{
    switch (command)
    {
        case MAV_CMD::MAV_CMD_USER_1:
            return QObject::tr("Send mission").toUtf8();
        case MAV_CMD::MAV_CMD_USER_2:
            return QObject::tr("Reception request").toUtf8();
        case MAV_CMD::MAV_CMD_MISSION_START:
            return QObject::tr("Start mission").toUtf8();
        case MAV_CMD::MAV_CMD_DO_PAUSE_CONTINUE:
            return QObject::tr("Pause or Resume").toUtf8();
        case MAV_CMD::MAV_CMD_NAV_LAND:
            return QObject::tr("Land (precision or emergency)").toUtf8();
        case MAV_CMD::MAV_CMD_DO_PARACHUTE:
            return QObject::tr("Retake Control").toUtf8();
        case MAV_CMD::MAV_CMD_NAV_RALLY_POINT:
            return QObject::tr("Go to rally point").toUtf8();
        default:
            return QObject::tr("Unknown").toUtf8();
    }
}

inline const char *senderSystemStr(const uint8_t sender)
{
    switch (sender)
    {
        case 50:
            return QObject::tr("the drone").toUtf8();
        case 101:
            return QObject::tr("the other ground station").toUtf8();
        case 102:
            return QObject::tr("the other ground station").toUtf8();
        case 103:
            return QObject::tr("the other ground station").toUtf8();
        case 104:
            return QObject::tr("the other ground station").toUtf8();
        default:
            return QObject::tr("UNKNOWN TARGET (ID: %1)").arg(sender).toUtf8();
//            std::stringstream s;
//            s << QObject::tr("UNKNOWN TARGET (ID: )").toUtf8(); // " << sender << "
//            return s.str().c_str();
    }
}

#endif // MAVLINKCOMMANDS_H
