#include <QDebug>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "statemachine.h"
#include "commander.h"
#include "logs/loggingcategories.h"
#include "mavlinkcommands.h"

StateMachine::StateMachine(QSharedPointer<SystemManager> comManager, QSharedPointer<WeatherReport> weatherReport, QSharedPointer<MissionManager> missionManager) : QObject(),
_eventGenerated(false),
_eventData(nullptr),
_maxStates(ST_MAX_STATES),
_currentState(0),
_previousState(0),
_previousDroneState(OnboardState::BOOTING),
_commandCounter(0),
_currentCmdType(0),
_currentParam1(0),
_currentParam2(0),
_currentTargetSystem(0),
_isSender(false),
_isDroneConnected(false),
_isDroneReady(false),
_missionId(0),
_missionVersion(0),
_currentMissionName("Unknown"),
_currentMissionLength(0),
_currentMissionStatus(MissionStatus::NO_MISSION),
_requestState(RequestState::SEND_REQUEST),
_currentCargoWeight(0),
_currentCargoVolume(0),
_comManager(comManager),
_weatherReport(weatherReport),
_missionManager(missionManager),
_receiverId(0),
_preventiveSafetyOverwrite(false)
{
    qInfo(generalLog()).nospace() << "Initialising state machine with state: " << statesStr(static_cast<States>(_currentState)) << ".";
}

StateMachine::~StateMachine()
{
    qInfo() << "~StateMachine()";
    Commander::getInstance()->_timeoutHandler.remove(_basicCmdCookie);

    if(_eventData)
    {
        delete _eventData;
        _eventData = nullptr;
    }


    if(_basicCmdCookie)
    {
        _basicCmdCookie = nullptr;
    }

    _weatherReport.clear();
    _comManager.clear();
}

// Called once per external event to try to transition to a new state
void StateMachine::externalEvent(unsigned char newState, EventData* eventData)
{
    if (newState == EVENT_IGNORED)
    {
        // If event ignored (no or invalid state change), just delete event data
        if (eventData)
        {
            delete eventData;
        }
    }
    else
    {
        // Else generate the internal event and execute the state engine
        internalEvent(newState, eventData);
        stateEngine();
    }
}

// Called from within a state function or by valid external event to transition to a new state
void StateMachine::internalEvent(unsigned char newState, EventData* eventData)
{
    _eventData = eventData;
    _eventGenerated = true;
    if(_currentState != newState)
    {
        _previousState = _currentState;
        if(_currentState == ST_SEND_COMMAND)
        {
            // If exiting send_command state, stop ACK timer
            _commandCounter = 0;
            Commander::getInstance()->_timeoutHandler.remove(_basicCmdCookie);
        }
    }
    _currentState = newState;

    stateMachineStateChanged(static_cast<int>(newState));
    qInfo(generalLog()).nospace() << "Current GS state: " << statesStr(static_cast<States>(_currentState)) << ".";
}


// Execute the state machine state(s)
void StateMachine::stateEngine()
{
    EventData* eventDataTemp = nullptr;

    QMutexLocker locker(&_stateMutex); // mutex released when locker out of scope

    // While events are being generated keep executing states
    while (_eventGenerated)
    {
        eventDataTemp = _eventData;
        _eventData = nullptr;
        _eventGenerated = false;

        assert(_currentState < _maxStates);

        // Execute the state (passing in event data, if any)
        const StateStruct* stateMap = getStateMap();
        (this->*stateMap[_currentState].stateFunc)(eventDataTemp);

        if (eventDataTemp)
        {
            delete eventDataTemp;
            eventDataTemp = nullptr;
        }
    }
}


// External events accepted by the state machine
// Transition tables

void StateMachine::resetState()
{
    static const unsigned char TRANSITIONS[] =
    {
        ST_IDLE,      // From ST_IDLE
        ST_IDLE,      // From ST_FIRST_CHECKS
        ST_IDLE,      // From ST_SECOND_CHECKS
        ST_IDLE,      // From ST_THIRD_CHECKS
        ST_IDLE,      // From ST_READY
        ST_IDLE,      // From ST_RUNNING (Landed)
        ST_IDLE,      // From ST_SEND_COMMAND (Cancel send mission)
        0
    };
    externalEvent(TRANSITIONS[_currentState], nullptr);
}

void StateMachine::confirmRoute(int missionId, int missionVersion)
{
    missionData* mission = new missionData;
    mission->missionId = missionId;
    mission->missionVersion = missionVersion;

    static const unsigned char TRANSITIONS[] =
    {
        ST_FIRST_CHECKS,    // From ST_IDLE
        EVENT_IGNORED,      // From ST_FIRST_CHECKS
        EVENT_IGNORED,      // From ST_SECOND_CHECKS
        EVENT_IGNORED,      // From ST_THIRD_CHECKS
        EVENT_IGNORED,      // From ST_READY
        EVENT_IGNORED,      // From ST_RUNNING
        EVENT_IGNORED,      // From ST_SEND_COMMAND
        0
    };

    if(TRANSITIONS[_currentState] == EVENT_IGNORED)
    {
        qWarning(generalLog()).nospace() << "Can't confirm route from " << statesStr(static_cast<States>(_currentState));
    }

    externalEvent(TRANSITIONS[_currentState], mission);
}

void StateMachine::sendReceptionRequestOrReady(int requestState)
{
    requestStateData* request = new requestStateData;
    request->requestState = static_cast<RequestState>(requestState);

    unsigned char fromIdle;
    if(requestState)
    {
        fromIdle = ST_SECOND_CHECKS;
    }
    else
    {
        fromIdle = EVENT_IGNORED;
    }

    /*static*/ const unsigned char TRANSITIONS[] =
    {
        fromIdle,           // From ST_IDLE
        ST_SECOND_CHECKS,   // From ST_FIRST_CHECKS
        ST_SECOND_CHECKS,   // From ST_SECOND_CHECKS
        EVENT_IGNORED,      // From ST_THIRD_CHECKS
        EVENT_IGNORED,      // From ST_READY
        EVENT_IGNORED,      // From ST_RUNNING
        EVENT_IGNORED,      // From ST_SEND_COMMAND
        0
    };

    if(TRANSITIONS[_currentState] == EVENT_IGNORED)
    {
        qWarning(generalLog()).nospace() << "Can't send reception request or ready from " << statesStr(static_cast<States>(_currentState));
    }

    externalEvent(TRANSITIONS[_currentState], request);
}

void StateMachine::checkDrone(QString missionName, double missionLength, int cargoWeight, int cargoVolume, QString sendingPilotName, QString receivingPilotName)
{
    _currentMissionName = missionName;
    _currentMissionLength = missionLength;
    _comManager->setSendingPilotName(sendingPilotName);
    _comManager->setReceivingPilotName(receivingPilotName);
    _currentCargoWeight = cargoWeight;
    _currentCargoVolume = cargoVolume;
    _preventiveSafetyOverwrite = false;

    static const unsigned char TRANSITIONS[] =
    {
        EVENT_IGNORED,      // From ST_IDLE
        ST_THIRD_CHECKS,    // From ST_FIRST_CHECKS
        ST_THIRD_CHECKS,    // From ST_SECOND_CHECKS
        EVENT_IGNORED,      // From ST_THIRD_CHECKS
        EVENT_IGNORED,      // From ST_READY
        EVENT_IGNORED,      // From ST_RUNNING
        EVENT_IGNORED,      // From ST_SEND_COMMAND
        0
    };

    if(TRANSITIONS[_currentState] == EVENT_IGNORED)
    {
        qWarning(generalLog()).nospace() << "Can't check drone from " << statesStr(static_cast<States>(_currentState));
    }

    externalEvent(TRANSITIONS[_currentState], nullptr);
}

bool StateMachine::sendCommand(int cmdType, int param1, int param2)
{
    if((!_commandCounter) || // No command in process (= not waiting for ack or resend)
       (!cmdType))           // Or command being resent
    {
        if(cmdType)
        {
            _currentCmdType = static_cast<uint16_t>(cmdType);
        }
        if(param1 > -1)
        {
            _currentParam1 = *reinterpret_cast<float*>(&param1);
        }
        if(param2 > -1)
        {
            _currentParam2 = *reinterpret_cast<float*>(&param2);
        }

        if((static_cast<uint16_t>(cmdType) == MAV_CMD::MAV_CMD_USER_1) && (param1 == 0.0))
        {
            _previousState = 0;
        }

        static const unsigned char TRANSITIONS[] =
        {
            EVENT_IGNORED,    // From ST_IDLE
            EVENT_IGNORED,    // From ST_FIRST_CHECKS
            ST_SEND_COMMAND,  // From ST_SECOND_CHECKS
            ST_SEND_COMMAND,  // From ST_THIRD_CHECKS
            ST_SEND_COMMAND,  // From ST_READY
            ST_SEND_COMMAND,  // From ST_RUNNING
            ST_SEND_COMMAND,  // From ST_SEND_COMMAND
            0
        };

        if(TRANSITIONS[_currentState] == EVENT_IGNORED)
        {
            qWarning(generalLog()).nospace() << "Can't send \"" + QString::fromUtf8(mavCmdUsedStr(static_cast<MAV_CMD>(_currentCmdType))) + "\" command from " << statesStr(static_cast<States>(_currentState));
            qWarning(generalLog()).nospace() << "Current/new cmd type: " << _currentCmdType << " / " << cmdType;
            qWarning(generalLog()).nospace() << "Current/new param1: " << _currentParam1 << " / " << param1;
            qWarning(generalLog()).nospace() << "Current/new param2: " << _currentParam2 << " / " << param2;
        }
        else
        {
            qInfo(generalLog()).nospace() << "Sending \"" + QString::fromUtf8(mavCmdUsedStr(static_cast<MAV_CMD>(_currentCmdType))) + "\" command from " << statesStr(static_cast<States>(_currentState));
            qInfo(generalLog()).nospace() << "Current/new cmd type: " << _currentCmdType << " / " << cmdType;
            qInfo(generalLog()).nospace() << "Current/new param1: " << _currentParam1 << " / " << param1;
            qInfo(generalLog()).nospace() << "Current/new param2: " << _currentParam2 << " / " << param2;
        }

        externalEvent(TRANSITIONS[_currentState], nullptr);

        return true;
    }
    else
    {
        qWarning(generalLog()).nospace() << "Request to send command " << QString::number(cmdType) << " while still trying to send command " << QString::number(_currentCmdType) << ".";
        messageToDisplay(tr("Command error"), tr("Request to send command %1 while still trying to send command %2. Please wait.").arg(cmdType).arg(_currentCmdType), "");
        return false;
    }
}

void StateMachine::returnToPreviousState()
{
    _commandCounter = 0;
    Commander::getInstance()->_timeoutHandler.remove(_basicCmdCookie);

    const unsigned char TRANSITIONS[] = // NOT STATIC, NEEDS REASSIGNING
    {
        EVENT_IGNORED,      // From ST_IDLE
        EVENT_IGNORED,      // From ST_FIRST_CHECKS
        EVENT_IGNORED,      // From ST_SECOND_CHECKS
        EVENT_IGNORED,      // From ST_THIRD_CHECKS
        EVENT_IGNORED,      // From ST_READY
        EVENT_IGNORED,      // From ST_RUNNING
        _previousState,     // From ST_SEND_COMMAND
        0
    };

    if(TRANSITIONS[_currentState] == EVENT_IGNORED)
    {
        qWarning(generalLog()).nospace() << "Can't return to previous state from " << statesStr(static_cast<States>(_currentState));
    }
    else
    {
        qInfo(generalLog()) << "Returning to " << statesStr(static_cast<States>(_previousState));
    }

    if((static_cast<States>(_previousState) == ST_THIRD_CHECKS) || (static_cast<States>(_previousState) == ST_RUNNING))
    {
        missionData* mission = new missionData;
        mission->missionId = _missionId;
        mission->missionVersion = _missionVersion;
        externalEvent(TRANSITIONS[_currentState], mission);
    }
    else if((static_cast<States>(_previousState) == ST_SECOND_CHECKS))
    {
        requestStateData* request = new requestStateData;
        request->requestState = _requestState;
        externalEvent(TRANSITIONS[_currentState], request);
    }
    else
    {
        externalEvent(TRANSITIONS[_currentState], nullptr);
    }
}

void StateMachine::changeDroneConnectionStatus(bool isConnected)
{
    _isDroneConnected = isConnected;

    if(isConnected)
    {
        droneConnected(true);
    }
    else
    {
        droneConnected(false);
        _isDroneReady = false;
    }

    static const unsigned char TRANSITIONS[] =
    {
        EVENT_IGNORED,      // From ST_IDLE
        EVENT_IGNORED,      // From ST_FIRST_CHECKS
        EVENT_IGNORED,      // From ST_SECOND_CHECKS
        ST_THIRD_CHECKS,    // From ST_THIRD_CHECKS
        ST_READY,           // From ST_READY
        EVENT_IGNORED,      // From ST_RUNNING
        EVENT_IGNORED,      // From ST_SEND_COMMAND
        0
    };

    if(TRANSITIONS[_currentState] == EVENT_IGNORED)
    {
        qInfo(generalLog()).nospace() << "Drone connection status changed without GS state change (staying in " << statesStr(static_cast<States>(_currentState)) << ").";
    }

    externalEvent(TRANSITIONS[_currentState], nullptr);
}

void StateMachine::runPreventiveCheck(bool overwrite) {
    _preventiveSafetyOverwrite = overwrite;

    static const unsigned char TRANSITIONS[] =
    {
        EVENT_IGNORED,      // From ST_IDLE
        EVENT_IGNORED,      // From ST_FIRST_CHECKS
        EVENT_IGNORED,      // From ST_SECOND_CHECKS
        ST_THIRD_CHECKS,    // From ST_THIRD_CHECKS
        EVENT_IGNORED,      // From ST_READY
        EVENT_IGNORED,      // From ST_RUNNING
        EVENT_IGNORED,      // From ST_SEND_COMMAND
        0
    };

    if(TRANSITIONS[_currentState] == EVENT_IGNORED)
    {
        qInfo(generalLog()).nospace() << "Preventive check required but not in corresponding state. (staying in " << statesStr(static_cast<States>(_currentState)) << ").";
    }

    externalEvent(TRANSITIONS[_currentState], nullptr);
}

void StateMachine::changeDroneReadyStatus(bool isReady)
{
    _isDroneReady = isReady;

    static const unsigned char TRANSITIONS[] =
    {
        ST_READY,           // From ST_IDLE
        EVENT_IGNORED,      // From ST_FIRST_CHECKS
        EVENT_IGNORED,      // From ST_SECOND_CHECKS
        ST_READY,           // From ST_THIRD_CHECKS
        ST_READY,           // From ST_READY
        EVENT_IGNORED,      // From ST_RUNNING
        ST_READY,           // From ST_SEND_COMMAND
        0
    };

    if(TRANSITIONS[_currentState] == EVENT_IGNORED)
    {
        qWarning(generalLog()).nospace() << "Drone ready status changed without GS state change (staying in " << statesStr(static_cast<States>(_currentState)) << ").";
    }

    externalEvent(TRANSITIONS[_currentState], nullptr);
}


// External events without direct state change
/*
void StateMachine::setConnectionParameters(QString IpAddr, QString port)
{
    _comManager->setGroundstationIp(IpAddr);
    _comManager->setGroundstationTcpPort(port);
    Commander::getInstance()->start();
}
*/
void StateMachine::handleTimeoutExpiration() const
{
    commandTimeoutExpired(static_cast<int>(_currentCmdType), _currentTargetSystem);
}

void StateMachine::commandAcknowledged(uint16_t cmdType, uint8_t ackResult, uint8_t senderId)
{
    if(_currentState != ST_SEND_COMMAND)
    {
        qWarning(generalLog()) << "Received unexpected ack " << ackResult << " for command " << cmdType << " from " << senderId << ".";
        return;
    }

    // If ack, display info
    // If in progress, wait 5 more seconds
    // If nack, popup to inform
    if(ackResult == MAV_RESULT::MAV_RESULT_ACCEPTED)
    {
        qInfo(generalLog()).nospace() << "MAV_RESULT_ACCEPTED " << QString::fromUtf8(mavCmdUsedStr(static_cast<MAV_CMD>(cmdType)));
        if(cmdType == MAV_CMD::MAV_CMD_USER_1)
        {
            _currentMissionStatus = MissionStatus::MISSION_ACCEPTED;
        }
        returnToPreviousState();
    }
    if(ackResult == MAV_RESULT::MAV_RESULT_IN_PROGRESS)
    {
        qInfo(generalLog()).nospace() << "MAV_RESULT_IN_PROGRESS " << QString::fromUtf8(mavCmdUsedStr(static_cast<MAV_CMD>(cmdType)));
        Commander::getInstance()->_timeoutHandler.refresh(10.0, _basicCmdCookie);
    }
    else if(ackResult)
    {
        qInfo(generalLog()).nospace() << "MAV_RESULT ERROR " << QString::fromUtf8(mavCmdUsedStr(static_cast<MAV_CMD>(cmdType)));

        if(cmdType == MAV_CMD::MAV_CMD_USER_1)
        {
            missionRefused(tr("Mission not accepted"), tr("Mission %1 (v.%2) was not accepted by the drone. Would you like to resend it?").arg(_missionId).arg(_missionVersion), tr("Look at drone logs for more info."));
            Commander::getInstance()->_timeoutHandler.remove(_basicCmdCookie);
            _currentMissionStatus = MissionStatus::MISSION_REFUSED;
            // resetState();
        }
        else
        {
            messageToDisplay(tr("Command not accepted"), tr("The \"%1\" command was not accepted by %2.").arg(QString::fromUtf8(mavCmdUsedStr(static_cast<MAV_CMD>(cmdType)))).arg(QString::fromUtf8(senderSystemStr(senderId))), "");
            returnToPreviousState();
        }
    }
}

void StateMachine::forwardDroneErrorMessage(uint8_t severity, QString errorText) const
{
    if(severity < 4)
    {
        infoToDisplay(tr("Drone error: ") + errorText);
    }
    else
    {
        infoToDisplay(tr("Drone warning: ") + errorText);
    }
}


// External events that generate internal state changes

void StateMachine::droneStateChange(uint32_t newIntState, uint8_t missionId)
{
    // Update drone state
    OnboardState newState = static_cast<OnboardState>(newIntState);

    // Update current mission
    if(missionId && (missionId != _missionId) && (newState != OnboardState::MISSION_RECEIVED))
    {
        _missionId = missionId;
        _missionVersion = 255;
        missionChanged(_missionId);
        _missionManager->setActiveMission(_missionId, _missionVersion);
        infoToDisplay(tr("Current mission has ID %1").arg(_missionId));
        qInfo(generalLog()).nospace() << "Current mission has ID " << _missionId << " (from drone).";
    }

    missionData* mission = new missionData;
    mission->missionId = missionId;

    droneStateChanged(static_cast<int>(newIntState), onboardStateStr(static_cast<OnboardState>(newIntState)));

    if(_previousDroneState == OnboardState::RC_CONTROL) {
        clearDisplayedWarningMessage();
    }

    if(newState == OnboardState::BOOTING)
    {
        return;
    }
    else if(newState == OnboardState::IDLE)
    {
        if(static_cast<States>(_currentState) > ST_THIRD_CHECKS)
        {
            // If MISSION_RUNNING: landing from mission (not just from manual flight)
            if(_currentMissionStatus == MissionStatus::MISSION_RUNNING)
            {
                _endMissionTime = QTime::currentTime();
                _totalMissionTime = QTime(0, 0, 0, 0).addSecs(static_cast<int>(_startMissionTime.secsTo(_endMissionTime)));
                qDebug(generalLog()).noquote().nospace() << "\n//////////////////////////////////////////////////\n"
                                                            "|||   MISSION FINISHED   \n"
                                                            "|||   Route ID: " << _missionId << "   \n"
                                                            "|||   Landing time: " << _endMissionTime.toString("hh:mm:ss") << "   \n"
                                                            "|||   Total time: " << _totalMissionTime.toString("hh:mm:ss") << "   \n"
                                                            "//////////////////////////////////////////////////";

                QMutexLocker locker(&_fileMutex); // mutex released when locker out of scope
                if(Commander::getInstance()->openMissionInfoLogFile())
                {
                    qInfo(missionInfoLog()).nospace().noquote() << _endMissionTime.toString("hh:mm:ss") << ","
                                                                << _totalMissionTime.toString("hh:mm:ss") << "\n";

                    Commander::getInstance()->closeMissionInfoLogFile();
                }
                else
                {
                    qWarning(generalLog()) << "Can't open file mission info log file!";
                }
            }
            else
            {
                resetState();
            }
            // No reset if state < Ready
            // Ignore drone change to idle while preparing mission
//            resetState();
        }
        _currentMissionStatus = MissionStatus::NO_MISSION;
    }
    else if(newState == OnboardState::RALLY_POINT)
    {
        _currentMissionStatus = MissionStatus::MISSION_RUNNING;
        internalEvent(ST_RUNNING, mission);
        stateEngine();
    }
    else if(newState == OnboardState::EMERGENCY_LANDING)
    {
        _currentMissionStatus = MissionStatus::MISSION_RUNNING;
        internalEvent(ST_RUNNING, mission);
        stateEngine();
    }
    else if(newState == OnboardState::PARACHUTE)
    {
        _currentMissionStatus = MissionStatus::MISSION_RUNNING;
        internalEvent(ST_RUNNING, mission);
        stateEngine();
    }
    else if(newState == OnboardState::LANDED_ERROR)
    {
        if(_currentMissionStatus == MissionStatus::MISSION_RUNNING)
        {
            _endMissionTime = QTime::currentTime();
            _totalMissionTime = QTime(0, 0, 0, 0).addSecs(static_cast<int>(_startMissionTime.secsTo(_endMissionTime)));
            qDebug(generalLog()).noquote().nospace() << "\n//////////////////////////////////////////////////\n"
                                                        "|||   MISSION FINISHED (LANDED ERROR)   \n"
                                                        "|||   Route ID: " << _missionId << "   \n"
                                                        "|||   Landing time: " << _endMissionTime.toString("hh:mm:ss") << "   \n"
                                                        "|||   Total time: " << _totalMissionTime.toString("hh:mm:ss") << "   \n"
                                                        "//////////////////////////////////////////////////";

            QMutexLocker locker(&_fileMutex); // mutex released when locker out of scope
            if(Commander::getInstance()->openMissionInfoLogFile())
            {
                qInfo(missionInfoLog()).nospace().noquote() << _endMissionTime.toString("hh:mm:ss") << ","
                                                            << _totalMissionTime.toString("hh:mm:ss") << "\n";

                Commander::getInstance()->closeMissionInfoLogFile();
            }
            else
            {
                qWarning(generalLog()) << "Can't open file mission info log file!";
            }
        }
        messageToDisplay(tr("Landed error"), tr("Please restart drone."), "");
        resetState();
    }
    else if(newState == OnboardState::MISSION_RECEIVED)
    {
        //Temporary state, the drone is running internal check on the mission
        return;
    }
    else if(newState == OnboardState::MISSION_READY)
    {
        _currentMissionStatus = MissionStatus::MISSION_ACCEPTED;
        missionReady();
        changeDroneReadyStatus(true);
    }
    else if(newState == OnboardState::MISSION_RUNNING)
    {
        if(_currentMissionStatus == MissionStatus::MISSION_ACCEPTED)
        {
            _startMissionTime = QTime::currentTime();
            qDebug(generalLog()).noquote().nospace() << "\n//////////////////////////////////////////////////\n"
                                                        "|||   MISSION STARTED   \n"
                                                        "|||   Route ID: " << _missionId << "   \n"
                                                        "|||   Route name: " << _currentMissionName << "   \n"
                                                        "|||   Sending pilot: " << _comManager->getSendingPilotName() << "   \n"
                                                        "|||   Receiving pilot: " << _comManager->getReceivingPilotName() << "   \n"
                                                        "|||   Cargo weight: " << _currentCargoWeight << "   \n"
                                                        "|||   Cargo volume: " << _currentCargoVolume << "   \n"
                                                        "|||   Take off time: " << _startMissionTime.toString("hh:mm:ss") << "   \n"
                                                        "//////////////////////////////////////////////////";

            QMutexLocker locker(&_fileMutex); // mutex released when locker out of scope
            if(Commander::getInstance()->openMissionInfoLogFile())
            {
                qInfo(missionInfoLog()).nospace().noquote() << _missionId << ","
                                                            << _missionVersion << ","
                                                            << _currentMissionName << ","
                                                            << _currentMissionLength << ","
                                                            << _comManager->getLocalSystemId() << ","
                                                            << _receiverId << ","
                                                            << _comManager->getSendingPilotName() << ","
                                                            << _comManager->getReceivingPilotName() << ","
                                                            << "Unknown" << ","
                                                            << "Unknown" << ","
                                                            << _currentCargoWeight << ","
                                                            << _currentCargoVolume << ","
                                                            << "Unknown" << ","
                                                            << _startMissionTime.toString("hh:mm:ss") << ",";

                Commander::getInstance()->closeMissionInfoLogFile();
            }
            else
            {
                qWarning(generalLog()) << "Can't open file mission info log file!";
            }
        }
        _currentMissionStatus = MissionStatus::MISSION_RUNNING;
        missionRunning();
        internalEvent(ST_RUNNING, mission);
        stateEngine();
    }
    else if(newState == OnboardState::MISSION_PAUSED)
    {
        missionPaused();
        _currentMissionStatus = MissionStatus::MISSION_PAUSED;
        internalEvent(ST_RUNNING, mission);
        stateEngine();
    }
    else if(newState == OnboardState::MANUAL_CONTROL)
    {
        _currentMissionStatus = MissionStatus::MISSION_RUNNING;//NO_MISSION;
        internalEvent(ST_RUNNING, mission);
        stateEngine();
    }
    else if(newState == OnboardState::RC_CONTROL)
    {
        warningMessageToDisplay(tr("Drone is in RC mode."), tr("To provide authority to the cargo extension, please put the control mode to \"P mode\" on the radio control."));

        _currentMissionStatus = MissionStatus::MISSION_RUNNING;//NO_MISSION;
        internalEvent(ST_RUNNING, mission);
        stateEngine();
    }
    else if(newState == OnboardState::PRECISION_LANDING)
    {
        if(mission->missionId)
        {
            _currentMissionStatus = MissionStatus::MISSION_RUNNING;
        }
        else
        {
            _currentMissionStatus = MissionStatus::NO_MISSION;
        }
        missionRunning();
        internalEvent(ST_RUNNING, mission);
        stateEngine();
    }

    _previousDroneState = newState;
}
// State functions

void StateMachine::ST_Idle()
{
    _currentCmdType = 0;
    _currentParam1 = 0;
    _currentParam2 = 0;
    _currentTargetSystem = 0;
    _commandCounter = 0;
    _isDroneReady = false;
    _missionId = 0;
    _missionVersion = 0;
    _currentMissionStatus = MissionStatus::NO_MISSION;
    _currentMissionName = "Unkown";
    _currentMissionLength = 0;
    _requestState = RequestState::SEND_REQUEST;
    _isSender = false;

    _missionManager->clearActiveMission();

    resetAll();
}

void StateMachine::ST_FirstChecks(const missionData* mission)
{
    _missionId = mission->missionId;
    _missionVersion = mission->missionVersion;
    qInfo(generalLog()).nospace() << "Mission ID set to " << _missionId << " (v." << _missionVersion << ").";

    _missionManager->setActiveMission(_missionId, _missionVersion);

    _weatherReport->checkWeatherConditions();
}

void StateMachine::ST_SecondChecks(const requestStateData* request)
{
    _requestState = request->requestState;

    // Avoid resending when returnToPreviousState from send_command
    if(static_cast<States>(_previousState) != ST_SEND_COMMAND)
    {
        // Send reception request to receiving station
        _currentCmdType = MAV_CMD::MAV_CMD_USER_2;
        _currentParam1 = *reinterpret_cast<float*>(&_missionId);
        _currentParam2 = *reinterpret_cast<const float*>(&(request->requestState)); // 0 = requesting, 1 = responding yes, 2 = responding no

        qInfo(generalLog()) << "2ND CHECK SEND REQUEST: " << request << " (pstate: " << _previousState << ")";

        internalEvent(ST_SEND_COMMAND, nullptr);

        if(request->requestState == RequestState::SEND_REQUEST)
        {
            receptionRequestSent();
        }
    }
    else if(request->requestState != RequestState::SEND_REQUEST)
    {
        internalEvent(ST_IDLE, nullptr);
    }
}

void StateMachine::computeDroneDistanceToHome() {
    QGeoCoordinate dronePosition;
    _comManager->getDronePosition(dronePosition);
    //qInfo(generalLog()) << "Drone - lat : " << dronePosition.latitude() << ", lon : " << dronePosition.longitude();
    //qInfo(generalLog()) << "Mission home - lat : " << _missionManager->getActiveMission()->getHomePosition()->latitude() << ", lon : " << _missionManager->getActiveMission()->getHomePosition()->longitude();
    _droneDistanceToHome = _missionManager->getActiveMission()->getHomePosition()->distanceTo(dronePosition);
}

uint8_t StateMachine::preventiveSafetyCheck() {
    if(_isDroneConnected) {
        //Battery

        if(_previousDroneState == OnboardState::RC_CONTROL) {
            qInfo(generalLog()) << "RC check : Fail";
            return 1;
        }
        else {
            qInfo(generalLog()) << "RC check : Pass";
        }

        //Home distance
        computeDroneDistanceToHome();
        if(_droneDistanceToHome > 50.0) {
            qInfo(generalLog()) << "Home distance check : Fail (" << _droneDistanceToHome << " meters)";
            return 1;
        }
        else {
            qInfo(generalLog()) << "Home distance check : Pass (" << _droneDistanceToHome << " meters)";
        }
    }
    else {
        return 1;
    }

    //drone battery

    return 0;
}

void StateMachine::ST_ThirdChecks()
{
    safetyChecksDone();

    if(_isDroneConnected)
    {
        if(_currentMissionStatus != MissionStatus::NO_MISSION) {
            qInfo(generalLog()) << "Drone already has a mission loaded.";
        }
        else if(_preventiveSafetyOverwrite == false && preventiveSafetyCheck() != 0) {
            qInfo(generalLog()) << "Preventive safety checks failed. Waiting...";
            //Send the warning
            //What strategy -> Timeout ? New data ? Overwrite ? User refresh
            bool isInRcMode = _previousDroneState == OnboardState::RC_CONTROL;
            preventiveCheckFailed(isInRcMode, _droneDistanceToHome);
        }
        else {
            _currentCmdType = MAV_CMD::MAV_CMD_USER_1;
            _currentParam1 = *reinterpret_cast<float*>(&_missionId);
    //        bool isMissionTypeDji = _comManager->isMissionTypeDji(); // AAA TODO
            _currentParam2 = *reinterpret_cast<float*>(&_missionVersion);//*reinterpret_cast<float*>(&isMissionTypeDji);

            qInfo(generalLog()) << "Mission ID: " << _missionId;

            internalEvent(ST_SEND_COMMAND, nullptr);
        }
    }
    else {
          qInfo(generalLog()) << "Mission loaded, waiting for the drone to connect.";
    }
}

void StateMachine::ST_Ready() const
{
    if(_isDroneReady)
    {
        missionReady(); // send when drone says MISSION_READY
    }
}

void StateMachine::ST_Running(const missionData* mission)
{
    _missionId = mission->missionId;
    _missionVersion = mission->missionVersion;
    missionChanged(_missionId);
    qInfo(generalLog()).nospace() << "Current mission has ID " << _missionId << " (v." << _missionVersion << ") (from running).";
}

void StateMachine::ST_SendCommand()
{
    mavlink_message_t message;
    mavlink_command_long_t payload;
    payload.command = _currentCmdType;
    payload.target_component = 0;

    // Find target system
    if(_currentCmdType == MAV_CMD::MAV_CMD_USER_2)
    {
        payload.target_system = 100;
        bool targetFound = false;
        for(uint8_t i = 100; i < 109; i++)
        {
            if(i == _comManager->getLocalSystemId())
            {
                continue;
            }
            if(_comManager->systemExistAndConnected(i))
            {
                if(!targetFound)
                {
                    payload.target_system = i;
                    _receiverId = i;
                    targetFound = true;
                }
                else
                {
                    messageToDisplay(tr("Multiple receiving stations connected."), tr("More than one receiving station is connected. The reception request might have been sent to the wrong one. Please cancel the mission, disconnect one receiving station and try again."), "");
                }
            }
        }
//        if(!targetFound)
//            messageToDisplay(tr("No receiving station connected."), tr("No receiving station has been detected. The reception request has been sent to the void. Please cancel the mission, connect a receiving station and try again."), "");
    }
    else
    {
        payload.target_system = Commander::getInstance()->getDroneId();
    }
    _currentTargetSystem = payload.target_system;

    qInfo(generalLog()).nospace() << "Command to send: " << _currentCmdType << ".";

    // Set command parameters
    // Pause cmd param1: pause (0) or continue (1)
    // Mission cmd param1: ID
    // Parachute cmd param1: disable (0), enable (1) or release (2)
    payload.param1 = _currentParam1;
    // Mission param2: version
    payload.param2 = _currentParam2;

    // Increment counter if no ACK received and command resent
    payload.confirmation = _commandCounter;

    // Send command
    mavlink_msg_command_long_encode(_comManager->getLocalSystemId(), _comManager->getLocalComponentId(), &message, &payload);
    if(_comManager->sendMessage(message))
    {
        qInfo(generalLog()).nospace() << "Command sent, counter: " << _commandCounter << ".";
    }
    else
    {
        qInfo(generalLog()).nospace() << "Command not sent!";
    }

    // Set timeout
    float cmdTimeout = 5.0;
    if(_currentCmdType == MAV_CMD::MAV_CMD_MISSION_START) {
        cmdTimeout = 10.0;
    }

    Commander::getInstance()->_timeoutHandler.add(std::bind(&StateMachine::handleTimeoutExpiration, this), cmdTimeout, &_basicCmdCookie );

    _commandCounter++;

    if(_currentCmdType == MAV_CMD::MAV_CMD_MISSION_START)
    {
        _isSender = true; // AAA TODO use for mission logs
    }

    return;
}

const StateStruct* StateMachine::getStateMap() const
{
    static const StateStruct StateMap[] =
    {

        { reinterpret_cast<StateFunc>(&StateMachine::ST_Idle) },
        { reinterpret_cast<StateFunc>(&StateMachine::ST_FirstChecks) },
        { reinterpret_cast<StateFunc>(&StateMachine::ST_SecondChecks) },
        { reinterpret_cast<StateFunc>(&StateMachine::ST_ThirdChecks) },
        { reinterpret_cast<StateFunc>(&StateMachine::ST_Ready) },
        { reinterpret_cast<StateFunc>(&StateMachine::ST_Running) },
        { reinterpret_cast<StateFunc>(&StateMachine::ST_SendCommand) },
        { static_cast<StateFunc>(nullptr) }
    };
    return &StateMap[0];
}
