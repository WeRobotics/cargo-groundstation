#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <QObject>
#include <QSharedPointer>
#include <QMutex>
#include <QtGlobal>

#include "statemachine/eventdata.h"
#include "systems/system_manager.h"
#include "systems/onboardstates.h"
#include "interface/missionmanager.h"
#include "weatherreport.h"

class Commander;

struct StateStruct;

struct missionData : public EventData
{
    int missionId = 0;
    int missionVersion = 0;
};

enum class RequestState {SEND_REQUEST, ACCEPT_REQUEST, REFUSE_REQUEST};
struct requestStateData : public EventData
{
    RequestState requestState = RequestState::SEND_REQUEST;
};

/*
 * State machine design inspired by David Lafreniere
 * Original blog post (seems to have disppeared): http://www.drdobbs.com/cpp/state-machine-design-in-c/184401236?pgno=1
 * Find in archives: http://web.archive.org/web/20180512232134/http://www.drdobbs.com/cpp/state-machine-design-in-c/184401236?pgno=1
 *                   pgno=1, 2 or 3
 *
 * The StateMachine class is a class managing state transitions and related events and chains of events.
 * It triggers GUI updates through a list of signals.
 *
 * The states are the following:
 * @state ST_IDLE: initial idle state. Reset all variables as well as the GUI display.
 * @state ST_FIRST_CHECKS: automated weather checks. Set mission id, send weather conditions query and display weather report in first popup.
 * @state ST_SECOND_CHECKS: safety check list. Display manual checks panel and cargo & pilots information in second popup. Send reception request.
 * Warning: cargo and receiving pilots information as well as reception request feature currently disabled. See README.md to re-activate features.
 * @state ST_THIRD_CHECKS: drone connection. Wait for drone connection and send mission id.
 * @state ST_READY: drone ready. Update start button.
 * @state ST_RUNNING: mission running. Update mission id.
 * @state ST_SEND_COMMAND: send MAVLink command. Fill message with proper parameters, send it and start ack timer.
 *
 * The following external events can trigger a direct external state change:
 * @event resetState: change to ST_IDLE from any state.
 * @event confirmRoute: change to ST_FIRST_CHECKS from ST_IDLE (on pressing "GO" button).
 * @event sendReceptionRequestOrReady: change to ST_SECOND_CHECKS from ST_IDLE (on accepting or refusing reception request),
 *                                     ST_FIRST_CHECKS (on pressing first popup continue button) or ST_SECOND_CHECKS.
 * @event checkDrone: change to ST_THIRD_CHECKS from ST_FIRST_CHECKS or ST_SECOND_CHECKS (on pressing second popup continue button).
 * @event sendCommand: change to ST_SEND_COMMAND from ST_SECOND_CHECKS (send reception request), ST_THIRD_CHECKS (send mission),
 *                     ST_READY (send start), ST_RUNNING (send pause, resume, emergency) or ST_SEND_COMMAND (resend command).
 * @event returnToPreviousState: change to saved previous state from ST_SEND_COMMAND (when done sending command = cmd ack, nack, or timeout and canceled by user).
 * @event changeDroneConnectionStatus: change to ST_THIRD_CHECKS from ST_THIRD_CHECKS or to ST_READY from ST_READY (drone (connection) status change).
 * @event changeDroneReadyStatus: change to ST_READY from ST_IDLE (mission sent by other station), ST_THIRD_CHECKS, ST_READY or ST_SEND_COMMAND.
 *
 * The following external events can trigger a direct INTERNAL state change:
 * @event droneStateChange: change to ST_RUNNING from any state (drone state is RALLY_POINT, EMERGENCY_LANDING, PARACHUTE, MISSION_RUNNING,
 *                          MISSION_PAUSED,MANUAL_CONTROL, RC_CONTROL or PRECISION_LANDING).
 *
 * The following external events do not trigger a direct state change:
 * @event setConnectionParameters: start Commander instance (= launch communication manager).
 * @event handleTimeoutExpiration: display failure of sending message and wait for user action.
 * @event commandAcknowledged: return to previous state (if ack or nack), reset message timer (if in progress).
 * @event forwardDroneErrorMessage: display drone error message.
 */
class StateMachine : public QObject
{
    Q_OBJECT

public:
    explicit StateMachine(QSharedPointer<SystemManager> comManager, QSharedPointer<WeatherReport> weatherReport, QSharedPointer<MissionManager> missionManager);
    ~StateMachine();

    // delete copy and move constructors and assign operators
    StateMachine(StateMachine const &) = delete; // Copy construct
    StateMachine(StateMachine &&) = delete; // Move construct
    StateMachine &operator=(StateMachine const &) = delete; // Copy assign
    StateMachine &operator=(StateMachine &&) = delete; // Move assign

signals:
    void resetAll() const;
    void stateMachineStateChanged(const int newState) const;
    void droneStateChanged(int newState, QString newStateStr) const;
    void controlStatusChanged(bool hasControl) const;
    void weatherConditionsUpdated(bool weatherIsGood, QString weatherReportTitle, QString weatherReportBody) const;
    void receptionRequestSent() const;
    void receptionRequestReceived(int stationId) const;
    void receptionAckReceived() const;
    void safetyChecksDone() const;
    void missionReady() const;
    void missionRunning() const;
    void missionPaused() const;
    void missionChanged(int newMission) const;
    void missionRefused(QString title, QString message, QString details) const;
    void droneConnected(bool isConnected) const;
    void connectionTypeUpdated(bool type) const;
    void antennaConnected(bool isConnected) const;
    void gamepadConnected(bool isConnected) const;
    void webserverConnected(bool isConnected) const;
    void groundstationConnected(bool isConnected, uint8_t systemId) const;
    void telemetryUpdated(QString lat, QString lon, QString alt, QString vx, QString vy, QString vz) const;
    void attitudeUpdated(QString roll, QString pitch, QString yaw) const;
    void droneBatteryUpdated(QString level) const;
    void droneTimeDistanceTemperatureHumidityUpdated(uint64_t time, float distance, float temperature, float humidity) const;
    void radioStatusUpdated(uint64_t address, uint8_t status) const;
    void messageToDisplay(QString title, QString message, QString details) const;
    void warningMessageToDisplay(QString message, QString details) const;
    void clearDisplayedWarningMessage() const;
    void infoToDisplay(QString message) const;
    void debugDataToDisplay(QString xDebug, QString yDebug, QString zDebug) const;
    void forwardRssi(uint8_t local, uint8_t remote = 0) const;
    void commandTimeoutExpired(int cmdType, int targetSystem) const;
    void sendRadioDetectionMessage() const;
    void preventiveCheckFailed(bool isInRcMode, float distanceToHome) const ;


public slots:
    // external events accepted by the state machine
    void resetState();
    void confirmRoute(int missionId = 0, int missionVersion = 0);
    void sendReceptionRequestOrReady(int requestState);
    void checkDrone(QString missionName, double missionLength, int cargoWeight, int cargoVolume, QString sendingPilotName, QString receivingPilotName);
    bool sendCommand(int cmdType = 0, int param1 = -1, int param2 = -1);
    void returnToPreviousState();
    void changeDroneConnectionStatus(bool isConnected);
    void changeDroneReadyStatus(bool isReady);
    void runPreventiveCheck(bool overwrite);

    // external events without direct state change
    //void setConnectionParameters(QString IpAddr, QString port);
    void handleTimeoutExpiration() const;
    void commandAcknowledged(uint16_t cmdType, uint8_t ackResult, uint8_t senderId);
    void forwardDroneErrorMessage(uint8_t severity, QString errorText) const;

    // external events that generate internal state changes
    void droneStateChange(uint32_t newIntState, uint8_t missionId);

    // internal functionss
    void computeDroneDistanceToHome();

private:
    uint8_t preventiveSafetyCheck();

    void externalEvent(unsigned char newState, EventData* eventData = nullptr);
    void internalEvent(unsigned char newState, EventData* eventData = nullptr);
    const StateStruct* getStateMap() const;

    // state machine state functions
    void ST_Idle();
    void ST_FirstChecks(const missionData *mission);
    void ST_SecondChecks(const requestStateData *request);
    void ST_ThirdChecks();
    void ST_Ready() const;
    void ST_Running(const missionData* mission);
    void ST_SendCommand();

    enum class MissionStatus {NO_MISSION, MISSION_ACCEPTED, MISSION_REFUSED, MISSION_RUNNING, MISSION_PAUSED, MISSION_FINISHED};
    enum { EVENT_IGNORED = 0xFE, CANNOT_HAPPEN };

    // For state change
    bool _eventGenerated;
    EventData* _eventData;
    void stateEngine();
    QMutex _stateMutex;
    QMutex _fileMutex;

    const int _maxStates;
    unsigned char _currentState;
    unsigned char _previousState;
    OnboardState _previousDroneState;

    // For sending MAVlink commands
    unsigned char _commandCounter;
    uint16_t _currentCmdType;
    float _currentParam1;
    float _currentParam2;
    unsigned char _currentTargetSystem;
    void* _basicCmdCookie;
    bool _isSender;

    // For missions
    bool _isDroneConnected;
    bool _isDroneReady;
    int _missionId;
    int _missionVersion;
    QString _currentMissionName;
    double _currentMissionLength;
    MissionStatus _currentMissionStatus;
    RequestState _requestState;
    int _currentCargoWeight;
    int _currentCargoVolume;

    QTime _startMissionTime;
    QTime _endMissionTime;
    QTime _totalMissionTime;

    // Others
    QSharedPointer<SystemManager> _comManager;
    QSharedPointer<WeatherReport> _weatherReport;
    QSharedPointer<MissionManager> _missionManager;

    uint8_t _receiverId;

    double _droneDistanceToHome;
    bool _preventiveSafetyOverwrite;

    enum States
    {
        ST_IDLE = 0,
        ST_FIRST_CHECKS,    // Weather check
        ST_SECOND_CHECKS,   // Safety checks, cargo & pilot info, reception ready
        ST_THIRD_CHECKS,    // Drone connected
        ST_READY,           // Drone ready
        ST_RUNNING,         // Mission running
        ST_SEND_COMMAND,    // Send MAVlink command to drone
        ST_MAX_STATES
    };

    inline const char *statesStr(const States state) const
    {
        switch (state)
        {
            case States::ST_IDLE:
                return QObject::tr("IDLE").toUtf8();
            case States::ST_FIRST_CHECKS:
                return QObject::tr("FIRST CHECKS").toUtf8();
            case States::ST_SECOND_CHECKS:
                return QObject::tr("SECOND CHECKS").toUtf8();
            case States::ST_THIRD_CHECKS:
                return QObject::tr("THIRD CHECKS").toUtf8();
            case States::ST_READY:
                return QObject::tr("READY").toUtf8();
            case States::ST_RUNNING:
                return QObject::tr("RUNNING").toUtf8();
            case States::ST_SEND_COMMAND:
                return QObject::tr("SEND COMMAND").toUtf8();
            default:
                return "Unknown";
        }
    }
};


typedef void (StateMachine::*StateFunc)(EventData *);
struct StateStruct
{
    StateFunc stateFunc;
};

#endif // STATEMACHINE_H
