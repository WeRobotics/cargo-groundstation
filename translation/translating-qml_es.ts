<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>Buttons</name>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="15"/>
        <source>CONFIRM MISSION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="17"/>
        <location filename="../qmlcomponents/Buttons.qml" line="27"/>
        <location filename="../qmlcomponents/Buttons.qml" line="31"/>
        <location filename="../qmlcomponents/Buttons.qml" line="97"/>
        <location filename="../qmlcomponents/Buttons.qml" line="406"/>
        <source>GO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="41"/>
        <location filename="../qmlcomponents/Buttons.qml" line="52"/>
        <source>MISSION
LOADING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="43"/>
        <location filename="../qmlcomponents/Buttons.qml" line="61"/>
        <source>CONNECT
TO DRONE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="69"/>
        <source>MANUAL
CONTROL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="71"/>
        <source>REMOTE
CONTROL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="75"/>
        <source>START</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="80"/>
        <source>RESUME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="86"/>
        <source>PAUSE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="92"/>
        <source>TABLET NOT
IN CONTROL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="108"/>
        <source>BATTERY
LOW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="197"/>
        <source>CHANGE
LANGUAGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="159"/>
        <source>LOCK ON
DRONE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="159"/>
        <source>UNLOCK
FROM DRONE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="181"/>
        <source>CHANGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="453"/>
        <source>EMERGENCY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="507"/>
        <source>LOCK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="507"/>
        <source>UNLOCK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Commander</name>
    <message>
        <location filename="../commander.cpp" line="193"/>
        <source>Serial connection error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commander.cpp" line="193"/>
        <source>No adequate serial port found. Initialising tcp connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commander.cpp" line="201"/>
        <source>Connection result</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmergencyPopup</name>
    <message>
        <location filename="../qmlcomponents/EmergencyPopup.qml" line="53"/>
        <source>EMERGENCY
LANDING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/EmergencyPopup.qml" line="97"/>
        <source>PRECISION
LANDING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/EmergencyPopup.qml" line="141"/>
        <source>RETAKE
CONTROL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/EmergencyPopup.qml" line="185"/>
        <source>GO TO RALLY
POINT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstPopup</name>
    <message>
        <location filename="../qmlcomponents/FirstPopup.qml" line="63"/>
        <source>Ignore the weather forecast and fly anyway. This is NOT RECOMMENDED.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/FirstPopup.qml" line="143"/>
        <source>   Cancel   </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GamepadController</name>
    <message>
        <location filename="../gamepadcontroller.cpp" line="163"/>
        <source>Gamepad disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gamepadcontroller.cpp" line="177"/>
        <source>Gamepad connected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapPlugin</name>
    <message>
        <location filename="../qmlcomponents/MapPlugin.qml" line="51"/>
        <source>XMLHttpRequest error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/MapPlugin.qml" line="51"/>
        <source>Can&apos;t get esri token for access to map.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupMessage</name>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="37"/>
        <source>Send mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="39"/>
        <source>Reception request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="41"/>
        <source>Start mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="43"/>
        <source>Pause or Resume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="45"/>
        <source>Land (precision or emergency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="47"/>
        <source>Retake Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="49"/>
        <source>Go to rally point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="51"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="54"/>
        <source>Command error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="57"/>
        <location filename="../qmlcomponents/PopupMessage.qml" line="59"/>
        <location filename="../qmlcomponents/PopupMessage.qml" line="61"/>
        <source>The</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="57"/>
        <source>command was not successfully transmitted to the drone. Would you like to resend it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="59"/>
        <source>command was not successfully transmitted to the other groundstation. Would you like to resend it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="61"/>
        <source>command was not successfully transmitted to UNKNOWN TARGET (ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="61"/>
        <source>). Would you like to resend it?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../communication/connection_result.h" line="37"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="39"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="41"/>
        <source>Socket error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="43"/>
        <source>Bind error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="45"/>
        <source>Socket connection error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="47"/>
        <source>Connection error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="49"/>
        <source>Not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="51"/>
        <source>System not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="53"/>
        <source>System busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="55"/>
        <source>Command denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="57"/>
        <source>Destination IP unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="59"/>
        <source>Connections exhausted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="61"/>
        <source>Invalid connection URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="63"/>
        <location filename="../statemachine/mavlinkcommands.h" line="23"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="166"/>
        <source>IDLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="168"/>
        <source>FIRST CHECKS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="170"/>
        <source>SECOND CHECKS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="172"/>
        <source>THIRD CHECKS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="174"/>
        <source>READY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="176"/>
        <source>RUNNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="178"/>
        <source>SEND COMMAND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="25"/>
        <source>Booting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="27"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="29"/>
        <source>Rally point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="31"/>
        <source>Emergency landing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="33"/>
        <source>Parachute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="35"/>
        <source>Landed error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="37"/>
        <source>Mission received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="39"/>
        <source>Mission ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="41"/>
        <source>Mission running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="43"/>
        <source>Mission paused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="45"/>
        <source>Manual control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="47"/>
        <source>RC control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="49"/>
        <source>Precision landing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="9"/>
        <source>Send mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="11"/>
        <source>Reception request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="13"/>
        <source>Start mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="15"/>
        <source>Pause or Resume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="17"/>
        <source>Land (precision or emergency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="19"/>
        <source>Retake Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="21"/>
        <source>Go to rally point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="31"/>
        <source>the drone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="33"/>
        <location filename="../statemachine/mavlinkcommands.h" line="35"/>
        <location filename="../statemachine/mavlinkcommands.h" line="37"/>
        <location filename="../statemachine/mavlinkcommands.h" line="39"/>
        <source>the other ground station</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="41"/>
        <source>UNKNOWN TARGET (ID: %1)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReceptionPopup</name>
    <message>
        <location filename="../qmlcomponents/ReceptionPopup.qml" line="37"/>
        <source>Station %1 is requesting you to receive a delivery.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/ReceptionPopup.qml" line="52"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/ReceptionPopup.qml" line="76"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RouteData</name>
    <message>
        <location filename="../route/routedata.cpp" line="24"/>
        <location filename="../route/routedata.cpp" line="25"/>
        <source>Select route</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RouteSelectionBox</name>
    <message>
        <location filename="../qmlcomponents/RouteSelectionBox.qml" line="83"/>
        <source>ALTITUDE PROFILE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/RouteSelectionBox.qml" line="368"/>
        <source>CANCEL</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SecondPopup</name>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="47"/>
        <source>Pilot name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="83"/>
        <location filename="../qmlcomponents/SecondPopup.qml" line="375"/>
        <source>Enter pilot name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="93"/>
        <location filename="../qmlcomponents/SecondPopup.qml" line="365"/>
        <location filename="../qmlcomponents/SecondPopup.qml" line="582"/>
        <source>Perform safety checks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="360"/>
        <source>Safety checks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="415"/>
        <source>Tablet battery charged</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="422"/>
        <source>Samples loaded into cargo box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="429"/>
        <source>Drone and cargo box labelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="436"/>
        <source>Cargo box secured under drone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="443"/>
        <source>Drone visually inspected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="472"/>
        <source>Authorities informed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="479"/>
        <source>Flight plan up-to-date and synchronised</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="486"/>
        <source>Take-off site cleared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="596"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="619"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPopup</name>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="110"/>
        <source>Select map provider: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="148"/>
        <source>Select map type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="179"/>
        <source>Confirm all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="222"/>
        <source>CANCEL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="248"/>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="258"/>
        <source>CONFIRM</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StateMachine</name>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="251"/>
        <source>Command error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="382"/>
        <source>Command not accepted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="392"/>
        <source>Drone error: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="394"/>
        <source>Drone warning: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="251"/>
        <source>Request to send command %1 while still trying to send command %2. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="376"/>
        <source>Mission not accepted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="376"/>
        <source>Mission %1 (v.%2) was not accepted by the drone. Would you like to resend it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="376"/>
        <source>Look at drone logs for more info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="382"/>
        <source>The &quot;%1&quot; command was not accepted by %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="422"/>
        <source>Current mission has ID %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="504"/>
        <source>Landed error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="504"/>
        <source>Please restart drone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="699"/>
        <source>Multiple receiving stations connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="699"/>
        <source>More than one receiving station is connected. The reception request might have been sent to the wrong one. Please cancel the mission, disconnect one receiving station and try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StationSystem</name>
    <message>
        <location filename="../systems/station_system.cpp" line="47"/>
        <source>Reception request accepted by receiving station.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="53"/>
        <location filename="../systems/station_system.cpp" line="59"/>
        <source>Reception request error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="53"/>
        <source>Reception request denied by receiving station.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="53"/>
        <source>Please contact the receiving station directly to discuss the issue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="59"/>
        <source>Reception request message with unexpected parameters received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="65"/>
        <source>Received reception request or ack aimed at another station.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TelemetryBox</name>
    <message>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="31"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="33"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="39"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="73"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="76"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="546"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="670"/>
        <source>from start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="32"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="40"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="72"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="692"/>
        <source>to end</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="135"/>
        <source>TELEMETRY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="333"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VirtualGamepad</name>
    <message>
        <location filename="../qmlcomponents/VirtualGamepad.qml" line="160"/>
        <source>   ARM   </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/VirtualGamepad.qml" line="174"/>
        <source> DISARM </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qmlcomponents/VirtualGamepad.qml" line="184"/>
        <source>TAKE OFF</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WeatherReport</name>
    <message>
        <location filename="../weatherreport.cpp" line="44"/>
        <location filename="../weatherreport.cpp" line="60"/>
        <location filename="../weatherreport.cpp" line="97"/>
        <location filename="../weatherreport.cpp" line="122"/>
        <location filename="../weatherreport.cpp" line="179"/>
        <location filename="../weatherreport.cpp" line="242"/>
        <location filename="../weatherreport.cpp" line="254"/>
        <source>Weather query failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="45"/>
        <location filename="../weatherreport.cpp" line="61"/>
        <source>Can&apos;t access location data.
Turn localization on and check
location on Google Maps.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="47"/>
        <location filename="../weatherreport.cpp" line="63"/>
        <location filename="../weatherreport.cpp" line="100"/>
        <location filename="../weatherreport.cpp" line="125"/>
        <location filename="../weatherreport.cpp" line="139"/>
        <location filename="../weatherreport.cpp" line="182"/>
        <location filename="../weatherreport.cpp" line="245"/>
        <location filename="../weatherreport.cpp" line="257"/>
        <location filename="../weatherreport.cpp" line="271"/>
        <source>          OK          </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="98"/>
        <location filename="../weatherreport.cpp" line="123"/>
        <location filename="../weatherreport.cpp" line="180"/>
        <location filename="../weatherreport.cpp" line="243"/>
        <location filename="../weatherreport.cpp" line="255"/>
        <source>Can&apos;t access weather data.
Check internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="112"/>
        <source>Risk detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="116"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="137"/>
        <location filename="../weatherreport.cpp" line="269"/>
        <source>Send reception request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="153"/>
        <source>Weather conditions and timing permit flight.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="155"/>
        <source>Weather conditions and/or timing prohibit flight.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="158"/>
        <source>Temp: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="159"/>
        <source>Wind speed: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="160"/>
        <source>Solar flares: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="162"/>
        <source>Time since sunset: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="164"/>
        <source>Time until sunrise: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="166"/>
        <source>Time until sunset: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
