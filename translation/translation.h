#ifndef TRANS_H
#define TRANS_H

#include <QObject>
#include <QTranslator>
#include <QDebug>
#include <QGuiApplication>
#include <QDir>
#include <QtQml/QQmlApplicationEngine>

/*
 * The Translator class is the class enabling a multilingual UI.
 *
 * The language can be changed using the changeLanguage method:
 * @param language: a string conatining the two-letters abbreviation of the language.
 * Warning: the corresponding translation file (.qm) must be available! See README.md for more info.
 */
class Translator : public QObject
{
    Q_OBJECT

public:
    Translator(QSharedPointer<QQmlApplicationEngine> engine);
    ~Translator();

    Q_INVOKABLE void changeLanguage(QString language);

signals:
    void languageChanged();

private:
    QSharedPointer<QTranslator> _translator;
    QSharedPointer<QQmlApplicationEngine> _engine;
};

#endif // TRANS_H
