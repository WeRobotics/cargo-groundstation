<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ne_NP" sourcelanguage="en">
<context>
    <name>Buttons</name>
    <message>
        <source>CONFIRM MISSION</source>
        <translation type="vanished">मिसन निश्चित गर्नुहोस</translation>
    </message>
    <message>
        <source>GO</source>
        <translation>गो</translation>
    </message>
    <message>
        <source>TURN ON
DRONE</source>
        <translation type="vanished">ड्रोन सुचारु गर्नुहोस</translation>
    </message>
    <message>
        <source>MISSION
LOADING</source>
        <translation>मिसन सुचारु हुँदैछ</translation>
    </message>
    <message>
        <source>CONNECT
TO DRONE</source>
        <translation>ड्रोन कनेक्ट गर्नुहोस</translation>
    </message>
    <message>
        <source>MANUAL
CONTROL</source>
        <translation>म्यानुअल नियन्त्रण/ नियन्त्रक</translation>
    </message>
    <message>
        <source>REMOTE
CONTROL</source>
        <translation>रिमोट नियन्त्रण/ नियन्त्रक</translation>
    </message>
    <message>
        <source>START</source>
        <translation>सुरु गर्नुहोस/ सुचारु गर्नुहोस्</translation>
    </message>
    <message>
        <source>RESUME</source>
        <translation>पुन सुचारु  गर्नुहोस</translation>
    </message>
    <message>
        <source>PAUSE</source>
        <translation>केहि समय रोक्नुहोस</translation>
    </message>
    <message>
        <source>TABLET NOT
IN CONTROL</source>
        <translation type="vanished">ट्याब्लेट नियन्त्रणमा छैन</translation>
    </message>
    <message>
        <source>BATTERY
LOW</source>
        <translation>ब्याट्री कम</translation>
    </message>
    <message>
        <source>UNLOCK FROM
DRONE</source>
        <translation type="vanished">ड्रोन बाट अनलक गर्नुहोस</translation>
    </message>
    <message>
        <source>LOCK ON
DRONE</source>
        <translation>ड्रोनमा
लक गर्नुहोस</translation>
    </message>
    <message>
        <source>CHANGE</source>
        <translation>परिवर्तन</translation>
    </message>
    <message>
        <source>CONNECT</source>
        <translation type="vanished">जडान गर्नुहोस</translation>
    </message>
    <message>
        <source>EMERGENCY</source>
        <translation>इमर्जेन्सी</translation>
    </message>
    <message>
        <source>LOCK</source>
        <translation>लक</translation>
    </message>
    <message>
        <source>UNLOCK</source>
        <translation>अनलक</translation>
    </message>
    <message>
        <source>UNLOCK
FROM DRONE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>RALLY
POINT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>EMERGENCY
LANDING</source>
        <translation type="unfinished">आकस्मिक अवतरण</translation>
    </message>
    <message>
        <source>PARACHUTE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nepali</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Commander</name>
    <message>
        <source>Serial connection error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No adequate serial port found. Initialising tcp connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connection result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Using serial baudrate: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Using serial API</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>NOT using serial API</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EmergencyPopup</name>
    <message>
        <source>EMERGENCY
LANDING</source>
        <translation>आकस्मिक अवतरण</translation>
    </message>
    <message>
        <source>PRECISION
LANDING</source>
        <translation>सटिक अवतरण</translation>
    </message>
    <message>
        <source>RETAKE
CONTROL</source>
        <translation>पुन: नियन्त्रणमा लिनुहोस</translation>
    </message>
    <message>
        <source>GO TO RALLY
POINT</source>
        <translation>र्याली बिन्दुमा जानुहोस</translation>
    </message>
</context>
<context>
    <name>FirstPopup</name>
    <message>
        <source>Ignore the weather forecast and fly anyway. This is NOT RECOMMENDED.</source>
        <translation>मौसम पूर्वानुमानको वास्ता नगर्नुहोस ,तर यो हामी सिफारिस गर्दैनौ.</translation>
    </message>
    <message>
        <source>   Cancel   </source>
        <translation>   रद्द गर्नुहोस्   </translation>
    </message>
</context>
<context>
    <name>GamepadController</name>
    <message>
        <source>Gamepad disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Gamepad connected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MapPlugin</name>
    <message>
        <source>XMLHttpRequest error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t get esri token for access to map.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupMessage</name>
    <message>
        <source>Send mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reception request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause or Resume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Land (precision or emergency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Retake Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Go to rally point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Command error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>command was not successfully transmitted to the drone. Would you like to resend it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>command was not successfully transmitted to the other groundstation. Would you like to resend it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>command was not successfully transmitted to UNKNOWN TARGET (ID: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>). Would you like to resend it?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>IDLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FIRST CHECKS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SECOND CHECKS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>THIRD CHECKS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>READY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>RUNNING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SEND COMMAND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Booting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rally point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Emergency landing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Parachute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Landed error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mission received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mission ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mission running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mission paused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Manual control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>RC control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Precision landing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Socket error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bind error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Socket connection error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connection error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Command denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Destination IP unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connections exhausted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid connection URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reception request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start mission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause or Resume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Land (precision or emergency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Retake Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Go to rally point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>the drone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>the other ground station</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>UNKNOWN TARGET (ID: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reception request accepted by receiving station.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reception request error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reception request denied by receiving station.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please contact the receiving station directly to discuss the issue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reception request message with unexpected parameters received.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Received reception request or ack aimed at another station.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReceptionPopup</name>
    <message>
        <source>Station %1 is requesting you to receive a delivery.</source>
        <translation>स्टेसन %1 ले तपाईलाई डेलिभरी प्राप्त गर्न अनुरोध गर्दछ.</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>स्वीकार्नुहोस्</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>रद्द गर्नुहोस्</translation>
    </message>
</context>
<context>
    <name>RouteData</name>
    <message>
        <source>Select route</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RouteSelectionBox</name>
    <message>
        <source>ALTITUDE PROFILE</source>
        <translation>उचाइ/उन्नतांश प्रोफाइल</translation>
    </message>
    <message>
        <source>CANCEL</source>
        <translation>रद्द गर्नुहोस्</translation>
    </message>
</context>
<context>
    <name>SecondPopup</name>
    <message>
        <source>Pilot name: </source>
        <translation>चालकको नाम: </translation>
    </message>
    <message>
        <source>Safety checks</source>
        <translation>सुरक्षा जाँच</translation>
    </message>
    <message>
        <source>Tablet battery charged</source>
        <translation>ट्याब्लेटको ब्याट्री  चार्ज भयो</translation>
    </message>
    <message>
        <source>Samples loaded into cargo box</source>
        <translation>कार्गो बाकसमा नमुना संकलन भयो</translation>
    </message>
    <message>
        <source>Drone and cargo box labelled</source>
        <translation>ड्रोन र कार्गो बक्स लेबल भयो</translation>
    </message>
    <message>
        <source>Cargo box secured under drone</source>
        <translation>कार्गो बक्स ड्रोनमा सुरक्षितसंग राखियो</translation>
    </message>
    <message>
        <source>Drone visually inspected</source>
        <translation>ड्रोन  निरीक्षण गरियो</translation>
    </message>
    <message>
        <source>Authorities informed</source>
        <translation>प्राधिकरणलाइ सुचित गरियो</translation>
    </message>
    <message>
        <source>Flight plan up-to-date and synchronised</source>
        <translation>उडान योजना अप-टू डेट र समक्रमण गरियो</translation>
    </message>
    <message>
        <source>Take-off site cleared</source>
        <translation>उडान क्षेत्र खाली गरियो</translation>
    </message>
    <message>
        <source>Perform safety checks</source>
        <translation>सुरक्षा जाँचहरू प्रदर्शन गर्नुहोस्</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>पुन: संचालन गर्नुहोस</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>रद्द गर्नुहोस्</translation>
    </message>
    <message>
        <source>Enter pilot name</source>
        <translation>चालकको नाम प्रविष्ट गर्नुहोस्</translation>
    </message>
</context>
<context>
    <name>SettingsPopup</name>
    <message>
        <source>Select map provider: </source>
        <translation>नक्सा प्रदाता/प्रदायक चयन गर्नुहोस्: </translation>
    </message>
    <message>
        <source>Select map type: </source>
        <translation>नक्साको प्रकार चयन गर्नुहोस्: </translation>
    </message>
    <message>
        <source>Confirm all</source>
        <translation>सम्पूर्ण निश्चित गर्नुहोस्</translation>
    </message>
    <message>
        <source>CANCEL</source>
        <translation>रद्द गर्नुहोस्</translation>
    </message>
    <message>
        <source>CONFIRM</source>
        <translation>निश्चित गर्नुहोस</translation>
    </message>
</context>
<context>
    <name>StateMachine</name>
    <message>
        <source>Command error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Command not accepted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drone error: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drone warning: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Landed error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please restart drone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Multiple receiving stations connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>More than one receiving station is connected. The reception request might have been sent to the wrong one. Please cancel the mission, disconnect one receiving station and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Request to send command %1 while still trying to send command %2. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mission not accepted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mission %1 (v.%2) was not accepted by the drone. Would you like to resend it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Look at drone logs for more info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The &quot;%1&quot; command was not accepted by %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current mission has ID %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TelemetryBox</name>
    <message>
        <source>TELEMETRY</source>
        <translation>टेलिमेट्रि</translation>
    </message>
    <message>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>from start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to end</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VirtualGamepad</name>
    <message>
        <source>   ARM   </source>
        <translation>   शस्त्र   </translation>
    </message>
    <message>
        <source> DISARM </source>
        <translation> नि: शस्त्र/शस्त्रहिन </translation>
    </message>
    <message>
        <source>TAKE OFF</source>
        <translation>उडान</translation>
    </message>
</context>
<context>
    <name>WeatherReport</name>
    <message>
        <source>Weather query failed.</source>
        <translation>मौसमी पुछताछ/ प्रश्न  विफल भयो.</translation>
    </message>
    <message>
        <source>Can&apos;t access location data.
Turn localization on and check
location on Google Maps.</source>
        <translation>स्थान सम्बन्धि डाटामा पहुँच पुगेन, गुगल नक्सामा स्थानीयकरण सक्षम गर्नुहोस् र स्थान जाँच गर्नुहोस्.</translation>
    </message>
    <message>
        <source>Weather query failed: can&apos;t access location data.</source>
        <translation type="vanished">मौसमी पुछताछ/ प्रश्न  विफल: स्थान सम्बन्धि डाटामा पहुँच पुगेन/ समन्वय हुन सकेन.</translation>
    </message>
    <message>
        <source>Checking weather forecast at coordinates: </source>
        <translation type="vanished">कोअर्डिनेटसमा मौसमी पूर्वानुमानको जांच गर्नुहोस्: </translation>
    </message>
    <message>
        <source>          OK          </source>
        <translation>          ओके          </translation>
    </message>
    <message>
        <source>Can&apos;t access weather data.
Check internet connection.</source>
        <translation>मौसमको डाटासंग समन्वय हुन सकेन, कृपया आफ्नो इन्टरनेट जडान जाच गर्नुहोस.</translation>
    </message>
    <message>
        <source>Weather query failed: can&apos;t access solar flare data.</source>
        <translation type="vanished">मौसमी पुछताछ/ प्रश्न  विफल, सोलार फ्लेयर डाटासंग समन्वय हुन सकेन.</translation>
    </message>
    <message>
        <source>Risk detected</source>
        <translation>खतरा</translation>
    </message>
    <message>
        <source>None</source>
        <translation>कुनै खतरा छैन</translation>
    </message>
    <message>
        <source>Weather query failed: can&apos;t access solar flare data: </source>
        <translation type="vanished">मौसमी पुछताछ/ प्रश्न  विफल, सोलार फ्लेयर डाटासंग समन्वय हुन सकेन: </translation>
    </message>
    <message>
        <source>Send reception request</source>
        <translation>पुन: संचालन गर्नुहोस</translation>
    </message>
    <message>
        <source>Weather conditions and timing permit flight.</source>
        <translation>मौसमी अवस्था र समय अनुमतिको उडान.</translation>
    </message>
    <message>
        <source>Weather conditions and/or timing prohibit flight.</source>
        <translation>मौसम स्थिति र / वा समय निषेध उडान।.</translation>
    </message>
    <message>
        <source>Temp: </source>
        <translation>तापक्रम: </translation>
    </message>
    <message>
        <source>Wind speed: </source>
        <translation>हावाको वेग: </translation>
    </message>
    <message>
        <source>Solar flares: </source>
        <translation>सोलार फ्लेयर्स: </translation>
    </message>
    <message>
        <source>Time since sunset: </source>
        <translation>सूर्यास्त पश्चातको  समय: </translation>
    </message>
    <message>
        <source>Time until sunrise: </source>
        <translation>सुर्योदयकोलागी समय: </translation>
    </message>
    <message>
        <source>Time until sunset: </source>
        <translation>सूर्यास्तकोलागी समय: </translation>
    </message>
    <message>
        <source>Weather report</source>
        <translation type="vanished">मौसमी टिपोट/ रिपोर्ट</translation>
    </message>
    <message>
        <source>Weather query failed: can&apos;t access weather data.</source>
        <translation type="vanished">मौसमी पुछताछ/ प्रश्न  विफल, मौसमी डाटासंग समन्वय हुन सकेन.</translation>
    </message>
    <message>
        <source>Weather query failed: can&apos;t access weather data: </source>
        <translation type="vanished">मौसमी पुछताछ/ प्रश्न  विफल, मौसमी डाटासंग समन्वय हुन सकेन: </translation>
    </message>
</context>
</TS>
