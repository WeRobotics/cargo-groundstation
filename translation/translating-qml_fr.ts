<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>Buttons</name>
    <message>
        <source>CONFIRM MISSION</source>
        <translation type="vanished">CONFIRMER LA MISSION</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="15"/>
        <location filename="../qmlcomponents/Buttons.qml" line="22"/>
        <location filename="../qmlcomponents/Buttons.qml" line="23"/>
        <location filename="../qmlcomponents/Buttons.qml" line="78"/>
        <location filename="../qmlcomponents/Buttons.qml" line="315"/>
        <source>GO</source>
        <translation>ALLER</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="32"/>
        <location filename="../qmlcomponents/Buttons.qml" line="46"/>
        <source>MISSION
LOADING</source>
        <translation>CHARGEMENT DE
LA MISSION</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="34"/>
        <location filename="../qmlcomponents/Buttons.qml" line="52"/>
        <source>CONNECT
TO DRONE</source>
        <translation>CONNECTER
LE DRONE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="57"/>
        <source>RALLY
POINT</source>
        <translation>POINT
RALLY</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="59"/>
        <source>EMERGENCY
LANDING</source>
        <translation>ATTERRISSAGE
D&apos;URGENCE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="61"/>
        <source>PARACHUTE</source>
        <translation>PARACHUTE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="63"/>
        <source>MANUAL
CONTROL</source>
        <translation>CONTROLE
MANUEL</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="65"/>
        <source>REMOTE
CONTROL</source>
        <translation>TELE-
COMMANDE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="67"/>
        <source>START</source>
        <translation>DECOLLER</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="70"/>
        <source>RESUME</source>
        <translation>REPRENDRE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="75"/>
        <source>PAUSE</source>
        <translation>PAUSE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="153"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="154"/>
        <source>French</source>
        <translation>Francais</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="155"/>
        <source>Nepali</source>
        <translation>Nepalais</translation>
    </message>
    <message>
        <source>TABLET NOT
IN CONTROL</source>
        <translation type="vanished">TABLETTE PAS
EN CONTROLE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="40"/>
        <source>BATTERY
LOW</source>
        <translation>BATTERIE
FAIBLE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="116"/>
        <source>UNLOCK
FROM DRONE</source>
        <translation>DETACHER
DU DRONE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="116"/>
        <source>LOCK ON
DRONE</source>
        <translation>CENTRER
SUR DRONE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="133"/>
        <source>CHANGE</source>
        <translation>CHANGER</translation>
    </message>
    <message>
        <source>CHANGE
LANGUAGE</source>
        <translation type="vanished">CHANGER
LA LANGUE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="355"/>
        <source>EMERGENCY</source>
        <translation>URGENCE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="373"/>
        <source>LOCK</source>
        <translation>BLOQUER</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/Buttons.qml" line="373"/>
        <source>UNLOCK</source>
        <translation>DEBLOQUER</translation>
    </message>
</context>
<context>
    <name>Commander</name>
    <message>
        <location filename="../commander.cpp" line="182"/>
        <source>Using serial baudrate: %1</source>
        <translation>Baudrate sériel utilisée: %1</translation>
    </message>
    <message>
        <location filename="../commander.cpp" line="189"/>
        <source>Using serial API</source>
        <translation>Sériel API utilisé</translation>
    </message>
    <message>
        <location filename="../commander.cpp" line="194"/>
        <source>NOT using serial API</source>
        <translation>Sériel API NON utilisé</translation>
    </message>
    <message>
        <location filename="../commander.cpp" line="210"/>
        <source>Serial connection error</source>
        <translation>Erreur connexion série</translation>
    </message>
    <message>
        <location filename="../commander.cpp" line="210"/>
        <source>No adequate serial port found. Initialising tcp connection.</source>
        <translation>Pas de port sériel détecté. Initialisation de la connexion TCP.</translation>
    </message>
    <message>
        <location filename="../commander.cpp" line="218"/>
        <source>Connection result</source>
        <translation>Résultat de la connexion</translation>
    </message>
</context>
<context>
    <name>EmergencyPopup</name>
    <message>
        <location filename="../qmlcomponents/EmergencyPopup.qml" line="41"/>
        <source>EMERGENCY
LANDING</source>
        <translation>ATTERRISSAGE
D&apos;URGENCE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/EmergencyPopup.qml" line="79"/>
        <source>PRECISION
LANDING</source>
        <translation>ATTERRISSAGE
DE PRECISION</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/EmergencyPopup.qml" line="117"/>
        <source>RETAKE
CONTROL</source>
        <translation>REPRENDRE
LE CONTROLE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/EmergencyPopup.qml" line="155"/>
        <source>GO TO RALLY
POINT</source>
        <translation>ALLER AU
POINT RALLY</translation>
    </message>
</context>
<context>
    <name>FirstPopup</name>
    <message>
        <location filename="../qmlcomponents/FirstPopup.qml" line="82"/>
        <source>Ignore the weather forecast and fly anyway. This is NOT RECOMMENDED.</source>
        <translation>Ignorer la prévision météo et voler quand même. CECI EST DECONSEILLE.</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/FirstPopup.qml" line="153"/>
        <source>   Cancel   </source>
        <translation>   Annuler   </translation>
    </message>
</context>
<context>
    <name>GamepadController</name>
    <message>
        <location filename="../gamepadcontroller.cpp" line="163"/>
        <source>Gamepad disconnected</source>
        <translation>Manette déconnectée</translation>
    </message>
    <message>
        <location filename="../gamepadcontroller.cpp" line="177"/>
        <source>Gamepad connected</source>
        <translation>Manette connectée</translation>
    </message>
</context>
<context>
    <name>MapPlugin</name>
    <message>
        <location filename="../qmlcomponents/MapPlugin.qml" line="50"/>
        <source>XMLHttpRequest error</source>
        <translation>Erreur XMLHttpRequest</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/MapPlugin.qml" line="50"/>
        <source>Can&apos;t get esri token for access to map.</source>
        <translation>Le jeton d&apos;accès aux cartes ESRI n&apos;a pas pu être obtenu.</translation>
    </message>
</context>
<context>
    <name>PopupMessage</name>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="38"/>
        <source>Send mission</source>
        <translation>Envoyer la mission</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="40"/>
        <source>Reception request</source>
        <translation>Demande de réception</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="42"/>
        <source>Start mission</source>
        <translation>Commencer la mission</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="44"/>
        <source>Pause or Resume</source>
        <translation>Pause ou reprendre</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="46"/>
        <source>Land (precision or emergency)</source>
        <translation>Atterrissage (de précision ou d&apos;urgence)</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="48"/>
        <source>Retake Control</source>
        <translation>Reprendre le contrôle</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="50"/>
        <source>Go to rally point</source>
        <translation>Aller au point rally</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="52"/>
        <source>Unknown</source>
        <translation>INCONNUE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="54"/>
        <source>Command error</source>
        <translation>Erreur de commande</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="57"/>
        <location filename="../qmlcomponents/PopupMessage.qml" line="59"/>
        <location filename="../qmlcomponents/PopupMessage.qml" line="61"/>
        <source>The</source>
        <translation>La</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="57"/>
        <source>command was not successfully transmitted to the drone. Would you like to resend it?</source>
        <translation>commande n&apos;a pas été envoyée au drone avec succès. Souhaitez-vous la renvoyer ?</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="59"/>
        <source>command was not successfully transmitted to the other groundstation. Would you like to resend it?</source>
        <translation>commande n&apos;a pas été envoyée à l&apos;autre station avec succès. Souhaitez-vous la renvoyer ?</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="61"/>
        <source>command was not successfully transmitted to UNKNOWN TARGET (ID: </source>
        <translation>commande n&apos;a pas été envoyée avec succès à CIBLE INCONNUE (ID: </translation>
    </message>
    <message>
        <location filename="../qmlcomponents/PopupMessage.qml" line="61"/>
        <source>). Would you like to resend it?</source>
        <translation>). Souhaitez-vous la renvoyer ?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../communication/connection_result.h" line="37"/>
        <source>Success</source>
        <translation>Succès</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="39"/>
        <source>Timeout</source>
        <translation>Temps expiré</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="41"/>
        <source>Socket error</source>
        <translation>Erreur socket</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="43"/>
        <source>Bind error</source>
        <translation>Erreur bind</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="45"/>
        <source>Socket connection error</source>
        <translation>Erreur de socket de connexion</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="47"/>
        <source>Connection error</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="49"/>
        <source>Not implemented</source>
        <translation>Non implementé</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="51"/>
        <source>System not connected</source>
        <translation>Système non connecté</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="53"/>
        <source>System busy</source>
        <translation>Système occupé</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="55"/>
        <source>Command denied</source>
        <translation>Commande refusée</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="57"/>
        <source>Destination IP unknown</source>
        <translation>IP de destination inconnue</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="59"/>
        <source>Connections exhausted</source>
        <translation>Connexions épuisées</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="61"/>
        <source>Invalid connection URL</source>
        <translation>Connexion URL invalide</translation>
    </message>
    <message>
        <location filename="../communication/connection_result.h" line="63"/>
        <location filename="../statemachine/mavlinkcommands.h" line="25"/>
        <source>Unknown</source>
        <translation>INCONNU</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="165"/>
        <source>IDLE</source>
        <translation>INACTIF</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="167"/>
        <source>FIRST CHECKS</source>
        <translation>PREMIERS CONTROLES</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="169"/>
        <source>SECOND CHECKS</source>
        <translation>DEUXIEMES CONTROLES</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="171"/>
        <source>THIRD CHECKS</source>
        <translation>TROISIEMES CONTROLES</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="173"/>
        <source>READY</source>
        <translation>PRET</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="175"/>
        <source>RUNNING</source>
        <translation>EN VOL</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.h" line="177"/>
        <source>SEND COMMAND</source>
        <translation>ENVOI DE COMMANDE</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="27"/>
        <source>Booting</source>
        <translation>Démarrage</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="29"/>
        <source>Idle</source>
        <translation>Inactif</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="31"/>
        <source>Rally point</source>
        <translation>Point rally</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="33"/>
        <source>Emergency landing</source>
        <translation>Atterrissage d&apos;urgence</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="35"/>
        <source>Parachute</source>
        <translation>Parachute</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="37"/>
        <source>Landed error</source>
        <translation>Erreur au sol</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="39"/>
        <source>Mission received</source>
        <translation>Mission reçue</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="41"/>
        <source>Mission ready</source>
        <translation>Mission prête</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="43"/>
        <source>Mission running</source>
        <translation>Mission en cours</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="45"/>
        <source>Mission paused</source>
        <translation>Mission en pause</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="47"/>
        <source>Manual control</source>
        <translation>Contrôle manuel</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="49"/>
        <source>RC control</source>
        <translation>Télécommande</translation>
    </message>
    <message>
        <location filename="../systems/onboardstates.h" line="51"/>
        <source>Precision landing</source>
        <translation>Atterrissage de précision</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="11"/>
        <source>Send mission</source>
        <translation>Envoyer la mission</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="13"/>
        <source>Reception request</source>
        <translation>Demande de réception</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="15"/>
        <source>Start mission</source>
        <translation>Commencer la mission</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="17"/>
        <source>Pause or Resume</source>
        <translation>Pause ou reprendre</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="19"/>
        <source>Land (precision or emergency)</source>
        <translation>Atterrissage (de précision ou d&apos;urgence)</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="21"/>
        <source>Retake Control</source>
        <translation>Reprendre le contrôle</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="23"/>
        <source>Go to rally point</source>
        <translation>Aller au point rally</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="33"/>
        <source>the drone</source>
        <translation>le drone</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="35"/>
        <location filename="../statemachine/mavlinkcommands.h" line="37"/>
        <location filename="../statemachine/mavlinkcommands.h" line="39"/>
        <location filename="../statemachine/mavlinkcommands.h" line="41"/>
        <source>the other ground station</source>
        <translation>l&apos;autre station</translation>
    </message>
    <message>
        <location filename="../statemachine/mavlinkcommands.h" line="43"/>
        <source>UNKNOWN TARGET (ID: %1)</source>
        <translation>CIBLE INCONNUE (ID: %1)</translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="47"/>
        <source>Reception request accepted by receiving station.</source>
        <translation>La demande de réception a été acceptée par la station concernée.</translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="53"/>
        <location filename="../systems/station_system.cpp" line="59"/>
        <source>Reception request error</source>
        <translation>Erreur de demande de réception</translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="53"/>
        <source>Reception request denied by receiving station.</source>
        <translation>La demande de réception a été refusée par la station concernée.</translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="53"/>
        <source>Please contact the receiving station directly to discuss the issue.</source>
        <translation>Veuillez contacter la station de réception pour elucider le problème.</translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="59"/>
        <source>Reception request message with unexpected parameters received.</source>
        <translation>Reçu demande de réception avec des paramètres inattendus.</translation>
    </message>
    <message>
        <location filename="../systems/station_system.cpp" line="65"/>
        <source>Received reception request or ack aimed at another station.</source>
        <translation>Reçu.demande de réception destinée une autre station.</translation>
    </message>
</context>
<context>
    <name>ReceptionPopup</name>
    <message>
        <location filename="../qmlcomponents/ReceptionPopup.qml" line="46"/>
        <source>Station %1 is requesting you to receive a delivery.</source>
        <translation>La station %1 souhaite vous envoyer une livraison.</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/ReceptionPopup.qml" line="58"/>
        <source>Accept</source>
        <translation>Accepter</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/ReceptionPopup.qml" line="81"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>RouteData</name>
    <message>
        <location filename="../route/routedata.cpp" line="24"/>
        <source>Select route</source>
        <translation>Choisir la route</translation>
    </message>
</context>
<context>
    <name>RouteSelectionBox</name>
    <message>
        <location filename="../qmlcomponents/RouteSelectionBox.qml" line="171"/>
        <source>ALTITUDE PROFILE</source>
        <translation>PROFIL D&apos;ALTITUDE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/RouteSelectionBox.qml" line="434"/>
        <source>CANCEL</source>
        <translation>ANNULER</translation>
    </message>
</context>
<context>
    <name>SecondPopup</name>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="62"/>
        <source>Pilot name: </source>
        <translation>Nom du pilote: </translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="99"/>
        <location filename="../qmlcomponents/SecondPopup.qml" line="374"/>
        <source>Enter pilot name</source>
        <translation>Entrer le nom du pilote</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="109"/>
        <location filename="../qmlcomponents/SecondPopup.qml" line="364"/>
        <location filename="../qmlcomponents/SecondPopup.qml" line="567"/>
        <source>Perform safety checks</source>
        <translation>Effectuer les contrôles de sécurité</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="359"/>
        <source>Safety checks</source>
        <translation>Contrôles de sécurité</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="411"/>
        <source>Tablet battery charged</source>
        <translation>Batterie de la tablette chargée</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="418"/>
        <source>Samples loaded into cargo box</source>
        <translation>Echantillon chargés dans la boîte</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="425"/>
        <source>Drone and cargo box labelled</source>
        <translation>Drone et boîte étiquetés</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="432"/>
        <source>Cargo box secured under drone</source>
        <translation>Boîte sécurisée sous le drone</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="439"/>
        <source>Drone visually inspected</source>
        <translation>Drone inspecté visuellement</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="468"/>
        <source>Authorities informed</source>
        <translation>Autorités informées</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="475"/>
        <source>Flight plan up-to-date and synchronised</source>
        <translation>Plan de vol à jour et synchronisé</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="482"/>
        <source>Take-off site cleared</source>
        <translation>Aire de décollage libre</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="581"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SecondPopup.qml" line="604"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
</context>
<context>
    <name>SettingsPopup</name>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="149"/>
        <source>Select map provider: </source>
        <translation>Choisir le fournisseur de carte: </translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="190"/>
        <source>Select map type: </source>
        <translation>Choisir le type de carte: </translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="223"/>
        <source>Confirm all</source>
        <translation>Confirmer tout</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="266"/>
        <source>CANCEL</source>
        <translation>ANNULER</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="22"/>
        <location filename="../qmlcomponents/SettingsPopup.qml" line="32"/>
        <source>CONFIRM</source>
        <translation>CONFIRMER</translation>
    </message>
</context>
<context>
    <name>StateMachine</name>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="253"/>
        <source>Command error</source>
        <translation>Erreur de commande</translation>
    </message>
    <message>
        <source>Request to send command </source>
        <translation type="vanished">Tentative d&apos;envoyer une commande </translation>
    </message>
    <message>
        <source> while still trying to send command </source>
        <translation type="vanished"> alors que toujours en train  d&apos;envoyer la commande </translation>
    </message>
    <message>
        <source>. Please wait.</source>
        <translation type="vanished">. Veuillez patienter.</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="384"/>
        <source>Command not accepted</source>
        <translation>Commande non acceptée</translation>
    </message>
    <message>
        <source>command was not accepted by </source>
        <translation type="vanished">la commande n&apos;a pas été acceptée par </translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="394"/>
        <source>Drone error: </source>
        <translation>Drone erreur: </translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="396"/>
        <source>Drone warning: </source>
        <translation>Drone avertissement: </translation>
    </message>
    <message>
        <source>Mission set to </source>
        <translation type="obsolete">Mission en cours</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="253"/>
        <source>Request to send command %1 while still trying to send command %2. Please wait.</source>
        <translation>Tentative d&apos;envoyer la commande %1  alors que toujours en train  d&apos;envoyer la commande %2. Veuillez patienter.</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="378"/>
        <source>Mission not accepted</source>
        <translation>Mission non acceptée</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="378"/>
        <source>Mission %1 (v.%2) was not accepted by the drone. Would you like to resend it?</source>
        <translation>La mission %1 (v.%2) n&apos;a pas été acceptée par le drone. Souhaitez-vous la renvoyer ?</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="378"/>
        <source>Look at drone logs for more info.</source>
        <translation>Regardez les logs du drone pour plus d&apos;infos.</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="384"/>
        <source>The &quot;%1&quot; command was not accepted by %2.</source>
        <translation>La commande %1 n&apos;a pas été acceptée par %2.</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="424"/>
        <source>Current mission has ID %1</source>
        <translation>La mission actuelle est la %1</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="503"/>
        <source>Landed error</source>
        <translation>Erreur au sol</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="503"/>
        <source>Please restart drone.</source>
        <translation>Veuillez redémarrer le drone.</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="696"/>
        <source>Multiple receiving stations connected.</source>
        <translation>Plusieurs stations sont connectées.</translation>
    </message>
    <message>
        <location filename="../statemachine/statemachine.cpp" line="696"/>
        <source>More than one receiving station is connected. The reception request might have been sent to the wrong one. Please cancel the mission, disconnect one receiving station and try again.</source>
        <translation>Plus d&apos;une station connectée. La demande de réception a pu être envoyée à la mauvaise. Veuillez annuler la mission, déconnecter les stations non concernées et réessayer.</translation>
    </message>
</context>
<context>
    <name>StationSystem</name>
    <message>
        <source>Reception request accepted by receiving station.</source>
        <translation type="vanished">La demande de réception a été acceptée par la station concernée.</translation>
    </message>
    <message>
        <source>Reception request error</source>
        <translation type="vanished">Erreur de demande de réception</translation>
    </message>
    <message>
        <source>Reception request denied by receiving station.</source>
        <translation type="vanished">La demande de réception a été refusée par la station concernée.</translation>
    </message>
    <message>
        <source>Please contact the receiving station directly to discuss the issue.</source>
        <translation type="vanished">Veuillez contacter la station de réception pour elucider le problème.</translation>
    </message>
    <message>
        <source>Reception request message with unexpected parameters received.</source>
        <translation type="vanished">Reçu demande de réception avec des paramètres inattendus.</translation>
    </message>
    <message>
        <source>Received reception request or ack aimed at another station.</source>
        <translation type="vanished">Reçu.demande de réception destinée une autre station.</translation>
    </message>
</context>
<context>
    <name>TelemetryBox</name>
    <message>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="28"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="30"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="36"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="70"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="73"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="565"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="682"/>
        <source>from start</source>
        <translation>du début</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="29"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="37"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="69"/>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="703"/>
        <source>to end</source>
        <translation>de la fin</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="134"/>
        <source>TELEMETRY</source>
        <translation>TELEMETRIE</translation>
    </message>
    <message>
        <location filename="../qmlcomponents/TelemetryBox.qml" line="406"/>
        <source>Idle</source>
        <translation>Inactif</translation>
    </message>
</context>
<context>
    <name>VirtualGamepad</name>
    <message>
        <location filename="../qmlcomponents/VirtualGamepad.qml" line="154"/>
        <source>   ARM   </source>
        <translation>   ARMER   </translation>
    </message>
    <message>
        <location filename="../qmlcomponents/VirtualGamepad.qml" line="163"/>
        <source> DISARM </source>
        <translation> DESARMER </translation>
    </message>
    <message>
        <location filename="../qmlcomponents/VirtualGamepad.qml" line="172"/>
        <source>TAKE OFF</source>
        <translation>DECOLLER</translation>
    </message>
</context>
<context>
    <name>WeatherReport</name>
    <message>
        <location filename="../weatherreport.cpp" line="46"/>
        <location filename="../weatherreport.cpp" line="62"/>
        <location filename="../weatherreport.cpp" line="99"/>
        <location filename="../weatherreport.cpp" line="124"/>
        <location filename="../weatherreport.cpp" line="181"/>
        <location filename="../weatherreport.cpp" line="244"/>
        <location filename="../weatherreport.cpp" line="256"/>
        <source>Weather query failed.</source>
        <translation>Echec de la requête météo.</translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="47"/>
        <location filename="../weatherreport.cpp" line="63"/>
        <source>Can&apos;t access location data.
Turn localization on and check
location on Google Maps.</source>
        <translation>Les informations de localisation
ne peuvent être obtenues.
Activer la localisation et
vérifier sur Google Maps.</translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="49"/>
        <location filename="../weatherreport.cpp" line="65"/>
        <location filename="../weatherreport.cpp" line="102"/>
        <location filename="../weatherreport.cpp" line="127"/>
        <location filename="../weatherreport.cpp" line="141"/>
        <location filename="../weatherreport.cpp" line="184"/>
        <location filename="../weatherreport.cpp" line="247"/>
        <location filename="../weatherreport.cpp" line="259"/>
        <location filename="../weatherreport.cpp" line="273"/>
        <source>          OK          </source>
        <translation>          OK          </translation>
    </message>
    <message>
        <source>Weather query failed: can&apos;t access location data.</source>
        <translation type="vanished">Echec de la requête météo:lLes informations de localisation ne peuvent être obtenues.</translation>
    </message>
    <message>
        <source>Checking weather forecast at coordinates: </source>
        <translation type="vanished">Vérification des données météo aux coordonnées: </translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="100"/>
        <location filename="../weatherreport.cpp" line="125"/>
        <location filename="../weatherreport.cpp" line="182"/>
        <location filename="../weatherreport.cpp" line="245"/>
        <location filename="../weatherreport.cpp" line="257"/>
        <source>Can&apos;t access weather data.
Check internet connection.</source>
        <translation>Impossible d&apos;accéder aux données météo.
Vérifez la connexion internet.</translation>
    </message>
    <message>
        <source>Weather query failed: can&apos;t access solar flare data.</source>
        <translation type="vanished">Echec de la requête météo: impossible d&apos;accéder aux données d&apos;éruptions solaires.</translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="114"/>
        <source>Risk detected</source>
        <translation>Risque détecté</translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="118"/>
        <source>None</source>
        <translation>Aucun risque détecté</translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="139"/>
        <location filename="../weatherreport.cpp" line="271"/>
        <source>Send reception request</source>
        <translation>Envoyer la demande de réception</translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="155"/>
        <source>Weather conditions and timing permit flight.</source>
        <translation>Les conditions météo et d&apos;horaire permettent de voler.</translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="157"/>
        <source>Weather conditions and/or timing prohibit flight.</source>
        <translation>Les conditions météo et/ou d&apos;horaire empêchent de voler.</translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="160"/>
        <source>Temp: </source>
        <translation>Temp: </translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="161"/>
        <source>Wind speed: </source>
        <translation>Vent: </translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="162"/>
        <source>Solar flares: </source>
        <translation>Eruptions solaires: </translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="164"/>
        <source>Time since sunset: </source>
        <translation>Coucher du soleil il y a: </translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="166"/>
        <source>Time until sunrise: </source>
        <translation>Lever du soleil dans: </translation>
    </message>
    <message>
        <location filename="../weatherreport.cpp" line="168"/>
        <source>Time until sunset: </source>
        <translation>Coucher du soleil dans: </translation>
    </message>
</context>
</TS>
