#include "logs/loggingcategories.h"
#include "translation.h"

Translator::Translator(QSharedPointer<QQmlApplicationEngine> engine) :
_translator(QSharedPointer<QTranslator>(new QTranslator(this))),
_engine(engine)
{}

Translator::~Translator()
{
    qInfo() << "~Translator()";
    _translator.clear();
    _engine.clear();
}

void Translator::changeLanguage(QString language)
{
    if((!_translator->load(QString(":/translation/translating-qml_%1.qm").arg(language))) && (language != "en"))
    {
        qWarning(generalLog()) << "Failed to load translation file (" << QString("translating-qml_%1.qm").arg(language) << "), falling back to English";
    }
    qApp->installTranslator(_translator.get());
    _engine->retranslate();

    emit languageChanged();
}
