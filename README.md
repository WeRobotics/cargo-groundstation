# CARGO GS APP DOCUMENTATION

## TABLE OF CONTENTS
1. [Brief description](#brief-description)
1. [Features](#features)
1. [Project status](#project-status)
1. [Installation](#installation)
1. [Necessary files](#necessary-files)
1. [Code notes](#code-notes)
1. [Translation](#translation)


## BRIEF DESCRIPTION
Cargo GS is a custom Android ground station application that allows the user to easily send a drone on a predefined mission, monitor it during the flight, and intervene in case of problem. It is built for medical cargo deliveries in remote and hard-to-reach areas in developing countries with the goal to be used by trained health workers without an engineering or drone pilot background. However, in the current state, a certified drone pilot should be in charge at all time for any use of the app. It is still a prototype.<br/>


## FEATURES
  * Multilingual user interface
  * Communication with the drone and other stations
  * Map and display of geographical information
  * Telemetry information
  * List of predefined routes
  * Remote mission monitoring
  * Pre-flight checklist
  * Automated safety checks (weather conditions, battery level, ...)
  * Emergency commands
  * Custom virtual remote control
  * Data logging
  * Failure handling information


## PROJECT STATUS
### Current status:
Up-to-date branches are:<br/>
  * Cargo-Comms: AudreyModifs
  * Cargo-Groundstation: master


### Re-enable disabled features:
###### Change TCP connection parameters and reconnect:
Uncomment lines as indicated by comments mentionning "tcp connection" in Buttons.qml and commander.cpp
###### Request acknowledgement from receiving station before a mission:
Uncomment lines as indicated by comments mentionning "reception request feature" in FirstPopup.qml and SecondPopup.qml
###### Add payload volume and weight to pre-flight checks:
Uncomment lines as indicated by comments mentionning "payload checks" in SecondPopup.qml
###### Add receiving pilot name to pre-flight checks:
Uncomment lines as indicated by comments mentionning "receiving pilot info" in SecondPopup.qml


### Features to fix or improve:
  * Display only the most recent version of each mission.
  * Properly delete serial connection when disconnected.
  * Improve failure handling information.
  * Complete tutorial information and add visual help (pictures or video).
  * Translation of route names in combobox.
  * Use start of route instead of tablet location for weather check.


### Features to add:
  * Log files upload.
  * Button to enable full speed in manual control mode.
  * After-flight checklist (things to charge, ...).
  * Automatically cache maps around location when wifi available.
  * Get and display ground altitude between waypoints.
  * Log take-off and landing coordinates.


## INSTALLATION

### Qt installation

###### Downloads:
  * Qt: open source version<br/>
    [Download here](https://www.qt.io/download-qt-installer?hsCtaTracking=9f6a2170-a938-42df-a8e2-a9f0b1d6cdce%7C6cb0de4f-9bb5-4778-ab02-bfb62735f3e5)
  * JDK: jdk1.8.0_191<br/>
    [Download the Java SE 8u221 jdk here](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
  * SDK: version 26.1.1<br/>
    [Download Android Studio here](https://developer.android.com/studio)
  * NDK: android-ndk-r10e (Do not use a newer version!)<br/>
    [Download from archives here](https://developer.android.com/ndk/downloads/older_releases.html)

Install the JDK and Android studio.<br/>
Extract the NDK files (place the folder high in the C: folders tree to avoid path length issues due to long file names and multiple layers of folders).<br/>
Run Qt installer (this requires a Qt account and approximately 4GB of free space).<br/>
When selecting components tick Archive and click Refresh, then select packages as in the following picture (do not take everything from Qt 5.11.2):<br/>
> ![alt text](documentation/qt_packages.PNG "Qt packages to install")

###### In git bash:
Clone Cargo-Groundstation git repository into cargo-groundstation folder and Cargo-Comms repository into communication folder:

```shell
  git clone https://gitlab.com/WeRobotics/cargo-groundstation.git cargo-groundstation
  cd cargo-groundstation
  git clone https://gitlab.com/WeRobotics/cargo-comms.git communication
  cd communication
  git checkout AudreyModifs
```

###### In Qt:
  * Tools->Options->Devices<br/>
  * Set JDK, SDK and NDK locations
  * In SDK Manager tab, install all the packages that are in the following picture (select them and click Apply):<br/>
  > ![alt text](documentation/sdk_packages.PNG "SDK packages to install")

  * Open GroundStation.pro file. If an error message pops up saying "No valid settings found", just click OK.


### TABLET SETUP
  * Connect to wifi
  * Download File Manager, DJI GO, qGroundControl, GPS Averaging, Termius, Fing, FolderSync
  * Enable developer mode
    * Open Settings->About device/tablet->Software info
    * Press 7 times on Build number
    * Developer options appear below or above About tablet/device
    * Turn dev options ON (if not ON already)
    * Turn USB debbugging ON
  * Connect to computer, allow connection, open GroundStation.pro in Qt and build Cargo GS app
  * Cargo_GS folder is created, app closes immediately
  * Transfer config and mission files to the Cargo_GS folder (missions in a missions subfolder)
  * Update config file for a new tablet (id, etc.)
  * Disconnect from computer, connect serial radio
  * App opens automatically


## NECESSARY FILES

### CONFIG FILE
A json config file named cargo_GS_config.json needs to be placed in the tablet internal storage in Internal shared storage\Cargo_GS.<br/>
It contains the following information:<br/>

```json
{
    "drones": [
        {
            "address": "0013A2004193CB48",
            "id": 50,
            "name": "M600 A"
        }
    ],
    "gsIp": "192.168.0.87",
    "gsTcpPort": "12345",
    "receivingPilotName": "Receiver",
    "relays": [
        {
            "address": "0013A20041887882",
            "id": 151,
            "latitude": 28.128195,
            "longitude": 82.874555,
            "name": "Relay A"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 152,
            "latitude": 28.075061,
            "longitude": 82.839317,
            "name": "Relay B"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 153,
            "latitude": 28.116112,
            "longitude": 82.834716,
            "name": "Relay C"
        }
    ],
    "sendingPilotName": "Sender",
    "serialBaudrate": 115200,
    "stations": [
        {
            "address": "0013A2004188786E",
            "id": 101,
            "latitude": 28.103866962190665,
            "local": false,
            "longitude": 82.85372614771416,
            "name": "Pyuthan Hospital"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 102,
            "latitude": 28.099150567937354,
            "local": true,
            "longitude": 82.85030413654863,
            "name": "Aayushma"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 103,
            "latitude": 28.13145123424568,
            "local": false,
            "longitude": 82.8947422191747,
            "name": "Dharmawati"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 104,
            "latitude": 28.106591567725562,
            "local": false,
            "longitude": 82.81636375331902,
            "name": "Addapata"
        },
        {
            "address": "FF00FF00FF00FF00",
            "id": 105,
            "latitude": 28.10387322196331,
            "local": false,
            "longitude": 82.8537390000995,
            "name": "Majhakot"
        }
    ],
    "undefined": [
        {
            "id": 200,
            "name": "Webserver 1"
        }
    ],
    "useConnectionApi": true,
    "useDjiMissions": false,
    "webserverIp": "51.75.168.103",
    "webserverTcpPort": "9929"
}
```
The local station id needs to be adapted for each tablet. Relays and stations information need to be adapted depending on the system setup, as well as connection and pilot information. The DJImission parameter needs to be false to use the waypoint commands allowing to go below the take-off altitude.

### MISSION FILES
Mission files are .plan files created using qGroundControl. They need to be named using the following format:<br/>
```
id_version_name-without-underscore.plan
Example: 4_1_Bohechio-ElCoco.plan
```
They need to be placed in the tablet internal storage in Internal shared storage\Cargo_GS\missions.



## CODE NOTES
More information about each class can be found directly in the code. This section only provides a general overview of the main parts of the code.<br/>
  * **Commander:** The core of the Cargo GS app is the Commander Singleton class. It is the link between the other main parts (the MAVLink communication and systems handler, the control interface (GUI, gamepad) and the state machine).<br/>
  * **StateMachine:** The StateMachine class is the core of the event and activity management. It manages state transitions and related events and chains of events, and triggers GUI updates through a list of signals.<br/>
  * **ControlInterface:** The ControlInterface class is in charge of the user interface. It creates and launches the application window and links the GUI QML code with the back-end C++ code.<br/>
  * **SystemManager:** The SystemManager class, derived from the MavlinkHandler class is the core of the communication management. It registers all known systems (drone, ground stations, etc.), creates and manages connections (tcp, serial, etc.), and sends, receives and distributes messages for processing.<br/>

A state machine diagram and a class diagram are available in the documentation folder. The .vpd files can be open using [VP Online](https://diagrams.visual-paradigm.com/#).<br/>
> ![alt text](documentation/Cargo_GS_StateMachine.png "Cargo GS state machine diagram")
---
> ![alt text](documentation/Cargo_GS_Class.png "Cargo GS class diagram")


## TRANSLATION
#### Make text translatable in code (done for current code)
In QML code: wrap every string to translate in qsTr()<br/>
In C++ code: wrap every string to translate in tr()<br/>
[See Qt documentation for more information and special cases.](https://doc.qt.io/qt-5/i18n-source-translation.html)

#### Create the .ts translation file
###### In command prompt:
```shell
  cd Documents
  cd cargo-groundstation
  /Qt/5.11.2/android_armv7/bin/lupdate qmlcomponents/Buttons.qml qmlcomponents/EmergencyPopup.qml qmlcomponents/FirstPopup.qml qmlcomponents/InfoAndCredits.qml qmlcomponents/InfoMessage.qml qmlcomponents/MainMap.qml qmlcomponents/MapPlugin.qml qmlcomponents/MavCmdEnum.qml qmlcomponents/PopupMessage.qml qmlcomponents/RallyPoint.qml qmlcomponents/ReceptionPopup.qml qmlcomponents/RouteSelectionBox.qml qmlcomponents/SafetyCheckBox.qml qmlcomponents/SecondPopup.qml qmlcomponents/SettingsPopup.qml qmlcomponents/TelemetryBox.qml qmlcomponents/VirtualGamepad.qml qmlcomponents/VirtualJoystick.qml qmlcomponents/VirtualGamepadButton.qml commander.cpp communication/connection_result.h gamepadmanager.cpp route/routedata.cpp statemachine/mavlinkcommands.h statemachine/statemachine.h statemachine/statemachine.cpp systems/station_system.cpp systems/onboardstates.h weatherreport.cpp -ts translation/translating-qml_fr.ts
```
*(Add all files that contain text to translate)*<br/>
*(Replace translation/translating-qml_fr.ts by the desired name for the file (one for each language))*

#### Translate all strings
###### In Qt:
  * File->Open File With...<br/>
  * Select .ts file<br/>
  * Select Qt Linguist<br/>
  * Translate each entry, mark it as done (Adapt arguments, spaces and line separations as in original text)<br/>
  * Save file<br/>

#### Create the .qm translation file
###### In command prompt:
```shell
/Qt/5.11.2/android_armv7/bin/lrelease translation/translating-qml_fr.ts
```
*(Do this for each language using the corresponding initials)*

#### Add a new language to Cargo GS app
###### In Qt:
  * Open Buttons.qml
  * In languageListModel, add a ListElement with your new language: ListElement { name: "display_name"; code: "initials_as_in_qm_file"}
  * Rebuild project