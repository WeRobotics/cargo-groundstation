#ifndef WEATHERREPORT_H
#define WEATHERREPORT_H

#include <QObject>
#include <QNetworkReply>
#include <QGeoPositionInfoSource>
#include <QSharedPointer>

/*
 * The WeatherReport class is a class managing queries to a weather data API (openweathermap)
 * as well as the organization of the information received in a coherent report.
 *
 * Launches queries with checkWeatherConditions().
 *
 * Sends final report through weatherInfoReady signal:
 * @param weatherIsGood: a boolean indicating whether the drone can fly or not.
 * @param title: a string giving a general summary of the report.
 * @param report: a multiline string containing the weather report to display.
 */
class WeatherReport : public QObject
{
    Q_OBJECT
public:
    explicit WeatherReport();
    ~WeatherReport();

    // delete copy and move constructors and assign operators
    WeatherReport(WeatherReport const &) = delete; // Copy construct
    WeatherReport(WeatherReport &&) = delete; // Move construct
    WeatherReport &operator=(WeatherReport const &) = delete; // Copy assign
    WeatherReport &operator=(WeatherReport &&) = delete; // Move assign

signals:
    void weatherInfoReady(bool weatherIsGood, QString title, QString report);

public slots:
    void checkWeatherConditions();

private slots:
    void handleWeatherData(QSharedPointer<QNetworkReply> networkReply);
    void handleSolarFlaresData(QSharedPointer<QNetworkReply> networkReply);

private:
    void reinitialiseReport();
    void writeReport();

    QSharedPointer<QNetworkAccessManager> _networkAccessManager;

    const QString _appId; // for api.openweathermap.org
    QString _lat;
    QString _lon;

    int _sunriseTime;
    int _sunsetTime;
    int _queryTime;
    QString _weatherReportTitle;
    QString _weatherReportBody;
    QString _generalConditionsReport;
    int _generalConditionsReportId;
    double _temperatureReport;
    double _windSpeedReport;
    QString _flareReport;

    bool _flareReportReady;
    bool _weatherReportReady;
    bool _flightAllowed;
};
#endif // WEATHERREPORT_H
