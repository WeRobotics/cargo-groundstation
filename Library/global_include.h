#pragma once

#define UNUSED(x) (void)(x)

#include <chrono>
#include <thread>

// Instead of using the constant from math.h or cmath we define it ourselves. This way
// we don't import all the other C math functions and make sure to use the C++ functions
// from the standard library (e.g. std::abs() instead of abs()).
#ifndef M_PI
constexpr double M_PI = 3.14159265358979323846;
#endif

#ifndef M_PI_F
constexpr float M_PI_F = float(M_PI);
#endif

#define MIN(x_, y_) ((x_) > (y_)) ? (y_) : (x_)
#define MAX(x_, y_) ((x_) > (y_)) ? (x_) : (y_)

#ifdef WINDOWS
#define STRNCPY strncpy_s
#else
#define STRNCPY strncpy
#endif

typedef std::chrono::time_point<std::chrono::steady_clock> dl_time_t;

class Time
{
public:
    Time();
    virtual ~Time();

    virtual dl_time_t steadyTime();
    double elapsedSeconds();
    double elapsedSinceSeconds(const dl_time_t &since);
    dl_time_t steadyTimeInFuture(double durationSeconds);
    void shiftSteadyTimeBy(dl_time_t &time, double offsetSeconds);

    virtual void sleepFor(std::chrono::hours h);
    virtual void sleepFor(std::chrono::minutes m);
    virtual void sleepFor(std::chrono::seconds s);
    virtual void sleepFor(std::chrono::milliseconds ms);
    virtual void sleepFor(std::chrono::microseconds us);
    virtual void sleepFor(std::chrono::nanoseconds ns);
};

class FakeTime : public Time
{
public:
    FakeTime();

    virtual ~FakeTime();
    virtual dl_time_t steadyTime() override;
    virtual void sleepFor(std::chrono::hours h) override;
    virtual void sleepFor(std::chrono::minutes m) override;
    virtual void sleepFor(std::chrono::seconds s) override;
    virtual void sleepFor(std::chrono::milliseconds ms) override;
    virtual void sleepFor(std::chrono::microseconds us) override;
    virtual void sleepFor(std::chrono::nanoseconds ns) override;

private:
    std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> _current{};
    void addOverhead();
};

double toRadiansFromDegrees(double deg);
double toDegreesFromRadians(double rad);

float toRadiansFromDegrees(float deg);
float toDegreesFromRadians(float rad);

bool areEqual(float one, float two);
bool areEqual(double one, double two);
