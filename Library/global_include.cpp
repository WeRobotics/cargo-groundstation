#include "global_include.h"

#include <cfloat>
#include <cstdint>
#include <limits>
#include <cmath>

using std::chrono::steady_clock;

Time::Time() {}
Time::~Time() {}

dl_time_t Time::steadyTime()
{
    return steady_clock::now();
}

double Time::elapsedSeconds()
{
    auto now = steadyTime().time_since_epoch();

    return (now.count()) * steady_clock::period::num /
           static_cast<double>(steady_clock::period::den);
}

double Time::elapsedSinceSeconds(const dl_time_t &since)
{
    auto now = steadyTime();

    return ((now - since).count()) * steady_clock::period::num /
           static_cast<double>(steady_clock::period::den);
}

dl_time_t Time::steadyTimeInFuture(double durationSeconds)
{
    auto now = steadyTime();
    return now + std::chrono::milliseconds(int64_t(durationSeconds * 1e3));
}

void Time::shiftSteadyTimeBy(dl_time_t &time, double offsetSeconds)
{
    time += std::chrono::milliseconds(int64_t(offsetSeconds * 1e3));
}

void Time::sleepFor(std::chrono::hours h)
{
    std::this_thread::sleep_for(h);
}

void Time::sleepFor(std::chrono::minutes m)
{
    std::this_thread::sleep_for(m);
}

void Time::sleepFor(std::chrono::seconds s)
{
    std::this_thread::sleep_for(s);
}

void Time::sleepFor(std::chrono::milliseconds ms)
{
    std::this_thread::sleep_for(ms);
}

void Time::sleepFor(std::chrono::microseconds us)
{
    std::this_thread::sleep_for(us);
}

void Time::sleepFor(std::chrono::nanoseconds ns)
{
    std::this_thread::sleep_for(ns);
}

FakeTime::FakeTime() : Time()
{
    // Start with current time so we don't start from 0.
    _current = steady_clock::now();
}

FakeTime::~FakeTime() {}

dl_time_t FakeTime::steadyTime()
{
    return _current;
}

void FakeTime::sleepFor(std::chrono::hours h)
{
    _current += h;
    addOverhead();
}

void FakeTime::sleepFor(std::chrono::minutes m)
{
    _current += m;
    addOverhead();
}

void FakeTime::sleepFor(std::chrono::seconds s)
{
    _current += s;
    addOverhead();
}

void FakeTime::sleepFor(std::chrono::milliseconds ms)
{
    _current += ms;
    addOverhead();
}

void FakeTime::sleepFor(std::chrono::microseconds us)
{
    _current += us;
    addOverhead();
}

void FakeTime::sleepFor(std::chrono::nanoseconds ns)
{
    _current += ns;
    addOverhead();
}

void FakeTime::addOverhead()
{
    _current += std::chrono::microseconds(50);
}

double toRadiansFromDegrees(double deg)
{
    return deg / 180.0 * M_PI;
}

double toDegreesFromRadians(double rad)
{
    return rad / M_PI * 180.0;
}

float toRadiansFromDegrees(float deg)
{
    return deg / 180.0f * M_PI_F;
}

float toDegreesFromRadians(float rad)
{
    return rad / M_PI_F * 180.0f;
}

bool areEqual(float one, float two)
{
    return (std::fabs(one - two) < std::numeric_limits<float>::epsilon());
}

bool areEqual(double one, double two)
{
    return (std::fabs(one - two) < std::numeric_limits<double>::epsilon());
}
