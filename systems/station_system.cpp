#include "station_system.h"
#include "../commander.h"
#include "logs/loggingcategories.h"

StationSystem::StationSystem(uint8_t systemId) :
System(systemId, SYSTEM_TYPE_STATION)
{}

StationSystem::~StationSystem()
{
    qInfo() << "~StationSystem()";
}

void StationSystem::receiveMessage(const mavlink_message_t &message)
{
    if(message.msgid == MAVLINK_MSG_ID_COMMAND_LONG)
    {
        processReceptionReadyRequestOrAck(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_COMMAND_ACK)
    {
        processCmdAck(message);
    }
}

void StationSystem::processReceptionReadyRequestOrAck(const mavlink_message_t &message)
{
    // Decode message
    mavlink_command_long_t command;
    mavlink_msg_command_long_decode(&message, &command);

    // Send ack
    mavlink_command_ack_t ack;
    mavlink_message_t messageAck;
    ack.result = MAV_RESULT::MAV_RESULT_ACCEPTED;
    ack.command = command.command;
    ack.target_system = message.sysid;
    mavlink_msg_command_ack_encode(getSystemId(), 0, &messageAck, &ack);

    if(Commander::getInstance()->forwardMessage(messageAck))
    {
        qInfo(generalLog()).nospace() << "Ack to command " << ack.command << " sent.";
    }
    else
    {
        qInfo(generalLog()).nospace() << "Ack to command " << ack.command << " not sent!";
    }

    // If command addressed to self
    if(command.target_system == getSystemId())
    {
        if(*reinterpret_cast<uint32_t*>(&(command.param2)) == 0) // Reception Request
        {
            qInfo(generalLog()).nospace() << "Received targeted reception request.";
            receptionRequestReceived(message.sysid);
        }
        else if(*reinterpret_cast<uint32_t*>(&(command.param2)) == 1) // Reception Ack
        {
            qInfo(generalLog()).nospace() << "Reception request accepted by receiving station.";
            Commander::getInstance()->infoToDisplay(QObject::tr("Reception request accepted by receiving station."));
            receptionAckReceived();
        }
        else if(*reinterpret_cast<uint32_t*>(&(command.param2)) == 2) // Reception Nack
        {
            qWarning(generalLog()).nospace() << "Reception request denied by receiving station.";
            Commander::getInstance()->messageToDisplay(QObject::tr("Reception request error"), QObject::tr("Reception request denied by receiving station."), QObject::tr("Please contact the receiving station directly to discuss the issue."));
            resetState();
        }
        else
        {
            qWarning(generalLog()).nospace() << "Reception request message with unexpected parameters received.";
            Commander::getInstance()->messageToDisplay(QObject::tr("Reception request error"), QObject::tr("Reception request message with unexpected parameters received."), "");
        }
    }
    else
    {
        qWarning(generalLog()).nospace() << "Received reception request or ack aimed at another station.";
        Commander::getInstance()->infoToDisplay(QObject::tr("Received reception request or ack aimed at another station."));
    }
}

void StationSystem::processCmdAck(const mavlink_message_t &message)
{
    mavlink_command_ack_t acknowledgment;
    mavlink_msg_command_ack_decode(&message, &acknowledgment);

    if(acknowledgment.target_system == getSystemId())
    {
        commandAcknowledged(acknowledgment.command, acknowledgment.result, message.sysid);
    }
}
