#ifndef ONBOARDSTATES_H
#define ONBOARDSTATES_H

#include <QObject>

/*
 * The OnboardState class is an enum class containing the subset of
 * the drone states that can be of interest to a ground station.
 * Warning: This must always be kept coordinated with the onboard code.
 */
enum class OnboardState
{
    BOOTING,
    IDLE,
    RALLY_POINT,
    EMERGENCY_LANDING,
    PARACHUTE,
    LANDED_ERROR,
    MISSION_RECEIVED,
    MISSION_READY,
    MISSION_RUNNING,
    MISSION_PAUSED,
    MANUAL_CONTROL,
    RC_CONTROL,
    PRECISION_LANDING
};

inline const char *onboardStateStr(const OnboardState result)
{
    switch (result)
    {
        case OnboardState::BOOTING:
            return QObject::tr("Booting").toUtf8();
        case OnboardState::IDLE:
            return QObject::tr("Idle").toUtf8();
        case OnboardState::RALLY_POINT:
            return QObject::tr("Rally point").toUtf8();
        case OnboardState::EMERGENCY_LANDING:
            return QObject::tr("Emergency landing").toUtf8();
        case OnboardState::PARACHUTE:
            return QObject::tr("Parachute").toUtf8();
        case OnboardState::LANDED_ERROR:
            return QObject::tr("Landed error").toUtf8();
        case OnboardState::MISSION_RECEIVED:
            return QObject::tr("Mission received").toUtf8();
        case OnboardState::MISSION_READY:
            return QObject::tr("Mission ready").toUtf8();
        case OnboardState::MISSION_RUNNING:
            return QObject::tr("Mission running").toUtf8();
        case OnboardState::MISSION_PAUSED:
            return QObject::tr("Mission paused").toUtf8();
        case OnboardState::MANUAL_CONTROL:
            return QObject::tr("Manual control").toUtf8();
        case OnboardState::RC_CONTROL:
            return QObject::tr("RC control").toUtf8();
        case OnboardState::PRECISION_LANDING:
            return QObject::tr("Precision landing").toUtf8();
        default:
            return "Unknown";
    }
}

#endif // ONBOARDSTATES_H
