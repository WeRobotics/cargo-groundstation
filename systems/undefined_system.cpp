#include <QDebug>
#include "undefined_system.h"

UndefinedSystem::UndefinedSystem(uint8_t systemId) :
System(systemId, SYSTEM_TYPE_UNDEFINED)
{}

UndefinedSystem::~UndefinedSystem()
{
    qInfo() << "~UndefinedSystem()";
}

void UndefinedSystem::receiveMessage(const mavlink_message_t &message)
{}
