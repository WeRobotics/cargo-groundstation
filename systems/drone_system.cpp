#include <math.h>

#include "logs/loggingcategories.h"
#include "../commander.h"
#include "drone_system.h"
#include "onboardstates.h"

#define COMPONENT_ID 0

DroneSystem::DroneSystem(uint8_t systemId) :
System(systemId, SYSTEM_TYPE_DRONE),
_batteryRemaining(0),
_currentState(0),
_missionId(0),
_timeFromMissionStart(0),
_distanceRemaining(0),
_temperature(0),
_humidity(0),
_highLatency(false)
{}

DroneSystem::~DroneSystem()
{
    qInfo() << "~DroneSystem()";
}

void DroneSystem::receiveMessage(const mavlink_message_t &message)
{
    if(message.msgid == MAVLINK_MSG_ID_GLOBAL_POSITION_INT)
    {
        processGlobalPosition(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_SYS_STATUS)
    {
        processSystemStatus(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_STATUSTEXT)
    {
        processStatusText(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_HEARTBEAT)
    {
        processState(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_PING)
    {
        processPing(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_RADIO_STATUS)
    {
        processRadio(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_ATTITUDE)
    {
        processAttitude(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_DEBUG_VECT)
    {
        processDebugVect(message);
    }
    else if(message.msgid == MAVLINK_MSG_ID_HIGH_LATENCY2)
    {
        processHighLatency2(message);
    }
}

void DroneSystem::processHighLatency2(const mavlink_message_t &message)
{
    //Discard high latency message if the drone is connected by other means
    if(!_isConnected) {
        mavlink_high_latency2_t high_latency;
        mavlink_msg_high_latency2_decode(&message, &high_latency);

        _telemetry.latitude = high_latency.latitude / 10000000.0 / M_PI * 180;
        _telemetry.longitude = high_latency.longitude / 10000000.0 / M_PI * 180;
        _telemetry.altitude = high_latency.altitude;
        _telemetry.vx = high_latency.groundspeed / 10.f;
        _telemetry.vy = 0;
        _telemetry.vz = high_latency.climb_rate / 10.f;

        _batteryRemaining = high_latency.battery;

        if(_currentState != high_latency.custom_mode)
        {
            qInfo(generalLog()).nospace() << "Current drone state: " << onboardStateStr(static_cast<OnboardState>(high_latency.custom_mode));
            droneStateChanged(high_latency.custom_mode, high_latency.custom0);
        }

        _currentState = high_latency.custom_mode;
        _missionId = high_latency.custom0;

        if(_highLatency == false) {
            connectionTypeUpdated(true);
            _highLatency = true;
        }

        attitudeUpdated(QString::number(static_cast<double>(0)), QString::number(static_cast<double>(0)), QString::number(static_cast<double>(high_latency.heading * 1.4f)));

        droneBatteryUpdated(QString::number(_batteryRemaining));

        telemetryUpdated(QString::number(_telemetry.latitude),
                         QString::number(_telemetry.longitude),
                         QString::number( round(_telemetry.altitude * 10) / 10 ),
                         QString::number(static_cast<double>(_telemetry.vx), 'f', 2),
                         QString::number(static_cast<double>(_telemetry.vy), 'f', 2),
                         QString::number(static_cast<double>(_telemetry.vz), 'f', 2));

        qInfo(telemetryLog()).nospace().noquote() << QString::number(_telemetry.latitude, 'g', 8) << ","
                                                  << QString::number(_telemetry.longitude, 'g', 8) << ","
                                                  << QString::number(_telemetry.altitude, 'g', 8) << ","
                                                  << QString::number(static_cast<double>(_telemetry.vx), 'g', 8) << ","
                                                  << QString::number(static_cast<double>(_telemetry.vy), 'g', 8) << ","
                                                  << QString::number(static_cast<double>(_telemetry.vz), 'g', 8);
    }
}

void DroneSystem::processGlobalPosition(const mavlink_message_t &message)
{
    mavlink_global_position_int_t globalPosition;
    mavlink_msg_global_position_int_decode(&message, &globalPosition);

    _telemetry.latitude = globalPosition.lat / 10000000.0 / M_PI * 180;
    _telemetry.longitude = globalPosition.lon / 10000000.0 / M_PI * 180;
    _telemetry.altitude = globalPosition.alt / 1000.0;
    _telemetry.vx = globalPosition.vx / 100.f;
    _telemetry.vy = globalPosition.vy / 100.f;
    _telemetry.vz = globalPosition.vz / 100.f;

    if(_highLatency == true) {
        connectionTypeUpdated(false);
        _highLatency = false;
    }

    telemetryUpdated(QString::number(_telemetry.latitude),
                     QString::number(_telemetry.longitude),
                     QString::number( round(_telemetry.altitude * 10) / 10 ),
                     QString::number(static_cast<double>(_telemetry.vx), 'f', 2),
                     QString::number(static_cast<double>(_telemetry.vy), 'f', 2),
                     QString::number(static_cast<double>(_telemetry.vz), 'f', 2));

    qInfo(telemetryLog()).nospace().noquote() << QString::number(_telemetry.latitude, 'g', 8) << ","
                                              << QString::number(_telemetry.longitude, 'g', 8) << ","
                                              << QString::number(_telemetry.altitude, 'g', 8) << ","
                                              << QString::number(static_cast<double>(_telemetry.vx), 'g', 8) << ","
                                              << QString::number(static_cast<double>(_telemetry.vy), 'g', 8) << ","
                                              << QString::number(static_cast<double>(_telemetry.vz), 'g', 8);
}

void DroneSystem::getPosition(float &longitude, float &latitude) const
{
    longitude = _telemetry.longitude;
    latitude = _telemetry.latitude;
}

void DroneSystem::getPosition(QGeoCoordinate &position) const
{
    position.setLatitude(_telemetry.latitude);
    position.setLongitude(_telemetry.longitude);
    position.setAltitude(_telemetry.altitude);
}

void DroneSystem::processSystemStatus(const mavlink_message_t &message)
{
    mavlink_sys_status_t status;
    mavlink_msg_sys_status_decode(&message, &status);

    _batteryRemaining = status.battery_remaining;

    droneBatteryUpdated(QString::number(_batteryRemaining));
}

void DroneSystem::processState(const mavlink_message_t &message)
{
    mavlink_heartbeat_t heartbeat;
    mavlink_msg_heartbeat_decode(&message, &heartbeat);

    if(_currentState != heartbeat.custom_mode)
    {
        qInfo(generalLog()).nospace() << "Current drone state: " << onboardStateStr(static_cast<OnboardState>(heartbeat.custom_mode));
        droneStateChanged(heartbeat.custom_mode, heartbeat.base_mode);
    }

    _currentState = heartbeat.custom_mode;
    _missionId = heartbeat.base_mode;
}

void DroneSystem::processStatusText(const mavlink_message_t &message)
{
    mavlink_statustext_t statusText;
    mavlink_msg_statustext_decode(&message, &statusText);

    qInfo(generalLog()).nospace() << "Drone error message: " << statusText.text << ".";
    forwardDroneErrorMessage(statusText.severity, statusText.text);
}

void DroneSystem::processPing(const mavlink_message_t &message)
{
    mavlink_ping_t pingData;
    mavlink_msg_ping_decode(&message, &pingData);
    mavlink_message_t messageBack;

    pingData.target_system = message.sysid;
    pingData.target_component = message.compid;
    mavlink_msg_ping_encode(Commander::getInstance()->getLocalStationId(), COMPONENT_ID, &messageBack, &pingData);
    Commander::getInstance()->forwardMessage(messageBack);
}

void DroneSystem::processRadio(const mavlink_message_t &message)
{
    mavlink_radio_status_t radioData;
    mavlink_msg_radio_status_decode(&message, &radioData);

    forwardRssi(radioData.rssi, radioData.remrssi);
}

void DroneSystem::processAttitude(const mavlink_message_t &message)
{
    mavlink_attitude_t attitude;
    mavlink_msg_attitude_decode(&message, &attitude);

    attitudeUpdated(QString::number(static_cast<double>(attitude.roll)), QString::number(static_cast<double>(attitude.pitch)), QString::number(static_cast<double>(attitude.yaw)));
}

void DroneSystem::processDebugVect(const mavlink_message_t &message)
{
    mavlink_debug_vect_t debugVect;
    mavlink_msg_debug_vect_decode(&message, &debugVect);

    _timeFromMissionStart = debugVect.time_usec / 1000000;
    _distanceRemaining = debugVect.x;
    _temperature = debugVect.y;
    _humidity = debugVect.z;

    droneTimeDistanceTemperatureHumidityUpdated(_timeFromMissionStart, _distanceRemaining, _temperature, _humidity);

    QString niceTime = QTime(0, 0, 0, 0).addSecs(_timeFromMissionStart).toString("mm:ss");//(_time_spent * 1000).toISOString().substr(11, 8);
    qInfo(sensorLog()).nospace().noquote() << _timeFromMissionStart << ","
                                              << niceTime << ","
                                              << _distanceRemaining << ","
                                              << _temperature << ","
                                              << _humidity << ",";
}
