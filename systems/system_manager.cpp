#include <QFile>
#include <QDir>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "logs/loggingcategories.h"
#include "../commander.h"
#include "system_manager.h"
#include "drone_system.h"
#include "relay_system.h"
#include "station_system.h"
#include "undefined_system.h"

#define SERIAL_BAUDRATE 115200

using namespace std::placeholders;

SystemManager::SystemManager(uint8_t systemId, uint8_t componentId) :
MavlinkHandler(systemId, componentId),
_useDjiMissions(false),
_useConnectionApi(true),
_serialBaudrate(SERIAL_BAUDRATE),
_sendingPilotName(""),
_receivingPilotName(""),
_groundstationIp("192.168."),
_groundstationTcpPort("12345"),
_webserverIp("192.168.0.1"),
_webserverTcpPort("12345")
{}

SystemManager::~SystemManager()
{
    qInfo() << "~SystemManager()";
    unregisterAllMavlinkMessageHandlers(this);
}

bool SystemManager::init()
{
    qInfo(generalLog()) << "Initialising communication manager.";

    // Create webserver system
    _systems[200] = std::make_shared<UndefinedSystem>(200);
    connect(_systems[200].get(), SIGNAL(systemConnected(bool, uint8_t)), Commander::getInstance(), SIGNAL(webserverConnected(bool)));

    // Create ground station systems and get config parameters
    if(configStationsAndParameters())
    {
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_COMMAND_LONG, std::bind(&StationSystem::receiveMessage, _localStation, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_COMMAND_ACK, std::bind(&StationSystem::receiveMessage, _localStation, std::placeholders::_1), this);
        connect(_localStation.get(), SIGNAL(commandAcknowledged(uint16_t, uint8_t, uint8_t)), Commander::getInstance(), SIGNAL(commandAcknowledged(uint16_t, uint8_t, uint8_t)));

        return true;
    }
    else
    {
        return false;
    }
}

bool SystemManager::configStationsAndParameters()
{
    // Open config file and extract json document
    QDir generalDirectory(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation));
    generalDirectory.mkpath("Cargo_GS");

    QString path = generalDirectory.path().append("/Cargo_GS");

    QFile configFile(path.append("/cargo_GS_config.json"));
    if(!configFile.open(QFile::ReadOnly))
    {
        qCritical(generalLog()) << "Could not open or find config file.";
        return false;
    }

    QJsonDocument configFileJson = QJsonDocument::fromJson(configFile.readAll());
    configFile.close();

    // Extract config data from json document:
    // - missions type
    // - connection api or not
    // - serial baudrate
    // - pilot names
    // - GS and webserver IPs and ports
    // - stations IDs and local or not
    // - relays addresses, locations and names
    if (configFileJson.isObject())
    {
        QJsonObject jsonObject = configFileJson.object();
        QJsonObject tempJsonObject;
        QJsonValue jsonValue;

        if (jsonObject.contains(QStringLiteral("useDjiMissions")))
        {
            _useDjiMissions = jsonObject.value(QStringLiteral("useDjiMissions")).toBool();
            if(_useDjiMissions)
            {
                qInfo(generalLog()) << "Mission type set to DJI missions.";
            }
            else
            {
                qInfo(generalLog()) << "Mission type set to waypoints missions.";
            }
        }
        else
        {
            qWarning(generalLog()) << "Mission type not specified in config file. Using waypoints missions by default.";
        }

        if (jsonObject.contains(QStringLiteral("useConnectionApi")))
        {
            _useConnectionApi = jsonObject.value(QStringLiteral("useConnectionApi")).toBool();
            if(_useConnectionApi)
            {
                qInfo(generalLog()) << "Connection set to Api.";
            }
            else
            {
                qInfo(generalLog()) << "Connection set to normal (not Api).";
            }
        }
        else
        {
            qWarning(generalLog()) << "Connection type not specified in config file. Using Api type by default.";
        }

        if (jsonObject.contains(QStringLiteral("serialBaudrate")))
        {
            _serialBaudrate = jsonObject.value(QStringLiteral("serialBaudrate")).toInt();
        }
        else
        {
            qWarning(generalLog()) << "Serial baudrate not specified in config file. Using 115200 by default.";
        }

        if (jsonObject.contains(QStringLiteral("sendingPilotName")))
        {
            _sendingPilotName = jsonObject.value(QStringLiteral("sendingPilotName")).toString();
        }
        else
        {
            qWarning(generalLog()) << "Sending pilot name not specified in config file.";
        }

        if (jsonObject.contains(QStringLiteral("receivingPilotName")))
        {
            _receivingPilotName = jsonObject.value(QStringLiteral("receivingPilotName")).toString();
        }
        else
        {
            qWarning(generalLog()) << "Receiving pilot name not specified in config file.";
        }

        if (jsonObject.contains(QStringLiteral("gsIp")))
        {
            //_groundstationIp = jsonObject.value(QStringLiteral("gsIp")).toString();
        }
        else
        {
            qWarning(generalLog()) << "Groundstation IP not specified in config file.";
        }

        if (jsonObject.contains(QStringLiteral("gsTcpPort")))
        {
            //_groundstationTcpPort = jsonObject.value(QStringLiteral("gsTcpPort")).toString();
        }
        else
        {
            qWarning(generalLog()) << "Groundstation tcp port not specified in config file.";
        }

        if (jsonObject.contains(QStringLiteral("webserverIp")))
        {
            _webserverIp = jsonObject.value(QStringLiteral("webserverIp")).toString();
        }
        else
        {
            qWarning(generalLog()) << "Webserver IP not specified in config file.";
        }

        if (jsonObject.contains(QStringLiteral("webserverTcpPort")))
        {
            _webserverTcpPort = jsonObject.value(QStringLiteral("webserverTcpPort")).toString();
        }
        else
        {
            qWarning(generalLog()) << "Webserver tcp port not specified in config file.";
        }

        if (jsonObject.contains(QStringLiteral("stations")))
        {
            jsonValue = jsonObject.value(QStringLiteral("stations"));
            QJsonArray stationsArray = jsonValue.toArray();

            if(stationsArray.size())
            {
                for(int i = 0; i < stationsArray.size(); i++)
                {
                    jsonValue = stationsArray.at(i);
                    tempJsonObject = jsonValue.toObject();

                    if(tempJsonObject.contains(QStringLiteral("id")))
                    {
                        uint8_t currentId = static_cast<uint8_t>(tempJsonObject.value(QStringLiteral("id")).toInt());

                        if(tempJsonObject.contains(QStringLiteral("local")))
                        {
                            if(tempJsonObject.value(QStringLiteral("local")).toBool())
                            {
                                if(getLocalSystemId())
                                {
                                    qCritical(generalLog()) << "Config file contains more than one local station.";
                                    return false;
                                }
                                setLocalSystemId(currentId);
                                _localStation = std::make_shared<StationSystem>(currentId);
                                connect(_localStation.get(), SIGNAL(resetState()), Commander::getInstance(), SIGNAL(resetState()));
                                connect(_localStation.get(), SIGNAL(receptionAckReceived()), Commander::getInstance(), SIGNAL(receptionAckReceived()));
                                connect(_localStation.get(), SIGNAL(receptionRequestReceived(int)), Commander::getInstance(), SIGNAL(receptionRequestReceived(int)));
                            }
                        }
                        _systems[currentId] = std::make_shared<StationSystem>(currentId);
                        connect(_systems[currentId].get(), SIGNAL(systemConnected(bool, uint8_t)), Commander::getInstance(), SIGNAL(groundstationConnected(bool, uint8_t)));
                    }
                }
                if(!getLocalSystemId())
                {
                    qCritical(generalLog()) << "Config file contains no local station.";
                    return false;
                }
            }
            else
            {
                qCritical(generalLog()) << "Config file contains no station ('stations' keyword missing or misplaced).";
                return false;
            }
        }
        else
        {
            qCritical(generalLog()) << "Config file contains no station ('stations' keyword missing or misplaced).";
            return false;
        }

        if (jsonObject.contains(QStringLiteral("relays")))
        {
            jsonValue = jsonObject.value(QStringLiteral("relays"));
            QJsonArray stationsArray = jsonValue.toArray();

            if(stationsArray.size())
            {
                for(int i = 0; i < stationsArray.size(); i++)
                {
                    jsonValue = stationsArray.at(i);
                    tempJsonObject = jsonValue.toObject();

                    uint8_t id = 0;
                    QString name = "Unknown";
                    uint64_t address = 0x0000000000000000;
                    double lat = 0, lon = 0;
                    QGeoCoordinate coord;

                    if(tempJsonObject.contains(QStringLiteral("id")))
                    {
                        id = static_cast<uint8_t>(tempJsonObject.value(QStringLiteral("id")).toInt());
                    }
                    else
                    {
                        qWarning(generalLog()) << "Relay station without id (in config file) is ignored.";
                        continue;
                    }
                    if(tempJsonObject.contains(QStringLiteral("name")))
                    {
                        name = tempJsonObject.value(QStringLiteral("name")).toString();
                    }
                    if(tempJsonObject.contains(QStringLiteral("address")))
                    {
                        bool ok;
                        address = tempJsonObject.value(QStringLiteral("address")).toString().toULongLong(&ok, 16);
                        if (!ok)
                        {
                            qInfo(generalLog()) << "Radio hex address parsing from config file failed.";
                        }
                    }
                    if((tempJsonObject.contains(QStringLiteral("latitude"))) && (tempJsonObject.contains(QStringLiteral("longitude"))))
                    {
                        lat = tempJsonObject.value(QStringLiteral("latitude")).toDouble();
                        lon = tempJsonObject.value(QStringLiteral("longitude")).toDouble();
                    }

                    coord = QGeoCoordinate(lat, lon);
                    _systems[id] = std::make_shared<RelaySystem>(id, name, address, coord);
                }
            }
        }
    }
    else
    {
        qCritical(generalLog()) << "Config file is empty or unreadable (JSON format required).";
        return false;
    }

    return true;
}

void SystemManager::receiveMessage(const mavlink_message_t &message)
{
    if(message.msgid == MAVLINK_MSG_ID_HEARTBEAT)
    {
        mavlink_heartbeat_t heartbeat;
        mavlink_msg_heartbeat_decode(&message, &heartbeat);

        if(heartbeat.type == MAV_TYPE_ONBOARD_CONTROLLER)
        {
            initDrone(message.sysid);
        }
        else if(heartbeat.type == MAV_TYPE_GCS)
        {
            qWarning(generalLog()) << "Heartbeat from an unknown station received.";
        }
        else if(heartbeat.type == MAV_TYPE_ANTENNA_TRACKER)
        {
            qWarning(generalLog()) << "Heartbeat from an unknown antenna received.";
        }
        else
        {
            qWarning(generalLog()) << "Heartbeat from an unknown system received.";
        }
    }
    else if(message.msgid == MAVLINK_MSG_ID_HIGH_LATENCY2) {
        if(systemExistAndConnected(message.sysid))
        {
            Commander::getInstance()->_timeoutHandler.refresh(*_systems[message.sysid]->getHbCookieHighLatency());
        }
        else if(systemExist(message.sysid))
        {
            _systems[message.sysid]->setConnectedHighLatency();
            Commander::getInstance()->_timeoutHandler.add(std::bind(&System::hbTimeoutCbHighLatency, _systems[message.sysid]), 25.0f, _systems[message.sysid]->getHbCookieHighLatency());
        }
        else {
            mavlink_high_latency2_t high_latency;
            mavlink_msg_high_latency2_decode(&message, &high_latency);

            if(high_latency.type == MAV_TYPE_ONBOARD_CONTROLLER)
            {
                initDrone(message.sysid);
            }
        }

        MavlinkHandler::receiveMessage(message);
    }
    else
    {
        MavlinkHandler::receiveMessage(message);
    }
}

void SystemManager::initDrone(uint8_t sysid) {
    if(_drone != nullptr)
    {
        // HB received from a second drone
        qWarning(generalLog()) << "Heartbeat from another drone received. Only one drone should be connected at the same time.";
        Commander::getInstance()->infoToDisplay("HB from a second drone received.");
    }
    else
    {
        // Drone instance created when first HB received (no need to know in advance which drone will be used)
        _drone = std::make_shared<DroneSystem>(sysid);
        _systems[sysid] = _drone;
        Commander::getInstance()->setDroneId(sysid);

        qInfo(generalLog()) << "Drone instance created !.";

        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_GLOBAL_POSITION_INT, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_SYS_STATUS, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_STATUSTEXT, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_HEARTBEAT, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_PING, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_RADIO_STATUS, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_ATTITUDE, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_DEBUG_VECT, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);
        this->registerMavlinkMessageHandler(MAVLINK_MSG_ID_HIGH_LATENCY2, std::bind(&DroneSystem::receiveMessage, _drone, std::placeholders::_1), this);

        connect(_drone.get(), SIGNAL(systemConnected(bool, uint8_t)), Commander::getInstance(), SIGNAL(droneConnected(bool)));
        connect(_drone.get(), SIGNAL(connectionTypeUpdated(bool, uint8_t)), Commander::getInstance(), SIGNAL(connectionTypeUpdated(bool)));
        connect(_drone.get(), SIGNAL(droneStateChanged(uint32_t, uint8_t)), Commander::getInstance(), SIGNAL(droneStateChanged(uint32_t, uint8_t)));

        connect(_drone.get(), SIGNAL(telemetryUpdated(QString, QString, QString, QString, QString, QString)), Commander::getInstance(), SIGNAL(telemetryUpdated(QString, QString, QString, QString, QString, QString)));
        connect(_drone.get(), SIGNAL(attitudeUpdated(QString, QString, QString)), Commander::getInstance(), SIGNAL(attitudeUpdated(QString, QString, QString)));
        connect(_drone.get(), SIGNAL(droneBatteryUpdated(QString)), Commander::getInstance(), SIGNAL(droneBatteryUpdated(QString)));
        connect(_drone.get(), SIGNAL(droneTimeDistanceTemperatureHumidityUpdated(uint64_t, float, float, float)), Commander::getInstance(), SIGNAL(droneTimeDistanceTemperatureHumidityUpdated(uint64_t, float, float, float)));
        connect(_drone.get(), SIGNAL(forwardDroneErrorMessage(uint8_t, QString)), Commander::getInstance(), SIGNAL(forwardDroneErrorMessage(uint8_t, QString)));
        connect(_drone.get(), SIGNAL(forwardRssi(uint8_t, uint8_t)), Commander::getInstance(), SIGNAL(forwardRssi(uint8_t, uint8_t)));
        connect(_drone.get(), SIGNAL(debugDataToDisplay(QString, QString, QString)), Commander::getInstance(), SIGNAL(debugDataToDisplay(QString, QString, QString)));
    }
}

bool SystemManager::sendMessage(const mavlink_message_t &message)
{
    return MavlinkHandler::sendMessage(message);
}

void SystemManager::sendHeartbeat()
{
    mavlink_message_t message;

    mavlink_msg_heartbeat_pack(getLocalSystemId(),
                               getLocalComponentId(),
                               &message,
                               MAV_TYPE_GCS,
                               MAV_AUTOPILOT_INVALID,
                               0,
                               MAV_STATE_STANDBY,
                               0);

    sendMessage(message);
}

bool SystemManager::isDroneConnected() const
{
    return _drone->isConnected();
}

bool SystemManager::getDronePosition(float &longitude, float &latitude) const
{
    if(_drone->isConnected()) {
        _drone->getPosition(longitude, latitude);
        return true;
    }
    else {
        return false;
    }
}

bool SystemManager::getDronePosition(QGeoCoordinate &position) const
{
    if(_drone->isConnected()) {
        _drone->getPosition(position);
        return true;
    }
    else {
        return false;
    }
}

bool SystemManager::isMissionTypeDji() const
{
    return _useDjiMissions;
}

bool SystemManager::isConnectionApi() const
{
    return _useConnectionApi;
}

int SystemManager::getSerialBaudrate() const
{
    return _serialBaudrate;
}

QString SystemManager::getSendingPilotName() const
{
    return _sendingPilotName;
}

QString SystemManager::getReceivingPilotName() const
{
    return _receivingPilotName;
}

QString SystemManager::getGroundstationIp() const
{
    return _groundstationIp;
}

QString SystemManager::getGroundstationTcpPort() const
{
    return _groundstationTcpPort;
}

QString SystemManager::getWebserverIp() const
{
    return _webserverIp;
}

QString SystemManager::getWebserverTcpPort() const
{
    return _webserverTcpPort;
}

QVariantList SystemManager::getRelayStationList() const
{
    QVariantList relayStationList;

    for(const auto &i : _systems)
    {
        if(i.second->getSystemType() == SYSTEM_TYPE_RELAY)
        {
            relayStationList.append(QVariant::fromValue(i.second.get()));
        }
    }

    return relayStationList;
}

void SystemManager::setSendingPilotName(QString name)
{
    _sendingPilotName = name;
}

void SystemManager::setReceivingPilotName(QString name)
{
    _receivingPilotName = name;
}

void SystemManager::setGroundstationIp(QString ip)
{
    _groundstationIp = ip;
}

void SystemManager::setGroundstationTcpPort(QString port)
{
    _groundstationTcpPort = port;
}

void SystemManager::setWebserverIp(QString ip)
{
    _webserverIp = ip;
}

void SystemManager::setWebserverTcpPort(QString port)
{
    _webserverTcpPort = port;
}
