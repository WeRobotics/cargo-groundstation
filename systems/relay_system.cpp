#include <QDebug>
#include "relay_system.h"

RelaySystem::RelaySystem(const uint8_t systemId, const QString name, const uint64_t address, const QGeoCoordinate location) :
System(systemId, SYSTEM_TYPE_RELAY),
_name(name),
_address(address),
_location(location)
{}

RelaySystem::~RelaySystem()
{
    qInfo() << "~RelaySystem()";
}

void RelaySystem::receiveMessage(const mavlink_message_t &message)
{}

QString RelaySystem::name() const
{
    return _name;
}

uint64_t RelaySystem::address() const
{
    return _address;
}

QGeoCoordinate RelaySystem::location() const
{
    return _location;
}

