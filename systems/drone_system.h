#ifndef DRONE_SYSTEM_H
#define DRONE_SYSTEM_H

#include "../communication/system.h"

#include <QGeoCoordinate>

class Commander;

/*
 * The DroneSystem class is a class managing systems that send
 * a MAV_TYPE_ONBOARD_CONTROLLER MAVLink type in their heartbeat.
 * It is derived from the System class, which manages a single system
 * including its id, status, type, messages and signals.
 *
 * Processes the following message types:
 * - MAVLINK_MSG_ID_GLOBAL_POSITION_INT: drone lat, lon, alt, vNorth, vEast, vUp (display in GUI).
 * - MAVLINK_MSG_ID_SYS_STATUS: drone battery level (display in GUI).
 * - MAVLINK_MSG_ID_STATUSTEXT: drone error/warning message (display in GUI).
 * - MAVLINK_MSG_ID_HEARTBEAT: drone state, mission id (display in GUI, fire state machine).
 * - MAVLINK_MSG_ID_PING: drone ping (send back).
 * - MAVLINK_MSG_ID_RADIO_STATUS: rssi.
 * Warning: MAVLINK_MSG_ID_RADIO_STATUS currently unused (with serial API connection).
 * - MAVLINK_MSG_ID_ATTITUDE: drone roll, pitch, yaw.
 * Warning: MAVLINK_MSG_ID_ATTITUDE currently unused.
 * - MAVLINK_MSG_ID_DEBUG_VECT: time from mission start, distance remaining, raspi temperature (display in GUI).
 * Warning: temperature should be changed to cargo temp + humidity.
 */
class DroneSystem: public System
{
public:
    DroneSystem(uint8_t systemId);
	~DroneSystem();

	void receiveMessage(const mavlink_message_t &message);

    void getPosition(float &longitude, float &latitude) const;
    void getPosition(QGeoCoordinate &position) const;

private:
    void processGlobalPosition(const mavlink_message_t &message);
    void processSystemStatus(const mavlink_message_t &message);
    void processStatusText(const mavlink_message_t &message);
    void processState(const mavlink_message_t &message);
    void processPing(const mavlink_message_t &message);
    void processRadio(const mavlink_message_t &message);
    void processAttitude(const mavlink_message_t &message);
    void processDebugVect(const mavlink_message_t &message);
    void processHighLatency2(const mavlink_message_t &message);

    struct TelemetryData
    {
        double latitude;
        double longitude;
        double altitude;
		float vx;
		float vy;
		float vz;
	};

    struct TelemetryData _telemetry;
    uint8_t _batteryRemaining;
    uint32_t _currentState;
    uint8_t _missionId;
    uint64_t _timeFromMissionStart;
    float _distanceRemaining;
    float _temperature;
    float _humidity;
    bool _highLatency;
};
#endif // DRONE_SYSTEM_H
