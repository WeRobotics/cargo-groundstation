#ifndef SYSTEM_MANAGER_H
#define SYSTEM_MANAGER_H

#include <list>
#include <mutex>
#include <atomic>
#include <QGeoCoordinate>

#include "unistd.h"

#include "../communication/mavlink_handler.h"
#include "drone_system.h"
#include "station_system.h"

/*
 * The SystemManager class is a class managing all connected systems.
 * It is derived from the MavlinkHandler class which manages the communication.
 *
 * It registers all known systems (drone, ground stations, etc.) in the _system map container.
 *
 * It reads and registers configuration information from the tablet config file (cargo_GS_config.json):
 * @var _useDjiMissions: a boolean defining if basic DJI or waypoint-by-waypoint mission type is used.
 * Warning: _useDjiMissions currently unused. Send instead of mission version (param2 in MAV_CMD_USER_1 message),
 *          see StateMachine::ST_ThirdChecks(). Must be interpreted correctly by onboard code.
 * @var _useConnectionApi: a boolean defining if API or transparent mode is used (serial connection),
 *                         see https://www.digi.com/resources/documentation/digidocs/pdfs/90001506.pdf.
 * @var _serialBaudrate: an integer defining the serial baud rate.
 * @var _sendingPilotName: a string containing the name of the pilot at the sending station (can later be changed in GUI).
 * @var _receivingPilotName: a string containing the name of the pilot at the receiving station (can later be changed in GUI).
 * Warning: _receivingPilotName currently unused. See README.md to re-activate functionality.
 * @var _groundstationIp: a string containing the direct tcp connection ip.
 * @var _groundstationTcpPort: a string containing the direct tcp connection port.
 * Warning: _groundstationIp and _groundstationIp currently unused. See README.md to re-activate functionality.
 * @var _webserverIp:  a string containing the webserver tcp connection ip.
 * @var _webserverTcpPort: a string containing the webserver tcp connection port.
 */
class SystemManager : public MavlinkHandler
{
public:
    SystemManager(uint8_t systemId, uint8_t componentId);
    ~SystemManager();

    bool init();

    void receiveMessage(const mavlink_message_t &message);
    bool sendMessage(const mavlink_message_t &message);
    void sendHeartbeat();

    bool isDroneConnected() const;
    bool getDronePosition(float &longitude, float &latitude) const;
    bool getDronePosition(QGeoCoordinate &position) const;
    bool isMissionTypeDji() const;
    bool isConnectionApi() const;
    int getSerialBaudrate() const;
    QString getSendingPilotName() const;
    QString getReceivingPilotName() const;
    QString getGroundstationIp() const;
    QString getGroundstationTcpPort() const;
    QString getWebserverIp() const;
    QString getWebserverTcpPort() const;
    QVariantList getRelayStationList() const;

    void setSendingPilotName(QString name);
    void setReceivingPilotName(QString name);
    void setGroundstationIp(QString ip);
    void setGroundstationTcpPort(QString port);
    void setWebserverIp(QString ip);
    void setWebserverTcpPort(QString port);

private:
    void initDrone(uint8_t sysid);
    bool configStationsAndParameters();
    std::shared_ptr<DroneSystem> _drone;
    std::shared_ptr<StationSystem> _localStation;

    bool _useDjiMissions;
    bool _useConnectionApi;
    int _serialBaudrate;
    QString _sendingPilotName;
    QString _receivingPilotName;
    QString _groundstationIp;
    QString _groundstationTcpPort;
    QString _webserverIp;
    QString _webserverTcpPort;
};

#endif // SYSTEM_MANAGER_H
