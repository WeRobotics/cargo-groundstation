#ifndef UNDEFINED_SYSTEM_H
#define UNDEFINED_SYSTEM_H

#include "../communication/system.h"

/*
 * The UndefinedSystem class is a class managing systems that do not send
 * a recognized MAVLink type in their heartbeat.
 * It is derived from the System class, which manages a single system
 * including its id, status, type, messages and signals.
 *
 * Currently used for the webserver.
 */
class UndefinedSystem : public System
{
public:
    UndefinedSystem(uint8_t systemId);
    ~UndefinedSystem();

    void receiveMessage(const mavlink_message_t &message);
};

#endif // UNDEFINED_SYSTEM_H
