#ifndef STATION_SYSTEM_H
#define STATION_SYSTEM_H

#include "../communication/system.h"

class Commander;

/*
 * The StationSystem class is a class managing systems that send
 * a MAV_TYPE_GCS MAVLink type in their heartbeat.
 * It is derived from the System class, which manages a single system
 * including its id, status, type, messages and signals.
 *
 * Processes the following message types:
 * - MAVLINK_MSG_ID_COMMAND_LONG for a reception request (from sending station) or confirmation (from receiving station).
 * - MAVLINK_MSG_ID_COMMAND_ACK for the acknowledge of a mavlink message.
 */
class StationSystem: public System
{

public:
    StationSystem(uint8_t systemId);
	~StationSystem();

	void receiveMessage(const mavlink_message_t &message);

private:
    void processReceptionReadyRequestOrAck(const mavlink_message_t &message);
    void processCmdAck(const mavlink_message_t &message);
};

#endif // STATION_SYSTEM_H
