#ifndef RELAY_SYSTEM_H
#define RELAY_SYSTEM_H

#include <QGeoCoordinate>

#include "../communication/system.h"

/*
 * The RelaySystem class is a class managing systems that send
 * a ?? (antenna?) MAVLink type in their heartbeat.
 * It is derived from the System class, which manages a single system
 * including its id, status, type, messages and signals.
 *
 * It contains specific information retrieved by the SystemManager from
 * the tablet config file (cargo_GS_config.json) for display in the GUI:
 * @var _name: a string containing the name of the relay station.
 * @var _address: an unsigned long long containing the hex address of the radio.
 * @var _location: a QGeoCoordinate containing the geographical position of the relay station.
 */
class RelaySystem : public System
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(uint64_t address READ address NOTIFY addressChanged)
    Q_PROPERTY(QGeoCoordinate location READ location NOTIFY locationChanged)

public:
    RelaySystem(const uint8_t systemId, const QString name, const uint64_t address, const QGeoCoordinate location);
    ~RelaySystem();

    void receiveMessage(const mavlink_message_t &message);

    QString name() const;
    uint64_t address() const;
    QGeoCoordinate location() const;

signals:
    void nameChanged();
    void addressChanged();
    void locationChanged();

private:
    const QString _name;
    const uint64_t _address;
    const QGeoCoordinate _location;
};

#endif // RELAY_SYSTEM_H
