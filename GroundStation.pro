QT += qml
QT += gamepad
#android{QT += androidextras}
QT += location
QT += core gui quick location
QT += quickwidgets
QT += core

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 console
CONFIG += mobility
MOBILITY += systeminfo
symbian:TARGET.CAPABILITY = ReadDeviceData

QMAKE_CXXFLAGS += -std=c++11
#QMAKE_CXXFLAGS += -stdlib=libc++

SOURCES += main.cpp \
    interface/missionmanager.cpp \
    lockhelper.cpp \
    interface/controlinterface.cpp \
    commander.cpp \
    weatherreport.cpp \
    Library/global_include.cpp \
    timer/call_every_handler.cpp \
    timer/timeout_handler.cpp \
    systems/drone_system.cpp \
    systems/station_system.cpp \
    systems/system_manager.cpp \
    statemachine/statemachine.cpp \
    interface/routedata.cpp \
    logs/loggingcategories.cpp \
    translation/translation.cpp \
    gamepadmanager.cpp \
    systems/undefined_system.cpp \
    systems/relay_system.cpp

HEADERS += \
    interface/missionmanager.h \
    lockhelper.h \
    interface/controlinterface.h \
    commander.h \
    weatherreport.h \
    Library/global_include.h \
    timer/call_every_handler.h \
    timer/timeout_handler.h \
    systems/drone_system.h \
    systems/station_system.h \
    systems/system_manager.h \
    statemachine/statemachine.h \
    statemachine/eventdata.h \
    interface/routedata.h \
    systems/onboardstates.h \
    logs/loggingcategories.h \
    statemachine/mavlinkcommands.h \
    translation/translation.h \
    gamepadmanager.h \
    systems/undefined_system.h \
    systems/relay_system.h

include(communication/mavlinklib.pri)
include(openSSL/android-openssl.pri)
android
{
    include(android/android.pri)
}

DEFINES += QT_DEPRECATED_WARNINGS

TEMPLATE = app
TARGET = GroundStation
INSTALLS += target

RESOURCES += \
    qml.qrc \

TARGET.CAPABILITY += SwEvent

lupdate_only{
DISTFILES = qmlcomponents/*.qml
}

TRANSLATIONS = translation/translating-qml_np.ts

DISTFILES += \
    main.qml \
    uml.qmodel
