#include <QDebug>
#include <QtGamepad>
#include <QQmlContext>
#include <QQmlComponent>
#ifdef __ANDROID__
#include <QtAndroid>
#include <QAndroidJniEnvironment>
#include "qserialport.h"
#include "qserialportinfo.h"
//#else
//#include <QSerialPort>
//#include <QSerialPortInfo>
#endif

#include "commander.h"
#include "communication/mavlink_handler.h"
#include "lockhelper.h"
#include "gamepadmanager.h"
#include "systems/system_manager.h"
#include "logs/loggingcategories.h"

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string.h>

#define COMPONENT_ID 0

Commander* Commander::_instance = nullptr;

void logMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
static QMap<QString, QSharedPointer<QFile>> logFiles;
static QSharedPointer<QFile> uncategorizedDataLogFile;
static QSharedPointer<QFile> generalDataLogFile;
static QSharedPointer<QFile> telemetryDataLogFile;
static QSharedPointer<QFile> sensorDataLogFile;
static QSharedPointer<QFile> messagesSentOrReceivedLogFile;
static QSharedPointer<QFile> missionInfoLogFile;

Commander::Commander() : QObject(),
_timeoutHandler(_time),
_callEveryHandler(_time),
_currentLogFilesPath(""),
_dailyMissionsFilePath(""),
_droneId(0),
_localStationId(0)
{}

Commander::~Commander()
{
    qInfo() << "~Commander()";
    _comManager->unregisterAllMavlinkMessageHandlers(this);

    uncategorizedDataLogFile.data()->close();
    generalDataLogFile.data()->close();
    sensorDataLogFile.data()->close();
    telemetryDataLogFile.data()->close();
    messagesSentOrReceivedLogFile.data()->close();
    missionInfoLogFile.data()->close();

    uncategorizedDataLogFile.clear();
    generalDataLogFile.clear();
    sensorDataLogFile.clear();
    telemetryDataLogFile.clear();
    messagesSentOrReceivedLogFile.clear();
    missionInfoLogFile.clear();
    logFiles.clear();

    _engine.clear();
    _comManager.clear();
    _controlInterface.clear();
    _stateMachine.clear();
    _weatherObject.clear();
    _gamepadManager.clear();
    _missionManager.clear();
}

void Commander::init(QObject *parent)
{
    _connectionTimer = new QTimer(parent);
    _systemTimer = new QTimer(parent);

    setupLogs();

    _engine = QSharedPointer<QQmlApplicationEngine>(new QQmlApplicationEngine());
    uint8_t componentId = COMPONENT_ID;
    _comManager = QSharedPointer<SystemManager>(new SystemManager(0, componentId));

    if(_comManager->init())
    {
        qInfo(generalLog()).nospace() << "Successful configuration from file.";
        qInfo(generalLog()).nospace() << "Local station initialised with ID: " << _comManager->getLocalSystemId() << ".";
        _gamepadManager = QSharedPointer<GamepadManager>(new GamepadManager(_comManager));
        _keepScreenOnHelper.reset(new KeepAwakeHelper());
        _weatherObject = QSharedPointer<WeatherReport>(new WeatherReport());
        _missionManager = QSharedPointer<MissionManager>(new MissionManager());
        _stateMachine = QSharedPointer<StateMachine>(new StateMachine(_comManager, _weatherObject, _missionManager));



        _controlInterface = QSharedPointer<ControlInterface>(new ControlInterface(_engine, _stateMachine, _gamepadManager, _missionManager, _comManager->getGroundstationIp(), _comManager->getGroundstationTcpPort(), _comManager->getSendingPilotName(), _comManager->getReceivingPilotName(), _comManager->getRelayStationList()));

        setupConnections();
    }
    else
    {
        qInfo(generalLog()).nospace() << "Unsuccessful configuration from file.";
        _controlInterface = QSharedPointer<ControlInterface>(new ControlInterface(_engine));
    }

    start();
}

void Commander::start()
{
    // Stop all in case of restart
    //_comManager->unregisterAllMavlinkMessageHandlers(this);
    //_comManager->stop();

    // Uncomment for tcp connection
    // If unsuccessful, try and connect TCP connection
//    if(connectResult == ConnectionResult::UNINITIALIZED)
//    {
//        qInfo(generalLog()) << "No adequate serial port found. Initialising TCP connection.";
//        infoToDisplay(tr("No adequate serial port found. Initialising tcp connection."));
//        if(_comManager->isConnectionApi())
//        {
//            connectResult =_comManager->add_tcp_connection_api(ConnectionType::UNKNOWN_TCP1, _comManager->getGroundstationIp().toStdString(), _comManager->getGroundstationTcpPort().toInt());
//        }
//        else
//        {
//            connectResult =_comManager->add_tcp_connection(ConnectionType::UNKNOWN_TCP1, _comManager->getGroundstationIp().toStdString(), _comManager->getGroundstationTcpPort().toInt());
//        }
//    }
//    else
//    {
//        qInfo(generalLog()) << "Serial port successfully connected. Skipping TCP connection.";
//    }



    /*ConnectionResult connectResultDirect = */
    addDirectConnection();
    /*ConnectionResult connectResultWebserver = */
    addWebserverConnection();

    // Start communication manager
    /*
    if((connectResultDirect == ConnectionResult::SUCCESS) || (connectResultWebserver == ConnectionResult::SUCCESS))
    {
        _comManager->start();
        _timeoutHandler.runOnce();
    }*/

    _comManager->start();
}

ConnectionResult Commander::addDirectConnection() {
    ConnectionResult connectResultDirect = ConnectionResult::UNINITIALIZED;
    infoToDisplay(tr("Initialising direct connection."));
    qInfo(generalLog()).nospace() << "Initialising direct connection.";

    // Try to find and connect serial connection
    QList<QSerialPortInfo> serialPortList;
    serialPortList = QSerialPortInfo::availablePorts();

    if(serialPortList.count() != 0)
    {
        int radioBaudrate = _comManager->getSerialBaudrate();
        qInfo(generalLog()).nospace() << "Using serial baudrate: " << radioBaudrate << ".";

        if(serialPortList.count() == 1)
        {
            if(_comManager->isConnectionApi())
            {
                connectResultDirect =_comManager->addSerialConnectionApi(ConnectionType::DIRECT, serialPortList.at(0).systemLocation().toStdString(), radioBaudrate);
            }
            else
            {
                connectResultDirect =_comManager->addSerialConnection(ConnectionType::DIRECT, serialPortList.at(0).systemLocation().toStdString(), radioBaudrate);
            }
        }
        else
        {
            qWarning(generalLog()) << "More than one serial port found.";
        }
    }
    else
    {
        qWarning(generalLog()) << "No serial port found.";
    }

    infoToDisplay(tr("Direct connection result: %1.").arg(connectionResultStr(connectResultDirect)));
    qInfo(generalLog()).nospace() << "Direct connection result: " << connectionResultStr(connectResultDirect) << ".";
    _stateMachine->antennaConnected(connectResultDirect == ConnectionResult::SUCCESS);

    return connectResultDirect;
}

ConnectionResult Commander::addWebserverConnection() {
    // Try and connect TCP connection to webserver
    ConnectionResult connectResultWebserver = ConnectionResult::UNINITIALIZED;
    infoToDisplay(tr("Initialising TCP connection to webserver."));
    qInfo(generalLog()) << "Initialising TCP connection to webserver.";
    connectResultWebserver =_comManager->addTcpConnection(ConnectionType::WEBSERVER, _comManager->getWebserverIp().toStdString(), _comManager->getWebserverTcpPort().toInt());
    //infoToDisplay(tr("Webserver connection result: %1.").arg(connectionResultStr(connectResultWebserver)));
    qInfo(generalLog()).nospace() << "Webserver connection result: " << connectionResultStr(connectResultWebserver) << ".";
    _stateMachine->webserverConnected(false);

    return connectResultWebserver;
}

void Commander::removeDirectConnection()
{
    _comManager->removeConnection(ConnectionType::DIRECT);
}

uint8_t Commander::getDroneId() const
{
    return _droneId;
}

void Commander::setDroneId(uint8_t id)
{
    _droneId = id;
}

uint8_t Commander::getLocalStationId() const
{
    return _localStationId;
}

void Commander::setLocalStationId(uint8_t id)
{
    _localStationId = id;
}

bool Commander::openMissionInfoLogFile() const
{
    if(_dailyMissionsFilePath.isEmpty())
    {
        return false;
    }
    else
    {
        QDir generalDirectory(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation));
        QString dailyLogsDirectoryName = QDateTime::currentDateTime().toString("yyyy_MM_dd");
        missionInfoLogFile.reset(new QFile(_dailyMissionsFilePath + "/MissionsInfo.csv"));
        bool fileOpens = missionInfoLogFile.data()->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);

        logFiles["mission"] = missionInfoLogFile;

        return fileOpens;
    }
}

void Commander::closeMissionInfoLogFile() const
{
    missionInfoLogFile.data()->close();
    logFiles.remove("mission");
}

void Commander::setSendingChannel(ConnectionType channel)
{
    _comManager->setSendingChannel(channel);
}

void Commander::checkConnection()
{
    _stateMachine->antennaConnected(_comManager->sendDiagnoseRequest());
}

void Commander::checkTimeout()
{
    _timeoutHandler.runOnce();
    _callEveryHandler.runOnce();
}

bool Commander::forwardMessage(const mavlink_message_t &message)
{
    return _comManager->sendMessage(message);
}

void Commander::sendRadioDetectionMessage()
{
   _comManager->sendRadioDetectionMessage();
}

bool Commander::checkAndroidWritePermission() const
{
    QtAndroid::PermissionResult result = QtAndroid::checkPermission("android.permission.WRITE_EXTERNAL_STORAGE");
    if(result == QtAndroid::PermissionResult::Denied)
    {
        QtAndroid::requestPermissionsSync( QStringList() << "android.permission.WRITE_EXTERNAL_STORAGE" );
        result = QtAndroid::checkPermission("android.permission.WRITE_EXTERNAL_STORAGE");
        if(result == QtAndroid::PermissionResult::Denied)
        {
            return false;
        }
    }

    return true;
}

void Commander::setupLogs()
{
    if (checkAndroidWritePermission())
    {
        QDir generalDirectory(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation));

        QString dailyLogsDirectoryName = QDateTime::currentDateTime().toString("yyyy_MM_dd");
        QString currentLogDirectoryName = QDateTime::currentDateTime().toString("hh_mm");
        generalDirectory.mkpath("Cargo_GS/" + dailyLogsDirectoryName + "/" + currentLogDirectoryName);

        _dailyMissionsFilePath = generalDirectory.path().append("/Cargo_GS/" + dailyLogsDirectoryName);
        _currentLogFilesPath = generalDirectory.path().append("/Cargo_GS/" + dailyLogsDirectoryName + "/" + currentLogDirectoryName);

        uncategorizedDataLogFile.reset(new QFile(_currentLogFilesPath + "/logFile.txt"));
        uncategorizedDataLogFile.data()->open(QIODevice::WriteOnly | QIODevice::Text);
        generalDataLogFile.reset(new QFile(_currentLogFilesPath + "/GeneralData.txt"));
        generalDataLogFile.data()->open(QIODevice::WriteOnly | QIODevice::Text);
        telemetryDataLogFile.reset(new QFile(_currentLogFilesPath + "/TelemetryData.csv"));
        telemetryDataLogFile.data()->open(QIODevice::WriteOnly | QIODevice::Text);
        sensorDataLogFile.reset(new QFile(_currentLogFilesPath + "/SensorData.csv"));
        sensorDataLogFile.data()->open(QIODevice::WriteOnly | QIODevice::Text);
        messagesSentOrReceivedLogFile.reset(new QFile(_currentLogFilesPath + "/MessagesSentOrReceived.csv"));
        messagesSentOrReceivedLogFile.data()->open(QIODevice::WriteOnly | QIODevice::Text);

        logFiles["default"] = uncategorizedDataLogFile;
        logFiles["general"] = generalDataLogFile;
        logFiles["telemetry"] = telemetryDataLogFile;
        logFiles["sensor"] = sensorDataLogFile;
        logFiles["messages"] = messagesSentOrReceivedLogFile;

        QTextStream out(sensorDataLogFile.data());
        out << "Date,Time,Time from start [s],Time from start [mm:ss],Remaining distance,Temperature,Humidity" << Qt::endl;

        out.setDevice(messagesSentOrReceivedLogFile.data());
        out << "Date,Time,Direction,Connection type,Sender ID,Message ID,Message type" << Qt::endl;

        out.setDevice(telemetryDataLogFile.data());
        out << "Date,Time,lat,lon,alt,vN,vE,vz" << Qt::endl;

        if(!QFile::exists(_dailyMissionsFilePath + "/MissionsInfo.csv"))
        {
            missionInfoLogFile.reset(new QFile(_dailyMissionsFilePath + "/MissionsInfo.csv"));
            missionInfoLogFile.data()->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append);
            out.setDevice(missionInfoLogFile.data());
            out << "Id,Version,Name,Length,Sender ID,Receiver ID,Sending pilot,Receiving pilot,Cargo weight,Cargo volume,Cargo description,Take-off time,Landing time,Flight duration" << Qt::endl;
            missionInfoLogFile.data()->close();
        }

        qInstallMessageHandler(logMessageHandler);

        qInfo(generalLog()) << "Launching cargo groundstation.";
    }
    else
    {
        qWarning() << "NO LOGGING: Permission to write to Android external storage denied.";
    }
}

void Commander::setupConnections()
{
    connect(_systemTimer, SIGNAL(timeout()), this, SLOT(checkTimeout()));
    _systemTimer->start(1000); // timeout every second
    connect(_connectionTimer, SIGNAL(timeout()), this, SLOT(checkConnection()));
    _connectionTimer->start(3000); // timeout every 3 seconds

    connect(_weatherObject.get(), SIGNAL(weatherInfoReady(bool, QString, QString)), _stateMachine.get(), SIGNAL(weatherConditionsUpdated(bool, QString, QString)));
    connect(_gamepadManager.get(), SIGNAL(gamepadConnected(bool)), _stateMachine.get(), SIGNAL(gamepadConnected(bool)));
    connect(_stateMachine.get(), SIGNAL(controlStatusChanged(bool)), _gamepadManager.get(), SLOT(updateControlState(bool)));
    connect(_stateMachine.get(), SIGNAL(sendRadioDetectionMessage()), this, SLOT(sendRadioDetectionMessage()));

    connect(this, SIGNAL(telemetryUpdated(QString, QString, QString, QString, QString, QString)), _stateMachine.get(), SIGNAL(telemetryUpdated(QString, QString, QString, QString, QString, QString)));
    connect(this, SIGNAL(attitudeUpdated(QString, QString, QString)), _stateMachine.get(), SIGNAL(attitudeUpdated(QString, QString, QString)));
    connect(this, SIGNAL(droneBatteryUpdated(QString)), _stateMachine.get(), SIGNAL(droneBatteryUpdated(QString)));
    connect(this, SIGNAL(droneTimeDistanceTemperatureHumidityUpdated(uint64_t, float, float, float)), _stateMachine.get(), SIGNAL(droneTimeDistanceTemperatureHumidityUpdated(uint64_t, float, float, float)));
    connect(this, SIGNAL(commandAcknowledged(uint16_t, uint8_t, uint8_t)), _stateMachine.get(), SLOT(commandAcknowledged(uint16_t, uint8_t, uint8_t)));
    connect(this, SIGNAL(forwardDroneErrorMessage(uint8_t, QString)), _stateMachine.get(), SLOT(forwardDroneErrorMessage(uint8_t, QString)));
    connect(this, SIGNAL(droneStateChanged(uint32_t, uint8_t)), _stateMachine.get(), SLOT(droneStateChange(uint32_t, uint8_t)));
    connect(this, SIGNAL(messageToDisplay(QString, QString, QString)), _stateMachine.get(), SIGNAL(messageToDisplay(QString, QString, QString)));
    connect(this, SIGNAL(infoToDisplay(QString)), _stateMachine.get(), SIGNAL(infoToDisplay(QString)));
    connect(this, SIGNAL(forwardRssi(uint8_t, uint8_t)), _stateMachine.get(), SIGNAL(forwardRssi(uint8_t, uint8_t)));
    connect(this, SIGNAL(debugDataToDisplay(QString, QString, QString)), _stateMachine.get(), SIGNAL(debugDataToDisplay(QString, QString, QString)));
    connect(this, SIGNAL(resetState()), _stateMachine.get(), SLOT(resetState()));
    connect(this, SIGNAL(receptionRequestReceived(int)), _stateMachine.get(), SIGNAL(receptionRequestReceived(int)));
    connect(this, SIGNAL(receptionAckReceived()), _stateMachine.get(), SIGNAL(receptionAckReceived()));
    connect(this, SIGNAL(webserverConnected(bool)), _stateMachine.get(), SIGNAL(webserverConnected(bool)));
    connect(this, SIGNAL(groundstationConnected(bool, uint8_t)), _stateMachine.get(), SIGNAL(groundstationConnected(bool, uint8_t)));
    connect(this, SIGNAL(radioStatusUpdated(uint64_t, uint8_t)), _stateMachine.get(), SIGNAL(radioStatusUpdated(uint64_t, uint8_t)));
    connect(this, SIGNAL(droneConnected(bool)), _stateMachine.get(), SLOT(changeDroneConnectionStatus(bool)));
    connect(this, SIGNAL(connectionTypeUpdated(bool)), _stateMachine.get(), SLOT(connectionTypeUpdated(bool)));
}

void logMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QScopedPointer<QFile> currentFile;
    currentFile.reset(logFiles.value(context.category, nullptr).data());
    if(currentFile.isNull())
    {
        return;
    }

    QTextStream out(currentFile.data());

    if(!strcmp(context.category, "default"))
    {
        out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
        switch (type)
        {
        case QtInfoMsg:     out << "INFO"; break;
        case QtDebugMsg:    out << "DEBUG"; break;
        case QtWarningMsg:  out << "WARNING"; break;
        case QtCriticalMsg: out << "CRITICAL"; break;
        case QtFatalMsg:    out << "FATAL"; break;
        }
        out << " " << context.file << ", l." << context.line;
        out << ": " << msg << Qt::endl;
    }
    else if(!strcmp(context.category, "general"))
    {
        out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
        switch (type)
        {
        case QtInfoMsg:     out << "INFO"; break;
        case QtDebugMsg:    out << "DEBUG"; break;
        case QtWarningMsg:  out << "WARNING"; break;
        case QtCriticalMsg: out << "CRITICAL"; break;
        case QtFatalMsg:    out << "FATAL"; break;
        }
        out << ": " << msg << Qt::endl;
    }
    else if(!strcmp(context.category, "telemetry"))
    {
        out << QDateTime::currentDateTime().toString("yyyy-MM-dd,hh:mm:ss.zzz");
        out << "," << msg << Qt::endl;
    }
    else if(!strcmp(context.category, "sensor"))
    {
        out << QDateTime::currentDateTime().toString("yyyy-MM-dd,hh:mm:ss.zzz");
        out << "," << msg << Qt::endl;
    }
    else if(!strcmp(context.category, "messages"))
    {
        out << QDateTime::currentDateTime().toString("yyyy-MM-dd,hh:mm:ss.zzz");
        out << "," << msg << Qt::endl;
    }
    else if(!strcmp(context.category, "mission"))
    {
        out << msg;
    }

    out.flush();    // Clear the buffered data

    currentFile.take();
}
