#pragma once

#include <mutex>
#include <memory>
#include <functional>
#include <map>
#include "../Library/global_include.h"

/*
 * The CallEveryHandler class is a class managing multiple recurring timers
 * referenced by unique cookies.
 */
class CallEveryHandler
{
public:
    CallEveryHandler(Time &time);
    ~CallEveryHandler();

    // delete copy and move constructors and assign operators
    CallEveryHandler(CallEveryHandler const &) = delete; // Copy construct
    CallEveryHandler(CallEveryHandler &&) = delete; // Move construct
    CallEveryHandler &operator=(CallEveryHandler const &) = delete; // Copy assign
    CallEveryHandler &operator=(CallEveryHandler &&) = delete; // Move assign

    void add(std::function<void()> callback, float intervalInSeconds, void **cookie);
    void change(float intervalInSeconds, const void *cookie);
    void reset(const void *cookie);
    void remove(const void *cookie);

    void runOnce();

private:
    struct Entry
    {
        std::function<void()> callback;
        dl_time_t lastTime;
        float intervalInSeconds;
    };

    std::map<void *, std::shared_ptr<Entry>> _entries{};
    std::mutex _entriesMutex{};
    bool _iteratorInvalidated{false};

    Time &_time;
};
