#include "timeout_handler.h"
#include <QDebug>

TimeoutHandler::TimeoutHandler(Time &time) :
_time(time)
{}

TimeoutHandler::~TimeoutHandler()
{
    qInfo() << "~TimeoutHandler()";
}

void TimeoutHandler::add(std::function<void()> callback, double durationInSeconds, void **cookie)
{
    auto newTimeout = std::make_shared<Timeout>();
    newTimeout->callback = callback;
    newTimeout->time = _time.steadyTimeInFuture(durationInSeconds);
    newTimeout->durationInSeconds = durationInSeconds;

    void *newCookie = static_cast<void *>(newTimeout.get());

    // Brackets for lock_guard
    {
        std::lock_guard<std::mutex> lock(_timeoutsMutex);
        _timeouts.insert(std::pair<void *, std::shared_ptr<Timeout>>(newCookie, newTimeout));
    }

    if (cookie != nullptr)
    {
        *cookie = newCookie;
    }
}

void TimeoutHandler::refresh(const void *cookie)
{
    std::lock_guard<std::mutex> lock(_timeoutsMutex);

    auto it = _timeouts.find(const_cast<void *>(cookie));
    if (it != _timeouts.end())
    {
        dl_time_t futureTime = _time.steadyTimeInFuture(it->second->durationInSeconds);
        it->second->time = futureTime;
    }
}

void TimeoutHandler::refresh(double durationInSeconds, const void *cookie)
{
    std::lock_guard<std::mutex> lock(_timeoutsMutex);

    auto it = _timeouts.find(const_cast<void *>(cookie));
    if (it != _timeouts.end())
    {
        it->second->durationInSeconds = durationInSeconds;
        dl_time_t futureTime = _time.steadyTimeInFuture(it->second->durationInSeconds);
        it->second->time = futureTime;
    }
}

void TimeoutHandler::remove(const void *cookie)
{
    std::lock_guard<std::mutex> lock(_timeoutsMutex);

    auto it = _timeouts.find(const_cast<void *>(cookie));
    if (it != _timeouts.end())
    {
        _timeouts.erase(const_cast<void *>(cookie));
        _iteratorInvalidated = true;
    }
}

void TimeoutHandler::runOnce()
{
    _timeoutsMutex.lock();

    dl_time_t now = _time.steadyTime();

    for (auto it = _timeouts.begin(); it != _timeouts.end(); /* no ++it */)
    {
        // If time is passed, call timeout callback.
        if (it->second->time < now)
        {
            if (it->second->callback)
            {
                // Get a copy for the callback because we will remove it.
                std::function<void()> callback = it->second->callback;

                // Self-destruct before calling to avoid locking issues.
                _timeouts.erase(it++);

                // Unlock while we callback because it might in turn want to add timeouts.
                _timeoutsMutex.unlock();
                callback();
                _timeoutsMutex.lock();
            }

        } else
        {
            ++it;
        }

        // We start over if anyone has messed with this while we called the callback.
        if (_iteratorInvalidated)
        {
            _iteratorInvalidated = false;
            it = _timeouts.begin();
        }
    }
    _timeoutsMutex.unlock();
}
