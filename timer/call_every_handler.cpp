#include <QDebug>
#include "call_every_handler.h"

CallEveryHandler::CallEveryHandler(Time &time) :
_time(time)
{}

CallEveryHandler::~CallEveryHandler()
{
    qInfo() << "~CallEveryHandler()";
}

void CallEveryHandler::add(std::function<void()> callback, float intervalInSeconds, void **cookie)
{
    auto newEntry = std::make_shared<Entry>();
    newEntry->callback = callback;
    newEntry->lastTime = _time.steadyTime();
    newEntry->intervalInSeconds = intervalInSeconds;

    void *newCookie = static_cast<void *>(newEntry.get());

    // Brackets for lock_guard
    {
        std::lock_guard<std::mutex> lock(_entriesMutex);
        _entries.insert(std::pair<void *, std::shared_ptr<Entry>>(newCookie, newEntry));
    }

    if (cookie != nullptr)
    {
        *cookie = newCookie;
    }
}

void CallEveryHandler::change(float intervalInSeconds, const void *cookie)
{
    std::lock_guard<std::mutex> lock(_entriesMutex);

    auto it = _entries.find(const_cast<void *>(cookie));
    if (it != _entries.end())
    {
        it->second->intervalInSeconds = intervalInSeconds;
    }
}

void CallEveryHandler::reset(const void *cookie)
{
    std::lock_guard<std::mutex> lock(_entriesMutex);

    auto it = _entries.find(const_cast<void *>(cookie));
    if (it != _entries.end())
    {
        it->second->lastTime = _time.steadyTime();
    }
}

void CallEveryHandler::remove(const void *cookie)
{
    std::lock_guard<std::mutex> lock(_entriesMutex);

    auto it = _entries.find(const_cast<void *>(cookie));
    if (it != _entries.end())
    {
        _entries.erase(const_cast<void *>(cookie));
        _iteratorInvalidated = true;
    }
}

void CallEveryHandler::runOnce()
{
    _entriesMutex.lock();

    for (auto it = _entries.begin(); it != _entries.end(); ++it)
    {
        if (_time.elapsedSinceSeconds(it->second->lastTime) > double(it->second->intervalInSeconds))
        {
            _time.shiftSteadyTimeBy(it->second->lastTime, double(it->second->intervalInSeconds));

            if (it->second->callback)
            {
                // Get a copy for the callback because we unlock.
                std::function<void()> callback = it->second->callback;

                // Unlock while we callback because it might in turn want to add timeouts.
                _entriesMutex.unlock();
                callback();
                _entriesMutex.lock();
            }
        }

        // We leave the loop.
        // FIXME: there should be a nicer way to do this.
        if (_iteratorInvalidated)
        {
            _iteratorInvalidated = false;
            break;
        }
    }
    _entriesMutex.unlock();
}
