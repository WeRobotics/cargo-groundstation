#pragma once

#include <mutex>
#include <memory>
#include <functional>
#include <map>
#include <thread>
#include "../Library/global_include.h"

/*
 * The TimeoutHandler class is a class managing multiple one shot timers
 * referenced by unique cookies.
 */
class TimeoutHandler
{
public:
    TimeoutHandler(Time &time);
    ~TimeoutHandler();

    // delete copy and move constructors and assign operators
    TimeoutHandler(TimeoutHandler const &) = delete; // Copy construct
    TimeoutHandler(TimeoutHandler &&) = delete; // Move construct
    TimeoutHandler &operator=(TimeoutHandler const &) = delete; // Copy assign
    TimeoutHandler &operator=(TimeoutHandler &&) = delete; // Move assign

    void add(std::function<void()> callback, double durationInSeconds, void **cookie);
    void refresh(const void *cookie);
    void refresh(double durationInSeconds, const void *cookie);
    void remove(const void *cookie);

    void runOnce();

private:
    struct Timeout
    {
        std::function<void()> callback;
        dl_time_t time;
        double durationInSeconds;
    };

    std::map<void *, std::shared_ptr<Timeout>> _timeouts{};
    std::mutex _timeoutsMutex{};
    bool _iteratorInvalidated{false};

    Time &_time;
};
