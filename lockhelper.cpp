#include "lockhelper.h"
#include <QAndroidJniObject>
#include <QDebug>
#include "jni.h"
#include "logs/loggingcategories.h"

KeepAwakeHelper::KeepAwakeHelper()
{
    QAndroidJniObject activity = QAndroidJniObject::callStaticObjectMethod("org/qtproject/qt5/android/QtNative",
                                                                           "activity",
                                                                           "()Landroid/app/Activity;");
    if (activity.isValid())
    {
        QAndroidJniObject serviceName = QAndroidJniObject::getStaticObjectField<jstring>("android/content/Context",
                                                                                         "POWER_SERVICE");
        if (serviceName.isValid())
        {
            QAndroidJniObject powerManager = activity.callObjectMethod("getSystemService",
                                                                       "(Ljava/lang/String;)Ljava/lang/Object;",
                                                                       serviceName.object<jobject>());
            if (powerManager.isValid())
            {
                jint levelAndFlags = QAndroidJniObject::getStaticField<jint>("android/os/PowerManager",
                                                                             "SCREEN_DIM_WAKE_LOCK");

                QAndroidJniObject tag = QAndroidJniObject::fromString("My Tag");

                _wakeLock = powerManager.callObjectMethod("newWakeLock",
                                                          "(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;",
                                                          levelAndFlags,
                                                          tag.object<jstring>());
            }
        }
    }

    if (_wakeLock.isValid())
    {
        _wakeLock.callMethod<void>("acquire", "()V");
        qInfo(generalLog()) << "Screen captured, can't go to standby anymore.";
    }
    else
    {
        assert(false);
    }
}

KeepAwakeHelper::~KeepAwakeHelper()
{
    qInfo() << "~KeepAwakeHelper()";

    if (_wakeLock.isValid())
    {
        _wakeLock.callMethod<void>("release", "()V");
        qInfo(generalLog()) << "Screen freed, can now go to standby";
    }
}
