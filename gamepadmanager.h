#ifndef GAMEPAD_MANAGER
#define GAMEPAD_MANAGER

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QElapsedTimer>
#include <QSharedPointer>
#include <QGamepad>

#include "systems/system_manager.h"

class Commander;

/*
 * The GamepadManager class is a class managing the interaction
 * between a physical or virtual gamepad and a drone.
 *
 * Automatically detects a new physical gamepad connected via Bluetooth
 * to the tablet and adds it.
 *
 * Transmits gamepad commands to the drone:
 * - Every 50ms for joystick commands.
 * - When pressed or released for button commands.
 *
 * Commands based on an 8Bitdo SN30 Pro:
 * - Left joystick: throttle (up-down), yaw (left-right).
 * - Right joystick: pitch (up-down), roll (left-right).
 * - X button: arm.
 * - Y button: disarm.
 * - A button: DE-ACTIVATED (land).
 * - B button: take-off.
 */
class GamepadManager : public QObject
{
        Q_OBJECT
public:
    explicit GamepadManager(QSharedPointer<SystemManager> comManager);
    ~GamepadManager();

    // delete copy and move constructors and assign operators
    GamepadManager(GamepadManager const &) = delete; // Copy construct
    GamepadManager(GamepadManager &&) = delete; // Move construct
    GamepadManager &operator=(GamepadManager const &) = delete; // Copy assign
    GamepadManager &operator=(GamepadManager &&) = delete; // Move assign

signals:
    void gamepadConnected(bool isConnected);

public slots:
    void checkConnection();
    void updateControlState(bool hasControl);
    void updateVirtualJoystickCommand(QString buttonChar, double value);
    void updateVirtualButtonCommand(QString buttonChar, bool pressed);
    void updateVirtualSendContinuous(bool shouldSend);
    void sendCommandToDrone();

private:
    void initGamepadInstance();
    void updateJoystickCommand(char buttonChar, double value);
    void updateButtonCommand(char buttonChar, bool pressed);

    QGamepad* _gamepad;
    QTimer* _sendMessageTimer = nullptr;
    QTimer* _connectionTimer = nullptr;
    QSharedPointer<SystemManager> _comManager;

    mavlink_manual_control_t _payload;
    bool _sendContinuous;
    bool _sendOnce;
    bool _hasControl;
};

#endif // GAMEPAD_MANAGER
