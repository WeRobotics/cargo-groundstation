#include <QApplication>
#include <QtQuickControls2/QQuickStyle>
#include <QFont>
#include <QTimer>

#include "commander.h"
#include "systems/relay_system.h"

#include <jni.h>
#include "qserialport.h"

// Must return the JNI version needed by the native library
// (used for serial connection (radio))
jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    Q_UNUSED(reserved);

    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
    {
        return -1;
    }

    QSerialPort::setNativeMethods();

    return JNI_VERSION_1_6;
}

int main(int argc, char **argv)
{
    QApplication application(argc, argv);
    QFont defaultFont("Helvetica", 20);
    application.setFont(defaultFont);

    Commander::getInstance()->init(&application);

    qRegisterMetaType<uint8_t>("uint8_t");
    qRegisterMetaType<uint16_t>("uint16_t");
    qRegisterMetaType<uint32_t>("uint32_t");
    qRegisterMetaType<uint64_t>("uint64_t");

    int ret = application.exec();

    return ret;
}
