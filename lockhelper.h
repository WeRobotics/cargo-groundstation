#pragma once
#include <QAndroidJniObject>

/*
 * From https://stackoverflow.com/questions/34179653/how-do-i-prevent-an-android-device-from-going-to-sleep-from-qt-application
 *
 * The KeepAwakeHelper class is a class managing the device sleep and standby.
 *
 * The screen and CPU stay awake while the helper object exists.
 * The lock is released when the helper object goes out of scope.
 */
class KeepAwakeHelper
{
public:
    KeepAwakeHelper();
    ~KeepAwakeHelper();

private:
    QAndroidJniObject _wakeLock;
};
