import QtGraphicalEffects 1.0
import QtPositioning 5.6
import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11

/*
 * The telemetryBox Column contains and displays telemetry information.
 *
 * It contains the following main elements:
 * @item rssiIcon: white if an antenna is connected (serial), black otherwise.
 * @item gamepadIcon: white if a gamepad is connected (bluetooth), black otherwise.
 * @item cloudIcon: white if the webserver is connected (tcp), black otherwise.
 * @item tabletIcon: white if at least one other ground station is connected, black otherwise.
 * @item rssiText: white if an antenna is connected, black otherwise. Rssi from antenna.
 * @item groundstationsConnected: white if at least one other ground station is connected, black otherwise. Ids of other ground stations connected.
 * @item droneIcon: white if the drone is connected, black otherwise.
 * @item cargoIcon: white if the drone is connected, black otherwise.
 * @item batteryIcon: white if the drone is connected, black otherwise. Changes depending on drone battery level.
 * @item batteryText: white if the drone is connected, black otherwise. Drone remaining battery percentage.
 * @item stateText: white if the drone is connected, black otherwise. Drone current state (see systems/onboardstates.h)
 * @item sensorsText: white if the drone is connected, black otherwise. Temperature of the drone Raspberry Pi.
 * @item velocityIcon: white if the drone is connected, black otherwise.
 * @item locationIcon: white if the drone is connected, black otherwise.
 * @item altitudeIcon: white if the drone is connected, black otherwise.
 * @item velocityText: white if the drone is connected, black otherwise. Drone horizontal (v) and vertical (vz) velocities.
 * @item locationText: white if the drone is connected, black otherwise. Drone latitude, longitude and altitude.
 * @item timeFromStartText: white if the drone is connected, black otherwise. Time since the start of the current mission. From drone. Stops increasing if connection is lost.
 * @item timeSinceDisconnectText: visible if the drone is disconnected. Tentative to continue counting time from mission start when connection lost.
 * Warning: timeSinceDisconnectText not working and currently disabled.
 * @item routeProgress: progress bar to visually indicate the proportion of a mission already accomplished.
 * @item distanceFromStartText: horizontal distance flown from the start of the mission.
 * @item distanceToEndText: horizontal distance remaining until the end of the mission.
 *
 * The whole container can be hidden by pressing on the bottom arrow (showHideButton) or by flicking it up (flickingDetection).
 */
Column
{
    id: telemetryBox

    readonly property int smallLineHeight: mainWindow.height / 18
    readonly property int bigLineHeight: mainWindow.height / 12
    readonly property int boxSpacing: 40
    readonly property int boxWidth: mainWindow.width * 0.45
    readonly property int sideMargin: 30
    readonly property int topMostMargin: 10

    property real lengthToPrint: 0
    property int secondsSinceDisconnect: 0

    anchors { top: parent.top; left: parent.left; topMargin: 0; leftMargin: 20 }
    spacing: 0

    Connections
    {
        target: mainWindow
        function onNoRouteSelected() {
            distanceFromStartText.text = "-.--km\n" + qsTr("from start")
            distanceToEndText.text = "-.--km\n" + qsTr("to end")
            timeFromStartText.text = "--:--:--" + qsTr(" from start")
            sensorsText.text = "T: --°C\nH: --%"
            timeSinceDisconnectTimer.stop()
            secondsSinceDisconnect = 0
            timeSinceDisconnectText.text =  " (+ " + new Date(secondsSinceDisconnect * 1000).toISOString().substr(11, 8) + ")"
        }
        function onRouteSelected(ID, length) {
            lengthToPrint = Math.round(length / 10) / 100;
            distanceFromStartText.text = "0.00 km\n" + qsTr("from start")
            distanceToEndText.text = lengthToPrint + " km\n" + qsTr("to end")
        }
    }

    Timer
    {
        id: timeSinceDisconnectTimer

        interval: 1000 // 1 second
        onTriggered:
        {
            secondsSinceDisconnect += 1
            timeSinceDisconnectText.text =  " (+ " + new Date(secondsSinceDisconnect * 1000).toISOString().substr(11, 8) + ")"
            timeSinceDisconnectTimer.start()
        }
    }

    Connections
    {
        target: stateMachine
        function onControlStatusChanged(hasControl) {
            if(hasControl)
            {
                telemetryBoxTitleRectangle.visible = false
                telemetryBoxMainRectangle.visible = false
                progressRectangle.visible = false
                distanceRectangle.visible = false
                timeRectangle.visible = false
            }
        }
        function onDroneBatteryUpdated(level) {
            batteryText.text = level + "%"
            batteryIcon.source = level > 90 ? "../icons/B100.png" : (level > 80 ? "../icons/B90.png" : level > 60 ? "../icons/B80.png" : level > 50 ? "../icons/B60.png" : level > 30 ? "../icons/B50.png" : level > 20 ? "../icons/B30.png" : "../icons/B20.png")
        }
        function onDroneStateChanged(newState, newStateStr) {
            stateText.text = newStateStr;
        }
        function onDroneTimeDistanceTemperatureHumidityUpdated(time, distance, temperature, humidity) {
            if(currentDroneState > 7) // In mission
            {
                // Update distances
                var distanceFlown = routeList[currentRouteIndex].length - distance
                if(distanceFlown < 0)
                {
                    distanceFlown = 0
                }
                distanceToEndText.text = (distance / 1000).toFixed(2) + "km\n" + qsTr("to end")
                distanceFromStartText.text = (distanceFlown / 1000).toFixed(2) + "km\n" + qsTr("from start")

                // Update time
                var niceDisplayTime = new Date(time * 1000).toISOString().substr(11, 8);
                timeFromStartText.text = niceDisplayTime + qsTr(" from start")

                // Add 1 sec to time since disconnect if not restarted
                secondsSinceDisconnect = 0
                timeSinceDisconnectText.text =  " (+ " + new Date(secondsSinceDisconnect * 1000).toISOString().substr(11, 8) + ")"
                timeSinceDisconnectTimer.start()

                // Update progress bar
                routeProgress.value = distanceFlown / routeList[currentRouteIndex].length
            }

            // Update temperature and humidity
            sensorsText.text = "T: " + Math.round(temperature * 10) / 10 + "°C\nH: " + Math.round(humidity * 10) / 10 + "%"
        }
        function onForwardRssi(local, remote) {
            if(local)
            {
                rssiText.text = "RSSI: -" + local + " dBm"
            }
            else
            {
                rssiText.text = "RSSI:   " + local + " dBm"
            }
        }
        function onTelemetryUpdated(lat, lon, alt, vx, vy, vz) {
            // Update position
            var droneLocationCoord = QtPositioning.coordinate(lat, lon, alt)
            if(droneLocationCoord.isValid) // Via sim or drone: radians
            {
                var coordStr = droneLocationCoord.toString().split(",", 3)
                locationText.text = coordStr[0] + "\n" + coordStr[1] + "\n\n" + coordStr[2]
            }
            else // Via MobaXTerm: degrees
            {
                var newLat = lat * Math.PI / 180 // parseFloat(lat)
                var newLon = lon * Math.PI / 180 //parseFloat(lon)
                var newdroneLocationCoord = QtPositioning.coordinate(newLat, newLon, alt)
                var newCoordStr = newdroneLocationCoord.toString().split(",", 3)
                locationText.text = newCoordStr[0] + "\n" + newCoordStr[1] + "\n\n" + newCoordStr[2]
            }

            // Update velocity
            var realVelocity = Math.sqrt(Math.pow(parseInt(vx), 2) + Math.pow(parseInt(vy), 2))
            velocityText.text = "v: " + Number(realVelocity).toFixed(2) + " m/s\n" + "vz: " + vz.substring(0, 5) + " m/s"
        }
        function onResetAll() {
            routeProgress.value = 0
        }
    }

    Binding
    {
        target: groundstationsConnected
        property: "text"
        value: connectedStationsIdList
    }

    Rectangle
    {
        id: telemetryBoxTitleRectangle

        color: darkBackgroundColor
        height: smallLineHeight + topMostMargin
        width: boxWidth

        Text
        {
            anchors { fill: parent; margins: 0; topMargin: 10 }

            color: itemOnDarkBackgroundColor;
            font.pointSize: 20
            fontSizeMode: Text.Fit
            text: qsTr("TELEMETRY")
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea
        {
            id: flickingDetection

            property bool flicking: true
            property real initX: 0
            property real initY: 0
            property real minY: flickingDetection.height

            anchors { top: telemetryBoxTitleRectangle.top; left: telemetryBoxTitleRectangle.left; right: telemetryBoxTitleRectangle.right }
            height: telemetryBoxTitleRectangle.height + telemetryBoxMainRectangle.height + timeRectangle.height + progressRectangle.height + distanceRectangle.height + showHideButton.height + 30

            onPressed:
            {
                initX = mouseX
                initY = mouseY
                minY = mouseY
                flicking = true
                flickTimer.start()
            }
            onPositionChanged:
            {
                if(mouseY < minY)
                {
                    minY = mouseY
                }
                else if(flicking && (mouseY > minY))
                {
                    flicking = false
                }
            }
            onReleased:
            {
                if(flicking && (Math.abs(mouseX-initX) < 300) && (initY - mouseY > 50))
                {
                    telemetryBoxTitleRectangle.visible = false
                    telemetryBoxMainRectangle.visible = false
                    progressRectangle.visible = false
                    distanceRectangle.visible = false
                    timeRectangle.visible = false
                }
            }

            Timer
            {
                id: flickTimer

                interval: 500 // ms
                onTriggered: flickingDetection.flicking = false
            }
        }
    }

    // Main rectangle containing 1 Row containing 6 Columns:
    // Icons - Texts - Icons - Texts - Icons - Texts
    // First 2 columns for connection information (radio, gamepad, webserver, groundstations)
    // Next 2 colums for drone information (battery, state, temperature, humidity)
    // Last 2 colums also for drone information (velocity, location, altitude)
    Rectangle
    {
        id: telemetryBoxMainRectangle

        color: darkBackgroundColor
        height: 2.5 * smallLineHeight + 3 * topMostMargin
        width: boxWidth
        z: timeRectangle.z + 1

        Row
        {
            anchors { top: parent.top; left: parent.left; topMargin: topMostMargin; leftMargin: sideMargin }
            spacing: topMostMargin

            // Connection information icons (radio, gamepad, webserver, groundstations)
            Column
            {
                spacing: 2 * topMostMargin

                Image
                {
                    id: rssiIcon

                    source: "../icons/Radio.png" // Icons made by "https://www.freepik.com/"
                    smooth: true
                    visible: false
                    height: smallLineHeight
                    width: height
                }

                Colorize
                {
                    height: smallLineHeight
                    width: height
                    source: rssiIcon
                    hue: 0.0
                    saturation: 0.0
                    lightness: antennaIsConnected ? 1.0 : 0.0
                }

                Image
                {
                    id: satelliteIcon

                    source: "../icons/Satellite.png" // Icons made by "https://www.freepik.com/"
                    smooth: true
                    visible: false
                    height: smallLineHeight
                    width: height
                }

                Colorize
                {
                    height: smallLineHeight
                    width: height
                    source: satelliteIcon
                    hue: 0.0
                    saturation: 0.0
                    lightness: isHighLatency ? 1.0 : 0.0
                }
                /*
                Image
                {
                    id: gamepadIcon

                    source: "../icons/Gamepad.png" // Icons made by "https://www.freepik.com/"
                    smooth: true
                    visible: false
                    height: smallLineHeight
                    width: height
                }

                Colorize
                {
                    height: smallLineHeight
                    width: height
                    source: gamepadIcon
                    hue: 0.0
                    saturation: 0.0
                    lightness: gamepadIsConnected ? 1.0 : 0.0
                }isHighLatency
                */


                Row
                {
                    Image
                    {
                        id: cloudIcon

                        source: "../icons/Cloud.png" // Icons made by "https://www.flaticon.com/authors/smashicons"
                        smooth: true
                        visible: false
                        height: smallLineHeight / 2
                        width: height
                    }

                    Colorize
                    {
                        height: smallLineHeight / 2
                        width: height
                        source: cloudIcon
                        hue: 0.0
                        saturation: 0.0
                        lightness: webserverIsConnected ? 1.0 : 0.0
                    }

                    Image
                    {
                        id: tabletIcon

                        source: "../icons/Tablet.png"
                        smooth: true
                        visible: false
                        height: smallLineHeight / 2
                        width: height
                    }

                    Colorize
                    {
                        height: smallLineHeight / 2
                        width: height
                        source: tabletIcon
                        hue: 0.0
                        saturation: 0.0
                        lightness: (connectedGroundstationsCounter > 0) ? 1.0 : 0.0
                    }
                }
            }

            // Connection information texts (radio, groundstations)
            Column
            {
                spacing: 0

                Item
                {
                    id: fillerItemAboveRssiText

                    height: (rssiIcon.height - rssiText.height) / 2
                    width: 1
                }

                Text
                {
                    id: rssiText

                    height: smallLineHeight / 2 + 5
                    width: ((telemetryBoxMainRectangle.width - 3 * smallLineHeight - 5 * topMostMargin - 2 * sideMargin) / 3) - 2 * topMostMargin

                    text: "RSSI: --- dBm"
                    color: antennaIsConnected ? itemOnDarkBackgroundColor : darkBackgroundColor
                    font.italic: antennaIsConnected ? false : true
                    font.pointSize: 30
                    fontSizeMode: Text.Fit
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }

                Item
                {
                    id: fillerItemAboveGroundstations

                    height: rssiIcon.height + satelliteIcon.height - rssiText.height - fillerItemAboveRssiText.height + 4 * topMostMargin
                    width: 1
                }

                Row
                {
                    height: tabletIcon.height

                    Text
                    {
                        id: groundstationsConnected

                        color: (connectedGroundstationsCounter > 0) ? itemOnDarkBackgroundColor : darkItemOnDarkBackgroundColor
                        font.italic: (connectedGroundstationsCounter > 0) ? false : true
                        font.pointSize: 15
                        text: "Number"
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignTop
                        height: parent.height
                        width: (telemetryBoxMainRectangle.width - 3 * smallLineHeight - 5 * topMostMargin - 2 * sideMargin) / 3
                    }
                }
            }

            // Drone information icons (drone, cargo box)
            Column
            {
                spacing: 2 * topMostMargin

                Image
                {
                    id: droneIcon

                    source: "../icons/Drone.PNG" // Icons made by ?
                    smooth: true
                    visible: false
                    height: smallLineHeight
                    width: height
                }

                Colorize
                {
                    height: smallLineHeight
                    width: height
                    source: droneIcon
                    hue: 0.0
                    saturation: 0.0
                    lightness: droneIsConnected ? 1.0 : 0.0
                }

                Image
                {
                    id: cargoIcon

                    source: "../icons/Cargo.png" // Icons made by "https://www.freepik.com/"
                    smooth: true
                    visible: false
                    height: smallLineHeight
                    width: height
                }

                Colorize
                {
                    height: smallLineHeight
                    width: height
                    source: cargoIcon
                    hue: 0.0
                    saturation: 0.0
                    lightness: droneIsConnected ? 1.0 : 0.0
                }
            }

            // Drone information texts (battery, state, temperature, humidity)
            Column
            {
                spacing: 0 //topMostMargin

                Row
                {
                    height: droneIcon.height / 2 + 5

                    Image
                    {
                        id: batteryIcon

                        source: "../icons/B100.png" // Icons made by ?
                        smooth: true
                        visible: false
                        height: parent.height
                        width: height
                    }

                    Colorize
                    {
                        height: parent.height
                        width: height
                        source: batteryIcon
                        hue: 0.0
                        saturation: 0.0
                        lightness: droneIsConnected ? 1.0 : 0.0
                    }

                    Text
                    {
                        id: batteryText

                        color: droneIsConnected ? itemOnDarkBackgroundColor : darkItemOnDarkBackgroundColor
                        font.italic: droneIsConnected ? false : true
                        font.pointSize: 30
                        fontSizeMode: Text.Fit
                        height: parent.height
                        text: "00%"
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignLeft
                    }
                }

                Row
                {
                    height: droneIcon.height / 2 + 5

                    Text
                    {
                        id: stateText

                        color: droneIsConnected ? itemOnDarkBackgroundColor : darkItemOnDarkBackgroundColor
                        font.italic: droneIsConnected ? false : true
                        font.pointSize: 30
                        fontSizeMode: Text.Fit
                        text: qsTr("Idle")
                        horizontalAlignment: Text.AlignLeft
                        verticalAlignment: Text.AlignVCenter
                        height: parent.height
                        width: (telemetryBoxMainRectangle.width - 3 * smallLineHeight - 5 * topMostMargin - 2 * sideMargin) / 3
                        wrapMode: Text.WordWrap
                    }
                }

                Item
                {
                    id: fillerItemAboveSensors

                    height: 2 * topMostMargin - 10
                    width: 1
                }

                Text
                {
                    id: sensorsText

                    color: droneIsConnected ? itemOnDarkBackgroundColor : darkItemOnDarkBackgroundColor
                    font.italic: droneIsConnected ? false : true
                    font.pointSize: 30
                    fontSizeMode: Text.Fit
                    text: "T: --°C\nH: --%"//"--:--:--"
                    height: smallLineHeight + 5
                    width: (telemetryBoxMainRectangle.width - 3 * smallLineHeight - 5 * topMostMargin - 2 * sideMargin) / 3
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                }
            }

            // Drone information icons (velocity, location, altitude)
            Column
            {
                spacing: topMostMargin //+ 0.3 * smallLineHeight

                Image
                {
                    id: velocityIcon

                    source: "../icons/Velocity.png" // Icons made by "https://www.freepik.com/"
                    smooth: true
                    visible: false
                    height: smallLineHeight
                    width: height
                }

                Colorize
                {
                    height: smallLineHeight
                    width: height
                    source: velocityIcon
                    hue: 0.0
                    saturation: 0.0
                    lightness: droneIsConnected ? 1.0 : 0.0
                }

                Image
                {
                    id: locationIcon

                    source: "../icons/Location.png" // Icons made by "https://www.freepik.com/"
                    smooth: true
                    visible: false
                    height: smallLineHeight
                    width: height
                }

                Colorize
                {
                    height: smallLineHeight
                    width: height
                    source: locationIcon
                    hue: 0.0
                    saturation: 0.0
                    lightness: droneIsConnected ? 1.0 : 0.0
                }

                Image
                {
                    id: altitudeIcon

                    source: "../icons/AltitudeSquare.png" // Icons made by "https://www.freepik.com/"
                    smooth: true
                    visible: false
                    height: smallLineHeight * 1.3
                    width: height
                }

                Colorize
                {
                    height: smallLineHeight * 1.3
                    width: height
                    source: altitudeIcon
                    hue: 0.0
                    saturation: 0.0
                    lightness: droneIsConnected ? 1.0 : 0.0
                }
            }

            // Drone information texts (velocity, location, altitude)
            Column
            {
                spacing: 1.5 * topMostMargin

                Text
                {
                    id: velocityText

                    text: "v: 0.00 m/s\nvz: 0.00 m/s"
                    color: droneIsConnected ? itemOnDarkBackgroundColor : darkItemOnDarkBackgroundColor
                    font.italic: droneIsConnected ? false : true
                    font.pointSize: 30
                    fontSizeMode: Text.Fit
                    height: /*1.5 * */smallLineHeight
                    width: (telemetryBoxMainRectangle.width - 3 * smallLineHeight - 5 * topMostMargin - 2 * sideMargin) / 3
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                }

                Text
                {
                    id: locationText

                    color: droneIsConnected ? itemOnDarkBackgroundColor : darkItemOnDarkBackgroundColor
                    font.italic: droneIsConnected ? false : true
                    font.pointSize: 30
                    fontSizeMode: Text.Fit
                    text: "00° 00' 0.0\" E\n00° 00' 0.0\" N\n\n\n000.0 m"
                    height: 2 * smallLineHeight
                    width: (telemetryBoxMainRectangle.width - 3 * smallLineHeight - 5 * topMostMargin - 2 * sideMargin) / 3
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignTop
                }
            }
        }
    }

    // Display time since mission start
    Rectangle
    {
        id: timeRectangle

        color: darkBackgroundColor
        height: smallLineHeight + topMostMargin
        width: boxWidth
        z: progressRectangle.z + 1

        Row
        {
            anchors {horizontalCenter: parent.horizontalCenter; top: parent.bottom }
            spacing: 0

            Text
            {
                id: timeFromStartText

                color: droneIsConnected ? itemOnDarkBackgroundColor : darkItemOnDarkBackgroundColor
                font.italic: droneIsConnected ? false : true
                font.pointSize: 30
                fontSizeMode: Text.Fit
                text: "--:--:--" + qsTr(" from start")
                horizontalAlignment: Text.AlignRight //Text.AlignHCenter//
                verticalAlignment: Text.AlignBottom
                height: smallLineHeight / 2
                width: boxWidth / 2
                wrapMode: Text.WrapAnywhere
            }
            Text
            {
                id: timeSinceDisconnectText

                color: ((!droneIsConnected) && (currentGroundstationState == 5) && secondsSinceDisconnect) ? itemOnDarkBackgroundColor : "transparent"
                font.italic: true
                font.pointSize: 30
                fontSizeMode: Text.Fit
                text: " (+ 00:00:00)"
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignBottom
                height: smallLineHeight / 2
                width: boxWidth / 2
                wrapMode: Text.WrapAnywhere
            }
        }
    }

    // Display distance progression during mission
    Rectangle
    {
        id: progressRectangle

        color: darkBackgroundColor
        height: bigLineHeight - 10
        width: boxWidth

        Image
        {
            id: startIcon

            anchors { top: parent.top; left: parent.left; leftMargin: sideMargin; topMargin: boxSpacing / 4 }
            source: "../icons/Start.png" // Icons made by "https://www.freepik.com/"
            smooth: true
            visible: false
            height: progressRectangle.height - anchors.topMargin
            width: height
        }

        Colorize
        {
            anchors { top: parent.top; left: parent.left; leftMargin: sideMargin; topMargin: boxSpacing / 4 }
            height: progressRectangle.height - anchors.topMargin
            width: height
            source: startIcon
            hue: 0.0
            saturation: 0.0
            lightness: 1.0
        }

        ProgressBar
        {
            id: routeProgress

            anchors { bottom: parent.bottom; left: startIcon.right; leftMargin: 0; bottomMargin: 0 }
            padding: 2
            value: 0.0

            background: Rectangle
            {
                color: itemOnDarkBackgroundColor
                implicitHeight: 10
                implicitWidth: progressRectangle.width - startIcon.width - endIcon.width - 2 * sideMargin
                radius: 5
            }

            contentItem: Item
            {
                implicitHeight: 8
                implicitWidth: progressRectangle.width - startIcon.width - endIcon.width - 2 * sideMargin // progressRectangle.width - 2 * progressRectangle.height

                Rectangle
                {
                    color: routeColor
                    radius: 4
                    height: parent.height
                    width: routeProgress.visualPosition * parent.width
                }
            }
        }

        Image
        {
            id: endIcon

            anchors { top: parent.top; left: routeProgress.right; leftMargin: 0; topMargin: startIcon.anchors.topMargin }
            source: "../icons/End.png" // Icons made by "https://www.flaticon.com/authors/xnimrodx"
            smooth: true
            visible: false
            height: startIcon.height
            width: height
        }

        Colorize
        {
            anchors { top: parent.top; left: routeProgress.right; leftMargin: 0; topMargin: startIcon.anchors.topMargin }
            height: startIcon.height
            width: height
            source: endIcon
            hue: 0.0
            saturation: 0.0
            lightness: 1.0
        }
    }

    Rectangle
    {
        id: distanceRectangle

        color: darkBackgroundColor
        height: smallLineHeight
        width: boxWidth

        Row
        {
            anchors { top: parent.top; left: parent.left; leftMargin: sideMargin; rightMargin: sideMargin; topMargin: boxSpacing / 6 }
            spacing: 0

            Text
            {
                id: distanceFromStartText

                color: itemOnDarkBackgroundColor;
                font.italic: droneIsConnected ? false : true
                font.pointSize: 30
                fontSizeMode: Text.Fit
                text: "-.--km\n" + qsTr("from start")
                verticalAlignment: Text.AlignTop
                horizontalAlignment: Text.AlignLeft
                height: distanceRectangle.height - boxSpacing / 6
                width: startIcon.width * 4
            }

            Item
            {
                height: 1 // dummy value != 0
                width: distanceRectangle.width - distanceFromStartText.width - distanceToEndText.width - 2 * sideMargin
            }

            Text
            {
                id: distanceToEndText

                color: itemOnDarkBackgroundColor;
                font.italic: droneIsConnected ? false : true
                font.pointSize: 30
                fontSizeMode: Text.Fit
                text: "-.--km\n" + qsTr("to end")
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignTop
                height: distanceRectangle.height - boxSpacing / 6
                width: endIcon.width * 1.5
            }
        }
    }

    // Make telemetryBox collapsable
    Button
    {
        id: showHideButton

        clip: true
        height: bigLineHeight - 20
        width: boxWidth

        background: Rectangle
        {
            anchors { fill: parent; topMargin: -radius }
            color: darkBackgroundColor
            radius: 20
        }

        display: AbstractButton.IconOnly
        icon { source: telemetryBoxTitleRectangle.visible ? "../icons/ArrowUp.png" : "../icons/ArrowDown.png"; height: showHideButton.height; width: showHideButton.height; color: itemOnDarkBackgroundColor } // Icons made by "https://www.flaticon.com/authors/dave-gandy"
        text: " "

        onClicked:
        {
            telemetryBoxTitleRectangle.visible = !telemetryBoxTitleRectangle.visible
            telemetryBoxMainRectangle.visible = !telemetryBoxMainRectangle.visible
            progressRectangle.visible = !progressRectangle.visible
            distanceRectangle.visible = !distanceRectangle.visible
            timeRectangle.visible = !timeRectangle.visible
        }
    }
}
