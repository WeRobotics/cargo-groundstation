import QtQuick 2.8
import QtQuick.Controls 2.4

/*
 * The receptionRequestPopup Popup contains a request for the user to confirm being ready for receiving a drone.
 *
 * The popup appears when another station sends a reception request.
 * Accepting or refusing the reception sends a message back to the sending station to inform them of the decision.
 */
Popup
{
    id: receptionRequestPopup

    property int requestingStationId: 0

    closePolicy: Popup.NoAutoClose
    focus: true
    modal: true
    x: Math.round((parent.width - receptionRequestPopupMainRectangle.width) / 2)
    y: Math.round((parent.height - receptionRequestPopupMainRectangle.height) / 2)

    Connections
    {
        target: stateMachine
        function onReceptionRequestReceived() {
            requestingStationId = stationId
            receptionRequestPopup.open()
        }
        function onResetAll() {
            receptionRequestPopup.close()
        }
    }

    Rectangle
    {
        id: receptionRequestPopupMainRectangle

        height: childrenRect.height + 100
        width: childrenRect.width + 100

        Text
        {
            id: receptionRequestTitleText

            anchors { top: parent.top; topMargin: 50; left: parent.left; leftMargin: 50 }
            font.pointSize: 25
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Station %1 is requesting you to receive a delivery.").arg(requestingStationId)
        }

        Row
        {
            anchors { top: receptionRequestTitleText.bottom; topMargin: 50; horizontalCenter: receptionRequestTitleText.horizontalCenter }
            spacing: 50

            Button
            {
                id: receptionRequestConfirmButton

                text: qsTr("Accept")

                contentItem: Text
                {
                    color: receptionRequestConfirmButton.enabled ? enabledTextColor : disabledTextColor
                    font.pointSize: 25
                    fontSizeMode: Text.Fit
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: receptionRequestConfirmButton.text
                }

                onClicked:
                {
                    receptionRequestPopup.close()
                    stateMachine.sendReceptionRequestOrReady(1); // Accept
                }
            }

            Button
            {
                id: receptionRequestsettingsCancelButton

                text: qsTr("Cancel")

                contentItem: Text
                {
                    color: receptionRequestsettingsCancelButton.enabled ? enabledTextColor : disabledTextColor
                    font.pointSize: 25
                    fontSizeMode: Text.Fit
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: receptionRequestsettingsCancelButton.text
                }

                onClicked:
                {
                    receptionRequestPopup.close()
                    stateMachine.sendReceptionRequestOrReady(2); // Refuse
                }
            }
        }
    }
}
