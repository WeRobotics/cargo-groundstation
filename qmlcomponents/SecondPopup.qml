import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4

/*
 * The safetyCheckPopup Popup contains the safety checks and mission information (pilot, cargo).
 *
 * It includes:
 * @item sendingPilotTextEdit: a field to register the name of the pilot sending the drone.
 * @item receivingPilotTextEdit: a field to register the name of the pilot receiving the drone.
 * Warning: receivingPilotTextEdit currently disabled and hidden. See README.md to re-activate functionality.
 * @item payloadWeightTextEdit: a field to register the weight of the cargo.
 * @item payloadVolumeTextEdit: a field to register the volume of the cargo.
 * Warning: payloadWeightTextEdit and payloadVolumeTextEdit currently disabled and hidden. See README.md to re-activate functionality.
 * @item allSafetyChecksGroup: container for all safety checks checkboxes.
 * @item receptionAckBypass: a checkbox to bypass a lack of confirmation from the receiving station.
 * Warning: reception request feature currently disabled and hidden. See README.md to re-activate functionality.
 * @item safetyCheckConfirmButton: button to continue to the next step (drone connection and send mission).
 * @item safetyCheckCancelButton: button to cancel and go back to idle state.
 * @item receptionReadyTimer: a timer to resend a reception request and make the current acknowledgement invalid after 15 minutes.
 * Warning: reception request feature currently disabled and hidden. See README.md to re-activate functionality.
 */
Popup
{
    id: safetyCheckPopup

    closePolicy: Popup.NoAutoClose
    focus: true
    modal: true
    x: Math.round((parent.width - safetyCheckMainRectangle.width) / 2)
    y: Math.round((parent.height - safetyCheckMainRectangle.height) / 2)

    readonly property int maxPayloadWeight: 2000 // [g]
    readonly property int maxPayloadVolume: 2000 // [ml]
    readonly property int checkboxSize: mainWindow.height / 24

    // Uncomment for reception request feature
//    property bool receptionReady: false

    Connections
    {
        target: stateMachine
        // Uncomment for reception request feature
//        onReceptionAckReceived:
//        {
//            safetyCheckPopup.receptionReady = true
//            receptionReadyTimer.start()
//        }
        // Uncomment for reception request feature
//        onReceptionRequestSent: safetyCheckPopup.open()
        function onResetAll()
        {
            allSafetyChecksGroup.checkState = Qt.Unchecked
            safetyCheckPopup.close()
            // Uncomment for payload checks
//            payloadWeightTextEdit.text = ""
//            payloadVolumeTextEdit.text = ""
            // Uncomment for reception request feature
//            receptionReady = false
//            receptionAckBypass.checkState = Qt.Unchecked
        }
    }

    // Global container for the safety checks popup. Contains:
    // - text areas for pilot name(s) (and payload weight and volume)
    // - checkboxes for pre-flight checks
    // - continue and cancel buttons
    Rectangle
    {
        id: safetyCheckMainRectangle

        height: childrenRect.height + 100
        width: childrenRect.width + 100

        Row
        {
            id: sendingPilotRow

            anchors { top: parent.top; topMargin: 50; left: parent.left; leftMargin: 50 }
            spacing: 20

            Label
            {
                id: sendingPilotLabel

                font.pointSize: 20
                fontSizeMode: Text.Fit
                height: sendingPilotTextEdit.font.pointSize * 3
                text: qsTr("Pilot name: ")
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }

            Rectangle
            {
                border.color: sendingPilotTextEdit.activeFocus ? problemTextColor : enabledTextColor
                height: sendingPilotTextEdit.font.pointSize * 3
                width: 400

                TextField
                {
                    id: sendingPilotTextEdit

                    anchors { fill: parent; margins: 5 }
                    focus: false
                    font.pointSize: sendingPilotLabel.font.pointSize
                    height: parent.height
                    Keys.onReturnPressed: sendingPilotTextEdit.focus = false
                    text: defaultSendingPilotName
                    horizontalAlignment: TextInput.AlignLeft
                    verticalAlignment: TextInput.AlignBottom

                    MouseArea
                    {
                        anchors.fill: parent
                        onClicked:
                        {
                            parent.focus = true
                            parent.text = ""
                        }
                    }

                    onFocusChanged:
                    {
                        if((!sendingPilotTextEdit.length) /*|| (!receivingPilotTextEdit.length)*/) // Uncomment for receiving pilot info
                            safetyCheckConfirmButton.text = qsTr("Enter pilot name")
                        // Uncomment for payload checks
//                        else if(!payloadWeightTextEdit.length || !payloadVolumeTextEdit.length)
//                            safetyCheckConfirmButton.text = "Enter payload data"
//                        else if((payloadWeightTextEdit.text <= 0) || (payloadVolumeTextEdit.text <= 0))
//                            safetyCheckConfirmButton.text = "Payload error"
//                        else if (payloadWeightTextEdit.text > maxPayloadWeight)
//                            safetyCheckConfirmButton.text = "Payload too heavy"
//                        else if (payloadVolumeTextEdit.text > maxPayloadVolume)
//                            safetyCheckConfirmButton.text = "Payload too bulky"
                        else if (safetyMainCheck.checkState != 2)
                            safetyCheckConfirmButton.text = qsTr("Perform safety checks")
                        // Uncomment for reception request feature
//                        else if(!receptionReady)
//                            safetyCheckConfirmButton.text = "Waiting for receiving station"
                    }
                }
            }
        }

        // Uncomment for receiving pilot info + change anchors of next row
//        Row
//        {
//            id: receivingPilotRow

//            anchors { top: sendingPilotRow.bottom; topMargin: 20; left: parent.left; leftMargin: 50 }
//            spacing: 20

//            Label
//            {
//                id: receivingPilotLabel

//                font.pointSize: 20
//                fontSizeMode: Text.Fit
//                height: receivingPilotTextEdit.font.pointSize * 3
//                text: "Receiving pilot name: "
//                horizontalAlignment: Text.AlignLeft
//                verticalAlignment: Text.AlignVCenter
//            }

//            Rectangle
//            {
//                border.color: receivingPilotTextEdit.activeFocus ? problemTextColor : enabledTextColor
//                height: receivingPilotTextEdit.font.pointSize * 3
//                width: sendingPilotTextEdit.width

//                TextField
//                {
//                    id: receivingPilotTextEdit

//                    anchors { fill: parent; margins: 5 }
//                    focus: false
//                    font.pointSize: receivingPilotLabel.font.pointSize
//                    height: parent.height
//                    Keys.onReturnPressed: receivingPilotTextEdit.focus = false
//                    text: defaultReceivingPilotName
//                    horizontalAlignment: TextInput.AlignLeft
//                    verticalAlignment: TextInput.AlignBottom

//                    MouseArea
//                    {
//                        anchors.fill: parent
//                        onClicked:
//                        {
//                            parent.focus = true
//                            parent.text = ""
//                        }
//                    }

//                    onFocusChanged:
//                    {
//                        if((!sendingPilotTextEdit.length) || (!receivingPilotTextEdit.length))
//                            safetyCheckConfirmButton.text = "Enter pilot names"
//                        else if(!payloadWeightTextEdit.length || !payloadVolumeTextEdit.length)
//                            safetyCheckConfirmButton.text = "Enter payload data"
//                        else if((payloadWeightTextEdit.text <= 0) || (payloadVolumeTextEdit.text <= 0))
//                            safetyCheckConfirmButton.text = "Payload error"
//                        else if (payloadWeightTextEdit.text > maxPayloadWeight)
//                            safetyCheckConfirmButton.text = "Payload too heavy"
//                        else if (payloadVolumeTextEdit.text > maxPayloadVolume)
//                            safetyCheckConfirmButton.text = "Payload too bulky"
//                        else if (safetyMainCheck.checkState != 2)
//                            safetyCheckConfirmButton.text = "Perform safety checks"
//                        else if(!receptionReady)
//                            safetyCheckConfirmButton.text = "Waiting for receiving station"
//                    }
//                }
//            }
//        }

        // Uncomment for payload checks + change anchors of safetychecks below
//        Row
//        {
//            id: payloadRow

//            anchors { top: sendingPilotRow.bottom; topMargin: 20; left: parent.left; leftMargin: 50 }
//            spacing: 20

//            Label
//            {
//                id: payloadLabel

//                font.pointSize: 20
//                fontSizeMode: Text.Fit
//                height: payloadWeightTextEdit.font.pointSize * 3
//                text: "Payload weight and volume: "
//                horizontalAlignment: Text.AlignLeft
//                verticalAlignment: Text.AlignVCenter
//            }

//            Rectangle
//            {
//                border.color: payloadWeightTextEdit.activeFocus ? problemTextColor : enabledTextColor
//                height: payloadWeightTextEdit.font.pointSize * 3
//                width: 4 * payloadWeightLabel.width

//                TextField
//                {
//                    id: payloadWeightTextEdit

//                    anchors { fill: parent; margins: 5 }
//                    focus: false
//                    font.pointSize: payloadLabel.font.pointSize
//                    height: parent.height
//                    inputMethodHints: Qt.ImhDigitsOnly
//                    Keys.onReturnPressed: payloadWeightTextEdit.focus = false
//                    horizontalAlignment: TextInput.AlignRight
//                    verticalAlignment: TextInput.AlignBottom

//                    MouseArea
//                    {
//                        anchors.fill: parent
//                        onClicked:
//                        {
//                            parent.focus = true
//                            parent.text = ""
//                        }
//                    }

//                    onFocusChanged:
//                    {
//                        if(!payloadWeightTextEdit.length || !payloadVolumeTextEdit.length)
//                            safetyCheckConfirmButton.text = "Enter payload data"
//                        else if((payloadWeightTextEdit.text <= 0) || (payloadVolumeTextEdit.text <= 0))
//                            safetyCheckConfirmButton.text = "Payload error"
//                        else if (payloadWeightTextEdit.text > maxPayloadWeight)
//                            safetyCheckConfirmButton.text = "Payload too heavy"
//                        else if (payloadVolumeTextEdit.text > maxPayloadVolume)
//                            safetyCheckConfirmButton.text = "Payload too bulky"
//                        else if((!sendingPilotTextEdit.length) /*|| (!receivingPilotTextEdit.length)*/)
//                            safetyCheckConfirmButton.text = "Enter pilot names"
//                        else if (safetyMainCheck.checkState != 2)
//                            safetyCheckConfirmButton.text = "Perform safety checks"
//                        else if(!receptionReady)
//                            safetyCheckConfirmButton.text = "Waiting for receiving station"
//                    }
//                }
//            }

//            Label
//            {
//                id: payloadWeightLabel

//                font.pointSize: 20
//                fontSizeMode: Text.Fit
//                height: payloadWeightTextEdit.font.pointSize * 3
//                text: "[g]"
//                horizontalAlignment: Text.AlignLeft
//                verticalAlignment: Text.AlignVCenter
//            }

//            Rectangle
//            {
//                border.color: payloadVolumeTextEdit.activeFocus ? problemTextColor : enabledTextColor
//                height: payloadVolumeTextEdit.font.pointSize * 3
//                width: 4 * payloadWeightLabel.width

//                TextField
//                {
//                    id: payloadVolumeTextEdit

//                    anchors { fill: parent; margins: 5 }
//                    focus: false
//                    font.pointSize: payloadLabel.font.pointSize
//                    height: parent.height
//                    inputMethodHints: Qt.ImhDigitsOnly
//                    Keys.onReturnPressed: payloadVolumeTextEdit.focus = false
//                    horizontalAlignment: TextInput.AlignRight
//                    verticalAlignment: TextInput.AlignBottom

//                    MouseArea
//                    {
//                        anchors.fill: parent
//                        onClicked:
//                        {
//                            parent.focus = true
//                            parent.text = ""
//                        }
//                    }

//                    onFocusChanged:
//                    {
//                        if(!payloadWeightTextEdit.length || !payloadVolumeTextEdit.length)
//                            safetyCheckConfirmButton.text = "Enter payload data"
//                        else if((payloadWeightTextEdit.text <= 0) || (payloadVolumeTextEdit.text <= 0))
//                            safetyCheckConfirmButton.text = "Payload error"
//                        else if (payloadVolumeTextEdit.text > maxPayloadVolume)
//                            safetyCheckConfirmButton.text = "Payload too bulky"
//                        else if (payloadWeightTextEdit.text > maxPayloadWeight)
//                            safetyCheckConfirmButton.text = "Payload too heavy"
//                        else if((!sendingPilotTextEdit.length) /*|| (!receivingPilotTextEdit.length)*/)
//                            safetyCheckConfirmButton.text = "Enter pilot names"
//                        else if (safetyMainCheck.checkState != 2)
//                            safetyCheckConfirmButton.text = "Perform safety checks"
//                        else if(!receptionReady)
//                            safetyCheckConfirmButton.text = "Waiting for receiving station"
//                    }
//                }
//            }

//            Label
//            {
//                id: payloadVolumeLabel

//                font.pointSize: 20
//                fontSizeMode: Text.Fit
//                height: payloadVolumeTextEdit.font.pointSize * 3
//                text: "[ml]"
//                horizontalAlignment: Text.AlignLeft
//                verticalAlignment: Text.AlignVCenter
//            }
//        }

        Binding
        {
            target: safetyCheckConfirmButton
            property: "enabled"
            value: ((safetyMainCheck.checkState == 2) &&
                    (sendingPilotTextEdit.text.length > 0) //&&
                    // Uncomment for receiving pilot info
//                    (receivingPilotTextEdit.text.length > 0) &&
                    // Uncomment for payload checks
//                    (payloadWeightTextEdit.text > 0) &&
//                    (payloadWeightTextEdit.text < maxPayloadWeight) &&
//                    (payloadVolumeTextEdit.text > 0) &&
//                    (payloadVolumeTextEdit.text < maxPayloadVolume) &&
                    // Uncomment for reception request feature
//                    (safetyCheckPopup.receptionReady)
                    )
        }

        ButtonGroup
        {
            id: allSafetyChecksGroup

            checkState: safetyMainCheck.checkState
            exclusive: false
        }

        SafetyCheckBox
        {
            id: safetyMainCheck

            anchors { top: /*payloadRow*/sendingPilotRow.bottom; topMargin: 10; left: parent.left; leftMargin: 50 }
            checkable: false
            checkState: allSafetyChecksGroup.checkState
            indentationLevel: 0
            text: qsTr("Safety checks")

            onCheckStateChanged:
            {
                if (safetyMainCheck.checkState != 2)
                    safetyCheckConfirmButton.text = qsTr("Perform safety checks")
                // Uncomment for payload checks
//                else if(!payloadWeightTextEdit.length || !payloadVolumeTextEdit.length)
//                    safetyCheckConfirmButton.text = "Enter payload data"
//                else if((payloadWeightTextEdit.text <= 0) || (payloadVolumeTextEdit.text <= 0))
//                    safetyCheckConfirmButton.text = "Payload error"
//                else if (payloadWeightTextEdit.text > maxPayloadWeight)
//                    safetyCheckConfirmButton.text = "Payload too heavy"
//                else if (payloadVolumeTextEdit.text > maxPayloadVolume)
//                    safetyCheckConfirmButton.text = "Payload too bulky"
                else if((!sendingPilotTextEdit.length) /*|| (!receivingPilotTextEdit.length)*/) // Uncomment for receiving pilot info
                    safetyCheckConfirmButton.text = qsTr("Enter pilot name")
                // Uncomment for reception request feature
//                else if(!receptionReady)
//                    safetyCheckConfirmButton.text = "Waiting for receiving station"
            }
        }

        Row
        {
            id: safetySecondaryChecks

            anchors { top: safetyMainCheck.bottom; left: parent.left; leftMargin: 50; topMargin: -10 }

            Column
            {
                spacing: -10
                // Uncomment for subcategory labels of checks (one for each column)
//                ButtonGroup
//                {
//                    id: materialSafetyChecksGroup
//                    checkState: materialCheck.checkState
//                    exclusive: false
//                }

//                SafetyCheckBox
//                {
//                    id: materialCheck

//                    ButtonGroup.group: allSafetyChecksGroup
//                    checkable: false
//                    checkState: materialSafetyChecksGroup.checkState
//                    indentationLevel: 1
//                    text: "Material inspection"
//                }

                SafetyCheckBox
                {
                    ButtonGroup.group: allSafetyChecksGroup // materialSafetyChecksGroup
                    indentationLevel: 1 // 2
                    text: qsTr("Tablet battery charged")
                }

                SafetyCheckBox
                {
                    ButtonGroup.group: allSafetyChecksGroup // materialSafetyChecksGroup
                    indentationLevel: 1 // 2
                    text: qsTr("Samples loaded into cargo box")
                }

                SafetyCheckBox
                {
                    ButtonGroup.group: allSafetyChecksGroup // materialSafetyChecksGroup
                    indentationLevel: 1 // 2
                    text: qsTr("Drone and cargo box labelled")
                }

                SafetyCheckBox
                {
                    ButtonGroup.group: allSafetyChecksGroup // materialSafetyChecksGroup
                    indentationLevel: 1 // 2
                    text: qsTr("Cargo box secured under drone")
                }

                SafetyCheckBox
                {
                    ButtonGroup.group: allSafetyChecksGroup // materialSafetyChecksGroup
                    indentationLevel: 1 // 2
                    text: qsTr("Drone visually inspected")
                }
            }

            Column
            {
                spacing: -10
                // Uncomment for subcategory labels of checks (one for each column)
//                ButtonGroup
//                {
//                    id: environmentalSafetyChecksGroup
//                    checkState: environmentalCheck.checkState
//                    exclusive: false
//                }

//                SafetyCheckBox
//                {
//                    id: environmentalCheck

//                    ButtonGroup.group: allSafetyChecksGroup
//                    checkable: false
//                    checkState: environmentalSafetyChecksGroup.checkState
//                    indentationLevel: 1
//                    text: "Environmental inspection"
//                }

                SafetyCheckBox
                {
                    ButtonGroup.group: allSafetyChecksGroup // environmentalSafetyChecksGroup
                    indentationLevel: 1 // 2
                    text: qsTr("Authorities informed")
                }

                SafetyCheckBox
                {
                    ButtonGroup.group: allSafetyChecksGroup // environmentalSafetyChecksGroup
                    indentationLevel: 1 // 2
                    text: qsTr("Flight plan up-to-date and synchronised")
                }

                SafetyCheckBox
                {
                    ButtonGroup.group: allSafetyChecksGroup // environmentalSafetyChecksGroup
                    indentationLevel: 1 // 2
                    text: qsTr("Take-off site cleared")
                }
            }
        }

        // Uncomment for reception request feature
//        Binding
//        {
//            target: receptionAckBypass
//            property: "visible"
//            value: ((!safetyCheckPopup.receptionReady) || ((safetyCheckPopup.receptionReady) && (receptionAckBypass.checked)))
//        }

        // Uncomment for reception request feature + change anchors of next Row
//        CheckBox
//        {
//            id: receptionAckBypass

//            anchors { top: safetySecondaryChecks.bottom; topMargin: 0; left: parent.left; leftMargin: 50 }
//            font.pointSize: 20
//            text: "Continue without confirmation from the receiving station.\nNOT RECOMMENDED unless the delivery has been confirmed by phone call or text message."
//            width: safetySecondaryChecks.width

//            indicator: Rectangle
//            {
//                id: receptionAckBypassIndicator

//                anchors.verticalCenter: parent.verticalCenter
//                border { color: "red"; width: 2 }
//                radius: 3
//                height: parent.height / 2
//                width: height

//                Rectangle
//                {
//                    anchors { margins: 4; fill: parent }
//                    color: "red"
//                    radius: 1
//                    visible: receptionAckBypass.checked
//                }
//            }

//            contentItem: Text
//            {
//                anchors { left: receptionAckBypassIndicator.right; leftMargin: 30; right: parent.right }
//                color: "red"
//                font.pointSize: 20
//                text: receptionAckBypass.text
//                wrapMode: Text.Wrap
//            }

//            onCheckStateChanged:
//            {
//                receptionReady = receptionAckBypass.checkState

//                if(!receptionReady)
//                {
//                    stateMachine.sendReceptionRequestOrReady(0);

//                    if(!payloadWeightTextEdit.length || !payloadVolumeTextEdit.length)
//                        safetyCheckConfirmButton.text = "Enter payload data"
//                    else if((payloadWeightTextEdit.text <= 0) || (payloadVolumeTextEdit.text <= 0))
//                        safetyCheckConfirmButton.text = "Payload error"
//                    else if (payloadWeightTextEdit.text > maxPayloadWeight)
//                        safetyCheckConfirmButton.text = "Payload too heavy"
//                    else if (payloadVolumeTextEdit.text > maxPayloadVolume)
//                        safetyCheckConfirmButton.text = "Payload too bulky"
//                    else if((!sendingPilotTextEdit.length) /*|| (!receivingPilotTextEdit.length)*/)
//                        safetyCheckConfirmButton.text = "Enter pilot names"
//                    else if (safetyMainCheck.checkState != 2)
//                        safetyCheckConfirmButton.text = "Perform safety checks"
//                    else if(!receptionReady)
//                        safetyCheckConfirmButton.text = "Waiting for receiving station"
//                }
//            }
//        }

        Row
        {
            anchors { top: /*receptionAckBypass*/safetySecondaryChecks.bottom; topMargin: 5; horizontalCenter: safetySecondaryChecks.horizontalCenter }
            spacing: 40

            Button
            {
                id: safetyCheckConfirmButton

                enabled: false
                text: qsTr("Perform safety checks")

                contentItem: Text
                {
                    color: safetyCheckConfirmButton.enabled ? enabledTextColor : disabledTextColor
                    font.pointSize: 25
                    fontSizeMode: Text.Fit
                    text: safetyCheckConfirmButton.text
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                onEnabledChanged:
                {
                    if(enabled)
                        safetyCheckConfirmButton.text = qsTr("Continue")
                }
                onClicked:
                {
                    // Uncomment for reception request feature
//                    receptionReadyTimer.stop()
                    safetyCheckPopup.close()
                    stateMachine.checkDrone(routeList[currentRouteIndex].displayName,
                                            routeList[currentRouteIndex].length,
                                            0, // Replace by payloadWeightTextEdit.text, for payload checks
                                            0, // Replace by payloadVolumeTextEdit.text, for payload checks
                                            sendingPilotTextEdit.text,
                                            "" // Replace by receivingPilotTextEdit.text for receiving pilot info
                                           )
                }
            }

            Button
            {
                id: safetyCheckCancelButton

                text: qsTr("Cancel")

                contentItem: Text
                {
                    color: safetyCheckCancelButton.enabled ? enabledTextColor : disabledTextColor
                    font.pointSize: 25
                    fontSizeMode: Text.Fit
                    text: qsTr(safetyCheckCancelButton.text)
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }
                onClicked:
                {
                    // Uncomment for reception request feature
//                    receptionReadyTimer.stop()
                    safetyCheckPopup.close()
                    stateMachine.resetState();
                }
            }
        }
    }

//     Uncomment for reception request feature
//    Timer
//    {
//        id: receptionReadyTimer

//        interval: 900000 // 15 minutes
//        onTriggered:
//        {
//            safetyCheckPopup.receptionReady = false

//            stateMachine.sendReceptionRequestOrReady(0);

//            if(!payloadWeightTextEdit.length || !payloadVolumeTextEdit.length)
//                safetyCheckConfirmButton.text = "Enter payload data"
//            else if((payloadWeightTextEdit.text <= 0) || (payloadVolumeTextEdit.text <= 0))
//                safetyCheckConfirmButton.text = "Payload error"
//            else if (payloadWeightTextEdit.text > maxPayloadWeight)
//                safetyCheckConfirmButton.text = "Payload too heavy"
//            else if (payloadVolumeTextEdit.text > maxPayloadVolume)
//                safetyCheckConfirmButton.text = "Payload too bulky"
//            else if((!sendingPilotTextEdit.length) /*|| (!receivingPilotTextEdit.length)*/)
//                safetyCheckConfirmButton.text = "Enter pilot names"
//            else if (safetyMainCheck.checkState != 2)
//                safetyCheckConfirmButton.text = "Perform safety checks"
//            else if(!receptionReady)
//                safetyCheckConfirmButton.text = "Waiting for receiving station"
//        }
//    }
}
