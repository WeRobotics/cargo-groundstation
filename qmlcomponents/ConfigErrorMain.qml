import QtQuick 2.8
import QtQuick.Controls 2.4

/*
 * The ConfigErrorMain mainWindow ApplicationWindow is the main QML object in case of configuration error.
 *
 * It contains only the infoAndCredits objets to tell the user to look at the logs and fix the configuration file.
 */
ApplicationWindow
{
    id: mainWindow

    readonly property int iconSize: mainWindow.height / 11.5
    readonly property string darkBackgroundColor: "#88000000"
    readonly property string itemOnDarkBackgroundColor: "#ffffff"//"#dcdcdc"
    readonly property string weRoboticsColor: "#C64F38"

    visibility: "FullScreen"
    visible: true

    InfoAndCredits
    {
        id: infoAndCredits
        configError: true
    }
}
