import QtGraphicalEffects 1.0
import QtQuick 2.8

/*
 * The joystickContainer Item is a custom joystick for the virtual gamepad.
 *
 * When pressed, it transfers continuous commands (roll, pitch, yaw or altitude) to the drone.
 * The joystick's center aligns under user's thumb on first touch and moves back to its original position when released.
 */
Item
{
    id: joystickContainer

    property string xChar: "K"
    property string yChar: "K"
    property real baseVOffset: 0
    property real baseHOffset: 0

    signal movedToAlign(real x, real y)

    anchors.centerIn: parent
    height: 300
    width: 300

    // Gradient coloured container disc
    Rectangle
    {
        id: joystick

        property real angle : 0
        property real distance : 0
        property real normalizedX: 0
        property real normalizedY: 0

        anchors.fill: parent
        color: "#aaaaaa"
        border.color: "#666666"
        radius: width * 0.5

        onNormalizedXChanged: gamepadManager.updateVirtualJoystickCommand(xChar, normalizedX)
        onNormalizedYChanged: gamepadManager.updateVirtualJoystickCommand(yChar, normalizedY)

        Binding
        {
            target: joystick
            property: "normalizedX"
            value: thumb.anchors.horizontalCenterOffset / width * 4
        }

        Binding
        {
            target: joystick
            property: "normalizedY"
            value: thumb.anchors.verticalCenterOffset / width * 4
        }

        // When joystick released
        ParallelAnimation
        {
            id: returnToCenterAnimation

            NumberAnimation { target: thumb.anchors; property: "horizontalCenterOffset";
                to: 0; duration: 200; easing.type: Easing.OutSine }
            NumberAnimation { target: thumb.anchors; property: "verticalCenterOffset";
                to: 0; duration: 200; easing.type: Easing.OutSine }
        }
    }

    // Detect and act on any touch, move, or release in the joystickContainer area
    MouseArea
    {
        property real fingerAngle : Math.atan2(mouseX, mouseY)
        property int mcx : mouseX - width * 0.5
        property int mcy : mouseY - height * 0.5
        property bool fingerOutOfBounds : fingerDistance2 < distanceBound2
        property real fingerDistance2 : mcx * mcx + mcy * mcy
        property real distanceBound : width * 0.5 - thumb.width * 0.5
        property real distanceBound2 : distanceBound * distanceBound

        anchors.fill: parent

        // Align joystick under user's thumb on first touch
        onPressed:
        {
            movedToAlign(mouseX - width * 0.5, mouseY - height * 0.5)
            joystickContainer.anchors.horizontalCenterOffset = baseHOffset + mouseX - width * 0.5
            joystickContainer.anchors.verticalCenterOffset = baseVOffset + mouseY - height * 0.5
            gamepadManager.updateVirtualSendContinuous(true)
            returnToCenterAnimation.stop()
        }

        // Return joystick to original position
        onReleased:
        {
            movedToAlign(0, 0)
            joystickContainer.anchors.horizontalCenterOffset = baseHOffset
            joystickContainer.anchors.verticalCenterOffset = baseVOffset
            gamepadManager.updateVirtualSendContinuous(false)
            returnToCenterAnimation.restart()
        }

        // Update display of joystick
        onPositionChanged:
        {
            if (fingerOutOfBounds)
            {
                thumb.anchors.horizontalCenterOffset = mcx
                thumb.anchors.verticalCenterOffset = mcy
            }
            else
            {
                var angle = Math.atan2(mcy, mcx)
                thumb.anchors.horizontalCenterOffset = Math.cos(angle) * distanceBound
                thumb.anchors.verticalCenterOffset = Math.sin(angle) * distanceBound
            }
        }
    }

    RadialGradient
    {
        id: joystickGradient

        anchors.fill: joystick
        source: joystick

        gradient: Gradient
        {
            GradientStop { position: 0.0; color: "#666666" } //
            GradientStop { position: 0.5; color: "#cccccc" } //
        }
    }

    // Gradient coloured "thumb" + shadow on container
    DropShadow
    {
        id: thumbShadow

        anchors.fill: thumb
        color: darkBackgroundColor
        radius: 8.0
        samples: 2 * radius + 1
        source: thumb
        transparentBorder: true
        horizontalOffset: source.width / 15
        verticalOffset: source.width / 15
    }

    Rectangle
    {
        id: thumb

        anchors.centerIn: joystick
        color: "#222222"
        radius: height * 0.5
        height: joystick.height / 2
        width: joystick.width / 2
    }

    RadialGradient
    {
        id: thumbGradient

        anchors.fill: thumb
        source: thumb
        horizontalOffset: - source.width / 15
        verticalOffset: - source.width / 15

        gradient: Gradient
        {
            GradientStop { position: 0.0; color: "#555555" }
            GradientStop { position: 0.5; color: "#222222" }
        }
    }
}
