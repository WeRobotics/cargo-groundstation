import QtGraphicalEffects 1.0
import QtLocation 5.11//12
import QtPositioning 5.6
import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.11
import Qt.labs.folderlistmodel 2.11
import Qt.labs.platform 1.0

/*
 * The mainMap Map is the core of the GUI.
 * It fills the screen and displays the drone and tablet locations on a map
 * as well as the planned route with its rally points, the path flown and relay stations.
 *
 * Three different map providers are available (see MapPlugin.qml) each with a list of various map types.
 */
Map
{
    id: mainMap

    readonly property int earthRadius: 6378137; // meters (m)

    property real yawAngle: 0
    property real scaleValue: 0
    property real scaleLength: mainMap.width / 4

    property var droneOnMap: MapQuickItem
    {
        id: droneOnMap

        anchorPoint { x: droneIcon.width / 2; y: droneIcon.height / 2 }
        coordinate: QtPositioning.coordinate(0,0)

        sourceItem: Image
        {
            id: droneIcon

            opacity: droneIsConnected ? 1.0 : 0.3
            source: "../icons/Drone.PNG"
            transform: Rotation { id: droneHeading; origin.x: droneIcon.width / 2; origin.y: droneIcon.height / 2; angle: - mainMap.bearing}
            height: 60
            width: height

            Image
            {
                id: droneHeadingIcon

                anchors { horizontalCenter: droneIcon.horizontalCenter; bottom: droneIcon.top }
                height: 30
                width: 30
                source: "../icons/ArrowUp.png"
            }
        }
    }

    function updateTabletLocationAccuracy()
    {
        accuracyCircle.height = tabletLocationAccuracy / metersInOnePixel()
    }

    // Re-compute scale length and value (distance represented)
    function updateScale()
    {
        // Scale should be ~1/4 of screen width
        var approximateScaleLength = mainMap.width / 4;
        var approximateScaleValue = metersInOnePixel() / 1000 * approximateScaleLength; // in km

        if(approximateScaleValue < 1) // km
        {
            var indexOfFirstNonZeroDigit = 1
            var indexFound = false
            while(!indexFound)
            {
                indexOfFirstNonZeroDigit++
                if(approximateScaleValue.toString().charAt(indexOfFirstNonZeroDigit) != '0')
                    indexFound = true
            }

            scaleValue = Math.pow(10, Math.floor(approximateScaleValue * Math.pow(10, indexOfFirstNonZeroDigit - 1)).toString().length - 1) * parseInt(approximateScaleValue.toString().substring(indexOfFirstNonZeroDigit, indexOfFirstNonZeroDigit + 1)) / Math.pow(10, indexOfFirstNonZeroDigit - 1);
            scaleLength = approximateScaleLength * scaleValue / approximateScaleValue
        }
        else
        {
            scaleValue = Math.pow(10, Math.floor(approximateScaleValue).toString().length - 1) * parseInt(approximateScaleValue.toString().substring(0, 1));
            scaleLength = mainMap.width / 4 * scaleValue / approximateScaleValue
        }
    }

    // Compute how much distance is represented in a pixel at current zoom level
    function metersInOnePixel()
    {
        var earthCircumference = 2 * Math.PI * earthRadius
        var latitudeAtHalfScreenHeightInRadians = radians((visibleRegion.boundingGeoRectangle().topRight.latitude + visibleRegion.boundingGeoRectangle().bottomRight.latitude) / 2)
        return metersInOnePixel = earthCircumference * Math.cos(latitudeAtHalfScreenHeightInRadians) / Math.pow(2, mainMap.zoomLevel + 8)
    }

    function radians(degrees)
    {
        return (degrees / 180 * Math.PI);
    }

    // Display selected mission path (route)
    function drawRouteOnMap()
    {
        routeOnMap.path = []
        rallyPointList.clear()

        if(currentRouteIndex)
        {
            // Display route line
            for(var i = 0; i < routeList[currentRouteIndex].getNumberOfWaypoints(); i++)
                routeOnMap.addCoordinate(routeList[currentRouteIndex].getWaypoint(i))

            // Display rally point icons
            var rallyPointCoord
            for(var j = 0; j < routeList[currentRouteIndex].getNumbeOfRallyPoints(); j++)
            {
                rallyPointCoord = routeList[currentRouteIndex].getRallyPoint(j)
                rallyPointList.append({pointLat : rallyPointCoord.latitude, pointLon: rallyPointCoord.longitude});
            }

            // Recenter map
            mainMap.center = routeList[currentRouteIndex].getWaypoint(0)
            mainMap.zoomLevel = 17;
        }

        mainMap.bearing = 0

        updateScale()
        updateTabletLocationAccuracy()
    }

    anchors { fill: parent; margins: 0 }
    bearing: 0
    center: tabletLocation
    plugin: mapPlugin
    z: -1
    zoomLevel: 10

    Connections
    {
        target: stateMachine
        function onAttitudeUpdated(roll, pitch, yaw) {
            // Update drone heading on display
            yawAngle = yaw // Angle between heading and north in radians [-pi;pi]
            if(yawAngle < 0)
                yawAngle += 2 * Math.PI;
            droneHeading.angle = - mainMap.bearing + yawAngle * 180 / Math.PI
        }
        function onResetAll() {
            center = tabletLocation
            zoomLevel = 10
            bearing = 0
            updateScale()
            updateTabletLocationAccuracy()
            droneTrace.path = []
        }
        function onTelemetryUpdated(lat, lon, alt, vx, vy, vz) {
            // Update drone icon position on map
            droneOnMap.coordinate = QtPositioning.coordinate(lat, lon, alt)
            if(lockOnDrone)
                mainMap.center = droneOnMap.coordinate

            // Update drone trace on map
            if(parseInt(lat) && parseInt(lon) && parseInt(alt))
            {
                droneTrace.addCoordinate(QtPositioning.coordinate(lat, lon, alt))
            }
        }
    }

    Component.onCompleted: {updateScale(); updateTabletLocationAccuracy();}

    MapPolyline
    {
        id: routeOnMap

        line { color: routeColor; width: routeWidth }
        opacity: 1.0
        smooth: true
    }

    MapPolyline
    {
        id: droneTrace

        line { color: itemOnDarkBackgroundColor; width: routeWidth }
        opacity: 1.0
        smooth: true
    }

    MapQuickItem
    {
        id: tabletLocationOnMap

        anchorPoint { x: locationDot.width / 2; y: locationDot.height / 2 }
        coordinate: tabletLocation

        sourceItem: Rectangle
        {
            id: accuracyCircle

            color: "#8811BAC7"
            radius: height * 0.5
            height: 60
            width: height

            Rectangle
            {
                id: locationDot

                anchors { horizontalCenter: parent.horizontalCenter; verticalCenter: parent.verticalCenter }
                border { color: "white"; width: 2 }
                color: Material.accent
                radius: height * 0.5
                height: 25
                width: height
            }
        }
    }

    Binding
    {
        target: routeStartOnMap
        property: "coordinate"
        value: routeList[currentRouteIndex].getWaypoint(0)
    }

    MapQuickItem
    {
        id: routeStartOnMap

        anchorPoint { x: startIcon.width / 2; y: startIcon.height }
        coordinate: QtPositioning.coordinate(0,0)

        sourceItem: Image
        {
            id: startIcon

            source: "../icons/Start.png"
            visible: currentRouteIndex? true : false
            height: 60
            width: height
        }
    }

    Binding
    {
        target: routeEndOnMap
        property: "coordinate"
        value: routeList[currentRouteIndex].getWaypoint(routeList[currentRouteIndex].getNumberOfWaypoints() - 1)
    }

    MapQuickItem
    {
        id: routeEndOnMap

        anchorPoint { x: endIcon.width * 82 / 512; y: endIcon.height * 502 / 512 }
        coordinate: QtPositioning.coordinate(0,0)

        sourceItem: Image
        {
            id: endIcon

            source: "../icons/End.png"
            visible: currentRouteIndex? true : false
            height: 60
            width: height
        }
    }

    MouseArea
    {
        id: doubleTapZoom

        property int mapCenterX: -1
        property int mapCenterY: -1

        anchors.fill : parent

        onPressed:
        {
            mapCenterX = mouse.x
            mapCenterY = mouse.y
        }

        onDoubleClicked:
        {
            mainMap.center = mainMap.toCoordinate(Qt.point(mapCenterX,mapCenterY))
            mainMap.zoomLevel += 1

            updateScale();
            updateTabletLocationAccuracy();
        }
    }

    ListModel
    {
        id: rallyPointList
    }

    MapItemView
    {
        model: rallyPointList
        delegate: RallyPoint
        {
            coordinate: QtPositioning.coordinate(pointLat, pointLon)
        }
    }

    MapItemView
    {
        model: relayStationList

        delegate: RelayStation{}
    }

    Rectangle
    {
        id: scaleRectangle

        anchors { left: mainMap.left; bottom: parent.bottom; bottomMargin: 30; leftMargin: 50 }
        color: "transparent"
        height: scaleBorderLeft.height + scaleTextLeft.height
        width: scaleLength + scaleTextLeft.width / 2 + scaleTextRight.width / 2

        Text
        {
            id: scaleTextLeft

            anchors { bottom: scaleBorderLeft.top; left: scaleRectangle.left; bottomMargin: 10 }
            color: "black";
            font.pointSize: 30
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            text: "0 km"
            height: 20
            width: 50
        }

        Rectangle
        {
            id: scaleBorderLeft

            anchors { bottom: scaleRectangle.bottom; horizontalCenter: scaleTextLeft.horizontalCenter }
            color: "black";
            visible: true
            height: 20
            width: 4
        }

        Rectangle
        {
            id: scaleLine

            anchors { verticalCenter: scaleBorderLeft.verticalCenter; left: scaleBorderLeft.left }
            color: "black";
            visible: true
            height: 5
            width: scaleLength
        }

        Rectangle
        {
            id: scaleBorderRight

            anchors { verticalCenter: scaleLine.verticalCenter; right: scaleLine.right }
            color: "black";
            visible: true
            height: 20
            width: 4
        }

        Text
        {
            id: scaleTextRight

            anchors { bottom: scaleBorderRight.top; horizontalCenter: scaleBorderRight.horizontalCenter; bottomMargin: 10 }
            color: "black";
            font.pointSize: 30
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignTop
            text: scaleValue + " km"
            height: 20
            width: 50
        }
    }

    Binding
    {
        target: scaleLine
        property: "width"
        value: scaleLength
    }

    gesture.onPinchFinished: {updateScale(); updateTabletLocationAccuracy();}
    gesture.onPanFinished: {updateScale(); updateTabletLocationAccuracy();}
    gesture.onTiltFinished: {updateScale(); updateTabletLocationAccuracy();}
}
