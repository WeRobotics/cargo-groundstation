import QtGraphicalEffects 1.0
import QtQuick 2.8
import QtQuick.Controls 2.4

/*
 * The infoAndCredits Rectangle contains a brief description of the app and context, a tutorial (in progress)
 * and failure handling procedures in a swipe view.
 *
 * It also displays the WeRobotics logo with a link to the Werobotics website and different contact options.
 */
Rectangle
{
    id: infoAndCredits

    readonly property int sideMargins: 50
    property bool configError: false

    anchors.fill: parent
    color: itemOnDarkBackgroundColor
    focus: true

    Component.onCompleted:
    {
        if(configError)
        {
            menuSwipeView.setCurrentIndex(2)
            groundFailureCombo.currentIndex = 2
        }
    }

    // Logo + text
    Rectangle
    {
        id: weRoboticsLogoHolder

        anchors { top: parent.top; left: parent.left; }
        color: "white"
        height: childrenRect.height + 20
        width: parent.width / 2 - 50
        z: menuSwipeView.z + 1

        Image
        {
            id: weRoboticsLogo

            anchors { top: parent.top; left: parent.left; leftMargin: 20; topMargin: 20 }
            source: "../icons/WeR.png"
        }

        Rectangle
        {
            anchors { top: weRoboticsLogo.bottom; left: weRoboticsLogo.left; leftMargin: 10 }
            color: "white"
            height: weRoboticsLogo.height / 4
            width: weRoboticsLogo.width - 15

            Text
            {
                anchors.fill: parent
                color: weRoboticsColor
                font.pointSize: 30
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: "Robotics for the Benefit of All"
            }
        }

        Rectangle
        {
            anchors { top: weRoboticsLogo.top; left: weRoboticsLogo.right; right: parent.right; leftMargin: 10; topMargin: 5 }
            color: "white"
            height: weRoboticsLogo.height

            Text
            {
                anchors.fill: parent

                color: weRoboticsColor
                font.pointSize: 30
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                text: "CARGO\nGROUND\nSTATION"
            }
        }
    }

    MouseArea
    {
        anchors.fill: weRoboticsLogoHolder

        onClicked: Qt.openUrlExternally("https://werobotics.org/");
    }

    // Main content
    Item
    {
        id: swipeViewHolder

        anchors.fill: parent

        SwipeView
        {
            id: menuSwipeView

            currentIndex: menuPageIndicator.currentIndex
            anchors.fill: parent

            Item
            {
                id: aboutPage

                Text
                {
                    id: aboutTitle

                    anchors { top: parent.top; right: parent.right; }
                    height: weRoboticsLogoHolder.height
                    width: parent. width - weRoboticsLogoHolder.width
                    font.pointSize: 50
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    text: "ABOUT"
                }

                Rectangle
                {
                    anchors { top: aboutTitle.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }

                    DropShadow
                    {
                        anchors.fill: aboutText
                        color: darkBackgroundColor
                        radius: 10.0
                        samples: 2 * radius + 1
                        source: aboutText
                        transparentBorder: true
                    }

                    Rectangle
                    {
                        id: aboutText

                        anchors { top: parent.top; left: parent.left; margins: 20 }
                        height: parent.height / 2 + 80
                        width: parent.width / 2 - 100
                        Text
                        {
                            anchors { fill: parent; margins: 20; }
                            font.pointSize: 30
                            fontSizeMode: Text.Fit
                            horizontalAlignment: Text.AlignJustify
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("Cargo GS is a custom Android ground station application that allows the user to easily send a drone on a predefined mission, monitor it during the flight, and intervene in case of problem. It is built for medical cargo deliveries in remote and hard-to-reach areas in developing countries with the goal to be used by trained health workers without an engineering or drone pilot background. However, in the current state, a certified drone pilot should be in charge at all time for any use of the app. It is still a prototype.")
                            wrapMode: Text.Wrap
                        }
                    }

                    DropShadow
                    {
                        anchors.fill: authorText
                        color: darkBackgroundColor
                        radius: 10.0
                        samples: 2 * radius + 1
                        source: authorText
                        transparentBorder: true
                    }

                    Rectangle
                    {
                        id: authorText

                        anchors { top: aboutText.bottom; left: parent.left; margins: 20 }
                        height: parent.height / 4 - 80
                        width: parent.width / 2 - 100
                        Text
                        {
                            anchors { fill: parent; margins: 20; }
                            font.pointSize: 30
                            fontSizeMode: Text.Fit
                            horizontalAlignment: Text.AlignJustify
                            verticalAlignment: Text.AlignVCenter
                            text: qsTr("by Audrey Marullaz for WeRobotics")
                        }
                    }

                    DropShadow
                    {
                        anchors.fill: contactText
                        color: darkBackgroundColor
                        radius: 10.0
                        samples: 2 * radius + 1
                        source: contactText
                        transparentBorder: true
                    }

                    Rectangle
                    {
                        id: contactText

                        anchors { top: parent.top; right: creditsText.left; margins: 20 }
                        height: parent.height * 3 / 4 + 20
                        width: parent.width / 4 + 80

                        Column
                        {
                            anchors { fill: parent; margins: 20; }
                            spacing: 20

                            Button
                            {
                                id: emailButton

                                onClicked: Qt.openUrlExternally("mailto:engineering@werobotics.org");
                                height: 50
                                width: contactText.width - 40

                                background: Rectangle { anchors.fill: parent }

                                contentItem: Row
                                {
                                    spacing: 10

                                    Image
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        source: "../icons/Email.png" // Icons made by "https://www.flaticon.com/authors/yannick"
                                        smooth: true
                                        height: iconSize / 2
                                        width: height
                                    }
                                    Text
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        font.pointSize: 30
                                        fontSizeMode: Text.Fit
                                        height: iconSize / 2
                                        text: qsTr("Send us an email")
                                        verticalAlignment: Text.AlignVCenter
                                    }
                                }

                                states: State
                                {
                                    name: "pressed"; when: emailButton.pressed
                                    PropertyChanges { target: emailButton; scale: 1.2 }
                                }

                                transitions: Transition
                                {
                                    NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                }
                            }

                            Button
                            {
                                id: blogButton

                                onClicked: Qt.openUrlExternally("https://blog.werobotics.org");
                                height: 50
                                width: contactText.width - 40

                                background: Rectangle { anchors.fill: parent }

                                contentItem: Row
                                {
                                    spacing: 10

                                    Image
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        source: "../icons/Blog.png"
                                        smooth: true
                                        height: iconSize / 2
                                        width: height
                                    }
                                    Text
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        font.pointSize: 30
                                        fontSizeMode: Text.Fit
                                        height: iconSize / 2
                                        text: qsTr("Read us on our blog")
                                        verticalAlignment: Text.AlignVCenter
                                    }
                                }

                                states: State
                                {
                                    name: "pressed"; when: blogButton.pressed
                                    PropertyChanges { target: blogButton; scale: 1.2 }
                                }

                                transitions: Transition
                                {
                                    NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                }
                            }

                            Button
                            {
                                id: twitterButton

                                onClicked: Qt.openUrlExternally("https://twitter.com/werobotics");
                                height: 50
                                width: contactText.width - 40

                                background: Rectangle { anchors.fill: parent }

                                contentItem: Row
                                {
                                    spacing: 10

                                    Image
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        source: "../icons/Twitter.png" // Icons made by "https://www.flaticon.com/authors/freepik"
                                        smooth: true
                                        height: iconSize / 2
                                        width: height
                                    }
                                    Text
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        font.pointSize: 30
                                        fontSizeMode: Text.Fit
                                        height: iconSize / 2
                                        text: qsTr("Follow us on Twitter")
                                        verticalAlignment: Text.AlignVCenter
                                    }
                                }

                                states: State
                                {
                                    name: "pressed"; when: twitterButton.pressed
                                    PropertyChanges { target: twitterButton; scale: 1.2 }
                                }

                                transitions: Transition
                                {
                                    NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                }
                            }

                            Button
                            {
                                id: instagramButton

                                onClicked: Qt.openUrlExternally("https://www.instagram.com/werobotics/");
                                height: 50
                                width: contactText.width - 40

                                background: Rectangle { anchors.fill: parent }

                                contentItem: Row
                                {
                                    spacing: 10

                                    Image
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        source: "../icons/Instagram.png" // Icons made by "https://www.flaticon.com/authors/freepik"
                                        smooth: true
                                        height: iconSize / 2
                                        width: height
                                    }
                                    Text
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        font.pointSize: 30
                                        fontSizeMode: Text.Fit
                                        height: iconSize / 2
                                        text: qsTr("Follow us on Instagram")
                                        verticalAlignment: Text.AlignVCenter
                                    }
                                }

                                states: State
                                {
                                    name: "pressed"; when: instagramButton.pressed
                                    PropertyChanges { target: instagramButton; scale: 1.2 }
                                }

                                transitions: Transition
                                {
                                    NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                }
                            }

                            Button
                            {
                                id: facebookButton

                                onClicked: Qt.openUrlExternally("https://www.facebook.com/werobotics/");
                                height: 50
                                width: contactText.width - 40

                                background: Rectangle { anchors.fill: parent }

                                contentItem: Row
                                {
                                    spacing: 10

                                    Image
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        source: "../icons/Facebook.png" // Icons made by "https://www.flaticon.com/authors/freepik"
                                        smooth: true
                                        height: iconSize / 2
                                        width: height
                                    }
                                    Text
                                    {
                                        anchors.verticalCenter: parent.verticalCenter
                                        font.pointSize: 30
                                        fontSizeMode: Text.Fit
                                        height: iconSize / 2
                                        text: qsTr("Like us on Facebook")
                                        verticalAlignment: Text.AlignVCenter
                                    }
                                }

                                states: State
                                {
                                    name: "pressed"; when: facebookButton.pressed
                                    PropertyChanges { target: facebookButton; scale: 1.2 }
                                }

                                transitions: Transition
                                {
                                    NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
                                }
                            }
                        }
                    }

                    DropShadow
                    {
                        anchors.fill: creditsText
                        color: darkBackgroundColor
                        radius: 10.0
                        samples: 2 * radius + 1
                        source: creditsText
                        transparentBorder: true
                    }

                    Rectangle
                    {
                        id: creditsText

                        anchors { top: parent.top; right: parent.right; margins: 20 }
                        height: parent.height * 3 / 4 + 20
                        width: parent.width / 4 - 60

                        Column
                        {
                            anchors { fill: parent; margins: 20; }
                            spacing: 5

                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 25
                                fontSizeMode: Text.Fit
                                font.underline: true
                                text: qsTr("Image credits:")
                            }
                            Item
                            {
                                height: 20
                                width: 1
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Anas Ramadan")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("http://web.archive.org/web/20170714195419/http://www.flaticon.com/authors/anas-ramadan");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Dave Gandy")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("https://www.flaticon.com/authors/dave-gandy");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Eleonor Wang")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("http://web.archive.org/web/20190420094441/https://www.flaticon.com/authors/eleonor-wang");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Eucalyp")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("https://www.flaticon.com/authors/eucalyp");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Freepik")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("https://www.freepik.com/");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Google")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("https://www.flaticon.com/authors/google");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Smashicons")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("https://www.flaticon.com/authors/smashicons");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Srip")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("https://www.flaticon.com/authors/srip");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Xnimrodx")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("https://www.flaticon.com/authors/xnimrodx");
                                }
                            }
                            Text
                            {
                                anchors.horizontalCenter: parent.horizontalCenter
                                font.pointSize: 20
                                fontSizeMode: Text.Fit
                                text: qsTr("Yannick")
                                MouseArea
                                {
                                    anchors.fill: parent
                                    onClicked: Qt.openUrlExternally("https://www.flaticon.com/authors/yannick");
                                }
                            }
                        }
                    }
                }
            }
            Item
            {
                id: tutorialPage


                Text
                {
                    id: tutorialTitle

                    anchors { top: parent.top; right: parent.right; }
                    height: weRoboticsLogoHolder.height
                    width: parent. width - weRoboticsLogoHolder.width
                    font.pointSize: 50
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    text: "TUTORIAL"
                }

                Rectangle
                {
                    anchors { top: tutorialTitle.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }
                    Text
                    {
                        anchors.centerIn: parent
                        font.pointSize: 30
                        fontSizeMode: Text.Fit
                        height: parent. height / 2
                        width: parent.width * 3 / 4
                        horizontalAlignment: Text.AlignJustify
                        verticalAlignment: Text.AlignVCenter
                        text: qsTr("DO THIS BETTER!! (VIDEO?)\n1) Select a route and press GO.\n2) Confirm the weather conditions.\n3) Perform safety checks as instructed.\n4) Turn drone ON.\n5) Start mission and monitor drone.")
                        wrapMode: Text.Wrap
                    }
                }
            }
            Item
            {
                id: failurePage

                Text
                {
                    id: failureTitle

                    anchors { top: parent.top; right: parent.right; }
                    height: weRoboticsLogoHolder.height
                    width: parent. width - weRoboticsLogoHolder.width
                    font.pointSize: 50
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    text: "FAILURE HANDLING"
                }

                Rectangle
                {
                    anchors { top: failureTitle.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }

                    DropShadow
                    {
                        anchors.fill: groundFailureRectangle
                        color: darkBackgroundColor
                        radius: 10.0
                        samples: 2 * radius + 1
                        source: groundFailureRectangle
                        transparentBorder: true
                    }

                    Rectangle
                    {
                        id: groundFailureRectangle

                        anchors { top: parent.top; left: parent.left; bottom: parent.bottom; margins: sideMargins / 2; bottomMargin: menuPageIndicator.height + sideMargins / 2; }
                        width: parent.width / 2 - sideMargins

                        Rectangle
                        {
                            id: groundFailureTitle

                            anchors { top: parent.top; left: parent.left; margins: sideMargins / 2 }
                            color: "transparent"
                            height: mainWindow.height / 18
                            width: parent.width - sideMargins

                            Text
                            {
                                anchors.fill: parent
                                font.pointSize: 30
                                fontSizeMode: Text.Fit
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                text: "Ground failure handling"
                            }
                        }

                        // Customized ComboBox
                        ComboBox
                        {
                            id: groundFailureCombo

                            anchors { top: groundFailureTitle.bottom; left: parent.left; margins: sideMargins / 2; }
//                            currentIndex: configError ? 2 : 0
                            font.pointSize: 25
                            height: mainWindow.height / 12
                            width: parent.width - sideMargins

                            model: ListModel
                            {
                                id: groundFailureListModel
                                ListElement { problem: "  Restart procedure     "; solution: "1. Stop the onboard application\n   (press the yellow button for 2 seconds).\n2. Wait 10s.\n3.Turn the drone off.\n4. Check the wiring of all the system.\n5. Turn the drone on." }
                                ListElement { problem: "  Drone is in landed error     "; solution: "(This is caused by a missing component at start.)\n1. Check the error message on the tablet.\n2. Do a restart procedure." }
                                ListElement { problem: "  Groundstation app config error     "; solution: "Something is missing in the config file.\n1. Check logs.\n2. Check and correct config file." }
                                ListElement { problem: "  Weather query fails due to location data     "; solution: "1. Check that localization service is turned on.\n2. Open Google Maps app and wait until tablet location is found.\n3. Go back to Cargo GS app and try again." }
                                ListElement { problem: "  Mission not accepted by drone     "; solution: "1. Try to start the mission again.\n2. Check error message on tablet, check battery level, check mission file." }
                            }
                            textRole: "problem"

                            onEnabledChanged: groundFailureCanvas.requestPaint()

                            onCurrentIndexChanged:
                            {
                                groundSolutionText.text = groundFailureListModel.get(currentIndex).solution
                            }

                            delegate: ItemDelegate
                            {
                                contentItem: Text
                                {
                                    color: groundFailureCombo.highlightedIndex === index ? weRoboticsColor : "black";
                                    elide: Text.ElideRight
                                    font: groundFailureCombo.font
                                    fontSizeMode: Text.Fit
                                    text: problem
                                    verticalAlignment: Text.AlignVCenter
                                }
                                background: Rectangle
                                {
                                    anchors { left: parent.left; leftMargin: 0 }
                                    color: groundFailureCombo.highlightedIndex === index ? "mistyrose" : "white";
                                    radius: 20
                                    width: parent.width - 2
                                }
                                highlighted: groundFailureCombo.highlightedIndex === index
                                width: groundFailureCombo.width
                            }

                            indicator: Canvas
                            {
                                id: groundFailureCanvas

                                contextType: "2d"
                                height: groundFailureCombo.height / 5
                                width: 30
                                x: groundFailureCombo.width - width - groundFailureCombo.rightPadding / 2
                                y: groundFailureCombo.topPadding + (groundFailureCombo.availableHeight - height) / 2

                                Connections
                                {
                                    target: groundFailureCombo
                                    function onPressedChanged() {
                                        groundFailureCanvas.requestPaint();
                                    }
                                }

                                onPaint:
                                {
                                    context.reset();
                                    context.moveTo(0, 0);
                                    context.lineTo(width, 0);
                                    context.lineTo(width / 2, height);
                                    context.closePath();
                                    context.fillStyle = groundFailureCombo.enabled ? weRoboticsColor : disabledTextColor
                                    context.fill();
                                }
                            }

                            contentItem: Text
                            {
                                color: groundFailureCombo.enabled ? weRoboticsColor : "black";
                                font: groundFailureCombo.font
                                fontSizeMode: Text.Fit
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                text: groundFailureCombo.displayText
                            }

                            background: Rectangle
                            {
                                anchors.fill: parent
                                border { color: "black"; width: groundFailureCombo.visualFocus ? 2 : 1 }
                                color: "white"
                                radius: 20
                            }

                            popup: Popup
                            {
                                implicitHeight: contentItem.implicitHeight
                                padding: 1
                                width: groundFailureCombo.width
                                y: groundFailureCombo.height

                                contentItem: ListView
                                {
                                    clip: true
                                    currentIndex: groundFailureCombo.highlightedIndex
//                                    highlight: Rectangle
//                                    {
//                                        color: "mistyrose";
//                                        radius: 20
//                                        width: groundFailureCombo.width;
//                                        height: groundFailureCombo.height
//                                        y: parent.currentItem.y;
//                                    }
                                    implicitHeight: contentHeight
                                    model: groundFailureCombo.popup.visible ? groundFailureCombo.delegateModel : null
                                    ScrollIndicator.vertical: ScrollIndicator { }
                                }

                                background: Rectangle
                                {
                                    border.color: "black"
                                    color: "white"
                                    radius: 20
                                }
                            }
                        }

                        Rectangle
                        {
                            id: groundSolutionTextHolder

                            anchors { top: groundFailureCombo.bottom; left: parent.left; bottom: parent.bottom; margins: sideMargins / 2; }
                            color: "transparent"
                            width: parent.width - sideMargins

                            Text
                            {
                                id: groundSolutionText

                                anchors.fill: parent
                                font.pointSize: 20
                                horizontalAlignment: Text.AlignJustify
                                verticalAlignment: Text.AlignTop
                                text: ""
                                wrapMode: Text.Wrap
                            }

                            Text
                            {
                                id: configErrorText

                                anchors { left: parent.left; right: parent.right; bottom: parent.bottom}
                                color: weRoboticsColor
                                font.pointSize: 40
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                text: "CONFIG ERROR!"
                                visible: (configError && (groundFailureCombo.currentIndex == 2))
                            }
                        }
                    }

                    DropShadow
                    {
                        anchors.fill: flightFailureRectangle
                        color: darkBackgroundColor
                        radius: 10.0
                        samples: 2 * radius + 1
                        source: flightFailureRectangle
                        transparentBorder: true
                    }

                    Rectangle
                    {
                        id: flightFailureRectangle

                        anchors { top: parent.top; right: parent.right; bottom: parent.bottom; margins: sideMargins / 2; bottomMargin: menuPageIndicator.height + sideMargins / 2; }
                        width: parent.width / 2 - sideMargins

                        Rectangle
                        {
                            id: flightFailureTitle

                            anchors { top: parent.top; right: parent.right; margins: sideMargins / 2 }
                            color: "transparent"
                            height: mainWindow.height / 18
                            width: parent.width - sideMargins

                            Text
                            {
                                anchors.fill: parent
                                font.pointSize: 30
                                fontSizeMode: Text.Fit
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                text: "Flight failure handling"
                            }
                        }

                        // Customized ComboBox
                        ComboBox
                        {
                            id: flightFailureCombo

                            anchors { top: flightFailureTitle.bottom; right: parent.right; margins: sideMargins / 2; }
                            font.pointSize: 25
                            height: mainWindow.height / 12
                            width: parent.width - sideMargins

                            model: ListModel
                            {
                                id: flightFailureListModel
                                ListElement { problem: "  Critical battery (<20%)     "; solution: "- The drone will trigger a rally point landing autonomously.\n- The pilot can take over if in range, but this is NOT RECOMMENDED." }
                                ListElement { problem: "  Emergency battery (<15%)     "; solution: "- The drone will trigger an emergency landing autonomously and land where it is.\n- The pilot can take over if in range, but this is NOT RECOMMENDED." }
                                ListElement { problem: "  DJI emergency battery (< 10%)     "; solution: "- The DJI autopilot will trigger an emergency landing at current location.\n- The pilot cannot take over." }
                                ListElement { problem: "  Bad weather (wind, rain, ...)     "; solution: "- If the destination is not reachable the drone will trigger a rally point.\n- The pilot can trigger a rally point or an emergency landing at any moment." }
                                ListElement { problem: "  Ground station app crash     "; solution: "- Close the application and re-open it.\n- The drone will reconnect and send information about the current mission.\n- The drone is not affected by a crash of the ground station." }
                                ListElement { problem: "  The drone stops following the route     "; solution: "- Telemetry not updating but stable radio link (location & speed don’t change, temperature and time do).\n- Communication failure between onboard computer and DJI autopilot. The onboard computer will detect it after 10s and reboot.\n- If it happens more than 5 times, the drone will not be able to finish the mission. Get it back with the DJI remote control (and a car)." }
                            }
                            textRole: "problem"

                            onEnabledChanged: flightFailureCanvas.requestPaint()

                            onCurrentIndexChanged:
                            {
                                flightSolutionText.text = flightFailureListModel.get(currentIndex).solution
                            }

                            delegate: ItemDelegate
                            {
                                contentItem: Text
                                {
                                    color: flightFailureCombo.highlightedIndex === index ? weRoboticsColor : "black";
                                    elide: Text.ElideRight
                                    font: flightFailureCombo.font
                                    fontSizeMode: Text.Fit
                                    text: problem
                                    verticalAlignment: Text.AlignVCenter
                                }
                                background: Rectangle
                                {
                                    anchors { left: parent.left; leftMargin: 0 }
                                    color: flightFailureCombo.highlightedIndex === index ? "mistyrose" : "white";
                                    radius: 20
                                    width: parent.width - 2
                                }
                                highlighted: flightFailureCombo.highlightedIndex === index
                                width: flightFailureCombo.width
                            }

                            indicator: Canvas
                            {
                                id: flightFailureCanvas

                                contextType: "2d"
                                height: flightFailureCombo.height / 5
                                width: 30
                                x: flightFailureCombo.width - width - flightFailureCombo.rightPadding / 2
                                y: flightFailureCombo.topPadding + (flightFailureCombo.availableHeight - height) / 2

                                Connections
                                {
                                    target: flightFailureCombo
                                    function onPressedChanged() {
                                        flightFailureCanvas.requestPaint();
                                    }
                                }

                                onPaint:
                                {
                                    context.reset();
                                    context.moveTo(0, 0);
                                    context.lineTo(width, 0);
                                    context.lineTo(width / 2, height);
                                    context.closePath();
                                    context.fillStyle = flightFailureCombo.enabled ? weRoboticsColor : disabledTextColor
                                    context.fill();
                                }
                            }

                            contentItem: Text
                            {
                                color: flightFailureCombo.enabled ? weRoboticsColor : "black";
                                font: flightFailureCombo.font
                                fontSizeMode: Text.Fit
                                horizontalAlignment: Text.AlignHCenter
                                verticalAlignment: Text.AlignVCenter
                                text: flightFailureCombo.displayText
                            }

                            background: Rectangle
                            {
                                anchors.fill: parent
                                border { color: "black"; width: flightFailureCombo.visualFocus ? 2 : 1 }
                                color: "white"
                                radius: 20
                            }

                            popup: Popup
                            {
                                implicitHeight: contentItem.implicitHeight
                                padding: 1
                                width: flightFailureCombo.width
                                y: flightFailureCombo.height

                                contentItem: ListView
                                {
                                    clip: true
                                    currentIndex: flightFailureCombo.highlightedIndex
//                                    highlight: Rectangle
//                                    {
//                                        color: "mistyrose";
//                                        radius: 20
//                                        width: flightFailureCombo.width;
//                                        height: flightFailureCombo.height
//                                        y: parent.currentItem.y;
//                                    }
                                    implicitHeight: contentHeight
                                    model: flightFailureCombo.popup.visible ? flightFailureCombo.delegateModel : null
                                    ScrollIndicator.vertical: ScrollIndicator { }
                                }

                                background: Rectangle
                                {
                                    border.color: "black"
                                    color: "white"
                                    radius: 20
                                }
                            }
                        }

                        Rectangle
                        {
                            id: flightSolutionTextHolder

                            anchors { top: flightFailureCombo.bottom; right: parent.right; bottom: parent.bottom; margins: sideMargins / 2; }
                            color: "transparent"
                            width: parent.width - sideMargins

                            Text
                            {
                                id: flightSolutionText

                                anchors.fill: parent
                                font.pointSize: 20
                                horizontalAlignment: Text.AlignJustify
                                verticalAlignment: Text.AlignTop
                                text: ""
                                wrapMode: Text.Wrap
                            }
                        }
                    }
                }
            }
        }

        PageIndicator
        {
            id: menuPageIndicator

            anchors { bottom: menuSwipeView.bottom; horizontalCenter: parent.horizontalCenter; bottomMargin: 10 }
            count: menuSwipeView.count
            currentIndex: menuSwipeView.currentIndex
            interactive: true

            delegate: Image
            {
                source: "../icons/SwipeIndicator" + index + ".png" // icons made by <div>Icons made by https://www.flaticon.com/authors/freepik || srip || ? || freepik
                height: 50
                width: 50
                opacity: index === menuSwipeView.currentIndex ? 0.95 : pressed ? 0.7 : 0.45
            }
        }
    }

    Button
    {
        id: infoAndCreditsCloseButton

        anchors { top: parent.top; right: parent.right; margins: 20 }
        display: AbstractButton.IconOnly
        icon { source: "../icons/Close.png"; width: iconSize / 1.5; height: iconSize / 1.5 } // Icons made by "https://www.flaticon.com/authors/eleonor-wang"
        visible: !configError

        onClicked: infoAndCredits.visible = false
    }
}
