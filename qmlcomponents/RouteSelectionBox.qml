import QtCharts 2.2
import QtGraphicalEffects 1.0
import QtPositioning 5.6
import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.11

import "Charts"
import "."
import "Charts/QChart.js"        as Charts
import "Charts/QChartGallery.js" as ChartsData

/*
 * The routeSelectionBox Column contains and displays route information.
 *
 * It contains the following main elements:
 * @item altitudeGraph: a graph displaying the planned flight and ground altitude of the mission, as well as the actual flight altitude while executing the mission.
 * @item routeSelectionCombo: a combobox that allows the user to select a mission among the pre-defined routes.
 *
 * The whole container can be hidden by pressing on the bottom arrow (showHideButton) or by flicking it up (flickingDetection).
 */
Column
{
    id: routeSelectionBox

    readonly property int yScaleNumberOfSteps: 5
    readonly property int boxWidth: 2 * mainWindow.width / 5

    property real currentAltitude: 0

    function clearAltitudeGraph()
    {
        if(altitudeGraph.chartData)
        {
            altitudeGraph.chartData.labels = []
            altitudeGraph.chartData.datasets[0].data = []
            altitudeGraph.chartData.datasets[1].data = []
            altitudeGraph.chartData.datasets[2].data = []

            altitudeGraph.chartAnimated = false
            altitudeGraph.repaint();
        }
    }

    function repaintAltitudeGraph(missionIndex)
    {
        // Compute y-axis parameters
        var yMin = 0
        if(routeList[missionIndex].getGroundAltitudeListSize())
            yMin = Math.floor(routeList[missionIndex].minAlt / 100) * 100
        var yScaleIntervalSize = ((Math.floor(routeList[missionIndex].maxAlt / 100) + 1) * 100 - yMin) / yScaleNumberOfSteps

        altitudeGraph.chartOptions =
        {
            bezierCurve: false,
            scaleOverride: true,
            scaleSteps: yScaleNumberOfSteps,
            scaleStepWidth: yScaleIntervalSize,
            scaleStartValue: yMin,

            scaleLineColor: itemOnDarkBackgroundColor,
            scaleLineWidth: 2,
            scaleFontSize: 20,
            scaleFontColor: itemOnDarkBackgroundColor
        }

        // Compute x-axis parameters. Adapt xInterval depending on xMax for a nicer display
        var xMax = routeList[missionIndex].length / 1000;
        var xInterval = Math.round(xMax) / 10;
        if(xInterval)
        {
            for(var i = 0; i <= xMax; i+= xInterval)
            {
                altitudeGraph.chartData.labels.push(Math.round(10 * i) / 10);
            }
            altitudeGraph.chartData.labels.push(Math.round(10 * i) / 10);
        }
        else
        {
            xInterval = Math.round(xMax * 10) / 100;
            if(!xInterval)
                xInterval = 0.01;
            for(var ii = 0; ii <= xMax; ii+= xInterval)
            {
                altitudeGraph.chartData.labels.push(Math.round(100 * ii) / 100);
            }
            altitudeGraph.chartData.labels.push(Math.round(100 * ii) / 100);
        }

        // Compute planned flight altitude graph
        var xValuePlannedFlight = 0.0
        for(var j = 0; j < routeList[missionIndex].getNumberOfWaypoints(); j++)
        {
            if(j)
            {
                xValuePlannedFlight += routeList[missionIndex].getWaypoint(j).distanceTo(routeList[missionIndex].getWaypoint(j-1)) / 1000;
            }
            altitudeGraph.chartData.datasets[0].data.push({x: xValuePlannedFlight, y: routeList[missionIndex].getWaypoint(j).altitude});
        }

        // Compute ground altitude graph
        if(routeList[missionIndex].getGroundAltitudeListSize())
        {
            for(var k = 0; k < routeList[missionIndex].getGroundAltitudeListSize(); k++)
            {
                var xValueGround = 0;
                if(routeList[missionIndex].isReady)
                    xValueGround = routeList[missionIndex].length / (routeList[missionIndex].getGroundAltitudeListSize() - 1) / 1000 * k;
                else
                    xValueGround = altitudeGraph.chartData.datasets[0].data[k].x;
                altitudeGraph.chartData.datasets[1].data.push({x: xValueGround, y: routeList[missionIndex].groundAltitudeList[k]});
            }
        }

        // Repaint graph
        altitudeGraph.chartAnimated = true;
        altitudeGraph.repaint();
    }

    function findMissionIndex(newMission)
    {
        for (var i = routeList.length - 1; i >= 0; --i)
        {
            const mission = routeList[i];
            if (mission.id === newMission)
            {
                return mission.index;
            }
        }
    }

    anchors { top: parent.top; left: telemetryBox.right; topMargin: 0; leftMargin: 20 }

    Connections
    {
        target: stateMachine
        function onWeatherConditionsUpdated(weatherIsGood, weatherReportTitle, weatherReportBody) {
            routeSelectionCombo.enabled = false;
        }

        function onControlStatusChanged(hasControl) {
            if(hasControl)
            {
                routeSelectionBoxTitleRectangle.visible = false;
                altitudeGraphRectangle.visible = false;
                routeSelectionComboRectangle.visible = false;
            }
        }

        function onDroneTimeDistanceTemperatureHumidityUpdated(time, distance, temperature, humidity) {
            // Add new point to real flight altitude graph
            if(currentDroneState > 7) // Only if mission started
            {
                altitudeGraph.chartAnimated = false;
                var xValueRealFlight = Math.round((routeList[currentRouteIndex].length - distance) / 10) / 100;
                if(xValueRealFlight < 0)
                    xValueRealFlight = 0;
                altitudeGraph.chartData.datasets[2].data.push({x: xValueRealFlight, y: currentAltitude});
                altitudeGraph.repaint();
            }
        }

        function onMissionChanged(newMission) {
            routeSelectionCombo.currentIndex = findMissionIndex(newMission);
        }

        function onResetAll() {
            routeSelectionCombo.currentIndex = 0;
            routeSelectionCombo.enabled = true;
        }

        function onTelemetryUpdated(lat, lon, alt, vx, vy, vz) {
            currentAltitude = alt;
        }
    }

    Rectangle
    {
        id: routeSelectionBoxTitleRectangle

        color: darkBackgroundColor
        height: routeSelectionCombo.height *2 / 3
        width: boxWidth

        Text
        {
            anchors { fill: parent; margins: 0; topMargin: 10 }

            color: itemOnDarkBackgroundColor;
            font.pointSize: 20
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("ALTITUDE PROFILE")
        }

        MouseArea
        {
            id: flickingDetection

            property bool flicking: true
            property real initX: 0
            property real initY: 0
            property real minY: flickingDetection.height

            anchors { top: routeSelectionBoxTitleRectangle.top; left: routeSelectionBoxTitleRectangle.left; right: routeSelectionBoxTitleRectangle.right }
            height: routeSelectionBoxTitleRectangle.height + altitudeGraphRectangle.height + routeSelectionComboRectangle.height + showHideButton.height + 30

            onPressed:
            {
                initX = mouseX
                initY = mouseY
                minY = mouseY
                flicking = true
                flickTimer.start()
            }
            onPositionChanged:
            {
                if(mouseY < minY)
                {
                    minY = mouseY
                }
                else if(flicking && (mouseY > minY))
                {
                    flicking = false
                }
            }
            onReleased:
            {
                if(flicking && (Math.abs(mouseX-initX) < 300) && (initY - mouseY > 50))
                {
                    routeSelectionBoxTitleRectangle.visible = false
                    altitudeGraphRectangle.visible = false
                    routeSelectionComboRectangle.visible = false
                }
            }

            Timer
            {
                id: flickTimer

                interval: 500 // ms
                onTriggered: flickingDetection.flicking = false
            }
        }
    }

    Rectangle
    {
        id: altitudeGraphRectangle

        color: darkBackgroundColor
        height: mainWindow.height / 4
        width: boxWidth

        Chart
        {
            id: altitudeGraph

            anchors { fill: parent; topMargin: 0; bottomMargin: 0; leftMargin: 20; rightMargin: 30 }
            chartAnimated: true;
            chartAnimationDuration: 2000;
            chartAnimationEasing: Easing.InOutElastic;
            chartType: Charts.ChartType.LINE;

            Component.onCompleted:
            {
                chartOptions =
                {
                    scaleOverride: true,
                    scaleSteps: yScaleNumberOfSteps,
                    scaleStepWidth: 100,
                    scaleStartValue: 0,

                    scaleLineColor: itemOnDarkBackgroundColor,
                    scaleLineWidth: 2,
                    scaleFontSize: 20,
                    scaleFontColor: itemOnDarkBackgroundColor
                }

                chartData =
                {
                    labels: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                    datasets:
                    [
                        {   // Flight planned altitude
                            fillColor: "#00000000" ,
                            strokeColor: routeColor,
                            pointColor: routeColor,
                            pointStrokeColor: routeColor,
                            data: []
                        },
                        {   // Ground altitude
                            fillColor: darkBackgroundColor,
                            strokeColor: darkBackgroundColor,
                            pointColor: darkBackgroundColor,
                            pointStrokeColor: darkBackgroundColor,
                            data: []
                        },
                        {   // Flight real altitude
                            fillColor: "#00000000",
                            strokeColor: itemOnDarkBackgroundColor,
                            pointColor: itemOnDarkBackgroundColor,
                            pointStrokeColor: itemOnDarkBackgroundColor,
                            data: []
                        }
                    ]
                }
            }
        }
    }

    Binding
    {
        target: routeSelectionCombo
        property: "enabled"
        value: currentGroundstationState ? false : true
    }

    Rectangle
    {
        id: routeSelectionComboRectangle

        color: darkBackgroundColor
        height: mainWindow.height / 12 + 20
        width: boxWidth

        // Customized ComboBox
        ComboBox
        {
            id: routeSelectionCombo

            anchors { top: parent.top; left: parent.left; leftMargin: 30; topMargin: 20 }
            displayText: qsTr(currentText)
            font.pointSize: 25
            model: routeList
            textRole: "displayName"
            height: mainWindow.height / 12
            width: boxWidth - 80 - routeSelectionSettingsCancelButton.width

            onEnabledChanged: routeSelectionCanvas.requestPaint()

            // Tentative to fix the problem that "Select route" doesn't translate. NOT WORKING
            // AAA TODO
            Connections
            {
                target: translator
                function onLanguageChanged() {
//                    stateMachine.infoToDisplay("Change model")
                    var i = routeSelectionCombo.currentIndex

//                    routeList[0].setName(QT_TR_NOOP("Select route"));
//                    routeSelectionCombo.model.setProperty(0, "name", "CHANGEMENT")
//                    routeList.setProperty(0, "name", "CHANGEMENT")

                    routeSelectionCombo.model = routeList
                    routeSelectionCombo.currentIndex = i
                }
            }

            onCurrentIndexChanged:
            {
                currentRouteIndex = currentIndex
                currentRouteId = routeList[currentIndex].id
                currentRouteVersion = routeList[currentIndex].version

                clearAltitudeGraph()

                if(mainMap.visible)
                    mainMap.drawRouteOnMap()

                if(currentIndex)
                {
                    routeSelected(routeList[currentIndex].id, routeList[currentIndex].length)
                    repaintAltitudeGraph(currentIndex)
                }
                else
                {
                    noRouteSelected()
                }
            }

            delegate: ItemDelegate
            {
                contentItem: Text
                {
                    color: routeSelectionCombo.highlightedIndex === index ? darkBackgroundColor : itemOnDarkBackgroundColor;
                    elide: Text.ElideRight
                    font: routeSelectionCombo.font
                    fontSizeMode: Text.Fit
                    text: qsTr(modelData.displayName)
                    verticalAlignment: Text.AlignVCenter
                }
                highlighted: routeSelectionCombo.highlightedIndex === index
                width: routeSelectionCombo.width
            }

            indicator: Canvas
            {
                id: routeSelectionCanvas

                contextType: "2d"
                height: routeSelectionCombo.height / 5
                width: 30
                x: routeSelectionCombo.width - width - routeSelectionCombo.rightPadding
                y: routeSelectionCombo.topPadding + (routeSelectionCombo.availableHeight - height) / 2

                Connections
                {
                    target: routeSelectionCombo
                    function onPressedChanged() {
                        routeSelectionCanvas.requestPaint();
                    }
                }

                onPaint:
                {
                    context.reset();
                    context.moveTo(0, 0);
                    context.lineTo(width, 0);
                    context.lineTo(width / 2, height);
                    context.closePath();
                    context.fillStyle = routeSelectionCombo.enabled ? itemOnDarkBackgroundColor : disabledTextColor
                    context.fill();
                }
            }

            contentItem: Text
            {
                color: itemOnDarkBackgroundColor
                font: routeSelectionCombo.font
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: qsTr(routeSelectionCombo.displayText)
            }

            background: Rectangle
            {
                anchors.fill: parent
                border { color: routeSelectionCombo.popup.visible ? itemOnDarkBackgroundColor : darkBackgroundColor; width: routeSelectionCombo.visualFocus ? 2 : 1 }
                color: darkBackgroundColor
                radius: 20
            }

            popup: Popup
            {
                implicitHeight: contentItem.implicitHeight
                padding: 1
                width: routeSelectionCombo.width
                y: routeSelectionCombo.height

                contentItem: ListView
                {
                    clip: true
                    currentIndex: routeSelectionCombo.highlightedIndex
                    highlight: Rectangle { color: itemOnDarkBackgroundColor; radius: 20 }
                    implicitHeight: contentHeight
                    model: routeSelectionCombo.popup.visible ? routeSelectionCombo.delegateModel : null
                    ScrollIndicator.vertical: ScrollIndicator { }
                }

                background: Rectangle
                {
                    border.color: itemOnDarkBackgroundColor
                    color: darkBackgroundColor
                    radius: 20
                }
            }
        }

        Binding
        {
            target: routeSelectionSettingsCancelButton
            property: "enabled"
            value: !(routeSelectionCombo.enabled)
        }

        Button
        {
            id: routeSelectionSettingsCancelButton

            anchors { top: parent.top; right: parent.right; topMargin: 20; rightMargin: 30 }
            height: routeSelectionCombo.height
            visible: enabled

            background: Rectangle
            {
                anchors.fill: parent
                color: darkBackgroundColor
                radius: 10
            }

            contentItem: Column
            {
                spacing: 10
                Image
                {
                    id: cancelIcon

                    source: routeSelectionSettingsCancelButton.icon.source
                    height: routeSelectionSettingsCancelButton.height - 50
                    width: routeSelectionSettingsCancelButton.height - 50

                    ColorOverlay
                    {
                        anchors.fill: cancelIcon
                        color: routeSelectionSettingsCancelButton.enabled ? itemOnDarkBackgroundColor : disabledTextColor
                        source: cancelIcon
                    }
                }
                Text
                {
                    color: routeSelectionSettingsCancelButton.enabled ? itemOnDarkBackgroundColor : disabledTextColor
                    font: routeSelectionSettingsCancelButton.font
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom
                    text: routeSelectionSettingsCancelButton.text
                    width: cancelIcon.width
                }
            }

            display: AbstractButton.TextUnderIcon
            icon { source: "../icons/Close.png"; width: routeSelectionSettingsCancelButton.height - 50; height: routeSelectionSettingsCancelButton.height - 50 } // Icons made by "https://www.flaticon.com/authors/eleonor-wang"
            padding: 15
            text: qsTr("CANCEL")

            onClicked:
            {
                if(currentDroneState == 7) // MISSION_READY
                {
                    stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_USER_1, 0, 0)
                }
                stateMachine.resetState()
            }
        }
    }

    // Make RouteSelectionBox collapsable
    Button
    {
        id: showHideButton

        clip: true

        display: AbstractButton.IconOnly
        height: mainWindow.height / 12 - 20
        icon { source: routeSelectionBoxTitleRectangle.visible ? "../icons/ArrowUp.png" : "../icons/ArrowDown.png"; height: showHideButton.height; width: showHideButton.height; color: itemOnDarkBackgroundColor } // Icons made by "https://www.flaticon.com/authors/dave-gandy"
        text: " "
        width: boxWidth

        background: Rectangle
        {
            anchors { fill: parent; topMargin: -radius }
            color: darkBackgroundColor
            radius: 20
        }

        onClicked:
        {
            routeSelectionBoxTitleRectangle.visible = !routeSelectionBoxTitleRectangle.visible
            altitudeGraphRectangle.visible = !altitudeGraphRectangle.visible
            routeSelectionComboRectangle.visible = !routeSelectionComboRectangle.visible
        }
    }
}

