import QtQuick 2.8
import QtQuick.Dialogs 1.1

/*
 * The popupMessage Item creates the different versions of popup information messages.
 *
 * The message contains a title, a short body of text and potential additional details.
 *
 * The following signals generate a message:
 * @signal messageToDisplay: sent when a specific issue requires the user acknowledgement.
                             The popup asks the user to acknowledge the message.
 * @signal commandTimeoutExpired: sent when a command has been sent and no ack or nack has been received in the defined time limit.
                                  The popup asks the user whether they want to send the command again or not.
 * @signal missionRefused: sent when a nack has been received in answer to a MAV_CMD_USER_1 command.
 *                         The popup asks the user whether they want to send the command again or not.
 */
Item
{
    function setCommandName(cmdType)
    {
        var cmdName = ""
        if(cmdType === MavCmdEnum.MAV_CMD.MAV_CMD_USER_1)
            cmdName = qsTr("Send mission")
        else if(cmdType === MavCmdEnum.MAV_CMD.MAV_CMD_USER_2)
            cmdName = qsTr("Reception request")
        else if(cmdType === MavCmdEnum.MAV_CMD.MAV_CMD_MISSION_START)
            cmdName = qsTr("Start mission")
        else if(cmdType === MavCmdEnum.MAV_CMD.MAV_CMD_DO_PAUSE_CONTINUE)
            cmdName = qsTr("Pause or Resume")
        else if(cmdType === MavCmdEnum.MAV_CMD.MAV_CMD_NAV_LAND)
            cmdName = qsTr("Land (precision or emergency)")
        else if(cmdType === MavCmdEnum.MAV_CMD.MAV_CMD_DO_PARACHUTE)
            cmdName = qsTr("Retake Control")
        else if(cmdType === MavCmdEnum.MAV_CMD.MAV_CMD_NAV_RALLY_POINT)
            cmdName = qsTr("Go to rally point")
        else
            cmdName = qsTr("Unknown")

        return cmdName;
    }

    function setMessageBody(targetSystem, cmdName)
    {
        var message = ""
        if(targetSystem === 50)
            message = qsTr("The") + " \"" + cmdName + "\" " + qsTr("command was not successfully transmitted to the drone. Would you like to resend it?")
        else if((targetSystem === 101) || (targetSystem === 102) || (targetSystem === 103) || (targetSystem === 104))
            message = qsTr("The") + " \"" + cmdName + "\" " + qsTr("command was not successfully transmitted to the other groundstation. Would you like to resend it?")
        else
            message = qsTr("The") + " \"" + cmdName + "\" " + qsTr("command was not successfully transmitted to UNKNOWN TARGET (ID: ") + targetSystem + qsTr("). Would you like to resend it?")

        return message
    }

    Component
    {
        id: popupMessage

        MessageDialog
        {
            id: messageBox

            onYes: {stateMachine.sendCommand(); messageBox.destroy();}
            onNo: {stateMachine.returnToPreviousState(); messageBox.destroy();}
            onRejected: {stateMachine.resetState(); messageBox.destroy();} // for StandardButton.Cancel
            // onDiscard: do nothing, MessageDialog closes automatically
            visible: false
        }
    }

    Connections
    {
        target: stateMachine
        function onMessageToDisplay(message, details) {
            var messagePlugin = popupMessage.createObject(mainWindow,
                                                          {"title": title,
                                                           "text": message,
                                                           "detailedText": details,
                                                           "standardButtons": StandardButton.Discard});
            messagePlugin.open()
        }
        function onCommandTimeoutExpired(cmdType, targetSystem) {
            currentCommand = cmdType
            var cmdName = setCommandName(cmdType)
            var title = qsTr("Command error")
            var message = setMessageBody(targetSystem, cmdName)
            var messagePlugin

            if(cmdType === MavCmdEnum.MAV_CMD.MAV_CMD_USER_1) // Send mission
            {
                messagePlugin = popupMessage.createObject(mainWindow,
                                                          {"title": title,
                                                           "text": message,
                                                           "standardButtons": StandardButton.Yes | StandardButton.Cancel});
            }
            else
            {
                messagePlugin = popupMessage.createObject(mainWindow,
                                                          {"title": title,
                                                           "text": message,
                                                           "standardButtons": StandardButton.Yes | StandardButton.No});
            }
            messagePlugin.open()
        }
        function onMissionRefused(title, message, details) {
            var messagePlugin = popupMessage.createObject(mainWindow,
                                                          {"title": title,
                                                           "text": message,
                                                           "detailedText": details,
                                                           "standardButtons": StandardButton.Yes | StandardButton.Cancel});
            messagePlugin.open()
        }
    }
}
