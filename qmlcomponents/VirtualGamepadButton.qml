import QtGraphicalEffects 1.0
import QtQuick 2.8
import QtQuick.Controls 2.4

/*
 * The virtualGamepadButton Item is a custom button for the virtual gamepad.
 *
 * When pressed, it transfers a command (arm, disarm or take-off) to the drone.
 */
Item
{
    property string labelText: "Test"
    property string buttonChar: "K"

    height: 80
    width: 80

    Button
    {
        id: gamepadButton

        anchors.fill: parent

        // Order of items must be: DropShadow - Rectangle - RadialGradient
        DropShadow
        {
            anchors.fill: buttonBackground
            color: darkBackgroundColor
            radius: 8.0
            samples: 2 * radius + 1
            source: buttonBackground
            transparentBorder: true
            horizontalOffset: source.width / 15
            verticalOffset: source.width / 15
        }

        background: Rectangle
        {
            id: buttonBackground

            anchors.fill: parent
            color: "#3322aa"
            radius: width * 0.5
        }

        RadialGradient
        {
            anchors.fill: buttonBackground
            source: buttonBackground

            gradient: Gradient
            {
                GradientStop { position: 0.0; color: "#6644bb" }
                GradientStop { position: 0.5; color: "#442299" }
            }
        }

        onPressed: gamepadManager.updateVirtualButtonCommand(buttonChar, true)
        onReleased: gamepadManager.updateVirtualButtonCommand(buttonChar, false)

        states: State
        {
            name: "pressed"; when: gamepadButton.pressed
            PropertyChanges { target: gamepadButton; scale: 1.2 }
        }

        transitions: Transition
        {
            NumberAnimation { properties: "scale"; duration: 100; easing.type: Easing.InOutQuad }
        }
    }

    Text
    {
        anchors { top: gamepadButton.bottom; horizontalCenter: gamepadButton.horizontalCenter; topMargin: 10 }
        font.pointSize: 100
        fontSizeMode: Text.Fit
        text: labelText
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignTop
        height: gamepadButton.height
        width: gamepadButton.width
    }
}
