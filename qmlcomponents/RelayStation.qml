import QtGraphicalEffects 1.0
import QtLocation 5.11
import QtQuick 2.8

/*
 * The relayStation MapQuickItem defines the display of relay stations on the mainMap.
 */
MapQuickItem
{
    Connections
    {
        target: stateMachine
        function onRadioStatusUpdated(address, status) // Needs testing and corrections
        {
            if(address === modelData.address)
            {
                opacity = 1.0 // AAA TODO change to "status ? 1.0 : 0.3"
            }
        }
    }

    Connections
    {
        target: mainWindow
        function onRadioStatusReset()
        {
            opacity = 0.3
        }
    }

    anchorPoint { x: relayStationIcon.width / 2; y: relayStationIcon.height / 2 }
    coordinate: modelData.location
    opacity: 0.3

    sourceItem: Image
    {
        id: relayStationIcon

        source: "../icons/Radio.png"
        height: 50
        width: height

        Text
        {
            anchors.top: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            text: modelData.name
            width: parent.width
        }
    }
}



