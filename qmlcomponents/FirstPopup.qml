import QtQuick 2.8
import QtQuick.Controls 2.4

/*
 * The weatherCheckPopup Popup contains the weather report.
 *
 * It includes:
 * @item weatherTitleText: the general sumary of the weather report.
 * @item weatherReportText: the details of the weather report.
 * @item weatherBypass: a checkbox to bypass no-flight recommendations due to bad weather.
 * @item weatherCheckConfirmButton: button to continue to the next step (safety checks).
 * @item weatherCheckCancelButton: button to cancel and go back to idle state.
 */
Popup
{
    id: weatherCheckPopup

    closePolicy: Popup.NoAutoClose
    focus: true
    modal: true
    x: Math.round((parent.width - weatherCheckMainRectangle.width) / 2)
    y: Math.round((parent.height - weatherCheckMainRectangle.height) / 2)

    Connections
    {
        target: stateMachine
        function onWeatherConditionsUpdated (weatherIsGood, weatherReportTitle, weatherReportBody) {
            // Adapt texts of popup depending on weather report
            if(weatherIsGood)
            {
                weatherTitleText.color = "green"
                weatherCheckConfirmButton.enabled = true
                weatherBypass.visible = false
                // Comment out for reception request feature
                weatherCheckConfirmButton.text = qsTr("Go to safety checks")
                // Uncomment for reception request feature
//                weatherCheckConfirmButton.text = qsTr("Send reception request")
            }
            else
            {
                weatherTitleText.color = "red"
                weatherCheckConfirmButton.enabled = false
                weatherBypass.visible = true
                weatherCheckConfirmButton.text = qsTr("          OK          ")
            }

            weatherTitleText.text = weatherReportTitle
            weatherReportText.text = weatherReportBody

            weatherCheckPopup.open()
        }
        function onResetAll() {
            weatherBypass.checkState = Qt.Unchecked
            weatherCheckPopup.close()
        }
    }

    Rectangle
    {
        id: weatherCheckMainRectangle

        height: childrenRect.height + 100
        width: childrenRect.width + 100

        Text
        {
            id: weatherTitleText

            anchors { top: parent.top; left: parent.left; topMargin: 50; leftMargin: 50 }
            font.pointSize: Math.round(1.5 * weatherReportText.font.pointSize)
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            width: weatherCheckButtonsRow.width
            wrapMode: Text.WordWrap
        }
        Text
        {
            id: weatherReportText

            anchors { top: weatherTitleText.bottom; left: weatherBypass.left; topMargin: 40; leftMargin: weatherBypassIndicator.width + 30 }
            font.pointSize: 20
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            width: weatherBypassText.width
            wrapMode: Text.Wrap
        }

        CheckBox
        {
            id: weatherBypass

            anchors { top: weatherReportText.bottom; topMargin: 20; horizontalCenter: weatherTitleText.horizontalCenter }
            font.pointSize: weatherReportText.font.pointSize
            text: qsTr("Ignore the weather forecast and fly anyway. This is NOT RECOMMENDED.")
            width: weatherCheckButtonsRow.width

            indicator: Rectangle
            {
                id: weatherBypassIndicator

                anchors.verticalCenter: parent.verticalCenter
                border { color: "red"; width: 2 }
                height: iconSize
                width: height
                radius: 3

                Rectangle
                {
                    anchors { margins: 4; fill: parent }
                    color: "red"
                    radius: 1
                    visible: weatherBypass.checked
                }
            }

            contentItem: Text
            {
                id: weatherBypassText

                anchors { left: weatherBypassIndicator.right; right: parent.right; leftMargin: 30 }
                color: "red"
                font.pointSize: weatherReportText.font.pointSize
                text: weatherBypass.text
                wrapMode: Text.Wrap
            }

            onCheckStateChanged: weatherCheckConfirmButton.enabled = weatherBypass.checkState
        }

        Row
        {
            id: weatherCheckButtonsRow

            anchors { top: weatherBypass.bottom; topMargin: 20; horizontalCenter: weatherTitleText.horizontalCenter }
            spacing: 60

            Button
            {
                id: weatherCheckConfirmButton

                text: " "

                contentItem: Text
                {
                    color: weatherCheckConfirmButton.enabled ? enabledTextColor : disabledTextColor
                    font.pointSize: 25
                    fontSizeMode: Text.Fit
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: weatherCheckConfirmButton.text
                }

                onClicked:
                {
                    weatherCheckPopup.close()
                    // Comment out for reception request feature
                    safetyCheckPopup.open()
                    // Uncomment for reception request feature
//                    stateMachine.sendReceptionRequestOrReady(0);
                }
            }

            Button
            {
                id: weatherCheckCancelButton

                text: qsTr("   Cancel   ")

                contentItem: Text
                {
                    color: weatherCheckCancelButton.enabled ? enabledTextColor : disabledTextColor
                    font.pointSize: 25
                    fontSizeMode: Text.Fit
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: qsTr(weatherCheckCancelButton.text)
                }

                onClicked:
                {
                    weatherCheckPopup.close()
                    stateMachine.resetState();
                }
            }
        }
    }
}
