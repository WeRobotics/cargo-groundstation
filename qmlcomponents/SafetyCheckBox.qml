import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Layouts 1.11

/*
 * The safetyCheckBox CheckBox is a custom checkbox used for the safety checks in the SecondPopup.
 */
CheckBox
{
    property int indentationLevel: 0

    font.pointSize: sendingPilotLabel.font.pointSize
    leftPadding: indicator.width * indentationLevel + 10

    indicator: Rectangle
    {
        id: checkboxOutsideRect

        anchors { verticalCenter: parent.verticalCenter; left: parent.left; leftMargin: width * indentationLevel }
        border { color: parent.activeFocus ? enabledTextColor : disabledTextColor; width: 1 }
        radius: 3
        height: checkboxSize
        width: height

        Rectangle
        {
            id: checkboxInsideRect

            anchors { margins: 4; fill: parent }
            color: Material.accent
            radius: 1
            visible: checkboxOutsideRect.parent.checked
        }
    }
}
