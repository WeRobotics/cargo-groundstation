import QtQuick 2.8
import QtQuick.Controls 2.4

/*
 * The infoMessageListContainer ListView contains all the temporary messages
 * displayed for 20 seconds on the bottom left of the screen.
 *
 * Messages are created using the infoToDisplay signal from the stateMachine.
 * Messages are deleted automatically after 20 second, or by a swipe from right to left.
 */
ListView
{
    id: infoMessageListContainer

    readonly property int displayTime: 20000

    anchors { bottom: parent.bottom; left: parent.left; margins: 0 }
    height: infoMessageListContainer.count * mainWindow.height / 18
    width: mainWindow.width / 2

    delegate: SwipeDelegate
    {
        id: infoMessageListDelegate

        height: mainWindow.height / 18
        width: parent.width

        contentItem: Text
        {
            anchors.margins: 10
            color: itemOnDarkBackgroundColor
            fontSizeMode: Text.Fit
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignBottom
            text: modelData
        }

        background: Rectangle
        {
            color: "#EE000000"
        }

        swipe.right: Rectangle
        {
            clip: true
            color: "transparent"
            height: parent.height
            width: parent.width
            SwipeDelegate.onClicked: infoMessageListDelegate.swipe.close()
        }

        // Delete message by swiping
        swipe.onCompleted:
        {
            contentItem.visible = false
            infoMessageListModel.remove(index)
        }

        ListView.onAdd: infoMessageListTimer.start()

        // Delete message by timeout
        Timer
        {
            id: infoMessageListTimer

            interval: displayTime
            onTriggered:
            {
                contentItem.visible = false
                infoMessageListModel.remove(index)
            }
        }
    }

    model: ListModel
    {
        id: infoMessageListModel
    }

    remove: Transition
    {
        SequentialAnimation
        {
            PauseAnimation { duration: 125 }
        }
    }

    displaced: Transition
    {
        SequentialAnimation
        {
            PauseAnimation { duration: 125 }
        }
    }

    ScrollIndicator.vertical: ScrollIndicator { }
}
