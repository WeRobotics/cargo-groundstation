import QtQuick 2.8
import QtQuick.Controls 2.4

/*
 * The emergencyPopup Popup contains all the emergency commands buttons.
 *
 * The available buttons are the following:
 * @button emergencyPopupCloseButton: closes the popup.
 * @button emergencyLandingButton: sends an emergency landing command.
 * @button precisionLandingButton: sends a precision landing command.
 * @button parachuteButton: sends a parachute release command.
 * Warning: the parachute command is currently interpreted differently by the onboard code!!!
 * @button rallyPointButton: sends a rally point command.
 */
Popup
{
    id: emergencyPopup

    closePolicy: Popup.NoAutoClose
    focus: true
    modal: true
    x: Math.round((parent.width - emergencyPopupMainRectangle.width) / 2)
    y: Math.round((parent.height - emergencyPopupMainRectangle.height) / 2)

    // Contains for buttons sending emergency commands:
    // - Emergency landing
    // - Precision landing
    // - Parachute (sending command but drone interprets it differently)
    // - Rally point
    Rectangle
    {
        id: emergencyPopupMainRectangle

        color: darkBackgroundColor
        height: childrenRect.height + 100
        width: childrenRect.width + 100

        Button
        {
            id: emergencyPopupCloseButton

            anchors { top: parent.top; right: rallyPointButton.right; topMargin: 30 }
            display: AbstractButton.IconOnly
            icon { source: "../icons/Close.png"; width: iconSize / 1.5; height: iconSize / 1.5 } // Icons made by "https://www.flaticon.com/authors/eleonor-wang"

            onClicked: emergencyPopup.close()
        }

        Button
        {
            id: emergencyLandingButton

            anchors { top: emergencyPopupCloseButton.bottom; left: parent.left; topMargin: 30; leftMargin: 50 }
            display: AbstractButton.TextUnderIcon
            icon { source: "../icons/Land.png"; width: emergencyIconSize; height: emergencyIconSize } // Icons made by "https://www.flaticon.com/authors/eucalyp"
            padding: 40
            text: qsTr("EMERGENCY\nLANDING")

            contentItem: Column
            {
                spacing: 20
                Image
                {
                    id: emergencyLandingIcon

                    source: emergencyLandingButton.icon.source
                    height: emergencyIconSize
                    width: emergencyIconSize
                }
                Text
                {
                    fontSizeMode: Text.Fit
                    text: emergencyLandingButton.text
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom
                    width: emergencyLandingIcon.width
                }
            }

            onClicked:
            {
                stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_NAV_LAND, 0, 3)
                emergencyPopup.close()
            }
        }

        Button
        {
            id: precisionLandingButton

            anchors { top: emergencyPopupCloseButton.bottom; left: emergencyLandingButton.right; topMargin: 30; leftMargin: 50 }
            display: AbstractButton.TextUnderIcon
            icon { source: "../icons/Aruco.png"; width: emergencyIconSize; height: emergencyIconSize }
            padding: 40
            text: qsTr("PRECISION\nLANDING")

            contentItem: Column
            {
                spacing: 20
                Image
                {
                    id: precisionLandingIcon

                    source: precisionLandingButton.icon.source
                    height: emergencyIconSize
                    width: emergencyIconSize
                }
                Text
                {
                    fontSizeMode: Text.Fit
                    text: precisionLandingButton.text
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom
                    width: precisionLandingIcon.width
                }
            }

            onClicked:
            {
                stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_NAV_LAND, 0, 1)
                emergencyPopup.close()
            }
        }

        Button
        {
            id: parachuteButton

            anchors { top: emergencyPopupCloseButton.bottom; left: precisionLandingButton.right; topMargin: 30; leftMargin: 50 }
            display: AbstractButton.TextUnderIcon
            icon { source: "../icons/AntennaConnected.png"; width: emergencyIconSize; height: emergencyIconSize } //"../icons/Parachute.png" // Icons made by "https://www.freepik.com/"
            padding: 40
            text: qsTr("RETAKE\nCONTROL")//"DEPLOY\nPARACHUTE"

            contentItem: Column
            {
                spacing: 20
                Image
                {
                    id: parachuteIcon

                    source: parachuteButton.icon.source
                    height: emergencyIconSize
                    width: emergencyIconSize
                }
                Text
                {
                    fontSizeMode: Text.Fit
                    text: parachuteButton.text
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom
                    width: parachuteIcon.width
                }
            }
            onClicked:
            {
//                stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_DO_PARACHUTE, 1) // enable AAA TODO move this to start of mission
                stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_DO_PARACHUTE, 2) // release
                emergencyPopup.close()
            }
        }

        Button
        {
            id: rallyPointButton

            anchors { top: emergencyPopupCloseButton.bottom; left: parachuteButton.right; topMargin: 30; leftMargin: 50 }
            display: AbstractButton.TextUnderIcon
            icon { source: "../icons/Rallypoint.png"; width: emergencyIconSize; height: emergencyIconSize } // Icons made by "https://www.flaticon.com/authors/smashicons"
            padding: 40
            text: qsTr("GO TO RALLY\nPOINT")

            contentItem: Column
            {
                spacing: 20
                Image
                {
                    id: rallyPointIcon

                    source: rallyPointButton.icon.source
                    height: emergencyIconSize
                    width: emergencyIconSize
                }
                Text
                {
                    fontSizeMode: Text.Fit
                    text: rallyPointButton.text
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignBottom
                    width: rallyPointIcon.width
                }
            }

            onClicked:
            {
                stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_NAV_RALLY_POINT)
                emergencyPopup.close()
            }
        }
    }
}
