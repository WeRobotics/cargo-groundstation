import QtPositioning 5.6
import QtQuick 2.0

/*
 * The tabletLocation Item provides the geographical position of the tablet for displaying on the mainMap.
 */
Item
{
    Binding
    {
        target: mainWindow
        property: "tabletLocation"
        value: tabletLocationSource.position.coordinate
    }

    Binding
    {
        target: mainWindow
        property: "tabletLocationAccuracy"
        value: tabletLocationSource.position.horizontalAccuracy
    }

    PositionSource
    {
        id: tabletLocationSource

        onSourceErrorChanged:
        {
            if (sourceError == PositionSource.NoError)
                return

            console.info("Tablet position source error: " + sourceError)
            stop()
        }

        onUpdateTimeout:
        {
            console.info("Tablet position timeout occurred!")
        }

        onPositionChanged:
        {
            if(tabletLocationSource.position.horizontalAccuracyValid)
            {
                tabletLocationAccuracy = tabletLocationSource.position.horizontalAccuracy
                locationAccuracyChanged()
            }
        }
    }
}
