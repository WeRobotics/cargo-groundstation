import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.11

/*
 * The settingsPopup Popup contains comboboxes and text fields for TCP connection and map display settings selection.
 *
 * It includes:
 * @item providers: a list of available map providers.
 * Warning: this list must correspond to the available plugins (see MapPlugin.qml).
 * @item ipTextEdit: a field to register the tcp connection destination ip.
 * @item portTextEdit: a field to register the tcp connection detination port.
 * @item providerCombo: a combobox to select the map provider (among the ones listed in the providers ListModel).
 * @item typeCombo: a combobox to select the map type (among the ones available for the selected map provider).
 * @item settingsConfirmButton: button to validate the selected settings.
 * @item settingsCancelButton: button to close the popup without changes.
 */
Popup
{
    property var savedProviderStr: mapPlugin.name
    property var savedTypeStr: mainMap.activeMapType.name
    property var mapProviderList: ListModel
    {
        id: providers
        ListElement { name: "esri"; display: "ArcGIS (Esri)"; currentType: 0 }
        ListElement { name: "mapbox"; display: "Mapbox"; currentType: 0 }
        ListElement { name: "osm"; display: "OpenStreetMap"; currentType: 0 }
//        ListElement { name: "mapboxgl"; display: "Mapbox GL"; currentType: 0 }
    }

    closePolicy: Popup.NoAutoClose
    focus: true
    modal: true
    x: Math.round((parent.width - settingsPopupLayout.width) / 2)
    y: Math.round((parent.height - settingsPopupLayout.height) / 2)

    // Only for TCP connection
    function changeCommunicationSettings()
    {
        ipLayout.visible = true
        portLayout.visible = true
        mapLayout.visible = false
        mapTypeLayout.visible = false
        settingsConfirmButton.text = qsTr("CONFIRM")
        settingsPopup.open()
    }

    function changeMapSettings()
    {
        ipLayout.visible = false
        portLayout.visible = false
        mapLayout.visible = true
        mapTypeLayout.visible = true
        settingsConfirmButton.text = qsTr("CONFIRM")
        settingsPopup.open()
    }

    function findIndexInModel(model, criteria)
    {
        for(var i = 0; i < model.count; ++i)
            if (criteria(model.get(i)))
                return i
        return null
    }

    function findIndexInList(model, criteria)
    {
        for(var i = 0; i < model.length; ++i)
            if (criteria(model[i]))
                return i
        return null
    }

    onOpened:
    {
        var mapProviderIndex = findIndexInModel(providers, function(item) { return item.name === mapPlugin.name })
        var mapTypeIndex = findIndexInList(typeCombo.model, function(item) { return item.name === mainMap.activeMapType.name })

        providerCombo.currentIndex = mapProviderIndex
        typeCombo.currentIndex = mapTypeIndex
        savedProviderStr = mapPlugin.name
        savedTypeStr = mainMap.activeMapType.name
        // Only map provider OR type can be changed at a time
        providerCombo.enabled = (mainMap.activeMapType.name === savedTypeStr)
        typeCombo.enabled = (pluginName === savedProviderStr)
    }

    ColumnLayout
    {
        id: settingsPopupLayout

        anchors.fill: parent

        // Only for TCP connection
        RowLayout
        {
            id: ipLayout

            Layout.margins: 20

            Label
            {
                id: ipLabel

                font.pointSize: 25
                text: "Enter groundstation IP: "
            }
            TextField
            {
                id: ipTextEdit

                focus: false
                font.pointSize: 25
                inputMethodHints: Qt.ImhFormattedNumbersOnly
                Keys.onReturnPressed: ipTextEdit.focus = false
                Layout.fillWidth: true
                Layout.minimumWidth: ipLabel.width
                placeholderText: "172.20.20.7"
                text: defaultIp

                MouseArea
                {
                    anchors.fill: parent
                    onClicked: parent.focus = true
                }
            }
        }

        // Only for TCP connection
        RowLayout
        {
            id: portLayout

            Layout.margins: 20

            Label
            {
                id: portlabel

                font.pointSize: 25
                text: "Enter port: "
            }
            TextField
            {
                id: portTextEdit

                font.pointSize: 25
                focus: false
                inputMethodHints: Qt.ImhDigitsOnly
                Keys.onReturnPressed: portTextEdit.focus = false
                Layout.fillWidth: true
                placeholderText: "12345" // "55545"
                text: defaultPort

                MouseArea
                {
                    anchors.fill: parent
                    onClicked: parent.focus = true
                }
            }
        }

        RowLayout
        {
            id: mapLayout

            Layout.margins: 20

            Label
            {
                id: providerLabel

                font.pointSize: 25
                text: qsTr("Select map provider: ")
            }

            ComboBox
            {
                id: providerCombo

                font.pointSize: 25
                Layout.fillWidth: true
                Layout.minimumWidth: 600
                Layout.minimumHeight: providerLabel.height * 2
                popup.font.pointSize: 25

                model: mapProviderList
                textRole: "display"

                onCurrentIndexChanged:
                {
                    pluginName = providers.get(currentIndex).name
                    // Only map provider OR type can be changed at a time
                    typeCombo.enabled = (pluginName === savedProviderStr)
                }
            }
        }

        RowLayout
        {
            id: mapTypeLayout

            Layout.margins: 20

            Label
            {
                id: typeLabel

                font.pointSize: 25
                text: qsTr("Select map type: ")
            }

            ComboBox
            {
                id: typeCombo

                font.pointSize: 25
                Layout.fillWidth: true
                Layout.minimumWidth: 600
                Layout.minimumHeight: providerLabel.height * 2
                model: mainMap.supportedMapTypes
                popup.font.pointSize: 25
                textRole: "name"

                onCurrentIndexChanged:
                {
                    providers.setProperty(providerCombo.currentIndex, "currentType", currentIndex)
                    mainMap.activeMapType = mainMap.supportedMapTypes[currentIndex]
                    // Only map provider OR type can be changed at a time
                    providerCombo.enabled = (mainMap.activeMapType.name === savedTypeStr)
                }
            }
        }

        Row
        {
            Layout.alignment: Qt.AlignHCenter
            Layout.margins: 20
            spacing: 40

            Button
            {
                id: settingsConfirmButton

                font.pointSize: 25
                text: qsTr("Confirm all")

                contentItem: Text
                {
                    color: enabledTextColor
                    font.pointSize: 25
                    text: settingsConfirmButton.text
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                onClicked:
                {
                    settingsPopup.close()

                    if(mapLayout.visible)
                    {
                        if(providerCombo.enabled)
                        {
                            // Save current map state
                            var tempBearing = mainMap.bearing
                            var tempZoom = mainMap.zoomLevel
                            var tempCenter = mainMap.center
                            var tempType = providers.get(providerCombo.currentIndex).currentType

                            // Create new map plugin (map is also recreated)
                            pluginComponent = Qt.createComponent("MapPlugin.qml");
                            if (pluginComponent.status === Component.Ready)
                                mapPlugin = pluginComponent.createObject(mainWindow, {"name": pluginName});

                            // Put map back in saved state
                            typeCombo.currentIndex = tempType
                            mainMap.drawRouteOnMap()
                            mainMap.bearing = tempBearing
                            mainMap.zoomLevel = tempZoom
                            mainMap.center = tempCenter
                        }
                    }
                    // Only for TCP connection
                    else if(ipLayout.visible)
                    {
                        stateMachine.setConnectionParameters(ipTextEdit.text, portTextEdit.text)
                    }
                }
            }

            Button
            {
                id: settingsCancelButton

                text: qsTr("CANCEL")
                visible: !(ipLayout.visible && mapLayout.visible)

                contentItem: Text
                {
                    color: settingsCancelButton.enabled ? enabledTextColor : disabledTextColor
                    font.pointSize: 25
                    text: settingsCancelButton.text
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                onClicked:
                {
                    typeCombo.currentIndex = findIndexInList(typeCombo.model, function(item) { return item.name === savedTypeStr })
                    settingsPopup.close()
                }
            }
        }
    }
}
