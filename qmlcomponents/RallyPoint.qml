import QtLocation 5.11
import QtQuick 2.8

/*
 * The rallyPoint MapQuickItem defines the display of rally points on the mainMap.
 */
MapQuickItem
{
    anchorPoint { x: rallyPointIcon.width / 2; y: rallyPointIcon.height / 2 }

    sourceItem: Image
    {
        id: rallyPointIcon

        opacity: 1.0
        source: "../icons/Rallypoint.png"
        height: 60
        width: height
    }
}
