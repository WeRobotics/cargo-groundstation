import QtLocation 5.11
import QtQuick 2.8
import Qt.labs.platform 1.0

/*
 * The mapPlugin Plugin contains the parameters to enable
 * different map providers.
 *
 * The following ones are currently implemented:
 * @provider esri: ArcGIS
 * @provider osm: OpenStreetMap
 * @provider mapbox: Mapbox
 */
Plugin
{
    id: mapPlugin

    readonly property string folderPath: StandardPaths.writableLocation(StandardPaths.GenericDataLocation) + "/Cargo_GS/PyuthanMap"

    // IMPLEMENTED:  "esri" // "osm" // "mapbox"
    // TO IMPLEMENT: "here" // "mapboxgl" // "googlemaps" (Need to download from https://github.com/vladest/googlemaps)
    name: "osm"

    parameters:
    [
//      DO NOT ADD ANY PARAMETER BEFORE esri.token
        PluginParameter {name: "esri.token"; value: "xxx"},
        PluginParameter {name: "mapbox.map_id"; value: "mapbox.streets"},
        PluginParameter {name: "mapbox.access_token"; value: "pk.eyJ1IjoibWFydWxsYXoiLCJhIjoiY2pxbmkzZWk0MTZqNTN4cW43ZDNsYm5sdSJ9.yfWEWQiYbLDqse0bq9OiZg"}//,
//        PluginParameter {name: "osm.mapping.offline.directory"; value: folderPath},
//        PluginParameter {name: "mapboxgl.access_token"; value: "pk.eyJ1IjoibWFydWxsYXoiLCJhIjoiY2pxbmkzZWk0MTZqNTN4cW43ZDNsYm5sdSJ9.yfWEWQiYbLDqse0bq9OiZg"},
//        PluginParameter {name: "mapboxgl.mapping.cache.directory"; value: ""},
//        PluginParameter { name: "here.app_id"; value: "****" },
//        PluginParameter { name: "here.token"; value: "*****" }
    ]

    Component.onCompleted:
    {
        // Get esri token if esri plugin
        // AAA TODO get token for 2 weeks if internet and store on tablet
        if(name == "esri")
        {
            var jsonObjectArray
            var request = new XMLHttpRequest();
            var url = "https://www.arcgis.com/sharing/rest/oauth2/token"
            var parameters = "client_id=watemh18iFo8OPPV&client_secret=29ac1ae0c0f84e21b519f2ac5d7e08cc&grant_type=client_credentials&expiration=300" // expiration in minutes

            request.open("POST", url, false);

            request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            request.setRequestHeader("Content-length", parameters.length);
            request.setRequestHeader("Connection", "close");

            request.onreadystatechange = function()
            {
                if (request.readyState === XMLHttpRequest.DONE )
                {
                    if(request.status === 200)
                        jsonObjectArray = JSON.parse(request.responseText);
                    else
                        stateMachine.messageToDisplay(qsTr("XMLHttpRequest error"), qsTr("Can't get esri token for access to map."), "...")
                }
            };

            request.send(parameters);

            mapPlugin.parameters[0].value = jsonObjectArray.access_token
        }

        // Create map
        mapComponent = Qt.createComponent("MainMap.qml");
        if (mapComponent.status === Component.Ready)
            mainMap = mapComponent.createObject(mainWindow, {"plugin": mapPlugin});
        mainWindow.changeMap.connect(settingsPopup.changeMapSettings)
        mainWindow.changeComm.connect(settingsPopup.changeCommunicationSettings)
        mainWindow.locationAccuracyChanged.connect(mainMap.updateTabletLocationAccuracy)

        mainMap.zoomLevel = 1
    }
}
