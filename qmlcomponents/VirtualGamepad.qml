import QtGraphicalEffects 1.0
import QtQuick 2.8

/*
 * The virtualGamepad Item is a custom virtual gamepad to manually control the drone from the tablet.
 *
 * It is made of two visually separated parts:
 * @part leftItem
 * @part rightItem
 *
 * The left part contains:
 * - three VirtualGamepadButton (arm, disarm, takeoff)
 * - one VirtualJoystick (altitude and yaw)
 *
 * The right part contains:
 * - one VirtualJoystick (pitch and roll)
 */
Item
{
    readonly property int smallMargin: 30
    readonly property int mediumMargin: 50

    height: 500
    width: mainWindow.width - 200
    x: (mainWindow.width - width) / 2
    y: (mainWindow.height - height) / 2

    // Make gamepad frames follow joysticks jumps due to first touch or release
    Connections
    {
        target: rightJoystick
        function onMovedToAlign(x, y) {
            rightSideControls.anchors.horizontalCenterOffset = x
            rightSideControls.anchors.verticalCenterOffset = y
        }
    }

    Connections
    {
        target: leftJoystick
        function onMovedToAlign(x, y) {
            leftSideControls.anchors.horizontalCenterOffset = x
            leftSideControls.anchors.verticalCenterOffset = y
            armButton.anchors.horizontalCenterOffset = x + smallMargin + armButton.width / 2 - leftItem.width / 2
            armButton.anchors.verticalCenterOffset = y + 2 * smallMargin + 3 * armButton.height / 2 - leftItem.height / 2
        }
    }

    // Left part:
    // - large frame (gradient coloured rectangle + shadow on map) for joystick + buttons
    // - joystick (gradient coloured container disc + gradient coloured "thumb" + shadow on container)
    Item
    {
        id: leftItem

        anchors { left: parent.left; verticalCenter: parent.verticalCenter }

        height: parent.height
        width: 500

        // Order of items must be: DropShadow - Rectangle - RadialGradient
        DropShadow
        {
            anchors.fill: leftSideControls
            color: darkBackgroundColor
            radius: 8.0
            samples: 2 * radius + 1
            source: leftSideControls
            transparentBorder: true
            horizontalOffset: source.width / 15
            verticalOffset: source.width / 15
        }

        Rectangle
        {
            id: leftSideControls

            anchors.centerIn: parent
            color: "#cccccc"
            radius: 100
            height: parent.height
            width: 500
        }

        RadialGradient
        {
            anchors.fill: leftSideControls
            source: leftSideControls
            horizontalOffset: - source.width / 15
            verticalOffset: - source.width / 15

            gradient: Gradient
            {
                GradientStop { position: 0.0; color: "#dddddd" }
                GradientStop { position: 0.5; color: "#bbbbbb" }
            }
        }

        VirtualJoystick
        {
            id: leftJoystick

            anchors { horizontalCenterOffset: parent.width / 2 - mediumMargin - width / 2; verticalCenterOffset: parent.height / 2 - mediumMargin - height / 2 }
            baseHOffset: parent.width / 2 - mediumMargin - width / 2
            baseVOffset: parent.height / 2 - mediumMargin - height / 2
            xChar: "R"
            yChar: "Z"
        }
    }

    // Right part:
    // - frame (gradient coloured rectangle + shadow on map) for joystick
    // - joystick (gradient coloured container disc + gradient coloured "thumb" + shadow on container)
    Item
    {
        id: rightItem

        anchors { right: parent.right; bottom: parent.bottom }
        height: 400
        width: 400

        // Order of items must be: DropShadow - Rectangle - RadialGradient
        DropShadow
        {
            anchors.fill: rightSideControls
            color: darkBackgroundColor
            radius: 8.0
            samples: 2 * radius + 1
            source: rightSideControls
            transparentBorder: true
            horizontalOffset: source.width / 15
            verticalOffset: source.width / 15
        }

        Rectangle
        {
            id: rightSideControls

            anchors.centerIn: parent
            color: "#cccccc"
            radius: 100
            height: 400
            width: 400
        }

        RadialGradient
        {
            anchors.fill: rightSideControls
            source: rightSideControls
            horizontalOffset: - source.width / 15
            verticalOffset: - source.width / 15

            gradient: Gradient
            {
                GradientStop { position: 0.0; color: "#dddddd" }
                GradientStop { position: 0.5; color: "#bbbbbb" }
            }
        }

        VirtualJoystick
        {
            id: rightJoystick

            xChar: "Y"
            yChar: "X"
        }
    }

    // Buttons (on left part)
    VirtualGamepadButton
    {
        id: armButton

        anchors { centerIn: leftItem; horizontalCenterOffset: smallMargin + width / 2 - leftItem.width / 2; verticalCenterOffset: 2 * smallMargin + 3 * height / 2 - leftItem.height / 2 }
        buttonChar: "X"
        labelText: qsTr("   ARM   ")
    }

    VirtualGamepadButton
    {
        id: disarmButton

        anchors { top: armButton.bottom; left: armButton.left; topMargin: mediumMargin + smallMargin }
        buttonChar: "Y"
        labelText: qsTr(" DISARM ")
    }

    VirtualGamepadButton
    {
        id: takeoffButton

        anchors { top: armButton.top; left: armButton.right; topMargin: - height - smallMargin; leftMargin: smallMargin }
        buttonChar: "B"
        labelText: qsTr("TAKE OFF")
    }
/*
    VirtualGamepadButton
    {
        id: landButton

        anchors { top: armButton.top; left: takeoffButton.right; topMargin: - height - smallMargin; leftMargin: width + mediumMargin }
        buttonChar: "A"
        labelText: qsTr("LANDING")
    }
*/
}

