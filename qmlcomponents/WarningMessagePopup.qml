import QtQuick 2.8
import QtQuick.Dialogs 1.1

/*
 * The warningMessagePopup Popup contains a priotitx message to diplay.
 *
 * It includes:
 * @item messageTitleText: the general sumary of the weather report.
 * @item messageContentText: the details of the weather report.
 * @item messageBypass: a checkbox to bypass no-flight recommendations due to bad weather.
 * @item messaageConfirmButton: button to continue to the next step (safety checks).
 * @item messageCancelButton: button to cancel and go back to idle state.
 */

Item
{
    property var messageBoxesContainer: []
    property var preventiveChecklistContainer: null
    Component
    {
        id: warningMessagePopup

        MessageDialog
        {
            id: warningMessageBox

            //onYes: {stateMachine.sendCommand(); warningMessageBox.destroy();}
            //onNo: {stateMachine.returnToPreviousState(); warningMessageBox.destroy();}
            //onRejected: {stateMachine.resetState(); warningMessageBox.destroy();} // for StandardButton.Cancel
            // onDiscard: do nothing, MessageDialog closes automatically
            visible: false
        }


    }

    Component
    {
        id: preventiveSafetyWarningPopup

        MessageDialog
        {
            id: preventiveSafetyWarningMessageBox

            onYes: {stateMachine.runPreventiveCheck(true); preventiveSafetyWarningMessageBox.destroy();}
            onAccepted: {stateMachine.runPreventiveCheck(false); preventiveSafetyWarningMessageBox.destroy();}
            onRejected: {stateMachine.resetState(); preventiveSafetyWarningMessageBox.destroy();}

            visible: false
        }
    }

    Connections
    {
        target: stateMachine
        function onWarningMessageToDisplay(message, details) {

            messageBoxesContainer.push(warningMessagePopup.createObject(mainWindow,
                                                          {"title": qsTr("Warning message(s)"),
                                                           "text": message,
                                                           "detailedText": details,
                                                           "standardButtons": StandardButton.Discard}));

            messageBoxesContainer[messageBoxesContainer.length-1].open();
        }

        function onClearDisplayedWarningMessage() {
            while(messageBoxesContainer.length > 0) {
               messageBoxesContainer.pop().destroy();
            }
        }

        function onPreventiveCheckFailed(isInRcMode, distanceToHome) {

            if(preventiveChecklistContainer != null) {
                preventiveChecklistContainer.destroy();
                preventiveChecklistContainer = null;
            }

            var details = qsTr("The application failed to run the checklist. If the mission is sent to the drone the loading will probably fail.") + "\n\r";
            if(distanceToHome > 50.0) {
                details += "\n\r" + qsTr(" - Drone is at %1 meters from home, below 50m required.").arg(distanceToHome);
            }

            if(isInRcMode) {
                details += "\n\r" + qsTr(" - The RC is not in P mode.");
            }

            preventiveChecklistContainer = preventiveSafetyWarningPopup.createObject(mainWindow,
                                                          {"title": qsTr("Preventive mission check"),
                                                           "text": qsTr("Do you want overwite the preventive check ?"),
                                                           "detailedText": details,
                                                           "standardButtons": StandardButton.Yes | StandardButton.Retry | StandardButton.Abort});
            preventiveChecklistContainer.open();
        }

        function onResetAll() {
            while(messageBoxesContainer.length > 0) {
               messageBoxesContainer.pop().destroy();
            }

            if(preventiveChecklistContainer != null) {
                preventiveChecklistContainer.destroy();
                preventiveChecklistContainer = null;
            }
        }
    }
}
