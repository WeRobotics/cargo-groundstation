import QtQuick 2.8
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.4
import QtGraphicalEffects 1.0

/*
 * The allButtons Item contains all the buttons displayed over the map.
 *
 * The available buttons are the following:
 * @button northUpButton: reorients the map with the North up.
 * @button followDroneButton: (un)locks the map center to(from) the drone position.
 * @button changeMapButton: opens a popup to select the map provider and type.
 * @button radioDetectionButton: sends radio detection message.
 * @button changeLanguageCombo: enables language selection.
 * @button changeCommButton: opens a popup to select the tcp connection settings and create the connection.
 * Warning: changeCommButton currently disabled and hidden. See README.md to re-activate functionality.
 * @button confirmButton: confirms route (if idle), starts mission (if ready) or pauses/resumes mission (if running).
 * @button emergencyButton: opens a popup containing buttons for emergency commands.
 * @button manualControlButton: Displays/Hides the virtual gamepad.
 */

Item
{
    id: allButtons

    readonly property int buttonWidth: iconSize * 1.4
    readonly property int emergencyIconSize: 2 * iconSize
    readonly property int minDroneBatteryLevelForTakeOff: 30

    // Uncomment for tcp connection parameters button
//    property string commButtonColor: antennaIsConnected ? "black" : "red"
    property int dronebatterylevel: 100
    property bool tempPauseState: false
    property string savedConfirmButtonText: qsTr("GO")

    anchors { fill: parent; margins: 0 }

    Connections
    {
        target: mainWindow
        function onNoRouteSelected() {
            confirmText.text = qsTr("GO");
        }
        function onRouteSelected(ID, length) {
            confirmText.text = qsTr("GO");
        }
    }

    Connections
    {
        target: stateMachine
        function onSafetyChecksDone()
        {
            if(droneIsConnected) {
                confirmText.text = qsTr("MISSION\nLOADING")
            }
            else {
                confirmText.text = qsTr("CONNECT\nTO DRONE")
            }
        }

        function onDroneBatteryUpdated(level)
        {
            dronebatterylevel = level
            if((level < minDroneBatteryLevelForTakeOff) && (currentGroundstationState == 4)) {
                confirmText.text = qsTr("BATTERY\nLOW")
            }
        }
        function onDroneConnected(isConnected)
        {
            if(isConnected) {
                confirmText.text = savedConfirmButtonText
                if((currentGroundstationState) && (currentGroundstationState < 4)) {
                    confirmText.text = qsTr("MISSION\nLOADING")
                }
            }
            else {
                savedConfirmButtonText = confirmText.text
                if(currentGroundstationState) {
                    confirmText.text = qsTr("CONNECT\nTO DRONE")
                }
            }
        }
        function onDroneStateChanged(newState, newStateStr)
        {
            if(newState == 2) // RALLY_POINT
                confirmText.text = qsTr("RALLY\nPOINT")
            else if(newState == 3) // EMERGENCY_LANDING
                confirmText.text = qsTr("EMERGENCY\nLANDING")
            else if(newState == 4) // PARACHUTE
                confirmText.text = qsTr("PARACHUTE")
            else if(newState == 10) // MANUAL_CONTROL
                confirmText.text = qsTr("MANUAL\nCONTROL")
            else if(newState == 11) // RC_CONTROL
                confirmText.text = qsTr("REMOTE\nCONTROL")
        }

        function onMissionReady() {
            confirmText.text = qsTr("START");
        }

        function onMissionPaused() {
            confirmText.text = qsTr("RESUME")
            tempPauseState = true
        }
        function onMissionRunning() {
            confirmText.text = qsTr("PAUSE")
            tempPauseState = false
        }
        function onResetAll() {
            confirmText.text = qsTr("GO")
        }
    }

    Binding
    {
        target: confirmButton
        property: "enabled"
        value: (currentRouteIndex != 0) && // Mission selected
               ((currentGroundstationState == 0) || // Idle, start of process to launch mission => GO button to execute weather check open first popup
                (((currentGroundstationState == 4) && (droneIsConnected) /*|| (currentDroneState == 7)*/) && (dronebatterylevel >= minDroneBatteryLevelForTakeOff)) || // Ready to start mission => START button
                ((droneIsConnected) && (currentGroundstationState == 5))) // Mission running => PAUSE/RESUME button
    }

    Binding
    {
        target: northUpButton
        property: "rotation"
        value: - mainMap.bearing
    }

    RoundButton
    {
        id: northUpButton

        anchors { top: parent.top; right: parent.right; margins: 10 }
        display: AbstractButton.IconOnly
        flat: true
        font.pointSize: 24
        icon { source: "../icons/North.png"; width: iconSize; height: iconSize } // Icons made by "https://www.freepik.com/"
        text: "N"

        onClicked: mainMap.bearing = 0
    }

    Button
    {
        id: followDroneButton

        anchors { top: northUpButton.bottom; right: parent.right; margins: 20 }
        display: AbstractButton.TextUnderIcon
        icon { source: "../icons/Drone.PNG"; width: iconSize; height: iconSize }
        text: lockOnDrone ? qsTr("UNLOCK\nFROM DRONE"): qsTr("LOCK ON\nDRONE")
        width: buttonWidth

        onClicked:
        {
            lockOnDrone = !lockOnDrone
            if(lockOnDrone)
                mainMap.center = mainMap.droneOnMap.coordinate
        }
    }

    Button
    {
        id: changeMapButton

        anchors { top: followDroneButton.bottom; right: parent.right; margins: 20 }
        display: AbstractButton.TextUnderIcon
        icon { source: "../icons/Map.png"; width: iconSize; height: iconSize } // Icons made by "https://www.flaticon.com/authors/smashicons"
        text: qsTr("CHANGE")
        width: buttonWidth

        onClicked: settingsPopup.changeMapSettings()
    }

    Button
    {
        id: radioDetectionButton

        anchors { top: changeMapButton.bottom; right: parent.right; margins: 20 }
        display: AbstractButton.TextUnderIcon
        icon { source: "../icons/Radio.png"; width: iconSize; height: iconSize }
        text: qsTr("DETECT")
        width: buttonWidth

        onClicked:
        {
            radioStatusReset()
            stateMachine.sendRadioDetectionMessage()
        }
    }

    // Customized ComboBox
    ComboBox
    {
        id: changeLanguageCombo

        anchors { top: radioDetectionButton.bottom; right: parent.right; margins: 20 }
        displayText: currentText
        font.pointSize: 25
        textRole: "name"
        height: mainWindow.height / 12
        width: buttonWidth

        model: ListModel
        {
            id: languageListModel

            ListElement { name: "English"; code: "en"}
            ListElement { name: "Français"; code: "fr"}
            ListElement { name: "Nepali"; code: "np"}
        }

        onEnabledChanged: canvas.requestPaint()

        onCurrentIndexChanged:
        {
            // Translate all texts in selected language
            translator.changeLanguage(languageListModel.get(currentIndex).code);
        }

        delegate: ItemDelegate
        {
            contentItem: Text
            {
                color: changeLanguageCombo.highlightedIndex === index ? darkBackgroundColor : itemOnDarkBackgroundColor;
                font: changeLanguageCombo.font
                fontSizeMode: Text.Fit
                text: name
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            highlighted: changeLanguageCombo.highlightedIndex === index
            width: changeLanguageCombo.width
        }

        indicator: Canvas
        {
            id: canvas

            contextType: "2d"
            x: (changeLanguageCombo.width - width) / 2
            y: changeLanguageCombo.availableHeight - height - 10
            height: changeLanguageCombo.height / 5
            width: 30

            Connections
            {
                target: changeLanguageCombo
                function onPressedChanged() {
                    canvas.requestPaint();
                }
            }

            onPaint:
            {
                context.reset();
                context.moveTo(0, 0);
                context.lineTo(width, 0);
                context.lineTo(width / 2, height);
                context.closePath();
                context.fillStyle = changeLanguageCombo.enabled ? itemOnDarkBackgroundColor : disabledTextColor
                context.fill();
            }
        }

        contentItem: Text
        {
            anchors { fill: parent;margins: 10; bottomMargin: 20 }
            color: changeLanguageCombo.enabled ? itemOnDarkBackgroundColor : disabledTextColor
            font: changeLanguageCombo.font
            fontSizeMode: Text.Fit
            text: changeLanguageCombo.displayText
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        background: Rectangle
        {
            anchors.fill: parent
            border.color: changeLanguageCombo.popup.visible ? itemOnDarkBackgroundColor : darkBackgroundColor;
            border.width: changeLanguageCombo.visualFocus ? 2 : 1
            color: darkBackgroundColor
            radius: 20
        }

        popup: Popup
        {
            implicitHeight: contentItem.implicitHeight
            padding: 1
            width: changeLanguageCombo.width
            y: changeLanguageCombo.height

            contentItem: ListView
            {
                clip: true
                currentIndex: changeLanguageCombo.highlightedIndex
                highlight: Rectangle { color: itemOnDarkBackgroundColor; radius: 20 }
                implicitHeight: contentHeight
                model: changeLanguageCombo.popup.visible ? changeLanguageCombo.delegateModel : null
                ScrollIndicator.vertical: ScrollIndicator { }
            }

            background: Rectangle
            {
                border.color: itemOnDarkBackgroundColor
                color: darkBackgroundColor
                radius: 20
            }
        }
    }

    // Uncomment for tcp connection parameters button + change anchors
//    Button
//    {
//        id: changeCommButton

//        anchors { top: changeMapButton.bottom; right: parent.right; margins: 20 }
//        display: AbstractButton.TextUnderIcon
//        icon { source: "../icons/AntennaConnected.png"; width: iconSize; height: iconSize } // Icons made by "https://www.flaticon.com/authors/google"
//        text: qsTr("CONNECT")

//        contentItem: Column
//        {
//            Image
//            {
//                id: connectIcon

//                source: changeCommButton.icon.source
//                height: iconSize
//                width: iconSize

//                ColorOverlay
//                {
//                    anchors.fill: connectIcon
//                    source: connectIcon
//                    color: commButtonColor
//                }
//            }
//            Text
//            {
//                color: commButtonColor
//                font: changeCommButton.font
//                text: changeCommButton.text
//                horizontalAlignment: Text.AlignHCenter
//                verticalAlignment: Text.AlignBottom
//                width: connectIcon.width
//            }
//        }

//        onClicked: settingsPopup.changeCommunicationSettings()
//    }

    Button
    {
        id: confirmButton

        anchors { bottom: parent.bottom; right: parent.right; margins: 20 }
        enabled: false
        height: emergencyButton.height
        width: emergencyButton.height

        Text
        {
            id: confirmText

            anchors { fill: parent; margins: 20 }
            color: confirmButton.enabled ? enabledTextColor : disabledTextColor
            font { bold: true; pointSize: 100 }
            fontSizeMode: Text.Fit
            height: confirmButton.height
            width: confirmButton.width
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("GO")
        }

        onClicked:
        {
            if(!currentGroundstationState) // IDLE => execute weather check and open first popup
            {
                confirmSignal()
                confirmButton.enabled = false
                stateMachine.confirmRoute(currentRouteId, currentRouteVersion)
            }
            else if(currentGroundstationState == 4) // READY => start mission
            {
                stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_MISSION_START)
            }
            else // MISSION_RUNNING => pause or resume
            {
                // AAA TODO make this depend on currentDroneState
                if(tempPauseState)
                {
                    tempPauseState = !tempPauseState
                    stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_DO_PAUSE_CONTINUE, 1)
                }
                else
                {
                    tempPauseState = !tempPauseState
                    stateMachine.sendCommand(MavCmdEnum.MAV_CMD.MAV_CMD_DO_PAUSE_CONTINUE, 0)
                }
            }
        }
    }

    Button
    {
        id: emergencyButton

        anchors { bottom: parent.bottom; right: confirmButton.left; margins: 20 }
        display: AbstractButton.TextUnderIcon
        icon { source: "../icons/Emergency.png"; color: "#FF5722"; width: iconSize; height: iconSize } // Icons made by "https://www.freepik.com/"
        text: "<font color='#FF5722'>" + qsTr("EMERGENCY") + "</font>"
        width: height

        onClicked: emergencyPopup.open()
    }

    EmergencyPopup
    {
        id: emergencyPopup
    }

    Button
    {
        id: manualControlButton

        anchors { bottom: parent.bottom; right: emergencyButton.left; margins: 20 }

        display: AbstractButton.TextUnderIcon
        icon { source: "../icons/Gamepad.png"; width: iconSize; height: iconSize }
        text: hasManualControl ? qsTr("LOCK") : qsTr("UNLOCK")
        width: buttonWidth

        onClicked: stateMachine.controlStatusChanged(!hasManualControl)
    }

    VirtualGamepad
    {
        id: virtualGamepad

        visible: ((!gamepadIsConnected) && hasManualControl)
    }

    RoundButton
    {
        id: infoButton

        anchors { bottom: parent.bottom; left: parent.left; margins: 0 }
        display: AbstractButton.IconOnly
        flat: true
        font.pointSize: 24
        icon { source: "../icons/Info.png"; width: iconSize / 2 + 10 ; height: width } // Icons made by "https://www.flaticon.com/authors/anas-ramadan"
        text: "I"

        onClicked:
        {
            infoComponent = Qt.createComponent("InfoAndCredits.qml");
            infoAndCredits = infoComponent.createObject(mainWindow);
        }
    }
}
